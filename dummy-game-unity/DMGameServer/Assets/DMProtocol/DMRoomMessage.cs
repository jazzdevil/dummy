﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Dummy.DMProtocol {

    public class ReqJoinRoomMessage : DMBaseMessage {
        public string userId;
        public string roomId;

        public ReqJoinRoomMessage() : base(DMProtocolConstants.kMsgId_ReqJoinRoom) { }

        public override void WriteBytes(BinaryWriter writer) {
            base.WriteBytes(writer);
            writer.Write(this.userId);
            writer.Write(this.roomId);
        }

        public override void ReadBytes(BinaryReader reader) {
            base.ReadBytes(reader);
            this.userId = reader.ReadString();
            this.roomId = reader.ReadString();
        }
    }

    public class RespJoinRoomMessage : DMBaseMessage {
        public bool success;
        public byte playerIndex;
        public string errMsg;

        public RespJoinRoomMessage() : base(DMProtocolConstants.kMsgId_RespJoinRoom) { }

        public override void WriteBytes(BinaryWriter writer) {
            base.WriteBytes(writer);
            writer.Write(this.success);
            writer.Write(this.playerIndex);
            if (!success) {
                writer.Write(this.errMsg);
            }
        }

        public override void ReadBytes(BinaryReader reader) {
            base.ReadBytes(reader);
            this.success = reader.ReadBoolean();
            this.playerIndex = reader.ReadByte();
            if (!success) {
                this.errMsg = reader.ReadString();
            }
        }
    }

}
