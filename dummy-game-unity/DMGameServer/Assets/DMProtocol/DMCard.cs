﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

// Hello
namespace Dummy.DMProtocol
{
    public struct DMCard
    {

        public enum CardKindEnum
        {
            None = 0,
            Clubs = 1,
            Diamonds,
            Hearts,
            Spades,
        }

        public const byte kMinRank = 2;
        public const byte kMaxRank = 14;

        public const byte unPlayerIndex = 100;

        /// <summary>
        /// Clubs, Diamonds, Spades, Hearts
        /// </summary>
        public CardKindEnum kind;

        public bool isHeadCard;

        /// <summary>
        /// 2 - 10, 11 => Jack, 12 => Queen, 13 => King, 14 => Ace
        /// </summary>
        public byte rank;
        public byte playerIndex;

        public override string ToString()
        {
            return CardShortName();
        }

        public bool isSpecialCard()
        {
            return CardShortName() == "qs" || CardShortName() == "2c";
        }

        public UInt16 ToUInt16()
        {
            return isHeadCard ? (UInt16)(((int)kind * 100) + rank + 1000) : (UInt16)(((int)kind * 100) + rank);
        }
        public static DMCard fromUInt16(UInt16 b)
        {
            bool head = false;
            if (b > 1000)
            {
                head = true;
                b -= 1000;
            }
            int k = b / 100;
            int r = b % 100;
            return new DMCard()
            {
                kind = (CardKindEnum)k,
                rank = (byte)r,
                isHeadCard = head

            };
        }

        public UInt16 ToUint16Kind()
        {
            return (UInt16)(((int)kind * 100) + rank);
        }

        public UInt16 ToUint16Rank()
        {

            return (UInt16)(((int)rank * 100) + kind);
        }

        public int CardScore()
        {
            int score = 0;
            if (rank >= 2 && rank <= 9)
            {
                score = 5;
            }
            else if (rank >= 10 && rank <= 13)
            {
                score = 10;
            }
            else if (rank == 14)
            {
                score = 15;
            }

            if (isHeadCard)
            {
                score = 50;
            }

            if ((rank == 2 && kind == CardKindEnum.Clubs) || (rank == 12 && kind == CardKindEnum.Spades))
            {
                score = 50;
                if (isHeadCard)
                {
                    score = 100;
                }
            }


            return score;
        }

        public static DMCard changePlayerIndex(DMCard c, byte playerIndex)
        {
            DMCard card = new DMCard();
            card.rank = c.rank;
            card.kind = (DMCard.CardKindEnum)c.kind;
            card.isHeadCard = c.isHeadCard;
            card.playerIndex = playerIndex;
            return card;
        }

        public static DMCard newCard(byte playerIndex, byte rank, byte kind)
        {
            DMCard card = new DMCard();
            card.rank = rank;
            card.kind = (DMCard.CardKindEnum)kind;
            card.isHeadCard = false;
            card.playerIndex = playerIndex;
            return card;
        }

        public string CardName()
        {
            string rankAsString = "";
            if (rank >= 2 && rank <= 10)
            {
                rankAsString = rank.ToString();
            }
            else if (rank == 11)
            {
                rankAsString = "Jack";
            }
            else if (rank == 12)
            {
                rankAsString = "Queen";
            }
            else if (rank == 13)
            {
                rankAsString = "King";
            }
            else if (rank == 14)
            {
                rankAsString = "Ace";
            }
            return rankAsString + " of " + kind.ToString();
        }

        public string CardShortName()
        {
            string rankAsString = "";
            if (rank >= 2 && rank <= 10)
            {
                rankAsString = rank.ToString();
            }
            else if (rank == 11)
            {
                rankAsString = "j";
            }
            else if (rank == 12)
            {
                rankAsString = "q";
            }
            else if (rank == 13)
            {
                rankAsString = "k";
            }
            else if (rank == 14)
            {
                rankAsString = "a";
            }

            string kindAsString = "";
            if (kind == CardKindEnum.Clubs)
            {
                kindAsString = "c";
            }
            else if (kind == CardKindEnum.Diamonds)
            {
                kindAsString = "d";
            }
            else if (kind == CardKindEnum.Hearts)
            {
                kindAsString = "h";
            }
            else if (kind == CardKindEnum.Spades)
            {
                kindAsString = "s";
            }

            return rankAsString + kindAsString;
        }

        static DMCard s_EmptyCard = new DMCard() { rank = 0, kind = CardKindEnum.None };
        public static DMCard EmptyCard()
        {
            return s_EmptyCard;
        }

        public bool IsIdentical(DMCard otherCard)
        {
            return (this.rank == otherCard.rank && this.kind == otherCard.kind);
        }

        //Pooh => ตอง
        public static bool isSameRank(DMCard[] cards)
        {
            byte r = cards[0].rank;
            for (int i = 1; i < cards.Length; i++)
            {
                if (cards[i].rank != r)
                {
                    return false;
                }
            }
            return true;
        }
        public static bool isSameRank(List<DMCard> cards)
        {
            byte r = cards[0].rank;
            for (int i = 1; i < cards.Count; i++)
            {
                if (cards[i].rank != r)
                {
                    return false;
                }
            }
            return true;
        }

        public static bool isRunOfSameRank(DMCard[] cards)
        {
            if (!isSameRank(cards))
            {
                return false;
            }
            SortByRankAscending(cards);

            for (int i = 0; i < cards.Length - 1; i++)
            {
                if (cards[i + 1].rank - cards[i].rank != 0)
                {
                    return false;
                }
            }

            return true;
        }
        public static bool isRunOfSameRank(List<DMCard> cards)
        {
            if (!isSameRank(cards))
            {
                return false;
            }
            SortByRankAscending(cards);

            for (int i = 0; i < cards.Count - 1; i++)
            {
                if (cards[i + 1].rank - cards[i].rank != 0)
                {
                    return false;
                }
            }

            return true;
        }

        //Pooh => เรียง
        public static bool isSameKind(DMCard[] cards)
        {
            CardKindEnum k = cards[0].kind;
            for (int i = 1; i < cards.Length; i++)
            {
                if (cards[i].kind != k)
                {
                    return false;
                }
            }
            return true;
        }
        public static bool isSameKind(List<DMCard> cards)
        {
            CardKindEnum k = cards[0].kind;
            for (int i = 1; i < cards.Count; i++)
            {
                if (cards[i].kind != k)
                {
                    return false;
                }
            }
            return true;
        }

        public static bool isRunOfSameKind(DMCard[] cards)
        {
            if (!isSameKind(cards))
            {
                return false;
            }

            SortByKindAscending(cards);

            for (int i = 0; i < cards.Length - 1; i++)
            {
                if (cards[i + 1].rank - cards[i].rank != 1)
                {
                    return false;
                }
            }

            return true;
        }

        public static bool isRunOfSameKind(List<DMCard> cards)
        {
            if (!isSameKind(cards))
            {
                return false;
            }

            SortByKindAscending(cards);
            for (int i = 0; i < cards.Count - 1; i++)
            {
                if (cards[i + 1].rank - cards[i].rank != 1)
                {
                    return false;
                }
            }

            return true;
        }

        public static void SortByRankAscending(DMCard[] cards)
        {
            for (int i = 0; i < cards.Length; i++)
            {
                for (int j = i + 1; j < cards.Length; j++)
                {
                    if (cards[j].ToUint16Rank() < cards[i].ToUint16Rank())
                    {
                        DMCard temp = cards[i];
                        cards[i] = cards[j];
                        cards[j] = temp;
                    }
                }
            }
        }
        public static void SortByRankAscending(List<DMCard> cards)
        {
            for (int i = 0; i < cards.Count; i++)
            {
                for (int j = i + 1; j < cards.Count; j++)
                {
                    if (cards[j].ToUint16Rank() < cards[i].ToUint16Rank())
                    {
                        DMCard temp = cards[i];
                        cards[i] = cards[j];
                        cards[j] = temp;
                    }
                }
            }
        }

        public static void SortByKindAscending(DMCard[] cards)
        {
            for (int i = 0; i < cards.Length; i++)
            {
                for (int j = i + 1; j < cards.Length; j++)
                {
                    if (cards[j].ToUint16Kind() < cards[i].ToUint16Kind())
                    {
                        DMCard temp = cards[i];
                        cards[i] = cards[j];
                        cards[j] = temp;
                    }
                }
            }
        }
        public static void SortByKindAscending(List<DMCard> cards)
        {
            for (int i = 0; i < cards.Count; i++)
            {
                for (int j = i + 1; j < cards.Count; j++)
                {
                    if (cards[j].ToUint16Kind() < cards[i].ToUint16Kind())
                    {
                        DMCard temp = cards[i];
                        cards[i] = cards[j];
                        cards[j] = temp;
                    }
                }
            }
        }

        public static void Shuffle(DMCard[] cards)
        {
            System.Random rand = new System.Random();
            for (int i = 0; i < cards.Length; i++)
            {
                int shuffleIndex = rand.Next(0, cards.Length);
                DMCard temp = cards[i];
                cards[i] = cards[shuffleIndex];
                cards[shuffleIndex] = temp;
            }
        }
        public static void Shuffle(List<DMCard> cards)
        {
            for (int i = 0; i < cards.Count; i++)
            {
                int shuffleIndex = UnityEngine.Random.Range(0, cards.Count);
                DMCard temp = cards[i];
                cards[i] = cards[shuffleIndex];
                cards[shuffleIndex] = temp;
            }
        }


        public enum callCardType
        {
            byRank = 0,
            byKind = 1,
        }

        public static void callCard(List<DMCard> cards, UInt16 type)
        {

            if (type == (UInt16)callCardType.byRank)
            {
                DMCard.SortByRankAscending(cards);
            }
            else if (type == (UInt16)callCardType.byKind)
            {
                DMCard.SortByKindAscending(cards);
            }
        }

        public static List<DMCard> GenerateFullDeckOfCard(int removeCards = 0)
        {
            List<DMCard> cards = new List<DMCard>();
            for (byte rank = 2; rank <= 14; rank++)
            {
                for (int kind = 1; kind <= 4; kind++)
                {
                    DMCard card = new DMCard();
                    card.rank = rank;
                    card.kind = (DMCard.CardKindEnum)kind;
                    card.isHeadCard = false;
                    card.playerIndex = DMCard.unPlayerIndex;
                    cards.Add(card);
                }
            }

            for (int i = 0; i < removeCards; i++)
            {
                cards.RemoveRange(0, 1);
            }
            return cards;
        }

        public static void debugFullDeckCard(List<DMCard> cards)
        {
            Debug.LogWarningFormat("[DMCard Debug] : begin debugFullDeckCard");
            foreach (var item in cards)
            {
                Debug.LogFormat("[DMCard Debug] : {0}", item.CardName());
            }
            Debug.LogWarningFormat("[DMCard Debug] : end debugFullDeckCard");
        }
    }

    public class DMMeldingCard
    {
        public enum MeldingTypeEnum
        {
            RunOfSameKind = 1,
            SameRank
        }

        public UInt16 id;
        public MeldingTypeEnum meldingType;
        public List<DMCard> cards;

        public DMMeldingCard()
        {
            id = 0;
            cards = new List<DMCard>();
        }

        public DMMeldingCard(DMCard[] cardsToBeMeld) : this()
        {
            if (cardsToBeMeld.Length < 3)
            {
                throw new InvalidOperationException("[DMMeldingCard] you con only create a melding card with atleast 3 cards");
            }

            // Identify melding type
            if (DMCard.isRunOfSameRank(cardsToBeMeld))
            {
                meldingType = MeldingTypeEnum.SameRank;
            }
            else if (DMCard.isRunOfSameKind(cardsToBeMeld))
            {
                meldingType = MeldingTypeEnum.RunOfSameKind;
            }
            else
            {
                string err = cardsToBeMeld[0].CardName();
                for (int i = cardsToBeMeld.Length - 1; i >= 0; i--)
                {
                    err = cardsToBeMeld[i].CardName() + ", " + err;
                }
                throw new InvalidOperationException(string.Format("[DMMeldingCard] cannot melding these cards: {0}", err));
            }

            this.cards.AddRange(cardsToBeMeld);
        }

        public void WriteBytes(BinaryWriter writer)
        {
            writer.Write((UInt16)id);
            writer.Write((byte)meldingType);
            writer.Write((byte)cards.Count);
            for (int i = 0; i < cards.Count; i++)
            {
                writer.Write((UInt16)cards[i].ToUInt16());
            }
        }

        public void ReadBytes(BinaryReader reader)
        {
            id = (UInt16)reader.ReadUInt16();
            meldingType = (MeldingTypeEnum)reader.ReadByte();
            int numberOfCards = reader.ReadByte();
            if (cards == null) cards = new List<DMCard>();
            for (int i = 0; i < numberOfCards; i++)
            {
                UInt16 cardAsUInt = reader.ReadUInt16();
                cards.Add(DMCard.fromUInt16(cardAsUInt));
            }
        }

        public DMMeldingCard Clone()
        {
            DMMeldingCard cloneMeldingCards = new DMMeldingCard();
            cloneMeldingCards.id = this.id;
            cloneMeldingCards.meldingType = this.meldingType;
            cloneMeldingCards.cards = new List<DMCard>();
            for (int i = 0; i < this.cards.Count; i++)
            {
                cloneMeldingCards.cards.Add(this.cards[i]);
            }
            return cloneMeldingCards;
        }

        public bool Validate()
        {
            if (meldingType == MeldingTypeEnum.SameRank)
            {
                return DMCard.isSameRank(cards);
            }
            else if (meldingType == MeldingTypeEnum.RunOfSameKind)
            {
                return DMCard.isRunOfSameKind(cards);
            }
            throw new System.InvalidOperationException("Invalid MeldingType: " + meldingType);
        }

        public bool CanAcceptLayoffCard(DMCard card)
        {
            List<DMCard> testCards = new List<DMCard>(cards);
            bool canAccept = false;
            if (meldingType == MeldingTypeEnum.RunOfSameKind)
            {
                testCards.Add(card);
                canAccept = DMCard.isRunOfSameKind(testCards);
            }
            else if (meldingType == MeldingTypeEnum.SameRank)
            {
                testCards.Add(card);
                canAccept = DMCard.isSameRank(testCards);
            }
            testCards = null;
            return canAccept;
        }

        public bool sortMeldingCard()
        {
            Debug.LogWarningFormat("[card] meld type {0}", meldingType);
            for (int i = 0; i < cards.Count; i++)
            {
                Debug.LogWarningFormat("[Card] : {0}", cards[i]);
            }

            if (DMCard.isSameRank(cards))
            {
                cards = cards.OrderBy(w => (int)(w.rank)).ToList();
                Debug.LogWarningFormat("[Card] : Work Rank");
            }
            else if (DMCard.isRunOfSameKind(cards))
            {
                cards = cards.OrderBy(w => (int)(w.kind)).ToList();
                Debug.LogWarningFormat("[Card] : Work Kind");
            }
            else
            {
                return false;
            }

            for (int i = 0; i < cards.Count; i++)
            {
                Debug.LogWarningFormat("[Card] : {0}", cards[i]);
            }
            return true;
        }

        public bool InsertLayoffCard(DMCard card, out int insertAtIndex)
        {
            if (!CanAcceptLayoffCard(card))
            {
                insertAtIndex = -1;
                return false;
            }

            if (meldingType == MeldingTypeEnum.SameRank)
            {
                int idx = 0;
                for (idx = 0; idx < cards.Count; idx++)
                {
                    if (cards[idx].kind > card.kind)
                    {
                        break;
                    }
                }
                cards.Insert(idx, card);
                insertAtIndex = idx;
                return true;
            }
            else if (meldingType == MeldingTypeEnum.RunOfSameKind)
            {
                int idx = 0;
                for (idx = 0; idx < cards.Count; idx++)
                {
                    if (cards[idx].rank > card.rank)
                    {
                        break;
                    }
                }
                cards.Insert(idx, card);
                insertAtIndex = idx;
                return true;
            }
            else
            {
                insertAtIndex = -1;
                return false;
            }
        }

        public static List<DMMeldingCard> FindAllPossibleMeldingCard(DMCard[] cards, int findMaxCard = 0)
        {
            cards = (DMCard[])cards.Clone();
            List<DMMeldingCard> possibleMeldingCards = new List<DMMeldingCard>();
            List<DMCard> temp = new List<DMCard>();

            // Find run od same kind melding cards
            DMCard.SortByKindAscending(cards);
            temp.Clear();
            for (int i = 0; i <= cards.Length - 2; i++)
            {
                temp.Clear();
                temp.Add(cards[i]);
                DMCard leftCard = cards[i];
                int j = i + 1;
                for (; j < cards.Length; j++)
                {
                    DMCard rightCard = cards[j];
                    if (rightCard.ToUint16Kind() - leftCard.ToUint16Kind() > 1) break;
                    temp.Add(cards[j]);
                    leftCard = rightCard;

                    if (findMaxCard != 0 && temp.Count >= findMaxCard)
                    {
                        break;
                    }
                }

                if (temp.Count >= 3)
                {
                    possibleMeldingCards.Add(new DMMeldingCard(temp.ToArray()));
                }
            }

            // Find same ranks melding cards
            DMCard.SortByRankAscending(cards);
            for (int i = 0; i < cards.Length - 2; i++)
            {
                temp.Clear();
                temp.Add(cards[i]);
                byte rank = cards[i].rank;
                int j = i + 1;
                for (; j < cards.Length; j++)
                {
                    if (cards[j].rank != rank) break;
                    temp.Add(cards[j]);
                    if (findMaxCard != 0 && temp.Count >= findMaxCard)
                    {
                        break;
                    }
                }

                if (temp.Count >= 3)
                {
                    possibleMeldingCards.Add(new DMMeldingCard(temp.ToArray()));
                }
            }

            return possibleMeldingCards;
        }

        /// <summary>
        /// Encode melding id from player index, and index of melding card
        /// </summary>
        /// <param name="playerIndex">Player owner's index</param>
        /// <param name="meldingIndex">Index of melding card in melding card set</param>
        /// <returns></returns>
        public static UInt16 EncodeMeldingCardIdBy(byte playerIndex, byte meldingIndex)
        {
            UInt16 meldingId = (UInt16)((playerIndex * 100) + meldingIndex);
            return meldingId;
        }

        /// <summary>
        /// Decode player index, and melding index from melding id
        /// </summary>
        /// <param name="meldingCardId">Melding card's id</param>
        /// <param name="playerIndex">Output Player index</param>
        /// <param name="meldingIndex">Output index of melding card</param>
        public static void DecodeMeldingCardId(UInt16 meldingCardId, out byte playerIndex, out byte meldingIndex)
        {
            playerIndex = (byte)(meldingCardId / 100);
            meldingIndex = (byte)(meldingCardId % 100);
        }
    }
}