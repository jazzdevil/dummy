﻿using System;


namespace Dummy.DMProtocol
{
    public class DMProtocolConstants
    {

        public const int kProtocolBufferSize = 1024;

        // Message ID
        public const ushort kMsgId_Unknown = 0;

        #region Global Messages
        // All Global message Id should be in format 1XXX (which is masked by 1)

        public const ushort kMsgIdMask_Global = 1;
        public const ushort kMsgId_ReqWhoAreYou = 1011;
        public const ushort kMsgId_RespWhoAreYou = 1012;

        public const ushort kMsgId_ReqJoinRoom = 1021;
        public const ushort kMsgId_RespJoinRoom = 1022;

        public const ushort kMsgId_ReqSendRoomChatMessage = 1031;
        public const ushort kMsgId_RespSendRoomChatMessage = 1032;
        public const ushort kMsgId_BroadcastRoomChatMessage = 1033;

        public const ushort kMsgId_ReqClientHeartbeat = 1041;
        public const ushort kMsgId_RespClientHeartbeat = 1042;

        #endregion

        #region Game Room Messages
        // All Game room Message Id should be in format 2XXX (which is masked by 2)

        public const ushort kMsgIdMask_GameRoom = 2;


        public const ushort kMsgId_UpdateRoomStatus = 2997;
        public const ushort kMsgId_ReqUpdateRoomStatus = 2998;
        public const ushort kMsgId_InitGameRoom = 2999;

        public const ushort kMsgId_ReqReadyToPlay = 2001;
        public const ushort kMsgId_RespReadyToPlay = 2002;
        public const ushort kMsgId_PlayerReadyToPlay = 2003;


        public const ushort kMsgId_StartingCardsDeal = 2005;
        public const ushort kMsgId_StartingCardsDealReady = 2006;

        public const ushort kMsgId_ReqQueryTurnState = 2007;
        public const ushort kMsgId_RespQueryTurnState = 2008;

        public const ushort kMsgId_TurnChange = 2009;

        public const ushort kMsgId_ReqDrawStockCard = 2011;
        public const ushort kMsgId_RespDrawStockCard = 2012;
        public const ushort kMsgId_PlayerDrawStockCard = 2013;

        public const ushort kMsgId_ReqDiscardCard = 2015;
        public const ushort kMsgId_RespDiscardCard = 2016;
        public const ushort kMsgId_PlayerDiscardCard = 2017;

        public const ushort kMsgId_ReqMeldingCard = 2021;
        public const ushort kMsgId_RespMeldingCard = 2022;
        public const ushort kMsgId_PlayerMeldingCard = 2023;

        public const ushort kMsgId_ReqPickupAndMeldingCard = 2025;
        public const ushort kMsgId_RespPickupAndMeldingCard = 2026;
        public const ushort kMsgId_PlayerPickupAndMeldingCard = 2027;

        public const ushort kMsgId_ReqLayoffCard = 2031;
        public const ushort kMsgId_RespLayoffCard = 2032;
        public const ushort kMsgId_PlayerLayoffCard = 2033;

        public const ushort kMsgId_PlayerScore = 2041;
        public const ushort kMsgId_otherPlayerScore = 2042;
        public const ushort kMsgId_PlayerCountTypeScore = 2043;


        public const ushort kMsgId_ReqKnockCard = 2051;
        public const ushort kMsgId_RespKnockCard = 2052;
        public const ushort kMsgId_PlayerKnockCard = 2053;


        public const ushort kMsgId_ReqPassLastTurn = 2061;
        public const ushort kMsgId_RespPassLastTurn = 2062;
        public const ushort kMsgId_PlayerPassLastTurn = 2063;

        public const ushort kMsgId_ReqEndCountdown = 2064;
        public const ushort kMsgId_RespEndCountdown = 2065;

        public const ushort kMsgId_leaveAndRemoveRoom = 3001;

        #endregion

        public static ushort GetMessageIdMask(ushort msgId)
        {
            return (ushort)(msgId / 1000);
        }

        public static string MsgIdAsString(ushort msgId)
        {
            switch (msgId)
            {
                case kMsgId_InitGameRoom: return "InitGameRoom";

                case kMsgId_ReqWhoAreYou: return "ReqWhoAreYou";
                case kMsgId_RespWhoAreYou: return "RespWhoAreYou";

                case kMsgId_ReqJoinRoom: return "ReqJoinRoom";
                case kMsgId_RespJoinRoom: return "RespJoinRoom";

                case kMsgId_ReqSendRoomChatMessage: return "ReqSendRoomChatMessage";
                case kMsgId_RespSendRoomChatMessage: return "RespSendRoomChatMessage";
                case kMsgId_BroadcastRoomChatMessage: return "BroadcastRoomChatMessage";

                case kMsgId_ReqClientHeartbeat: return "ReqClientHeartbeat";
                case kMsgId_RespClientHeartbeat: return "RespClientHeartbeat";

                case kMsgId_ReqReadyToPlay: return "ReqReadyToPlay";
                case kMsgId_RespReadyToPlay: return "RespReadyToPlay";
                case kMsgId_PlayerReadyToPlay: return "PlayerReadyToPlay";
                case kMsgId_UpdateRoomStatus: return "UpdateRoomStatus";
                case kMsgId_ReqUpdateRoomStatus: return "ReqUpdateRoomStatus";

                case kMsgId_StartingCardsDeal: return "StartingCardsDeal";
                case kMsgId_StartingCardsDealReady: return "StartingCardDealReady";

                case kMsgId_ReqQueryTurnState: return "ReqQueryTurnState";
                case kMsgId_RespQueryTurnState: return "RespQueryTurnState";
                case kMsgId_TurnChange: return "TurnChangeMessage";

                case kMsgId_ReqDrawStockCard: return "ReqDrawStockCard";
                case kMsgId_RespDrawStockCard: return "RespDrawStockCard";
                case kMsgId_PlayerDrawStockCard: return "PlayerDrawStockCard";

                case kMsgId_ReqDiscardCard: return "ReqDiscardCard";
                case kMsgId_RespDiscardCard: return "RespDiscardCard";
                case kMsgId_PlayerDiscardCard: return "PlayerDiscardCard";

                case kMsgId_ReqMeldingCard: return "ReqMeldingCard";
                case kMsgId_RespMeldingCard: return "RespMeldingCard";
                case kMsgId_PlayerMeldingCard: return "PlayerMeldingCard";

                case kMsgId_ReqPickupAndMeldingCard: return "ReqPickupAndMeldingCard";
                case kMsgId_RespPickupAndMeldingCard: return "RespPickupAndMeldingCard";
                case kMsgId_PlayerPickupAndMeldingCard: return "PlayerPickupAndMeldingCard";

                case kMsgId_ReqLayoffCard: return "ReqLayoffCard";
                case kMsgId_RespLayoffCard: return "RespLayoffCard";
                case kMsgId_PlayerLayoffCard: return "PlayerLayoffCard";

                case kMsgId_PlayerScore: return "RespPlayerScore";
                case kMsgId_otherPlayerScore: return "otherPlayerScore";
                case kMsgId_PlayerCountTypeScore: return "PlayCountTpyeScore";


                case kMsgId_ReqKnockCard: return "ReqKnockCard";
                case kMsgId_RespKnockCard: return "RespKnockCard";
                case kMsgId_PlayerKnockCard: return "KnockCard";

                case kMsgId_ReqPassLastTurn: return "ReqPassLastTurn";
                case kMsgId_RespPassLastTurn: return "RespPassLastTurn";
                case kMsgId_PlayerPassLastTurn: return "PlayerPassLastTurn";

                case kMsgId_ReqEndCountdown: return "ReqEndCountdown";
                case kMsgId_RespEndCountdown: return "RespEndCountdown";

                default: return "Unknow";

            }
        }
    }
}
