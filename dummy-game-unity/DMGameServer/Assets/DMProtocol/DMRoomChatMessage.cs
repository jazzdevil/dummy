﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Dummy.DMProtocol {
    public class ReqSendRoomChatMessage : DMBaseMessage {
        public string chatMessage;

        public ReqSendRoomChatMessage() : base(DMProtocolConstants.kMsgId_RespSendRoomChatMessage) { }

        public override void WriteBytes(BinaryWriter writer) {
            base.WriteBytes(writer);
            writer.Write(chatMessage);
        }

        public override void ReadBytes(BinaryReader reader) {
            base.ReadBytes(reader);
            chatMessage = reader.ReadString();
        }
    }

    public class RespSendRoomChatMessage : DMBaseMessage {
        public bool success;

        public RespSendRoomChatMessage() : base(DMProtocolConstants.kMsgId_RespSendRoomChatMessage) { }

        public override void WriteBytes(BinaryWriter writer) {
            base.WriteBytes(writer);
            writer.Write(success);
        }

        public override void ReadBytes(BinaryReader reader) {
            base.ReadBytes(reader);
            success = reader.ReadBoolean();
        }
    }

    public class BroadcastRoomChatMessage : DMBaseMessage {
        public int senderId;
        public string message;

        public BroadcastRoomChatMessage() : base(DMProtocolConstants.kMsgId_BroadcastRoomChatMessage) { }

        public override void WriteBytes(BinaryWriter writer) {
            base.WriteBytes(writer);
            writer.Write(senderId);
            writer.Write(message);
        }

        public override void ReadBytes(BinaryReader reader) {
            base.ReadBytes(reader);
            senderId = reader.ReadInt32();
            message = reader.ReadString();
        }
    }
}
