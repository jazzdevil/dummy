﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Dummy.DMProtocol;
using UnityEngine;

namespace Dummy.Server
{

    public class DMGameRoom : IMessageReceivable
    {
        public const byte kMaxPlayer = 4;
        public const byte kDefaultActionLimitTime = 10; // turn timer
        public const byte kDefaultServerActionLimitTimeShift = 0;
        public const byte kBotMaximumActionDelayTime = 1;
        public float countDownRemoveRoom = 60;

        public enum GameStateEnum
        {
            WaitForAllPlayerReady = 0,
            WaitForAllPlayerEndCountdown,
            DrawCardAtStart,
            WaitForClientResponseToDrawCardAsStart,
            StartGame,
            BeginTurn,
            WaitForPlayerAction,
            EndTurn,
            EndGame
        }

        public bool trunFollowClock;

        public string roomId;

        CasinoServer mServer;
        Queue<GameRoomMessageQueueEntry> mMessagesQueue;

        List<bool> mReadyToPlay;
        Dictionary<int, bool> mEndCountdown;

        Dictionary<int, bool> mStartingCardDealReady;
        List<DMPlayer> mPlayers;
        public Dictionary<DMPlayer, DMConnection> mPlayerToConnectionMapping;

        List<DMCard> mStockCards; // Cards left in stock cards
        List<DMCard> mDiscardCards; // Card has been discarded from player
        Dictionary<DMPlayer, List<DMCard>> mPlayerOnHandCards; // Player on hand Cards
        Dictionary<DMPlayer, List<DMMeldingCard>> mPlayerMeldingCardSet; // Player melding cards
        //Dictionary<DMPlayer, int> mPlayerScore; // Player score

        public bool currentTurnFlag_DrawStockCard = false; // In current turn, player has already picked stock card (จั่ว)
        public bool currentTurnFlag_DiscardCard = false; // In current turn, player has already discarded a card (ทิ้ง)
        public bool currentTurnFlag_MeldingCard = false; // In current turn, player has already meld card (เกิด)
        public bool currentTurnFlag_PutDummyCard = false; // In current turn, player has already put down the dummy card (ฝาก)
        bool currentTurnFlag_outOfCard = false;

        GameStateEnum mGameState;
        int mGameSubState;
        int mCurrentTurn; // N-th turn (could be 1, 2, 3, ... )
        int mCurrentPlayerTurn; // The current turn belong to which player (could be 0, 1, 2, or 3 since we have only 4 maximum players)
        float mActionTimer;

        private DMBotController mBotController;
        public Dictionary<DMPlayer, DMScoreController> mScoreControllers;

        [Obsolete]
        public DMGameRoom(CasinoServer server, string roomId)
        {
            this.roomId = roomId;
            mServer = server;
            mMessagesQueue = new Queue<GameRoomMessageQueueEntry>();
            mReadyToPlay = new List<bool>();
            mEndCountdown = new Dictionary<int, bool>();
            mStartingCardDealReady = new Dictionary<int, bool>();
            mPlayers = new List<DMPlayer>();
            mPlayerToConnectionMapping = new Dictionary<DMPlayer, DMConnection>();
            mBotController = new DMBotController(this);

            mGameSubState = 0;
            mActionTimer = 0f;
            mCurrentTurn = 1;
            mCurrentPlayerTurn = 0;
            trunFollowClock = true;
        }

        public DMPlayer PlayerAtIndex(int index)
        {
            return mPlayers[index];
        }

        public int GetNumberOfPlayer()
        {
            return mPlayers.Count;
        }

        public bool IsStartGame()
        {
            return mGameState == GameStateEnum.WaitForAllPlayerReady;
        }

        public void addTestLayoffCard(DMPlayer player, byte[] ranks, DMCard.CardKindEnum[] kinds)
        {
            DMCard[] cl = new DMCard[ranks.Length];
            int index = 0;
            for (int i = 0; i < ranks.Length; i++)
            {
                DMCard c = new DMCard();
                c.rank = ranks[i];
                c.playerIndex = player.playerIndex;
                if (kinds.Length == 1)
                {
                    c.kind = kinds[0];
                }
                else
                {
                    c.kind = kinds[i];
                }

                cl[index] = c;
                index++;
            }


            DMMeldingCard m = new DMMeldingCard(cl);
            m.id = EncodeMeldingCardIdBy(player.playerIndex, (byte)mPlayerMeldingCardSet[player].Count);
            mPlayerMeldingCardSet[player].Add(m);
            if (!player.isBot)
            {
                RespMeldingCardMessage respMeldingCardMessage = new RespMeldingCardMessage();
                respMeldingCardMessage.allow = true;
                respMeldingCardMessage.meldingCard = m;
                respMeldingCardMessage.nextActionLimitTime = kDefaultActionLimitTime;
                respMeldingCardMessage.playerIndexGotCondition = "";
                respMeldingCardMessage.condition = "";
                SetRoomIdBeforeSend(mPlayerToConnectionMapping[player], respMeldingCardMessage);
            }

            PlayerMeldingCardMessage playerMeldingCardMessage = new PlayerMeldingCardMessage();
            playerMeldingCardMessage.playerIndex = player.playerIndex;
            playerMeldingCardMessage.meldingCard = m;
            playerMeldingCardMessage.condition = "";
            playerMeldingCardMessage.playerIndexGotCondition = "";
            SendMappingMessage(player, mPlayers, mPlayerToConnectionMapping, playerMeldingCardMessage);

        }

        public bool Join(DMPlayer player)
        {
            if (mPlayers.Contains(player))
            {
                Debug.Log("Player has already joined room");
                return false;
            }
            if (mPlayers.Count == kMaxPlayer)
            {
                Debug.Log("Room is full");
                return false;
            }

            mPlayers.Add(player);
            player.playerIndex = (byte)mPlayers.IndexOf(player);
            if (player.isBot)
            {
                // Bot is just ready to play
                mReadyToPlay.Add(true);
                mEndCountdown.Add(player.playerIndex, true);
            }
            else
            {
                // Normal player has to send ReqReadyToPlayMessage to confirm it is ready
                mReadyToPlay.Add(false);
                mEndCountdown.Add(player.playerIndex, false);
            }
            if (!player.isBot)
            {
                mPlayerToConnectionMapping[player] = mServer.FindConnectionByDMPlayer(player);
            }

            Debug.LogFormat("[DMGameRoom] {0} join", player.userId);
            return true;
        }

        public void AddBots(int number = 0)
        {
            //Pooh
            if (number == 0)
            {
                number = kMaxPlayer;
            }
            //
            int idx = 0;
            while (mPlayers.Count < number)
            {
                DMPlayer bot = new DMPlayer(isBot: true);
                bot.userId = "bot-" + idx.ToString();
                Join(bot);
                idx++;
            }
        }

        public int GetNumberOfQueuedMessage()
        {
            return mMessagesQueue.Count;
        }

        public string Tag()
        {
            return "DMRoom[" + roomId + "]";
        }

        public void Receive(DMConnection conn, ushort messageId, byte[] rawMessage)
        {


            GameRoomMessageQueueEntry e = CreateMessageQueueEntry(conn, messageId, rawMessage);
            if (e == null)
            {
                Debug.LogWarningFormat("[DMRoom] decline message: {0}", DMProtocolConstants.MsgIdAsString(messageId));
                return;
            }

            mMessagesQueue.Enqueue(e);
        }


        //Receive Message from Client
        GameRoomMessageQueueEntry CreateMessageQueueEntry(DMConnection conn, ushort messageId, byte[] rawMessage)
        {
            DMBaseMessage message = null;
            switch (messageId)
            {
                case DMProtocolConstants.kMsgId_InitGameRoom:
                    message = DMProtocolUtility.CreateDMMessageFromBuffer<InitGameRoomMessage>(rawMessage);
                    break;
                case DMProtocolConstants.kMsgId_ReqUpdateRoomStatus:
                    message = DMProtocolUtility.CreateDMMessageFromBuffer<ReqUpdateRoomStatusMessage>(rawMessage);
                    break;
                case DMProtocolConstants.kMsgId_ReqReadyToPlay:
                    message = DMProtocolUtility.CreateDMMessageFromBuffer<ReqReadyToPlayMessage>(rawMessage);
                    break;
                case DMProtocolConstants.kMsgId_StartingCardsDealReady:
                    message = DMProtocolUtility.CreateDMMessageFromBuffer<StartingCardsDealReadyMessage>(rawMessage);
                    break;
                case DMProtocolConstants.kMsgId_ReqQueryTurnState:
                    message = DMProtocolUtility.CreateDMMessageFromBuffer<ReqQueryTurnStateMessage>(rawMessage);
                    break;
                case DMProtocolConstants.kMsgId_ReqDrawStockCard:
                    message = DMProtocolUtility.CreateDMMessageFromBuffer<ReqDrawStockCardMessage>(rawMessage);
                    break;
                case DMProtocolConstants.kMsgId_ReqDiscardCard:
                    message = DMProtocolUtility.CreateDMMessageFromBuffer<ReqDiscardCardMessage>(rawMessage);
                    break;
                case DMProtocolConstants.kMsgId_ReqMeldingCard:
                    message = DMProtocolUtility.CreateDMMessageFromBuffer<ReqMeldingCardMessage>(rawMessage);
                    break;
                case DMProtocolConstants.kMsgId_ReqPickupAndMeldingCard:
                    message = DMProtocolUtility.CreateDMMessageFromBuffer<ReqPickupAndMeldingCardMessage>(rawMessage);
                    break;
                case DMProtocolConstants.kMsgId_ReqLayoffCard:
                    message = DMProtocolUtility.CreateDMMessageFromBuffer<ReqLayoffCardMessage>(rawMessage);
                    break;
                case DMProtocolConstants.kMsgId_leaveAndRemoveRoom:
                    message = DMProtocolUtility.CreateDMMessageFromBuffer<leaveAndRemoveRoom>(rawMessage);
                    break;
                case DMProtocolConstants.kMsgId_ReqKnockCard:
                    message = DMProtocolUtility.CreateDMMessageFromBuffer<ReqKnockCardMessage>(rawMessage);
                    break;
                case DMProtocolConstants.kMsgId_ReqPassLastTurn:
                    message = DMProtocolUtility.CreateDMMessageFromBuffer<ReqPassLastTurnMessage>(rawMessage);
                    break;

                case DMProtocolConstants.kMsgId_ReqEndCountdown:
                    message = DMProtocolUtility.CreateDMMessageFromBuffer<ReqEndCountdownMessage>(rawMessage);
                    break;
                default:
                    Debug.LogErrorFormat("[DMGameRoom] cannot create MessageQueueEntry for messageId: {0}", DMProtocolConstants.MsgIdAsString(messageId));
                    return null;
            }

            var e = new GameRoomMessageQueueEntry();
            e.connection = conn;
            e.message = message;
            return e;
        }


        public void UpdateRoomStatus(bool isAllPlayersReady = false, bool isAllPlayersEndedCountdown = false)
        {
            // Update status to all users
            UpdateRoomStatusMessage roomStatusMsg = new UpdateRoomStatusMessage();
            roomStatusMsg.numberOfPlayers = (byte)mPlayers.Count;
            roomStatusMsg.userIds = new string[mPlayers.Count];
            roomStatusMsg.userReadys = new bool[mPlayers.Count];
            for (int i = 0; i < mPlayers.Count; i++)
            {
                roomStatusMsg.userIds[i] = mPlayers[i].userId;
                roomStatusMsg.userReadys[i] = mReadyToPlay[i];
            }
            roomStatusMsg.allPlayersReady = isAllPlayersReady;
            roomStatusMsg.allPlayersEndedCountdown = isAllPlayersEndedCountdown;

            for (int i = 0; i < mPlayers.Count; i++)
            {
                if (mPlayers[i].isBot)
                {
                    continue;
                }
                var conn_ = mPlayerToConnectionMapping[mPlayers[i]];
                SetRoomIdBeforeSend(conn_, roomStatusMsg);
            }
        }

        public void Process()
        {

            while (mMessagesQueue.Count > 0)
            {
                GameRoomMessageQueueEntry e = mMessagesQueue.Dequeue();
                DMBaseMessage msg = e.message;

                if (msg.roomID != roomId)
                {
                    Debug.LogErrorFormat("Not This ROOM MAN!! {0} {1}", msg.roomID, msg.messageID);
                    return;
                }
                DMConnection conn = e.connection;

                switch (msg.messageID)
                {
                    case DMProtocolConstants.kMsgId_InitGameRoom:
                        {

                        }
                        break;
                    case DMProtocolConstants.kMsgId_ReqUpdateRoomStatus:
                        {
                            UpdateRoomStatus();
                        }
                        break;
                    case DMProtocolConstants.kMsgId_ReqReadyToPlay:
                        {
                            mReadyToPlay[conn.player.playerIndex] = true;

                            bool isAllPlayersReady = true;
                            for (int i = 0; i < mReadyToPlay.Count; i++)
                            {
                                isAllPlayersReady &= mReadyToPlay[i];
                            }

                            // Response to requesting users
                            var respReadyToPlayMsg = new RespReadyToPlayMessage();
                            respReadyToPlayMsg.playerIndex = conn.player.playerIndex;
                            SetRoomIdBeforeSend(conn, respReadyToPlayMsg);
                            UpdateRoomStatus(isAllPlayersReady);

                            if (isAllPlayersReady)
                            {
                                if (mPlayers.Count != kMaxPlayer)
                                {
                                    AddBots();
                                    UpdateRoomStatus(isAllPlayersReady);
                                }

                                ChangeGameState(GameStateEnum.WaitForAllPlayerEndCountdown);
                            }
                            
                        }
                        break;
                    
                    case DMProtocolConstants.kMsgId_ReqEndCountdown:
                        {
                            mEndCountdown[conn.player.playerIndex] = true;
                      
                            bool isAllPlayersEndedCountdown = true;
                            foreach (var keyValue in mEndCountdown)
                            {
                                if (!keyValue.Value) {
                                    isAllPlayersEndedCountdown = false;
                                    break;
                                }
                            }
                          
                            var respEndCountdownMsg = new RespEndCountdownMessage();
                            SetRoomIdBeforeSend(conn, respEndCountdownMsg);
                            UpdateRoomStatus(true, isAllPlayersEndedCountdown);

                            if (isAllPlayersEndedCountdown)
                            {
                                ChangeGameState(GameStateEnum.DrawCardAtStart);
                            }
                        }
                        break;
                    
                    case DMProtocolConstants.kMsgId_StartingCardsDealReady:
                        {
                            mStartingCardDealReady[conn.player.playerIndex] = true;
                            bool isAllPlayerStartingCardDealReady = true;
                            for (int i = 0; i < mStartingCardDealReady.Count; i++)
                            {
                                isAllPlayerStartingCardDealReady &= mStartingCardDealReady[i];
                            }
                            if (isAllPlayerStartingCardDealReady)
                            {
                                ChangeGameState(GameStateEnum.StartGame);
                            }
                        }
                        break;
                    case DMProtocolConstants.kMsgId_ReqQueryTurnState:
                        {
                            byte playerIndex = conn.player.playerIndex;
                            RespQueryTurnStateMessage respMsg = new RespQueryTurnStateMessage();

                            respMsg.discardCards = new DMCard[mDiscardCards.Count];
                            for (int i = 0; i < mDiscardCards.Count; i++)
                            {
                                respMsg.discardCards[i] = mDiscardCards[i];
                            }

                            List<DMCard> playerOnHandCards = mPlayerOnHandCards[conn.player];
                            respMsg.playerOnHandCards = new DMCard[playerOnHandCards.Count];
                            for (int i = 0; i < playerOnHandCards.Count; i++)
                            {
                                respMsg.playerOnHandCards[i] = playerOnHandCards[i];
                            }

                            List<DMMeldingCard> meldingCards = mPlayerMeldingCardSet[conn.player];
                            respMsg.meldingCards = new DMMeldingCard[meldingCards.Count];
                            for (int i = 0; i < meldingCards.Count; i++)
                            {
                                respMsg.meldingCards[i] = meldingCards[i].Clone();
                            }

                            respMsg.numberOfStockCards = (byte)mStockCards.Count;

                            SetRoomIdBeforeSend(conn, respMsg);
                        }
                        break;
                    case DMProtocolConstants.kMsgId_ReqDrawStockCard:
                        {
                            DMPlayer reqPlayer = conn.player;
                            var playerOnHandCards = mPlayerOnHandCards[reqPlayer];

                            // Send response message to player who request to pick the stock card
                            RespDrawStockCardMessage resp = new RespDrawStockCardMessage();
                            resp.card = DMCard.EmptyCard();
                            resp.numberOfStockCards = (byte)mStockCards.Count;
                            resp.nextActionTimeLimit = kDefaultActionLimitTime;
                            if (mStockCards.Count > 0)
                            {
                                // Deal a card from stock
                                DMCard card = mStockCards[0];
                                mStockCards.RemoveAt(0);

                                // Put it in Player on hand card
                                playerOnHandCards.Add(card);

                                // Update response message
                                resp.card = card;
                                resp.numberOfStockCards = (byte)mStockCards.Count;

                                // Update Current Turn flag
                                currentTurnFlag_DrawStockCard = true;

                                // Reset Action time
                                ResetActionTimer();
                            }
                            SetRoomIdBeforeSend(conn, resp);

                            // Send PlayerDrawStockCardMessage to other clients to inform that player in this turn has already drawn the stock card
                            PlayerDrawStockCardMessage playerDrawStockCardMsg = new PlayerDrawStockCardMessage();
                            playerDrawStockCardMsg.playerIndex = reqPlayer.playerIndex;
                            playerDrawStockCardMsg.numberOfOnHandCards = (byte)playerOnHandCards.Count;
                            playerDrawStockCardMsg.numberOfStockCards = (byte)mStockCards.Count;
                            playerDrawStockCardMsg.nextActionLimitTime = kDefaultActionLimitTime;
                            for (int i = 0; i < mPlayers.Count; i++)
                            {
                                // Do not send message to Bot
                                if (mPlayers[i].isBot) continue;
                                // Do not send message to the player who request to pick stock card
                                if (i == reqPlayer.playerIndex) continue;

                                var otherConn = mPlayerToConnectionMapping[mPlayers[i]];
                                SetRoomIdBeforeSend(otherConn, playerDrawStockCardMsg);
                            }
                        }
                        break;
                    case DMProtocolConstants.kMsgId_ReqDiscardCard:
                        {
                            DMPlayer reqPlayer = conn.player;
                            ReqDiscardCardMessage discardCardMessage = msg as ReqDiscardCardMessage;
                            DMCard discardedCard = discardCardMessage.card;


                            for (int i = 0; i < mPlayerOnHandCards[reqPlayer].Count; i++)
                            {
                                mPlayerOnHandCards[reqPlayer][i] = DMCard.changePlayerIndex(mPlayerOnHandCards[reqPlayer][i], reqPlayer.playerIndex);
                            }

                            // Remove card on Player's OnHandCard
                            List<DMCard> onHandCard = mPlayerOnHandCards[reqPlayer];
                            for (int i = onHandCard.Count - 1; i >= 0; i--)
                            {
                                if (onHandCard[i].IsIdentical(discardedCard))
                                {
                                    discardedCard = onHandCard[i];
                                    onHandCard.RemoveAt(i);
                                    break;
                                }
                            }


                            Debug.LogErrorFormat("Player Discard Card {0}", discardedCard.playerIndex);
                            // Put that card in Discarded cards pile
                            mScoreControllers[reqPlayer].checkDiscardCondition(discardedCard);
                            mDiscardCards.Add(discardedCard);

                            bool isDropFull = System.Convert.ToBoolean(mScoreControllers[reqPlayer].GetAndSetScoreConditionFormType(DMScoreController.typeScore.dropFull));
                            bool isDropDummy = System.Convert.ToBoolean(mScoreControllers[reqPlayer].GetAndSetScoreConditionFormType(DMScoreController.typeScore.dropDummy));

                            if (isDropDummy || isDropFull)
                            {
                                PlayerScoreMessage playerScoreMessage = new PlayerScoreMessage();
                                playerScoreMessage.score = mScoreControllers[reqPlayer].Score;
                                SetRoomIdBeforeSend(conn, playerScoreMessage);
                                // Send bot score to the other player
                                otherPlayerScoreMessage otherPlayerScoreMessage = new otherPlayerScoreMessage();
                                otherPlayerScoreMessage.playerIndex = reqPlayer.playerIndex;
                                otherPlayerScoreMessage.score = mScoreControllers[reqPlayer].Score;
                                SendMappingMessage(reqPlayer, mPlayers, mPlayerToConnectionMapping, otherPlayerScoreMessage);
                            }

                            // Send response message to the requesting player
                            RespDiscardCardMessage respMessage = new RespDiscardCardMessage();
                            respMessage.card = discardedCard;
                            respMessage.nextActionTimeLimit = kDefaultActionLimitTime;
                            respMessage.isDropFull = isDropFull;
                            respMessage.isDropDummy = isDropDummy;
                            SetRoomIdBeforeSend(conn, respMessage);

                            // Update Current Turn flag
                            currentTurnFlag_DiscardCard = true;

                            // Send PlayerDiscardCardMessage to other clients to inform that player has discarded a card
                            PlayerDiscardCardMessage playerDiscardCardMessage = new PlayerDiscardCardMessage();
                            playerDiscardCardMessage.card = discardedCard;
                            playerDiscardCardMessage.numberOfOnHandCards = (byte)mPlayerOnHandCards[reqPlayer].Count;
                            playerDiscardCardMessage.discardCards = mDiscardCards.ToArray();
                            playerDiscardCardMessage.playerIndex = reqPlayer.playerIndex;
                            playerDiscardCardMessage.nextActionLimitTime = kDefaultActionLimitTime;
                            playerDiscardCardMessage.isDropFull = isDropFull;
                            playerDiscardCardMessage.isDropDummy = isDropDummy;
                            SendMappingMessage(reqPlayer, mPlayers, mPlayerToConnectionMapping, playerDiscardCardMessage);
                        }
                        break;
                    case DMProtocolConstants.kMsgId_ReqPickupAndMeldingCard:
                        {
                            DMPlayer reqPlayer = conn.player;

                            ReqPickupAndMeldingCardMessage reqPickupAndMeldingMessage = msg as ReqPickupAndMeldingCardMessage;

                            DMMeldingCard meldingCard = reqPickupAndMeldingMessage.meldingCard;
                            bool isMeldingValid = meldingCard.Validate();
                            bool isPickCardValid = false;

                            List<DMCard> usedCards = new List<DMCard>();
                            byte targetPlayerIndex = 0;
                            //TODO: Improve app security - Check wheter the requesting pickup card found in discarded cards pile or not
                            // Find the index of the selected pickup card in mDiscardCards
                            DMCard selectedPickupCard = reqPickupAndMeldingMessage.pickupCard;
                            int idx = 0;
                            for (; idx < mDiscardCards.Count; idx++)
                            {
                                if (selectedPickupCard.IsIdentical(mDiscardCards[idx]))
                                {
                                    targetPlayerIndex = mDiscardCards[idx].playerIndex;
                                    isPickCardValid = true;
                                    break;
                                }
                            }

                            //TODO  Improve app security  AND cannot Meld All Card
                            if (isMeldingValid && isPickCardValid)
                            {

                            }
                            else
                            {
                                return;
                            }

                            //ถ้าหยิบใบสุดท้าย จะนับเป็น อาจจะได้โง่
                            if (idx + 1 == mDiscardCards.Count)
                            {
                                Debug.LogErrorFormat("[DMGameRoom] GotFool");
                                mScoreControllers[reqPlayer].checkSetDropFool(reqPlayer.playerIndex, targetPlayerIndex, trunFollowClock);
                            }

                            // Find out which cards would be picked up and put them in player onhand cards
                            var onHandCards = mPlayerOnHandCards[reqPlayer];
                            DMCard[] allPickupCards = new DMCard[mDiscardCards.Count - idx];
                            int aIdx = 0;
                            for (int i = mDiscardCards.Count - 1; i >= idx; i--)
                            {
                                allPickupCards[aIdx] = mDiscardCards[i];
                                aIdx++;
                                mDiscardCards.RemoveAt(i);
                            }

                            onHandCards.AddRange(allPickupCards);

                            // Melding card
                            var meldingCardSet = mPlayerMeldingCardSet[reqPlayer];
                            DMMeldingCard reqMeldingCard = reqPickupAndMeldingMessage.meldingCard;

                            reqMeldingCard.id = EncodeMeldingCardIdBy(reqPlayer.playerIndex, (byte)meldingCardSet.Count);

                            Debug.LogFormat("[DMGameRoom] process ReqPickAndMeldingCardMessage - Generate Id {0} for meldingCard", reqMeldingCard.id);

                            // Remove those cards to be melding from onhand cards
                            int score = 0;
                            for (int i = 0; i < reqMeldingCard.cards.Count; i++)
                            {
                                DMCard c = reqMeldingCard.cards[i];
                                for (int j = 0; j < onHandCards.Count; j++)
                                {
                                    if (onHandCards[j].IsIdentical(c))
                                    {
                                        //add player index
                                        reqMeldingCard.cards[i] = DMCard.changePlayerIndex(c, reqPlayer.playerIndex);
                                        score += c.CardScore();
                                        usedCards.Add(onHandCards[j]);

                                        Debug.LogErrorFormat("[PICKANDMELD] CARD ON PLAYER INDEX", onHandCards[j].playerIndex);
                                        onHandCards.RemoveAt(j);

                                        if (c.isSpecialCard())
                                        {
                                            mScoreControllers[reqPlayer].AddCountByType(DMScoreController.typeScore.playSpeto);
                                        }
                                        if (c.isHeadCard)
                                        {
                                            mScoreControllers[reqPlayer].AddCountByType(DMScoreController.typeScore.playHead);
                                        }
                                    }
                                }
                            }

                            mScoreControllers[reqPlayer].addCardUse(usedCards);
                            //if u card who play before , set drop full

                            string targetConditions = "";
                            string targetIndexs = "";

                            DMScoreController.GetResetMeldCondition(usedCards,
                            mScoreControllers, mPlayers, out targetConditions, out targetIndexs);
                            string[] arrayTargetIndex;
                            if (targetConditions.Length > 0)
                            {
                                arrayTargetIndex = targetIndexs.Split(',').Distinct().ToArray();
                                foreach (string item in arrayTargetIndex)
                                {
                                    int index = Int32.Parse(item);
                                    PlayerScoreMessage playerScoreMessage = new PlayerScoreMessage();
                                    playerScoreMessage.score = mScoreControllers[mPlayers[index]].Score;
                                    if (!mPlayers[index].isBot)
                                    {
                                        SetRoomIdBeforeSend(mPlayerToConnectionMapping[mPlayers[index]], playerScoreMessage);
                                    }
                                    // Send bot score to the other player
                                    otherPlayerScoreMessage otherPlayerScoreMessage = new otherPlayerScoreMessage();
                                    otherPlayerScoreMessage.playerIndex = mPlayers[index].playerIndex;
                                    otherPlayerScoreMessage.score = mScoreControllers[mPlayers[index]].Score;
                                    SendMappingMessage(mPlayers[index], mPlayers, mPlayerToConnectionMapping, otherPlayerScoreMessage);
                                }
                            }

                            if (score != 0)
                            {
                                mScoreControllers[reqPlayer].AddScore(score);
                                PlayerScoreMessage playerScoreMessage = new PlayerScoreMessage();
                                playerScoreMessage.score = mScoreControllers[reqPlayer].Score;
                                SetRoomIdBeforeSend(conn, playerScoreMessage);
                                // Send bot score to the other player
                                otherPlayerScoreMessage otherPlayerScoreMessage = new otherPlayerScoreMessage();
                                otherPlayerScoreMessage.playerIndex = reqPlayer.playerIndex;
                                otherPlayerScoreMessage.score = mScoreControllers[reqPlayer].Score;
                                SendMappingMessage(reqPlayer, mPlayers, mPlayerToConnectionMapping, otherPlayerScoreMessage);
                            }

                            // And to player's melding card set and set cardplayerindex
                            meldingCardSet.Add(reqMeldingCard);
                            currentTurnFlag_MeldingCard = true;

                            RespPickupAndMeldingCardMessage respPickupAndMeldingMessage = new RespPickupAndMeldingCardMessage();
                            respPickupAndMeldingMessage.meldingCard = reqMeldingCard;
                            respPickupAndMeldingMessage.pickupCards = allPickupCards;
                            respPickupAndMeldingMessage.playerIndexGotCondition = targetIndexs;
                            respPickupAndMeldingMessage.condition = targetConditions;
                            respPickupAndMeldingMessage.nextActionLimitTime = kDefaultActionLimitTime;
                            SetRoomIdBeforeSend(conn, respPickupAndMeldingMessage);

                            PlayerPickupAndMeldingCardMessage playerPickupAndMeldingMessage = new PlayerPickupAndMeldingCardMessage();
                            playerPickupAndMeldingMessage.playerIndex = reqPlayer.playerIndex;
                            playerPickupAndMeldingMessage.meldingCard = reqMeldingCard;
                            playerPickupAndMeldingMessage.pickupCards = allPickupCards;
                            playerPickupAndMeldingMessage.numberOfOnHandCards = (byte)onHandCards.Count;
                            playerPickupAndMeldingMessage.nextActionLimitTime = kDefaultActionLimitTime;
                            playerPickupAndMeldingMessage.playerIndexGotCondition = targetIndexs;
                            playerPickupAndMeldingMessage.condition = targetConditions;
                            for (int i = 0; i < mPlayers.Count; i++)
                            {
                                // Do not send PlayerPickupAndMelding to the requesting player
                                if (mPlayers[i] == reqPlayer) continue;
                                // Do not send PlayerPickupAndMelding to Bot player
                                if (mPlayers[i].isBot) continue;

                                DMConnection otherConn = mPlayerToConnectionMapping[mPlayers[i]];
                                SetRoomIdBeforeSend(otherConn, playerPickupAndMeldingMessage);
                            }

                        }
                        break;
                    case DMProtocolConstants.kMsgId_ReqMeldingCard:
                        {
                            DMPlayer reqPlayer = conn.player;
                            List<DMMeldingCard> meldingCardSets = mPlayerMeldingCardSet[reqPlayer];

                            ReqMeldingCardMessage reqMeldingCardMessage = msg as ReqMeldingCardMessage;
                            DMMeldingCard meldingCard = reqMeldingCardMessage.meldingCard;
                            string targetConditions = "";
                            string targetIndexs = "";
                            bool isMeldingValid = meldingCard.Validate();
                            bool isHandCardValid = true;
                            List<DMCard> usedCards = new List<DMCard>();
                            int score = 0;
                            //TODO check Card On Hand valid
                            if (isMeldingValid)
                            {
                                // Set Id
                                meldingCard.id = EncodeMeldingCardIdBy(reqPlayer.playerIndex, (byte)meldingCardSets.Count);
                                Debug.LogFormat("[DMGameRoom] process ReqMeldingCardMessage - Generate Id {0} for meldingCard", meldingCard.id);

                                // Remove card on Player's OnHandCard
                                List<DMCard> onHandCard = mPlayerOnHandCards[reqPlayer];
                                for (int m_idx = 0; m_idx < meldingCard.cards.Count; m_idx++)
                                {
                                    DMCard card = meldingCard.cards[m_idx];
                                    Debug.LogFormat("[DMGameRoom] MeldingCard: card {0}", card.CardName());

                                    bool valid = false;
                                    for (int h_idx = onHandCard.Count - 1; h_idx >= 0; h_idx--)
                                    {
                                        if (onHandCard[h_idx].IsIdentical(card))
                                        {
                                            //add player index
                                            meldingCard.cards[m_idx] = DMCard.changePlayerIndex(card, reqPlayer.playerIndex);
                                            mScoreControllers[reqPlayer].addCardUse(card);
                                            usedCards.Add(onHandCard[h_idx]);
                                            onHandCard.RemoveAt(h_idx);
                                            score += card.CardScore();
                                            if (card.isSpecialCard())
                                            {
                                                mScoreControllers[reqPlayer].AddCountByType(DMScoreController.typeScore.playSpeto);
                                            }
                                            if (card.isHeadCard)
                                            {
                                                mScoreControllers[reqPlayer].AddCountByType(DMScoreController.typeScore.playHead);
                                            }
                                            valid = true;
                                            break;
                                        }
                                    }
                                    if (!valid)
                                    {
                                        Debug.LogWarningFormat("Invalid operation: a card: {0} to being melded is not on Player's hand", card.CardName());
                                    }
                                }
                                // Add to player's melding card set
                                meldingCardSets.Add(meldingCard.Clone());
                                //if u card who play before , set drop full

                                DMScoreController.GetResetMeldCondition(usedCards,
                                mScoreControllers, mPlayers, out targetConditions, out targetIndexs);
                                string[] arrayTargetIndex;
                                if (targetConditions.Length > 0)
                                {
                                    arrayTargetIndex = targetIndexs.Split(',').Distinct().ToArray();
                                    foreach (string item in arrayTargetIndex)
                                    {
                                        int index = Int32.Parse(item);
                                        PlayerScoreMessage playerScoreMessage = new PlayerScoreMessage();
                                        playerScoreMessage.score = mScoreControllers[mPlayers[index]].Score;
                                        if (!mPlayers[index].isBot)
                                        {
                                            SetRoomIdBeforeSend(mPlayerToConnectionMapping[mPlayers[index]], playerScoreMessage);
                                        }
                                        // Send bot score to the other player
                                        otherPlayerScoreMessage otherPlayerScoreMessage = new otherPlayerScoreMessage();
                                        otherPlayerScoreMessage.playerIndex = mPlayers[index].playerIndex;
                                        otherPlayerScoreMessage.score = mScoreControllers[mPlayers[index]].Score;
                                        SendMappingMessage(mPlayers[index], mPlayers, mPlayerToConnectionMapping, otherPlayerScoreMessage);
                                    }
                                }
                            }


                            if (score != 0)
                            {
                                mScoreControllers[reqPlayer].AddScore(score);
                                PlayerScoreMessage playerScoreMessage = new PlayerScoreMessage();
                                playerScoreMessage.score = mScoreControllers[reqPlayer].Score;
                                SetRoomIdBeforeSend(conn, playerScoreMessage);
                                // Send bot score to the other player
                                otherPlayerScoreMessage otherPlayerScoreMessage = new otherPlayerScoreMessage();
                                otherPlayerScoreMessage.playerIndex = reqPlayer.playerIndex;
                                otherPlayerScoreMessage.score = mScoreControllers[reqPlayer].Score;
                                SendMappingMessage(reqPlayer, mPlayers, mPlayerToConnectionMapping, otherPlayerScoreMessage);
                                currentTurnFlag_MeldingCard = true;
                            }

                            // Send response to the requesting player
                            RespMeldingCardMessage respMeldingCardMessage = new RespMeldingCardMessage();
                            respMeldingCardMessage.allow = isMeldingValid;
                            respMeldingCardMessage.meldingCard = meldingCard;
                            respMeldingCardMessage.nextActionLimitTime = kDefaultActionLimitTime;
                            respMeldingCardMessage.playerIndexGotCondition = targetIndexs;
                            respMeldingCardMessage.condition = targetConditions;
                            SetRoomIdBeforeSend(conn, respMeldingCardMessage);

                            // Broadcast PlayerMeldingCardMessage to other players
                            PlayerMeldingCardMessage playerMeldingCardMessage = new PlayerMeldingCardMessage();
                            playerMeldingCardMessage.playerIndex = reqPlayer.playerIndex;
                            playerMeldingCardMessage.meldingCard = meldingCard;
                            playerMeldingCardMessage.playerIndexGotCondition = targetIndexs;
                            playerMeldingCardMessage.condition = targetConditions;
                            SendMappingMessage(reqPlayer, mPlayers, mPlayerToConnectionMapping, playerMeldingCardMessage);

                        }
                        break;

                    case DMProtocolConstants.kMsgId_ReqLayoffCard:
                        {
                            DMPlayer reqPlayer = conn.player;
                            ReqLayoffCardMessage reqLayoffCard = msg as ReqLayoffCardMessage;


                            DMCard[] layoffCard = reqLayoffCard.layoffCards;
                            DMMeldingCard meldingCard = null;
                            DMPlayer targetPlayer = null;

                            int targetInsert;
                            int score = 0;
                            for (int i = 0; i < mPlayers.Count; i++)
                            {
                                List<DMMeldingCard> _meldingCardSet = mPlayerMeldingCardSet[mPlayers[i]];
                                for (int j = 0; j < _meldingCardSet.Count; j++)
                                {
                                    if (reqLayoffCard.targetMeldingCardId == _meldingCardSet[j].id)
                                    {
                                        targetPlayer = mPlayers[i];
                                        meldingCard = _meldingCardSet[j];
                                        break;
                                    }
                                }
                            }

                            if (meldingCard != null)
                            {
                                List<DMCard> onHandCard = mPlayerOnHandCards[reqPlayer];
                                for (int m_idx = 0; m_idx < layoffCard.Length; m_idx++)
                                {
                                    DMCard card = layoffCard[m_idx];
                                    card.playerIndex = reqPlayer.playerIndex;
                                    Debug.LogFormat("[DMGameRoom] LayoffCard: card {0}", card.CardName());

                                    if (meldingCard.InsertLayoffCard(card, out targetInsert))
                                    {
                                        bool valid = false;
                                        for (int h_idx = onHandCard.Count - 1; h_idx >= 0; h_idx--)
                                        {
                                            if (onHandCard[h_idx].IsIdentical(card))
                                            {
                                                mScoreControllers[reqPlayer].addCardUse(onHandCard[h_idx]);
                                                onHandCard.RemoveAt(h_idx);
                                                score += card.CardScore();
                                                valid = true;
                                                if (card.isSpecialCard())
                                                {
                                                    mScoreControllers[reqPlayer].AddCountByType(DMScoreController.typeScore.playSpeto);
                                                }
                                                if (card.isHeadCard)
                                                {
                                                    mScoreControllers[reqPlayer].AddCountByType(DMScoreController.typeScore.playHead);
                                                }
                                                break;
                                            }
                                        }
                                        if (!valid)
                                        {
                                            Debug.LogWarningFormat("[DMGameRoom]Invalid operation: a card: {0} to being melded is not on Player's hand", card.CardName());
                                        }
                                    }
                                    else
                                    {
                                        Debug.LogWarningFormat("[DMGameRoom] Can't insert layoff card", card.CardName());
                                    }
                                }
                                mScoreControllers[reqPlayer].checkLayoffCondition(targetPlayer, layoffCard[0], meldingCard);
                                byte targetCondition = 0;
                                DMScoreController.ResetAndGetTargetCondition(mScoreControllers, DMScoreController.typeScore.gotDummySpeto, out targetCondition);

                                if (score != 0)
                                {
                                    mScoreControllers[reqPlayer].AddScore(score);
                                    PlayerScoreMessage playerScoreMessage = new PlayerScoreMessage();
                                    playerScoreMessage.score = mScoreControllers[reqPlayer].Score;
                                    SetRoomIdBeforeSend(conn, playerScoreMessage);

                                    otherPlayerScoreMessage otherPlayerScoreMessage = new otherPlayerScoreMessage();
                                    otherPlayerScoreMessage.playerIndex = reqPlayer.playerIndex;
                                    otherPlayerScoreMessage.score = mScoreControllers[reqPlayer].Score;
                                    SendMappingMessage(reqPlayer, mPlayers, mPlayerToConnectionMapping, otherPlayerScoreMessage);
                                }

                                if (targetCondition != 0)
                                {
                                    currentTurnFlag_PutDummyCard = true;
                                    PlayerScoreMessage playerScoreMessage = new PlayerScoreMessage();
                                    playerScoreMessage.score = mScoreControllers[targetPlayer].Score;
                                    if (!targetPlayer.isBot)
                                    {
                                        SetRoomIdBeforeSend(mPlayerToConnectionMapping[targetPlayer], playerScoreMessage);
                                    }

                                    otherPlayerScoreMessage otherPlayerScoreMessage = new otherPlayerScoreMessage();
                                    otherPlayerScoreMessage.playerIndex = targetPlayer.playerIndex;
                                    otherPlayerScoreMessage.score = mScoreControllers[targetPlayer].Score;
                                    SendMappingMessage(targetPlayer, mPlayers, mPlayerToConnectionMapping, otherPlayerScoreMessage);
                                }

                                // Send response to the requesting player
                                RespLayoffCardMessage respLayoffCardMessage = new RespLayoffCardMessage();
                                respLayoffCardMessage.layoffCards = layoffCard;
                                respLayoffCardMessage.playerIndex = reqPlayer.playerIndex;
                                respLayoffCardMessage.targetMeldingCardId = reqLayoffCard.targetMeldingCardId;
                                respLayoffCardMessage.targetPlayerIndex = targetPlayer.playerIndex;
                                respLayoffCardMessage.targetCondition = targetCondition;
                                SetRoomIdBeforeSend(conn, respLayoffCardMessage);

                                // Broadcast PlayerLayoffCardMessage to other players
                                PlayerLayoffCardMessage playerLayoffCardMessage = new PlayerLayoffCardMessage();
                                playerLayoffCardMessage.targetMeldingCardId = reqLayoffCard.targetMeldingCardId;
                                playerLayoffCardMessage.playerIndex = reqPlayer.playerIndex;
                                playerLayoffCardMessage.layoffCards = layoffCard;
                                playerLayoffCardMessage.targetPlayerIndex = targetPlayer.playerIndex;
                                playerLayoffCardMessage.targetCondition = targetCondition;
                                SendMappingMessage(reqPlayer, mPlayers, mPlayerToConnectionMapping, playerLayoffCardMessage);
                            }

                        }
                        break;
                    case DMProtocolConstants.kMsgId_ReqKnockCard:
                        {
                            DMPlayer reqPlayer = conn.player;
                            ReqKnockCardMessage reqKnockCard = msg as ReqKnockCardMessage;

                            byte knockType = 0;
                            byte targetIndex = 0;
                            byte targetCondition = 0;

                            mScoreControllers[reqPlayer].getDropFool(trunFollowClock, out targetIndex, out targetCondition);

                            if (mPlayerOnHandCards[reqPlayer][0].IsIdentical(reqKnockCard.card))
                            {
                                DMCard knockCard = mPlayerOnHandCards[reqPlayer][0];

                                mScoreControllers[reqPlayer].addCardUse(knockCard);

                                //Remove card that will be dropped when knock
                                mPlayerOnHandCards[reqPlayer].Remove(knockCard);

                                for (int i = 0; i < mPlayers.Count; i++)
                                {
                                    if (mPlayers[i] == reqPlayer)
                                    {
                                        mScoreControllers[mPlayers[i]].calculateAllScoreByKnockCondition(mPlayerOnHandCards[mPlayers[i]], true, out knockType);
                                    }
                                    else
                                    {
                                        byte a;
                                        mScoreControllers[mPlayers[i]].calculateAllScoreByKnockCondition(mPlayerOnHandCards[mPlayers[i]], false, out a);
                                    }
                                    PlayerScoreMessage playerScoreMessage = new PlayerScoreMessage();
                                    playerScoreMessage.score = mScoreControllers[mPlayers[i]].Score;
                                    if (!mPlayers[i].isBot)
                                    {
                                        SetRoomIdBeforeSend(mPlayerToConnectionMapping[mPlayers[i]], playerScoreMessage);
                                    }

                                    otherPlayerScoreMessage otherPlayerScoreMessage = new otherPlayerScoreMessage();
                                    otherPlayerScoreMessage.playerIndex = mPlayers[i].playerIndex;
                                    otherPlayerScoreMessage.score = mScoreControllers[mPlayers[i]].Score;
                                    SendMappingMessage(mPlayers[i], mPlayers, mPlayerToConnectionMapping, otherPlayerScoreMessage);
                                }

                                RespKnockCardMessage respMessage = new RespKnockCardMessage();
                                respMessage.allow = true;
                                respMessage.knockType = knockType;
                                respMessage.haveDropFool = System.Convert.ToBoolean(targetCondition);
                                respMessage.targetIndex = targetIndex;
                                SetRoomIdBeforeSend(conn, respMessage);

                                PlayerKnockCardMessage playerKnockCardMessage = new PlayerKnockCardMessage();
                                playerKnockCardMessage.card = knockCard;
                                playerKnockCardMessage.knockType = knockType;
                                playerKnockCardMessage.playerIndex = reqPlayer.playerIndex;
                                playerKnockCardMessage.haveDropFool = System.Convert.ToBoolean(targetCondition);
                                playerKnockCardMessage.targetIndex = targetIndex;
                                SendMappingMessage(reqPlayer, mPlayers, mPlayerToConnectionMapping, playerKnockCardMessage);
                                ChangeGameState(GameStateEnum.EndGame);
                            }
                        }
                        break;
                    case DMProtocolConstants.kMsgId_ReqPassLastTurn:
                        {
                            DMPlayer reqPlayer = conn.player;
                            //PassLastTurnMessage reqKnockCard = msg as PassLastTurnMessage;
                            for (int i = 0; i < mPlayers.Count; i++)
                            {
                                mScoreControllers[mPlayers[i]].calculateOutOfDrawCard(mPlayerOnHandCards[mPlayers[i]]);

                                PlayerScoreMessage playerScoreMessage = new PlayerScoreMessage();
                                playerScoreMessage.score = mScoreControllers[mPlayers[i]].Score;
                                if (!mPlayers[i].isBot)
                                {
                                    SetRoomIdBeforeSend(mPlayerToConnectionMapping[mPlayers[i]], playerScoreMessage);
                                }

                                otherPlayerScoreMessage otherPlayerScoreMessage = new otherPlayerScoreMessage();
                                otherPlayerScoreMessage.playerIndex = mPlayers[i].playerIndex;
                                otherPlayerScoreMessage.score = mScoreControllers[mPlayers[i]].Score;
                                SendMappingMessage(mPlayers[i], mPlayers, mPlayerToConnectionMapping, otherPlayerScoreMessage);
                            }

                            RespPassLastTurnMessage respMsg = new RespPassLastTurnMessage();
                            SetRoomIdBeforeSend(conn, respMsg);

                            PlayerPassLastTurnMessage playerMsg = new PlayerPassLastTurnMessage();
                            SendMappingMessage(reqPlayer, mPlayers, mPlayerToConnectionMapping, playerMsg);

                            ChangeGameState(GameStateEnum.EndGame);
                        }
                        break;
                    default:
                        break;
                }

            }
        }

        public void UpdateGameState()
        {
            if (mGameState == GameStateEnum.WaitForAllPlayerReady)
            {
                // Wait until all players are ready
            }
            else if (mGameState == GameStateEnum.WaitForAllPlayerEndCountdown) 
            {
                // Wait until all players end their countdown
            }
            else if (mGameState == GameStateEnum.DrawCardAtStart)
            {
                // Create card deck and shuffle
                mStockCards = DMCard.GenerateFullDeckOfCard();
                //DMCard.callCard(mStockCards, (UInt16)DMCard.callCardType.byKind);
                DMCard.Shuffle(mStockCards);

                // Create Discard cards pile
                mDiscardCards = new List<DMCard>();

                // Create Player on hand cards pile and Melding cards
                mPlayerOnHandCards = new Dictionary<DMPlayer, List<DMCard>>();
                mPlayerMeldingCardSet = new Dictionary<DMPlayer, List<DMMeldingCard>>();
                mScoreControllers = new Dictionary<DMPlayer, DMScoreController>();
                for (int i = 0; i < mPlayers.Count; i++)
                {
                    mPlayerOnHandCards.Add(mPlayers[i], new List<DMCard>());
                    mPlayerMeldingCardSet.Add(mPlayers[i], new List<DMMeldingCard>());
                    mScoreControllers.Add(mPlayers[i], new DMScoreController(this, mPlayers[i]));
                }
                //addTestLayoffCard(mPlayers[3]);


                // Draw cards for each players
                // 4 players => 7 cards, 3 players => 9 cards, 2 players => 11 cards
                // DMCard a = DMCard.newCard(0, 10, 1);
                // mPlayerOnHandCards[mPlayers[0]].Add(a);
                // a = DMCard.newCard(0, 9, 1);
                // mPlayerOnHandCards[mPlayers[0]].Add(a);
                // a = DMCard.newCard(0, 8, 1);
                // mPlayerOnHandCards[mPlayers[0]].Add(a);
                // a = DMCard.newCard(0, 10, 2);
                // mPlayerOnHandCards[mPlayers[0]].Add(a);
                // a = DMCard.newCard(0, 10, 3);
                // mPlayerOnHandCards[mPlayers[0]].Add(a);
                // a = DMCard.newCard(0, 11, 1);
                // mPlayerOnHandCards[mPlayers[0]].Add(a);
                // a = DMCard.newCard(0, 11, 2);
                // mPlayerOnHandCards[mPlayers[0]].Add(a);
                // a = DMCard.newCard(0, 11, 3);
                // mPlayerOnHandCards[mPlayers[0]].Add(a);

                // a = DMCard.newCard(0, 3, 4);
                // mPlayerOnHandCards[mPlayers[0]].Add(a);
                // a = DMCard.newCard(0, 4, 4);
                // mPlayerOnHandCards[mPlayers[0]].Add(a);
                // mPlayerOnHandCards[mPlayers[0]].Add(a);
                // a = DMCard.newCard(0, 13, 3);
                // mPlayerOnHandCards[mPlayers[0]].Add(a);
                // a = DMCard.newCard(0, 13, 4);
                // mPlayerOnHandCards[mPlayers[0]].Add(a);
                // a = DMCard.newCard(0, 7, 2);
                // mPlayerOnHandCards[mPlayers[0]].Add(a);
                // a = DMCard.newCard(0, 11, 1);
                // mPlayerOnHandCards[mPlayers[0]].Add(a);
                // a = DMCard.newCard(0, 11, 4);
                // mPlayerOnHandCards[mPlayers[0]].Add(a);


                //a = DMCard.newCard(1, 14, 2);
                //mPlayerOnHandCards[mPlayers[0]].Add(a);


                // a = DMCard.newCard(0, 2, 1);
                // mPlayerOnHandCards[mPlayers[1]].Add(a);
                // a = DMCard.newCard(0, 13, 3);
                // mPlayerOnHandCards[mPlayers[1]].Add(a);
                // a = DMCard.newCard(0, 14, 1);
                // mPlayerOnHandCards[mPlayers[1]].Add(a);
                // a = DMCard.newCard(0, 2, 3);
                // mPlayerOnHandCards[mPlayers[1]].Add(a);


                // a = DMCard.newCard(0, 12, 3);
                // mPlayerOnHandCards[mPlayers[2]].Add(a);
                // a = DMCard.newCard(0, 12, 3);
                // mPlayerOnHandCards[mPlayers[2]].Add(a);

                // a = DMCard.newCard(0, 4, 2);
                // mPlayerOnHandCards[mPlayers[3]].Add(a);
                // a = DMCard.newCard(0, 6, 3);
                // mPlayerOnHandCards[mPlayers[3]].Add(a);

                for (int i = mStockCards.Count - 1; i >= 0; i--)
                {
                    DMCard tmp = mStockCards[i];
                    foreach (var p in mPlayerOnHandCards)
                    {
                        foreach (var c in p.Value)
                        {
                            if (c.IsIdentical(tmp))
                            {
                                mStockCards.RemoveAt(i);
                                break;
                            }
                        }
                    }
                }
                // DMCard firstDiscardCard = new DMCard();
                // firstDiscardCard.rank = 14;
                // firstDiscardCard.kind = DMCard.CardKindEnum.Clubs;
                // firstDiscardCard.isHeadCard = true;
                // mDiscardCards.Add(firstDiscardCard);

                //DMCard firstDiscardCard = new DMCard();
                // firstDiscardCard.rank = 9;
                // firstDiscardCard.kind = DMCard.CardKindEnum.Spades;
                // firstDiscardCard.isHeadCard = true;
                // mDiscardCards.Add(firstDiscardCard);

                //firstDiscardCard.rank = 9;
                //firstDiscardCard.kind = DMCard.CardKindEnum.Hearts;
                //firstDiscardCard.isHeadCard = true;
                //mDiscardCards.Add(firstDiscardCard);

                //firstDiscardCard.rank = 8;
                //firstDiscardCard.kind = DMCard.CardKindEnum.Spades;
                //firstDiscardCard.isHeadCard = true;
                //mDiscardCards.Add(firstDiscardCard);

                //firstDiscardCard.rank = 5;
                //firstDiscardCard.kind = DMCard.CardKindEnum.Diamonds;
                //firstDiscardCard.isHeadCard = true;
                //mDiscardCards.Add(firstDiscardCard);

                //firstDiscardCard.rank = 14;
                //firstDiscardCard.kind = DMCard.CardKindEnum.Spades;
                //firstDiscardCard.isHeadCard = false;
                //mDiscardCards.Add(firstDiscardCard);

                //firstDiscardCard.rank = 14;
                //firstDiscardCard.kind = DMCard.CardKindEnum.Hearts;
                //firstDiscardCard.isHeadCard = false;
                //mDiscardCards.Add(firstDiscardCard);



                for (int i = mStockCards.Count - 1; i >= 0; i--)
                {
                    DMCard tmp = mStockCards[i];
                    foreach (var c in mDiscardCards)
                    {
                        if (c.IsIdentical(tmp))
                        {
                            mStockCards.RemoveAt(i);
                            break;
                        }
                    }

                }

                // byte[] ranks = { 5, 6, 7 };
                // DMCard.CardKindEnum[] kinds = { DMCard.CardKindEnum.Clubs };
                // addTestLayoffCard(mPlayers[1], ranks, kinds);

                // ranks = new byte[] { 5, 6, 7 };
                // kinds = new DMCard.CardKindEnum[] { DMCard.CardKindEnum.Spades };
                // addTestLayoffCard(mPlayers[3], ranks, kinds);

                // ranks = new byte[] { 12, 13, 14 };
                // kinds = new DMCard.CardKindEnum[] { DMCard.CardKindEnum.Spades };
                // addTestLayoffCard(mPlayers[2], ranks, kinds);

                // ranks = new byte[] { 6, 7, 8 };
                // kinds = new DMCard.CardKindEnum[] { DMCard.CardKindEnum.Spades };
                // addTestLayoffCard(mPlayers[3], ranks, kinds);

                // ranks = new byte[] { 3, 4, 5 };
                // kinds = new DMCard.CardKindEnum[] { DMCard.CardKindEnum.Diamonds };
                // addTestLayoffCard(mPlayers[3], ranks, kinds);

                // ranks = new byte[] { 9, 9, 9 };
                // kinds = new DMCard.CardKindEnum[] { DMCard.CardKindEnum.Clubs, DMCard.CardKindEnum.Diamonds, DMCard.CardKindEnum.Hearts };
                // addTestLayoffCard(mPlayers[3], ranks, kinds);

                // ranks = new byte[] { 6, 7, 8 };
                // kinds = new DMCard.CardKindEnum[] { DMCard.CardKindEnum.Hearts };
                // addTestLayoffCard(mPlayers[3], ranks, kinds);

                // ranks = new byte[] { 3, 3, 3 };
                // kinds = new DMCard.CardKindEnum[] { DMCard.CardKindEnum.Clubs, DMCard.CardKindEnum.Hearts, DMCard.CardKindEnum.Spades };
                // addTestLayoffCard(mPlayers[3], ranks, kinds);

                // ranks = new byte[] { 14, 14, 14 };
                // kinds = new DMCard.CardKindEnum[] { DMCard.CardKindEnum.Clubs, DMCard.CardKindEnum.Hearts, DMCard.CardKindEnum.Spades };
                // addTestLayoffCard(mPlayers[3], ranks, kinds);

                // ranks = new byte[] { 2, 3, 4 };
                // kinds = new DMCard.CardKindEnum[] { DMCard.CardKindEnum.Clubs, DMCard.CardKindEnum.Clubs, DMCard.CardKindEnum.Clubs };
                // addTestLayoffCard(mPlayers[0], ranks, kinds);



                foreach (var player in mPlayers)
                {
                    foreach (var ms in mPlayerMeldingCardSet[player])
                    {
                        foreach (var c in ms.cards)
                        {
                            for (int i = mStockCards.Count - 1; i >= 0; i--)
                            {
                                DMCard tmp = mStockCards[i];
                                if (c.IsIdentical(tmp))
                                {
                                    mStockCards.RemoveAt(i);
                                    break;
                                }
                            }
                        }
                    }
                }
                //DMCard firstDiscardCard = new DMCard();
                //firstDiscardCard.rank = 2;
                //firstDiscardCard.kind = DMCard.CardKindEnum.Spades;
                //firstDiscardCard.isHeadCard = true;
                //mDiscardCards.Add(firstDiscardCard);

                //firstDiscardCard.rank = 3;
                //firstDiscardCard.kind = DMCard.CardKindEnum.Clubs;
                //firstDiscardCard.isHeadCard = true;
                //mDiscardCards.Add(firstDiscardCard);

                //firstDiscardCard.rank = 9;
                //firstDiscardCard.kind = DMCard.CardKindEnum.Hearts;
                //firstDiscardCard.isHeadCard = true;
                //mDiscardCards.Add(firstDiscardCard);

                //firstDiscardCard.rank = 8;
                //firstDiscardCard.kind = DMCard.CardKindEnum.Spades;
                //firstDiscardCard.isHeadCard = true;
                //mDiscardCards.Add(firstDiscardCard);

                //firstDiscardCard.rank = 5;
                //firstDiscardCard.kind = DMCard.CardKindEnum.Diamonds;
                //firstDiscardCard.isHeadCard = true;
                //mDiscardCards.Add(firstDiscardCard);

                //firstDiscardCard.rank = 14;
                //firstDiscardCard.kind = DMCard.CardKindEnum.Spades;
                //firstDiscardCard.isHeadCard = false;
                //mDiscardCards.Add(firstDiscardCard);

                //firstDiscardCard.rank = 14;
                //firstDiscardCard.kind = DMCard.CardKindEnum.Hearts;
                //firstDiscardCard.isHeadCard = false;
                //mDiscardCards.Add(firstDiscardCard);



                //for (int i = mStockCards.Count - 1; i >= 0; i--)
                //{
                //    DMCard tmp = mStockCards[i];
                //    foreach (var c in mDiscardCards)
                //    {
                //        if (c.IsIdentical(tmp))
                //        {
                //            mStockCards.RemoveAt(i);
                //        }
                //    }
                //}

                int numberOfDrawingCards = 7 + ((mPlayers.Count - kMaxPlayer) * 2);
                for (int i = 0; i < mPlayers.Count; i++)
                {
                    DMPlayer player = mPlayers[i];
                    int j = 0;
                    while (j < numberOfDrawingCards)
                    {
                        // if (player.playerIndex == 0)
                        // {
                        //     break;
                        // }

                        // if (mStockCards.Count < 3)
                        // {

                        //     break;
                        // }
                        mStockCards[0] = DMCard.changePlayerIndex(mStockCards[0], mPlayers[i].playerIndex);
                        mPlayerOnHandCards[player].Add(mStockCards[0]);
                        mStockCards.RemoveAt(0);
                        j++;
                    }

                    if (!player.isBot)
                    {
                        // Normal player would sent StartingCardsDealReadyMessage to confirm that 
                        // they have already see Starting card deal
                        StartingCardsDealMessage msg = new StartingCardsDealMessage();
                        msg.cards = mPlayerOnHandCards[player].ToArray();
                        SetRoomIdBeforeSend(mPlayerToConnectionMapping[player], msg);
                        mStartingCardDealReady[player.playerIndex] = false;
                    }
                    else
                    {
                        // Bot player do not have to confirm (He is not human though)
                        mStartingCardDealReady[player.playerIndex] = true;
                    }
                }

                // Put the first card on Discard card pile
                DMCard _firstDiscardCard = new DMCard();


                // mScoreControllers[mPlayers[3]].setWhatAreYouDone(DMScoreController.typeScore.dropFool, true);
                // mScoreControllers[mPlayers[3]].setWhatAreYouDone(DMScoreController.typeScore.dropSpeto, true);

                // mScoreControllers[mPlayers[1]].setWhatAreYouDone(DMScoreController.typeScore.dropHead, true);

                _firstDiscardCard = mStockCards[0];
                _firstDiscardCard.isHeadCard = true;
                mDiscardCards.Add(_firstDiscardCard);
                mStockCards.RemoveAt(0);
                // mStockCards.Clear();
                ChangeGameState(GameStateEnum.WaitForClientResponseToDrawCardAsStart);

            }
            else if (mGameState == GameStateEnum.WaitForClientResponseToDrawCardAsStart)
            {
                // Wait until all player response to StartingCardsDealMessage
                bool allClientResponseToStartingCardDeal = true;
                foreach (var kvp in mStartingCardDealReady)
                {
                    if (!kvp.Value)
                    {
                        allClientResponseToStartingCardDeal = false;
                        break;
                    }
                }

                if (allClientResponseToStartingCardDeal)
                {
                    ChangeGameState(GameStateEnum.StartGame);
                }
            }
            else if (mGameState == GameStateEnum.StartGame)
            {
                mCurrentTurn = 1;
                mCurrentPlayerTurn = 0;
                ChangeGameState(GameStateEnum.BeginTurn);
            }
            else if (mGameState == GameStateEnum.BeginTurn)
            {
                DMPlayer player = mPlayers[mCurrentPlayerTurn];
                mScoreControllers[player].resetWhatAreYouDone();//remove every u done when start u turn
                for (int i = 0; i < mPlayers.Count; i++)
                {
                    if (mPlayers[i].isBot)
                    {
                        continue;
                    }
                    TurnChangeMessage message = new TurnChangeMessage();
                    message.playerIndex = (byte)mCurrentPlayerTurn;
                    message.limitActionTimeInSeconds = kDefaultActionLimitTime;
                    SetRoomIdBeforeSend(mPlayerToConnectionMapping[mPlayers[i]], message);
                }

                ResetActionTimer();

                currentTurnFlag_DiscardCard = false;
                currentTurnFlag_MeldingCard = false;
                currentTurnFlag_DrawStockCard = false;
                currentTurnFlag_PutDummyCard = false;

                if (mStockCards.Count == 0)
                {
                    currentTurnFlag_outOfCard = true;
                    mBotController.state = DMBotController.botState.pickUpAndMeldCard;
                }

                ChangeGameState(GameStateEnum.WaitForPlayerAction);
            }
            else if (mGameState == GameStateEnum.WaitForPlayerAction)
            {
                DMPlayer player = mPlayers[mCurrentPlayerTurn];
                if (!player.isBot)
                {
                    mActionTimer -= Time.deltaTime;
                    if (mActionTimer <= 0)
                    {
                        if (currentTurnFlag_outOfCard)
                        {
                            for (int i = 0; i < mPlayers.Count; i++)
                            {
                                mScoreControllers[mPlayers[i]].calculateOutOfDrawCard(mPlayerOnHandCards[mPlayers[i]]);

                                PlayerScoreMessage playerScoreMessage = new PlayerScoreMessage();
                                playerScoreMessage.score = mScoreControllers[mPlayers[i]].Score;
                                if (!mPlayers[i].isBot)
                                {
                                    SetRoomIdBeforeSend(mPlayerToConnectionMapping[mPlayers[i]], playerScoreMessage);
                                }

                                otherPlayerScoreMessage otherPlayerScoreMessage = new otherPlayerScoreMessage();
                                otherPlayerScoreMessage.playerIndex = mPlayers[i].playerIndex;
                                otherPlayerScoreMessage.score = mScoreControllers[mPlayers[i]].Score;
                                SendMappingMessage(mPlayers[i], mPlayers, mPlayerToConnectionMapping, otherPlayerScoreMessage);
                            }

                            RespPassLastTurnMessage respMsg = new RespPassLastTurnMessage();
                            SetRoomIdBeforeSend(mPlayerToConnectionMapping[player], respMsg);

                            PlayerPassLastTurnMessage playerMsg = new PlayerPassLastTurnMessage();
                            SendMappingMessage(player, mPlayers, mPlayerToConnectionMapping, playerMsg);

                            ChangeGameState(GameStateEnum.EndGame);
                            return;
                        }
                        if (!currentTurnFlag_DrawStockCard && !currentTurnFlag_MeldingCard)
                        {
                            RespDrawStockCardMessage resp = new RespDrawStockCardMessage();
                            resp.card = DMCard.EmptyCard();
                            resp.numberOfStockCards = (byte)mStockCards.Count;
                            resp.nextActionTimeLimit = kDefaultActionLimitTime;
                            if (mStockCards.Count > 0)
                            {
                                DMCard card = mStockCards[0];
                                mStockCards.RemoveAt(0);
                                mPlayerOnHandCards[player].Add(card);
                                // Update response message
                                resp.card = card;
                                resp.numberOfStockCards = (byte)mStockCards.Count;
                                // Update Current Turn flag
                                currentTurnFlag_DrawStockCard = true;
                                // Reset Action time
                                ResetActionTimer();
                            }
                            SetRoomIdBeforeSend(mPlayerToConnectionMapping[player], resp);

                            // Send PlayerDrawStockCardMessage to other clients to inform that player in this turn has already drawn the stock card
                            PlayerDrawStockCardMessage playerDrawStockCardMsg = new PlayerDrawStockCardMessage();
                            playerDrawStockCardMsg.playerIndex = player.playerIndex;
                            playerDrawStockCardMsg.numberOfOnHandCards = (byte)playerOnHandCards.Count;
                            playerDrawStockCardMsg.numberOfStockCards = (byte)mStockCards.Count;
                            playerDrawStockCardMsg.nextActionLimitTime = kDefaultActionLimitTime;
                            SendMappingMessage(player, mPlayers, mPlayerToConnectionMapping, playerDrawStockCardMsg);
                        }
                        if (!currentTurnFlag_DiscardCard)
                        {
                            int randomDiscardIndex = UnityEngine.Random.Range(0, mPlayerOnHandCards[player].Count);
                            DMCard discardedCard = mPlayerOnHandCards[player][randomDiscardIndex];
                            mPlayerOnHandCards[player].RemoveAt(randomDiscardIndex);
                            mDiscardCards.Add(discardedCard);
                            byte targetCondition = 0;

                            bool isDropFull = false;
                            DMScoreController.ResetAndGetTargetCondition(mScoreControllers, DMScoreController.typeScore.dropFull, out targetCondition);
                            isDropFull = System.Convert.ToBoolean(targetCondition);

                            bool isDropDummy = false;
                            DMScoreController.ResetAndGetTargetCondition(mScoreControllers, DMScoreController.typeScore.dropDummy, out targetCondition);
                            isDropDummy = System.Convert.ToBoolean(targetCondition);

                            if (isDropDummy || isDropFull)
                            {
                                PlayerScoreMessage playerScoreMessage = new PlayerScoreMessage();
                                playerScoreMessage.score = mScoreControllers[player].Score;
                                SetRoomIdBeforeSend(mPlayerToConnectionMapping[player], playerScoreMessage);
                                // Send bot score to the other player
                                otherPlayerScoreMessage otherPlayerScoreMessage = new otherPlayerScoreMessage();
                                otherPlayerScoreMessage.playerIndex = player.playerIndex;
                                otherPlayerScoreMessage.score = mScoreControllers[player].Score;
                                SendMappingMessage(player, mPlayers, mPlayerToConnectionMapping, otherPlayerScoreMessage);
                            }

                            // Send response message to the requesting player
                            RespDiscardCardMessage respMessage = new RespDiscardCardMessage();
                            respMessage.card = discardedCard;
                            respMessage.nextActionTimeLimit = kDefaultActionLimitTime;
                            respMessage.isDropFull = isDropFull;
                            respMessage.isDropDummy = isDropDummy;
                            SetRoomIdBeforeSend(mPlayerToConnectionMapping[player], respMessage);

                            PlayerDiscardCardMessage playerDiscardCardMessage = new PlayerDiscardCardMessage();
                            playerDiscardCardMessage.card = discardedCard;
                            playerDiscardCardMessage.numberOfOnHandCards = (byte)mPlayerOnHandCards[player].Count;
                            playerDiscardCardMessage.discardCards = mDiscardCards.ToArray();
                            playerDiscardCardMessage.playerIndex = player.playerIndex;
                            playerDiscardCardMessage.nextActionLimitTime = kDefaultActionLimitTime;
                            playerDiscardCardMessage.isDropFull = isDropFull;
                            playerDiscardCardMessage.isDropDummy = isDropDummy;
                            SendMappingMessage(player, mPlayers, mPlayerToConnectionMapping, playerDiscardCardMessage);
                            currentTurnFlag_DiscardCard = true;
                        }
                        ChangeGameState(GameStateEnum.EndTurn);
                    }
                }
                else
                {
                    #region Bot Logic

                    mActionTimer -= Time.deltaTime;
                    if (kDefaultActionLimitTime - mActionTimer > kBotMaximumActionDelayTime)
                    {
                        ResetActionTimer();
                        if (mBotController.state == DMBotController.botState.none)
                        {
                            Debug.LogWarningFormat("[BotLogic] start");
                            string card_name_bot = "";
                            foreach (DMCard c in mPlayerOnHandCards[player])
                            {
                                card_name_bot += c.CardName() + " ";
                            }
                            Debug.Log(card_name_bot);
                            mBotController.state = DMBotController.botState.pickUpAndMeldCard;
                        }
                        else if (mBotController.state == DMBotController.botState.pickUpAndMeldCard)
                        {
                            if (mBotController.botPickupAndMeldCard(player))
                            {
                                Debug.LogWarningFormat("[BotLogic] go meld state");
                                mBotController.state = DMBotController.botState.MeldCard;
                                currentTurnFlag_MeldingCard = true;
                            }
                            else
                            {
                                Debug.LogWarningFormat("[BotLogic] go draw state");
                                mBotController.state = DMBotController.botState.drawCard;
                            }

                        }
                        else if (mBotController.state == DMBotController.botState.MeldCard)
                        {
                            if (mBotController.botMeldCard(player))
                            {
                                Debug.LogWarningFormat("[BotLogic] stay meld state");
                            }
                            else
                            {
                                Debug.LogWarningFormat("[BotLogic] go layoffcard state");
                                mBotController.state = DMBotController.botState.layoffCard;
                            }
                        }
                        else if (mBotController.state == DMBotController.botState.layoffCard)
                        {
                            if (mBotController.botLayoffCard(player))
                            {
                                Debug.LogWarningFormat("[BotLogic] stay layoffcard state");
                            }
                            else
                            {
                                Debug.LogWarningFormat("[BotLogic] go discard state");
                                mBotController.state = DMBotController.botState.discardCard;
                            }
                        }
                        else if (mBotController.state == DMBotController.botState.drawCard)
                        {
                            if (currentTurnFlag_outOfCard)
                            {
                                for (int i = 0; i < mPlayers.Count; i++)
                                {
                                    mScoreControllers[mPlayers[i]].calculateOutOfDrawCard(mPlayerOnHandCards[mPlayers[i]]);

                                    PlayerScoreMessage playerScoreMessage = new PlayerScoreMessage();
                                    playerScoreMessage.score = mScoreControllers[mPlayers[i]].Score;
                                    if (!mPlayers[i].isBot)
                                    {
                                        SetRoomIdBeforeSend(mPlayerToConnectionMapping[mPlayers[i]], playerScoreMessage);
                                    }

                                    otherPlayerScoreMessage otherPlayerScoreMessage = new otherPlayerScoreMessage();
                                    otherPlayerScoreMessage.playerIndex = mPlayers[i].playerIndex;
                                    otherPlayerScoreMessage.score = mScoreControllers[mPlayers[i]].Score;
                                    SendMappingMessage(mPlayers[i], mPlayers, mPlayerToConnectionMapping, otherPlayerScoreMessage);
                                }
                                PlayerPassLastTurnMessage playerMsg = new PlayerPassLastTurnMessage();
                                SendMappingMessage(player, mPlayers, mPlayerToConnectionMapping, playerMsg);
                                ChangeGameState(GameStateEnum.EndGame);
                                Debug.LogWarningFormat("[BotLogic] Pass end turn!");
                            }
                            else
                            {
                                mBotController.drawCard(player);
                                mBotController.state = DMBotController.botState.discardCard;
                                currentTurnFlag_DrawStockCard = true;
                                Debug.LogWarningFormat("[BotLogic] Draw!");
                            }
                        }
                        else if (mBotController.state == DMBotController.botState.discardCard)
                        {
                            if (mBotController.discardOrEndCard(player))
                            {
                                ChangeGameState(GameStateEnum.EndGame);
                                Debug.LogWarningFormat("[BotLogic] Knock!");
                            }
                            else
                            {
                                if (currentTurnFlag_outOfCard)
                                {
                                    for (int i = 0; i < mPlayers.Count; i++)
                                    {
                                        mScoreControllers[mPlayers[i]].calculateOutOfDrawCard(mPlayerOnHandCards[mPlayers[i]]);

                                        PlayerScoreMessage playerScoreMessage = new PlayerScoreMessage();
                                        playerScoreMessage.score = mScoreControllers[mPlayers[i]].Score;
                                        if (!mPlayers[i].isBot)
                                        {
                                            SetRoomIdBeforeSend(mPlayerToConnectionMapping[mPlayers[i]], playerScoreMessage);
                                        }

                                        otherPlayerScoreMessage otherPlayerScoreMessage = new otherPlayerScoreMessage();
                                        otherPlayerScoreMessage.playerIndex = mPlayers[i].playerIndex;
                                        otherPlayerScoreMessage.score = mScoreControllers[mPlayers[i]].Score;
                                        SendMappingMessage(mPlayers[i], mPlayers, mPlayerToConnectionMapping, otherPlayerScoreMessage);
                                    }
                                    PlayerPassLastTurnMessage playerMsg = new PlayerPassLastTurnMessage();
                                    SendMappingMessage(player, mPlayers, mPlayerToConnectionMapping, playerMsg);
                                    ChangeGameState(GameStateEnum.EndGame);
                                    Debug.LogWarningFormat("[BotLogic] Pass end turn!");
                                }
                                else
                                {
                                    Debug.LogWarningFormat("[BotLogic] discard end turn!");
                                    currentTurnFlag_DiscardCard = true;
                                    mBotController.state = DMBotController.botState.none;
                                }
                            }
                        }
                    }
                    #region old_bot_logic
                    // if (kDefaultActionLimitTime - mActionTimer > kBotMaximumActionDelayTime)
                    // {
                    //     var botOnHandCards = mPlayerOnHandCards[player];


                    //     if (!currentTurnFlag_DrawStockCard)
                    //     {
                    //         if (mStockCards.Count > 0)
                    //         {
                    //             Debug.LogFormat("[DMGameRoom] Player(Bot) {0} draw stock card", player.playerIndex);

                    //             // Deal a card from stock
                    //             DMCard card = mStockCards[0];
                    //             mStockCards.RemoveAt(0);
                    //             botOnHandCards.Add(card);

                    //             // Send PlayerDrawStockCardMessage to other clients to inform that player in this turn has already drawn the stock card
                    //             PlayerDrawStockCardMessage playerDrawStockCardMsg = new PlayerDrawStockCardMessage();
                    //             playerDrawStockCardMsg.playerIndex = player.playerIndex;
                    //             playerDrawStockCardMsg.numberOfOnHandCards = (byte)botOnHandCards.Count;
                    //             playerDrawStockCardMsg.numberOfStockCards = (byte)mStockCards.Count;
                    //             playerDrawStockCardMsg.nextActionLimitTime = kDefaultActionLimitTime;
                    //             for (int i = 0; i < mPlayers.Count; i++)
                    //             {
                    //                 // Do not send message to Bot
                    //                 if (mPlayers[i].isBot) continue;
                    //                 // Do not send message to the player who request to pick stock card
                    //                 if (i == player.playerIndex) continue;

                    //                 var otherConn = mPlayerToConnectionMapping[mPlayers[i]];
                    //                 SetRoomIdBeforeSend(otherConn, playerDrawStockCardMsg);
                    //             }

                    //             currentTurnFlag_DrawStockCard = true;
                    //         }
                    //     }

                    //     if (!currentTurnFlag_DiscardCard)
                    //     {
                    //         if (botOnHandCards.Count > 0)
                    //         {
                    //             Debug.LogFormat("[DMGameRoom] Player(Bot) {0} discard card", player.playerIndex);

                    //             // Random a card on hand and discard it
                    //             int randomDiscardIndex = UnityEngine.Random.Range(0, botOnHandCards.Count);
                    //             DMCard discardedCard = botOnHandCards[randomDiscardIndex];
                    //             botOnHandCards.RemoveAt(randomDiscardIndex);
                    //             mDiscardCards.Add(discardedCard);

                    //             currentTurnFlag_DiscardCard = true;

                    //             // Send PlayerDiscardCardMessage to other clients to inform that player has discarded a card
                    //             PlayerDiscardCardMessage playerDiscardCardMessage = new PlayerDiscardCardMessage();
                    //             playerDiscardCardMessage.card = discardedCard;
                    //             playerDiscardCardMessage.numberOfOnHandCards = (byte)botOnHandCards.Count;
                    //             playerDiscardCardMessage.discardCards = mDiscardCards.ToArray();
                    //             playerDiscardCardMessage.playerIndex = player.playerIndex;
                    //             playerDiscardCardMessage.nextActionLimitTime = kDefaultActionLimitTime;
                    //             for (int i = 0; i < mPlayers.Count; i++)
                    //             {
                    //                 // Do not send message to Bot
                    //                 if (mPlayers[i].isBot) continue;
                    //                 // Do not send message to the player who request to pick stock card
                    //                 if (i == player.playerIndex) continue;

                    //                 DMConnection otherConn = mPlayerToConnectionMapping[mPlayers[i]];
                    //                 SetRoomIdBeforeSend(otherConn, playerDiscardCardMessage);
                    //             }
                    //         }
                    //     }
                    // }
                    #endregion


                    #endregion // Bot Logic
                }

                if (currentTurnFlag_DiscardCard && !currentTurnFlag_outOfCard)
                {
                    if (currentTurnFlag_MeldingCard)
                    {
                        mScoreControllers[player].mMeldBeforeThisRound = true;
                    }
                    ChangeGameState(GameStateEnum.EndTurn);
                }
            }
            else if (mGameState == GameStateEnum.EndTurn)
            {

                if (trunFollowClock)
                {
                    mCurrentPlayerTurn = mCurrentTurn % mPlayers.Count;
                }
                else
                {
                    mCurrentPlayerTurn = mPlayers.Count - (mCurrentTurn % mPlayers.Count);
                }
                mCurrentTurn++;
                ChangeGameState(GameStateEnum.BeginTurn);
            }
            else if (mGameState == GameStateEnum.EndGame)
            {
                countDownRemoveRoom -= Time.deltaTime;
                if (countDownRemoveRoom <= 0)
                {
                    mServer.RemoveRoomByRoomId(roomId);
                }
            }
        }

        public void ChangeGameState(GameStateEnum toState)
        {
            mGameState = toState;
            mGameSubState = 0;
            if (toState == GameStateEnum.EndGame)
            {
                SendTypeScore();
            }
        }

        void ResetActionTimer(float actionTime = kDefaultActionLimitTime + kDefaultServerActionLimitTimeShift)
        {
            mActionTimer = actionTime;
        }

        DMPlayer checkFoolDrop(DMPlayer player, int targetPlayerIndex)
        {
            int _targetPlayerIndex = targetPlayerIndex - 1;
            if (_targetPlayerIndex > mPlayers.Count)
            {
                _targetPlayerIndex = 0;
            }
            else if (_targetPlayerIndex < 0)
            {
                _targetPlayerIndex = mPlayers.Count;
            }
            foreach (var _player in mPlayers)
            {
                if (_targetPlayerIndex == _player.playerIndex)
                {
                    return _player;
                }
            }
            return null;
        }

        public UInt16 EncodeMeldingCardIdBy(byte playerIndex, byte meldingIndex)
        {
            UInt16 meldingId = (UInt16)((playerIndex * 100) + meldingIndex);
            Debug.LogFormat("[DMGameRoom] Encoding MeldingCardId by playerIndex: {0} and meldingIndex: {1}", playerIndex, meldingIndex);
            return meldingId;
        }
        void DecodeMeldingCardId(UInt16 meldingCardId, out byte playerIndex, out byte meldingIndex)
        {
            playerIndex = (byte)(meldingCardId / 100);
            meldingIndex = (byte)(meldingCardId % 100);
        }

        public void SendMappingMessage(DMPlayer player, List<DMPlayer> players, Dictionary<DMPlayer, DMConnection> playerToConnectionMapping, DMBaseMessage message)
        {
            for (int i = 0; i < players.Count; i++)
            {
                // Do not send message to Bot
                if (players[i].isBot) continue;
                // Do not send message to the player who request to pick stock card
                if (i == player.playerIndex) continue;

                DMConnection otherConn = playerToConnectionMapping[players[i]];
                SetRoomIdBeforeSend(otherConn, message);
            }
        }

        public void SetRoomIdBeforeSend(DMConnection conn, DMBaseMessage message)
        {
            message.SetRoomID(roomId);
            if (conn.player.isConnect)
            {
                mServer.Send(conn, message);
            }

        }

        void SendTypeScore() // sent data structure
        {
            /*
            playerId 
            # type score
            # card on hand
            # card use
            */

            string data = "";
            foreach (var player in mPlayers)
            {
                data += player.playerIndex + "#";
                foreach (var score in mScoreControllers[player].mCountTypeScore)
                {
                    data += (int)score.Key + "=" + score.Value + "|";
                }
                data += "#";
                foreach (var card in mPlayerOnHandCards[player])
                {
                    data += card.ToUInt16().ToString() + "|";
                }
                data += "#";
                foreach (var usedCard in mScoreControllers[player].allCardsUse)
                {
                    data += usedCard.ToUInt16().ToString() + "|";
                }
                data += "@";
            }
            foreach (var card in mDiscardCards)
            {
                data += card.ToUInt16().ToString() + "|";
            }
            data += "@";
            foreach (var card in mStockCards)
            {
                data += card.ToUInt16().ToString() + "|";
            }
            data += "@";

            PlayerCountTypeScore msg = new PlayerCountTypeScore();
            msg.data = data;
            foreach (var player in mPlayers)
            {
                if (!player.isBot)
                {
                    SetRoomIdBeforeSend(mPlayerToConnectionMapping[player], msg);
                }
            }
        }

        class GameRoomMessageQueueEntry
        {
            public DMConnection connection;
            public DMBaseMessage message;
        }


        public List<DMCard> stockCards
        {
            get { return mStockCards; }
            set { mStockCards = value; }
        }
        public List<DMCard> discardCards
        {
            get { return mDiscardCards; }
            set { mDiscardCards = value; }
        }
        public List<DMPlayer> players
        {
            get { return mPlayers; }
            set { mPlayers = value; }
        }

        public Dictionary<DMPlayer, List<DMCard>> playerOnHandCards
        {
            get { return mPlayerOnHandCards; }
            set { mPlayerOnHandCards = value; }
        }

        public Dictionary<DMPlayer, List<DMMeldingCard>> playerMeldingCardSet
        {
            get { return mPlayerMeldingCardSet; }
            set { mPlayerMeldingCardSet = value; }
        }

        // public Dictionary<DMPlayer, int> playerScore
        // {
        //     get { return mPlayerScore; }
        //     set { mPlayerScore = value; }
        // }

        public Dictionary<DMPlayer, DMConnection> playerToConnectionMapping
        {
            get { return mPlayerToConnectionMapping; }
            set { mPlayerToConnectionMapping = value; }
        }
    }

}