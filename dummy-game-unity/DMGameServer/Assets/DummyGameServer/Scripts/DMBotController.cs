using System;
using System.Linq;
using System.Collections.Generic;
using Dummy.DMProtocol;
using UnityEngine;

namespace Dummy.Server
{


    public class DMBotController
    {

        private int maximunMeldCard = 3;

        public enum botState
        {
            none = 0,
            pickUpAndMeldCard = 1,
            MeldCard = 2,
            layoffCard = 3,
            drawCard = 4,
            discardCard = 5,
        }

        public botState state;

        CasinoServer mServer;

        DMGameRoom mGameRoom;

        public DMBotController(DMGameRoom _gameroom)
        {
            state = botState.none;
            mGameRoom = _gameroom;
        }



        public void drawCard(DMPlayer player)
        {
            Debug.LogErrorFormat("[DMGameRoom] Player(Bot) {0} Draw card", player.playerIndex);
            DMCard card = mGameRoom.stockCards[0];
            mGameRoom.stockCards.RemoveAt(0);
            mGameRoom.playerOnHandCards[player].Add(card);
            mGameRoom.currentTurnFlag_DrawStockCard = true;

            // Send PlayerDrawStockCardMessage to other clients to inform that player in this turn has already drawn the stock card
            PlayerDrawStockCardMessage playerDrawStockCardMsg = new PlayerDrawStockCardMessage();
            playerDrawStockCardMsg.playerIndex = player.playerIndex;
            playerDrawStockCardMsg.numberOfOnHandCards = (byte)mGameRoom.playerOnHandCards[player].Count;
            playerDrawStockCardMsg.numberOfStockCards = (byte)mGameRoom.stockCards.Count;
            playerDrawStockCardMsg.nextActionLimitTime = DMGameRoom.kDefaultActionLimitTime;
            mGameRoom.SendMappingMessage(player, mGameRoom.players, mGameRoom.playerToConnectionMapping, playerDrawStockCardMsg);
        }

        public bool botLayoffCard(DMPlayer player)
        {

            if (mGameRoom.playerOnHandCards[player].Count <= 1)
            {
                return false;
            }

            DMMeldingCard targetMeldingCard = null;
            DMCard[] layoffCard = null;
            DMPlayer targetPlayer = null;
            for (int i = 0; i < mGameRoom.playerOnHandCards[player].Count; i++)
            {
                layoffCard = new DMCard[] { mGameRoom.playerOnHandCards[player][i] };
                targetMeldingCard = FindMeldingCardForLayOffCard(mGameRoom.playerOnHandCards[player][i], out targetPlayer);
                if (targetMeldingCard != null)
                {
                    break;
                }
            }

            if (targetMeldingCard == null)
            {
                Debug.LogWarning("[BotLayoffCard] layoff card: Not find MeldingCard");
                return false;
            }
            else
            {
                Debug.LogFormat("[BotLayoffCard]targetMeldingCard ID : {0}", targetMeldingCard.id);
            }

            int targetInsert;
            int score = 0;

            List<DMCard> onHandCard = mGameRoom.playerOnHandCards[player];
            for (int m_idx = 0; m_idx < layoffCard.Length; m_idx++)
            {
                DMCard card = layoffCard[m_idx];
                Debug.LogFormat("[BotLayoffCard] LayoffCard: card {0}", card.CardName());
                //add player index
                card = DMCard.changePlayerIndex(card, player.playerIndex);
                if (targetMeldingCard.InsertLayoffCard(card, out targetInsert))
                {
                    bool valid = false;
                    for (int h_idx = onHandCard.Count - 1; h_idx >= 0; h_idx--)
                    {
                        if (onHandCard[h_idx].IsIdentical(card))
                        {
                            mGameRoom.mScoreControllers[player].addCardUse(onHandCard[h_idx]);
                            onHandCard.RemoveAt(h_idx);
                            score += card.CardScore();
                            Debug.LogFormat("[BotLayoffCard] Remove card: {0} from player[{1}]'s hand", card.CardName(), player.playerIndex);
                            valid = true;
                            break;
                        }
                    }
                    if (!valid)
                    {
                        Debug.LogWarningFormat("[BotLayoffCard]Invalid operation: a card: {0} to being melded is not on Player's hand", card.CardName());
                    }
                }
                else
                {
                    Debug.LogWarningFormat("[BotLayoffCard] Can't insert layoff card", card.CardName());
                }
            }

            if (score != 0)
            {
                mGameRoom.mScoreControllers[player].AddScore(score);
                // Send bot score to the other player
                otherPlayerScoreMessage otherPlayerScoreMessage = new otherPlayerScoreMessage();
                otherPlayerScoreMessage.playerIndex = player.playerIndex;
                otherPlayerScoreMessage.score = mGameRoom.mScoreControllers[player].Score;
                mGameRoom.SendMappingMessage(player, mGameRoom.players, mGameRoom.playerToConnectionMapping, otherPlayerScoreMessage);
            }

            mGameRoom.mScoreControllers[player].checkLayoffCondition(targetPlayer, layoffCard[0], targetMeldingCard);
            byte targetCondition = 0;
            DMScoreController.ResetAndGetTargetCondition(mGameRoom.mScoreControllers, DMScoreController.typeScore.gotDummySpeto, out targetCondition);

            if (targetCondition != 0)
            {
                mGameRoom.currentTurnFlag_PutDummyCard = true;
                PlayerScoreMessage playerScoreMessage = new PlayerScoreMessage();
                playerScoreMessage.score = mGameRoom.mScoreControllers[targetPlayer].Score;
                if (!targetPlayer.isBot)
                {
                    mGameRoom.SetRoomIdBeforeSend(mGameRoom.playerToConnectionMapping[targetPlayer], playerScoreMessage);
                }

                otherPlayerScoreMessage otherPlayerScoreMessage = new otherPlayerScoreMessage();
                otherPlayerScoreMessage.playerIndex = targetPlayer.playerIndex;
                otherPlayerScoreMessage.score = mGameRoom.mScoreControllers[targetPlayer].Score;
                mGameRoom.SendMappingMessage(targetPlayer, mGameRoom.players, mGameRoom.playerToConnectionMapping, otherPlayerScoreMessage);

                Debug.LogErrorFormat("[BOT] Player {0} GotStepoDummy {1}", targetPlayer.playerIndex, layoffCard[0].CardShortName());
            }

            // Broadcast PlayerLayoffCardMessage to other players
            PlayerLayoffCardMessage playerLayoffCardMessage = new PlayerLayoffCardMessage();
            playerLayoffCardMessage.targetMeldingCardId = targetMeldingCard.id;
            playerLayoffCardMessage.layoffCards = layoffCard;
            playerLayoffCardMessage.playerIndex = player.playerIndex;
            playerLayoffCardMessage.targetPlayerIndex = targetPlayer.playerIndex;
            playerLayoffCardMessage.targetCondition = targetCondition;
            mGameRoom.SendMappingMessage(player, mGameRoom.players, mGameRoom.playerToConnectionMapping, playerLayoffCardMessage);
            Debug.LogFormat("[DMGameRoom] Player(Bot) {0} Layoff card", player.playerIndex);

            return true;

        }
        public bool botMeldCard(DMPlayer player)
        {
            Debug.LogErrorFormat("[DMBotController] Player(Bot) {0} BotMeld", player.playerIndex);
            DMCard[] _onHandCards = mGameRoom.playerOnHandCards[player].ToArray();
            List<DMCard> allCards = new List<DMCard>();
            allCards.AddRange(_onHandCards);
            foreach (var c in allCards)
            {
                Debug.LogErrorFormat(" All Card{0}", c.CardName());
            }
            return pickUpAndMeldOrMeld(allCards, player, false);
        }

        public bool botPickupAndMeldCard(DMPlayer player)
        {
            Debug.LogErrorFormat("[DMBotController] Player(Bot) {0} PickUpAndMeld", player.playerIndex);
            DMCard[] _discardCards = mGameRoom.discardCards.ToArray();
            DMCard[] _onHandCards = mGameRoom.playerOnHandCards[player].ToArray();
            List<DMCard> allCards = new List<DMCard>();
            allCards.AddRange(_discardCards);
            allCards.AddRange(_onHandCards);
            foreach (var c in allCards)
            {
                Debug.LogErrorFormat("Pick All Card{0}", c.CardName());
            }
            return pickUpAndMeldOrMeld(allCards, player);
        }

        public bool discardOrEndCard(DMPlayer player)
        {
            if (mGameRoom.playerOnHandCards[player].Count == 1)
            {

                DMCard knockCard = mGameRoom.playerOnHandCards[player][0];

                Debug.LogErrorFormat("[DMGameRoom] Player(Bot) {0} Knock card", player.playerIndex);
                mGameRoom.mScoreControllers[player].addCardUse(knockCard);

                //Remove bot last card when knock
                mGameRoom.playerOnHandCards[player].Remove(knockCard);

                byte knockType = 0;
                byte targetIndex = 0;
                byte targetCondition = 0;

                mGameRoom.mScoreControllers[player].getDropFool(mGameRoom.trunFollowClock, out targetIndex, out targetCondition);

                for (int i = 0; i < mGameRoom.players.Count; i++)
                {
                    if (mGameRoom.players[i] == player)
                    {
                        mGameRoom.mScoreControllers[mGameRoom.players[i]].calculateAllScoreByKnockCondition(mGameRoom.playerOnHandCards[mGameRoom.players[i]], true, out knockType);
                    }
                    else
                    {
                        byte a;
                        mGameRoom.mScoreControllers[mGameRoom.players[i]].calculateAllScoreByKnockCondition(mGameRoom.playerOnHandCards[mGameRoom.players[i]], false, out a);
                    }
                    PlayerScoreMessage playerScoreMessage = new PlayerScoreMessage();
                    playerScoreMessage.score = mGameRoom.mScoreControllers[mGameRoom.players[i]].Score;
                    if (!mGameRoom.players[i].isBot)
                    {
                        mGameRoom.SetRoomIdBeforeSend(mGameRoom.playerToConnectionMapping[mGameRoom.players[i]], playerScoreMessage);
                    }

                    otherPlayerScoreMessage otherPlayerScoreMessage = new otherPlayerScoreMessage();
                    otherPlayerScoreMessage.playerIndex = mGameRoom.players[i].playerIndex;
                    otherPlayerScoreMessage.score = mGameRoom.mScoreControllers[mGameRoom.players[i]].Score;
                    mGameRoom.SendMappingMessage(mGameRoom.players[i], mGameRoom.players, mGameRoom.playerToConnectionMapping, otherPlayerScoreMessage);
                }

                PlayerKnockCardMessage playerKnockCardMessage = new PlayerKnockCardMessage();
                playerKnockCardMessage.card = knockCard;
                playerKnockCardMessage.knockType = knockType;
                playerKnockCardMessage.playerIndex = player.playerIndex;
                playerKnockCardMessage.haveDropFool = System.Convert.ToBoolean(targetCondition);
                playerKnockCardMessage.targetIndex = targetIndex;
                mGameRoom.SendMappingMessage(player, mGameRoom.players, mGameRoom.playerToConnectionMapping, playerKnockCardMessage);
                return true;
            }
            else if (mGameRoom.playerOnHandCards[player].Count > 1)
            {
                Debug.LogErrorFormat("[DMGameRoom] Player(Bot) {0} Discard card", player.playerIndex);
                // Random a card on hand and discard it
                int randomDiscardIndex = UnityEngine.Random.Range(0, mGameRoom.playerOnHandCards[player].Count);


                for (int i = 0; i < mGameRoom.playerOnHandCards[player].Count; i++)
                {
                    if (mGameRoom.playerOnHandCards[player][i].CardShortName() == "2c")
                    {
                        randomDiscardIndex = i;
                    }
                }

                DMCard discardedCard = mGameRoom.playerOnHandCards[player][randomDiscardIndex];



                for (int i = 0; i < mGameRoom.playerOnHandCards[player].Count; i++)
                {
                    mGameRoom.playerOnHandCards[player][i] = DMCard.changePlayerIndex(mGameRoom.playerOnHandCards[player][i], player.playerIndex);
                }

                discardedCard = mGameRoom.playerOnHandCards[player][randomDiscardIndex];
                mGameRoom.playerOnHandCards[player].RemoveAt(randomDiscardIndex);



                mGameRoom.mScoreControllers[player].checkDiscardCondition(discardedCard);
                mGameRoom.discardCards.Add(discardedCard);

                bool isDropFull = System.Convert.ToBoolean(mGameRoom.mScoreControllers[player].GetAndSetScoreConditionFormType(DMScoreController.typeScore.dropFull));
                bool isDropDummy = System.Convert.ToBoolean(mGameRoom.mScoreControllers[player].GetAndSetScoreConditionFormType(DMScoreController.typeScore.dropDummy));

                if (isDropDummy || isDropFull)
                {
                    Debug.LogErrorFormat("[BOT] Player {0} isDropDummy {1} isDropFull {2}", player.playerIndex, isDropDummy, isDropFull);
                    PlayerScoreMessage playerScoreMessage = new PlayerScoreMessage();
                    playerScoreMessage.score = mGameRoom.mScoreControllers[player].Score;
                    if (!player.isBot)
                    {
                        mGameRoom.SetRoomIdBeforeSend(mGameRoom.playerToConnectionMapping[player], playerScoreMessage);
                    }

                    // Send bot score to the other player
                    otherPlayerScoreMessage otherPlayerScoreMessage = new otherPlayerScoreMessage();
                    otherPlayerScoreMessage.playerIndex = player.playerIndex;
                    otherPlayerScoreMessage.score = mGameRoom.mScoreControllers[player].Score;
                    mGameRoom.SendMappingMessage(player, mGameRoom.players, mGameRoom.playerToConnectionMapping, otherPlayerScoreMessage);
                }

                // Send PlayerDiscardCardMessage to other clients to inform that player has discarded a card
                PlayerDiscardCardMessage playerDiscardCardMessage = new PlayerDiscardCardMessage();
                playerDiscardCardMessage.card = discardedCard;
                playerDiscardCardMessage.numberOfOnHandCards = (byte)mGameRoom.playerOnHandCards[player].Count;
                playerDiscardCardMessage.discardCards = mGameRoom.discardCards.ToArray();
                playerDiscardCardMessage.playerIndex = player.playerIndex;
                playerDiscardCardMessage.nextActionLimitTime = DMGameRoom.kDefaultActionLimitTime;
                playerDiscardCardMessage.isDropFull = isDropFull;
                playerDiscardCardMessage.isDropDummy = isDropDummy;
                mGameRoom.SendMappingMessage(player, mGameRoom.players, mGameRoom.playerToConnectionMapping, playerDiscardCardMessage);
                mGameRoom.currentTurnFlag_DiscardCard = true;
            }
            else if (mGameRoom.playerOnHandCards[player].Count < 1)
            {
                Debug.LogFormat("[DMGameRoom] Player(Bot) {0} discardOrEndCard have something error", player.playerIndex);
            }

            return false;


        }

        private bool pickUpAndMeldOrMeld(List<DMCard> allCards, DMPlayer player, bool isPickupAndMeld = true)
        {
            var allPossibleMeldingCards = DMMeldingCard.FindAllPossibleMeldingCard(allCards.ToArray(), maximunMeldCard);
            //     // Find out the combination of melding cards contain both card from Discard pile and OnHand card
            DMMeldingCard validMeldingCard = null;
            List<DMCard> usedCards = new List<DMCard>();
            for (int idx = 0; idx < allPossibleMeldingCards.Count; idx++)
            {
                bool hasCardFromDiscardPile = false;
                bool hasCardFromOnHandCards = false;
                DMMeldingCard meldingCard = allPossibleMeldingCards[idx];
                for (int i = 0; i < meldingCard.cards.Count; i++)
                {
                    DMCard c = meldingCard.cards[i];
                    if (!hasCardFromDiscardPile && isPickupAndMeld)
                    {
                        // Find card in Discarded card pile
                        for (int j = 0; j < mGameRoom.discardCards.Count; j++)
                        {
                            if (c.IsIdentical(mGameRoom.discardCards[j]))
                            {
                                hasCardFromDiscardPile = true;
                                break;
                            }
                        }
                    }
                    else
                    {
                        hasCardFromDiscardPile = true;
                    }
                    if (!hasCardFromOnHandCards)
                    {
                        // Find card in OnHand cards
                        for (int j = 0; j < mGameRoom.playerOnHandCards[player].Count; j++)
                        {
                            if (c.IsIdentical(mGameRoom.playerOnHandCards[player][j]))
                            {
                                hasCardFromOnHandCards = true;
                                break;
                            }
                        }
                    }
                    if (hasCardFromDiscardPile && hasCardFromOnHandCards)
                    {
                        break;
                    }
                }

                if (hasCardFromDiscardPile && hasCardFromOnHandCards)
                {
                    validMeldingCard = meldingCard;
                    break;
                }
            }

            if (validMeldingCard == null)
            {
                Debug.Log("[pickUpAndMeldOrMeld] Bot cannot find any possible melding card");
                return false;
            }

            if (isPickupAndMeld)
            {
                for (int l = 0; l < validMeldingCard.cards.Count; l++)
                {
                    DMCard card = validMeldingCard.cards[l];

                    for (int i = 0; i < mGameRoom.discardCards.Count; i++)
                    {
                        if (card.IsIdentical(mGameRoom.discardCards[i]))
                        {
                            DMCard[] allPickupCards = new DMCard[mGameRoom.discardCards.Count - i];
                            int aIdx = 0;
                            for (int j = mGameRoom.discardCards.Count - 1; j >= i; j--)
                            {
                                allPickupCards[aIdx] = mGameRoom.discardCards[j];
                                aIdx++;
                                mGameRoom.discardCards.RemoveAt(j);
                            }
                            mGameRoom.playerOnHandCards[player].AddRange(allPickupCards);
                            break;
                        }
                    }
                }
            }
            for (int l = 0; l < validMeldingCard.cards.Count; l++)
            {
                DMCard card = validMeldingCard.cards[l];
                Debug.LogErrorFormat("Meld Card {0}", card.CardName());
            }



            int score = 0;
            for (int l = 0; l < validMeldingCard.cards.Count; l++)
            {
                DMCard card = validMeldingCard.cards[l];

                for (int i = 0; i < mGameRoom.playerOnHandCards[player].Count; i++)
                {
                    if (card.IsIdentical(mGameRoom.playerOnHandCards[player][i]))
                    {
                        score += card.CardScore();
                        usedCards.Add(mGameRoom.playerOnHandCards[player][i]);
                        //add player index
                        validMeldingCard.cards[l] = DMCard.changePlayerIndex(card, player.playerIndex);
                        mGameRoom.playerOnHandCards[player].RemoveAt(i);
                        break;
                    }
                }
            }

            List<DMMeldingCard> meldingCardSets = mGameRoom.playerMeldingCardSet[player];
            validMeldingCard.id = mGameRoom.EncodeMeldingCardIdBy(player.playerIndex, (byte)meldingCardSets.Count);
            meldingCardSets.Add(validMeldingCard.Clone());

            mGameRoom.mScoreControllers[player].addCardUse(usedCards);

            string targetConditions = "";
            string targetIndexs = "";

            DMScoreController.GetResetMeldCondition(usedCards,
            mGameRoom.mScoreControllers, mGameRoom.players, out targetConditions, out targetIndexs);
            string[] arrayTargetIndex;
            if (targetConditions.Length > 0)
            {
                Debug.LogErrorFormat("[BOT] Player {0} Found Who Fail Condition {1}", player.playerIndex, targetConditions);
                arrayTargetIndex = targetIndexs.Split(',').Distinct().ToArray();
                foreach (string item in arrayTargetIndex)
                {
                    Debug.LogErrorFormat("[BOT] PlayerFail {0} ", item);

                    int index = Int32.Parse(item);
                    PlayerScoreMessage playerScoreMessage = new PlayerScoreMessage();
                    playerScoreMessage.score = mGameRoom.mScoreControllers[mGameRoom.players[index]].Score;
                    if (!mGameRoom.players[index].isBot)
                    {
                        mGameRoom.SetRoomIdBeforeSend(mGameRoom.playerToConnectionMapping[mGameRoom.players[index]], playerScoreMessage);
                    }
                    // Send bot score to the other player
                    otherPlayerScoreMessage otherPlayerScoreMessage = new otherPlayerScoreMessage();
                    otherPlayerScoreMessage.playerIndex = mGameRoom.players[index].playerIndex;
                    otherPlayerScoreMessage.score = mGameRoom.mScoreControllers[mGameRoom.players[index]].Score;
                    mGameRoom.SendMappingMessage(mGameRoom.players[index], mGameRoom.players, mGameRoom.playerToConnectionMapping, otherPlayerScoreMessage);
                }
            }

            if (score != 0)
            {
                mGameRoom.mScoreControllers[player].AddScore(score);
                otherPlayerScoreMessage otherPlayerScoreMessage = new otherPlayerScoreMessage();
                otherPlayerScoreMessage.playerIndex = player.playerIndex;
                otherPlayerScoreMessage.score = mGameRoom.mScoreControllers[player].Score;
                mGameRoom.SendMappingMessage(player, mGameRoom.players, mGameRoom.playerToConnectionMapping, otherPlayerScoreMessage);
            }

            if (isPickupAndMeld)
            {
                PlayerPickupAndMeldingCardMessage playerPickupAndMeldingMessage = new PlayerPickupAndMeldingCardMessage();
                playerPickupAndMeldingMessage.playerIndex = player.playerIndex;
                playerPickupAndMeldingMessage.meldingCard = validMeldingCard;
                playerPickupAndMeldingMessage.pickupCards = validMeldingCard.cards.ToArray();
                playerPickupAndMeldingMessage.numberOfOnHandCards = (byte)mGameRoom.playerOnHandCards[player].Count;
                playerPickupAndMeldingMessage.nextActionLimitTime = DMGameRoom.kDefaultActionLimitTime;
                playerPickupAndMeldingMessage.playerIndexGotCondition = targetIndexs;
                playerPickupAndMeldingMessage.condition = targetConditions;
                mGameRoom.SendMappingMessage(player, mGameRoom.players, mGameRoom.playerToConnectionMapping, playerPickupAndMeldingMessage);
            }
            else
            {
                PlayerMeldingCardMessage meldMessage = new PlayerMeldingCardMessage();
                meldMessage.playerIndex = player.playerIndex;
                meldMessage.meldingCard = validMeldingCard;
                meldMessage.playerIndexGotCondition = targetIndexs;
                meldMessage.condition = targetConditions;
                mGameRoom.SendMappingMessage(player, mGameRoom.players, mGameRoom.playerToConnectionMapping, meldMessage);
            }

            mGameRoom.currentTurnFlag_MeldingCard = true;



            return true;

        }

        public DMMeldingCard FindMeldingCardForLayOffCard(DMCard card, out DMPlayer targetIndex)
        {
            targetIndex = null;
            List<DMCard> testCards = new List<DMCard>();
            for (int i = 0; i < mGameRoom.players.Count; i++)
            {
                for (int j = 0; j < mGameRoom.playerMeldingCardSet[mGameRoom.players[i]].Count; j++)
                {
                    DMMeldingCard m = mGameRoom.playerMeldingCardSet[mGameRoom.players[i]][j];
                    testCards.Clear();
                    testCards.AddRange(m.cards);
                    testCards.Add(card);
                    if (m.meldingType == DMMeldingCard.MeldingTypeEnum.SameRank)
                    {
                        if (DMCard.isSameRank(testCards))
                        {
                            targetIndex = mGameRoom.players[i];
                            return m;
                        }
                    }
                    else if (m.meldingType == DMMeldingCard.MeldingTypeEnum.RunOfSameKind)
                    {
                        if (DMCard.isRunOfSameKind(testCards))
                        {
                            targetIndex = mGameRoom.players[i];
                            return m;
                        }
                    }
                }
            }
            return null;
        }
    }
}