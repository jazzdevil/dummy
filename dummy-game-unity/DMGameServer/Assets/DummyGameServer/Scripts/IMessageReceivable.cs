﻿using UnityEngine;
using System.Collections;

using Dummy.DMProtocol;

namespace Dummy.Server {

    public interface IMessageReceivable {
        string Tag();
        void Receive(DMConnection conn, ushort messageId, byte[] rawMessage);
        void Process();
    }

}
