﻿using UnityEngine;
using System.Collections;

namespace Dummy.Server
{
    public class DMPlayer
    {
        public byte playerIndex;
        public string userId;
        public bool isBot;
        public bool isConnect;

        public DMPlayer(bool isBot = false)
        {
            this.isBot = isBot;
            isConnect = true;
        }
    }

}
