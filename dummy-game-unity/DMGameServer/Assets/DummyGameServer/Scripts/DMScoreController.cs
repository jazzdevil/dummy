using System;
using System.Collections;
using System.Collections.Generic;
using Dummy.DMProtocol;
using UnityEngine;
using System.Linq;

namespace Dummy.Server
{
    public class DMScoreController
    {

        string[] dropSpeto2cCards = { "2s", "2d", "2h", "3c", "4c" };
        string[] dropSpetoqsCards = { "qc", "qd", "qh", "10s", "js", "ks", "as" };

        public enum typeScore
        {
            None = 0,
            Knock = 1, //nomal knock
            Color = 2, //สี
            OneTurn = 3,//มืด
            ColorAndOneTurn, // สี + มืด
            NoOneTurn, //โดนมืด
            //ทิ้งเต็ม
            dropFull,
            //ปี้หัว
            dropHead,
            //ทิ้งสเปโต
            dropSpeto,
            //ทิ้งโง่
            dropFool,
            //ทิ้งดั้มมี้
            dropDummy,
            //ถูกฝากสเปโต   ต้องใช้สเปโตต่อเท่านั้น เช่น  2 => 356   แต่  2 3 => 456 ไม่ได้
            gotDummySpeto,
            playSpeto,
            playHead,
        }
        DMGameRoom mGameRoom;
        public bool mMeldBeforeThisRound;

        DMPlayer player;
        private int score;
        private int addScore;
        private int removeScore;
        public int Score
        {
            get { return score; }
        }
        public Dictionary<typeScore, byte> mCountTypeScore; // นับว่าทำอะไรไปบ้าง
        public List<DMCard> allCardsUse;
        List<bool> mWhatAreYouDone;
        public DMScoreController(DMGameRoom _gameroom, DMPlayer _player)
        {
            mGameRoom = _gameroom;
            player = _player;
            mMeldBeforeThisRound = false;
            mWhatAreYouDone = new List<bool>();
            mCountTypeScore = new Dictionary<typeScore, byte>();
            allCardsUse = new List<DMCard>();
            foreach (typeScore val in Enum.GetValues(typeof(typeScore)))
            {
                mCountTypeScore.Add(val, 0);
                mWhatAreYouDone.Add(false);
            }
        }

        public void addCardUse(DMCard c)
        {
            allCardsUse.Add(c);
        }
        public void addCardUse(List<DMCard> cs)
        {
            allCardsUse.AddRange(cs);
        }

        string getTypeScore(typeScore type)
        {
            string _typeScore;

            switch (type)
            {
                case typeScore.dropFull:
                    _typeScore = "ทิ้งเต็ม";
                    break;
                case typeScore.dropHead:
                    _typeScore = "ปี้หัว";
                    break;
                case typeScore.dropSpeto:
                    _typeScore = "ทิ้งสเปโต";
                    break;
                case typeScore.dropFool:
                    _typeScore = "ทิ้งโง่";
                    break;
                case typeScore.dropDummy:
                    _typeScore = "ทิ้งดั้มมี้";
                    break;
                case typeScore.gotDummySpeto:
                    _typeScore = "ถูกฝากสเปโต";
                    break;
                default:
                    _typeScore = "";
                    break;
            }
            return _typeScore;
        }



        public void setFailConditionScore(typeScore type)
        {
            if (type == typeScore.dropFull)
            {
                RemoveScore(50);
            }
            if (type == typeScore.dropHead)
            {
                RemoveScore(50);
            }
            if (type == typeScore.dropSpeto)
            {
                RemoveScore(50);
            }
            if (type == typeScore.dropFool)
            {
                RemoveScore(50);
            }
            if (type == typeScore.dropDummy)
            {
                RemoveScore(50);
            }
            if (type == typeScore.gotDummySpeto)
            {
                RemoveScore(50);
            }
            mCountTypeScore[type] += 1;
        }



        public void resetWhatAreYouDone()
        {
            foreach (typeScore val in Enum.GetValues(typeof(typeScore)))
            {
                mWhatAreYouDone[(int)val] = false;
            }
        }


        public void setWhatAreYouDone(typeScore t, bool b = true)
        {
            mWhatAreYouDone[(int)t] = b;
        }

        public bool getWhatAreYourDone(typeScore t)
        {
            return mWhatAreYouDone[(int)t];
        }

        public void checkLayoffCondition(DMPlayer targetPlayer, DMCard card, DMMeldingCard meldingCard)
        {
            if (player.playerIndex == targetPlayer.playerIndex)
            {
                return;
            }

            if (!card.isSpecialCard())
            {
                return;
            }

            if (meldingCard.meldingType == DMMeldingCard.MeldingTypeEnum.RunOfSameKind)
            {
                mGameRoom.mScoreControllers[targetPlayer].setWhatAreYouDone(typeScore.gotDummySpeto);
                return;
            }

            byte takeGot = 0;
            for (int i = 0; i < meldingCard.cards.Count; i++)
            {
                if (card.IsIdentical(meldingCard.cards[i]))
                {
                    if (i - 1 >= 0 && meldingCard.cards[i - 1].playerIndex != player.playerIndex)
                    {
                        takeGot = meldingCard.cards[i - 1].playerIndex;
                        break;
                    }
                    if (i + 1 < meldingCard.cards.Count && meldingCard.cards[i + 1].playerIndex != player.playerIndex)
                    {
                        takeGot = meldingCard.cards[i + 1].playerIndex;
                        break;
                    }
                }
            }
            mGameRoom.mScoreControllers[mGameRoom.players[takeGot]].setWhatAreYouDone(typeScore.gotDummySpeto);
        }

        void checkDropFull(DMCard card)
        {

            List<DMCard> temp_cards = new List<DMCard>();
            temp_cards.AddRange(mGameRoom.discardCards);
            temp_cards.Add(card);
            var allPossibleMeldingCards = DMMeldingCard.FindAllPossibleMeldingCard(temp_cards.ToArray());
            if (allPossibleMeldingCards.Count > 0)
            {
                foreach (DMMeldingCard meldingCards in allPossibleMeldingCards)
                {
                    foreach (DMCard c in meldingCards.cards)
                    {
                        if (card.IsIdentical(c))
                        {
                            setWhatAreYouDone(typeScore.dropFull);
                            Debug.LogFormat("[ScoreController]CheckDiscardCondition Player : {0} dropFull : {1}", player.playerIndex, card.CardShortName());
                            return;
                        }
                    }
                }
            }
        }

        public void checkDiscardCondition(DMCard card)
        {

            Debug.LogFormat("[ScoreController]CheckDiscardCondition", player.playerIndex, card.CardShortName());
            DMMeldingCard targetMeldingCard = null;
            targetMeldingCard = FindMeldingCardForLayOffCard(card);
            if (targetMeldingCard != null)
            {
                setWhatAreYouDone(typeScore.dropDummy);
                Debug.LogFormat("[ScoreController]CheckDiscardCondition Player : {0} dropDummy : {1}", player.playerIndex, card.CardShortName());
            }

            checkDropFull(card);

            foreach (var discardCard in mGameRoom.discardCards)
            {
                if (discardCard.isHeadCard)
                {
                    if (card.rank == discardCard.rank)
                    {
                        setWhatAreYouDone(typeScore.dropHead);
                        Debug.LogFormat("[ScoreController]CheckDiscardCondition Player : {0} dropHead : {1} By ตอง", player.playerIndex, card.CardShortName());
                    }
                    else if (card.kind == discardCard.kind &&
                     (discardCard.rank - 2 <= card.rank && card.rank <= discardCard.rank + 2))
                    {
                        setWhatAreYouDone(typeScore.dropHead);
                        Debug.LogFormat("[ScoreController]CheckDiscardCondition Player : {0} dropHead : {1} By เรียง   {2}", player.playerIndex, card.CardShortName(), discardCard.CardShortName());
                    }

                    break;
                }
            }

            foreach (var discardCard in mGameRoom.discardCards)
            {
                if (discardCard.isSpecialCard())
                {
                    if ((discardCard.CardShortName() == "2c" && dropSpeto2cCards.Contains(card.CardShortName())) ||
                    (discardCard.CardShortName() == "qs" && dropSpetoqsCards.Contains(card.CardShortName())))
                    {
                        setWhatAreYouDone(typeScore.dropSpeto);
                        Debug.LogFormat("[ScoreController]CheckDiscardCondition Player : {0} dropStepo : {1} By 1s 1p", player.playerIndex, card.CardShortName());
                        break;
                    }

                }
                else if (card.isSpecialCard())
                {
                    if ((card.CardShortName() == "2c" && dropSpeto2cCards.Contains(discardCard.CardShortName())) ||
                    (card.CardShortName() == "qs" && dropSpetoqsCards.Contains(discardCard.CardShortName()))
                    )
                    {
                        setWhatAreYouDone(typeScore.dropSpeto);
                        Debug.LogFormat("[ScoreController]CheckDiscardCondition Player : {0} dropStepo : {1} By 1p 1s", player.playerIndex, card.CardShortName());
                        break;
                    }
                }
                else if (dropSpeto2cCards.Contains(discardCard.CardShortName()) && dropSpeto2cCards.Contains(card.CardShortName()) ||
                dropSpetoqsCards.Contains(discardCard.CardShortName()) && dropSpetoqsCards.Contains(card.CardShortName()))
                {
                    setWhatAreYouDone(typeScore.dropSpeto);
                    Debug.LogFormat("[ScoreController]CheckDiscardCondition Player : {0} dropStepo : {1} By 1p 1p", player.playerIndex, card.CardShortName());
                    break;
                }
            }
        }

        public void calculateOutOfDrawCard(List<DMCard> cards)
        {
            foreach (var c in cards)
            {
                RemoveScore(c.CardScore());
            }
        }

        public void calculateAllScoreByKnockCondition(List<DMCard> cards, bool winner, out byte type)
        {
            bool isColor = false;
            bool isOneTurn = false;
            type = 0;

            if (winner)
            {
                //ชนะได้ 50 แต้ม
                AddScore(50);
                type = (int)DMScoreController.typeScore.Knock;
                //น๊อตมืด
                if (!mMeldBeforeThisRound)
                {
                    AddScore(score); //*2
                    isOneTurn = true;
                    type = (int)DMScoreController.typeScore.OneTurn;
                }
                //น๊อคสี ดอกเหมือนกัน
                if (allCardsUse.All(c => c.kind == allCardsUse.First().kind))
                {
                    AddScore(score); //*2
                    isColor = true;
                    type = (int)DMScoreController.typeScore.Color;
                }
                //มืด+สี
                if (isColor && isOneTurn)
                {
                    type = (int)DMScoreController.typeScore.ColorAndOneTurn;
                }
                mCountTypeScore[(typeScore)type] = 1;
            }
            else
            {
                foreach (var c in cards)
                {
                    RemoveScore(c.CardScore());
                }
                //โดนมืด
                if (!mMeldBeforeThisRound)
                {
                    score = score * 2;
                    type = (int)DMScoreController.typeScore.NoOneTurn;
                    mCountTypeScore[(typeScore)type] = 1;
                }
            }
        }



        public DMMeldingCard FindMeldingCardForLayOffCard(DMCard card)
        {
            List<DMCard> testCards = new List<DMCard>();
            for (int i = 0; i < mGameRoom.players.Count; i++)
            {
                for (int j = 0; j < mGameRoom.playerMeldingCardSet[mGameRoom.players[i]].Count; j++)
                {
                    DMMeldingCard m = mGameRoom.playerMeldingCardSet[mGameRoom.players[i]][j];
                    testCards.Clear();
                    testCards.AddRange(m.cards);
                    testCards.Add(card);
                    if (m.meldingType == DMMeldingCard.MeldingTypeEnum.SameRank)
                    {
                        if (DMCard.isSameRank(testCards))
                        {
                            return m;
                        }
                    }
                    else if (m.meldingType == DMMeldingCard.MeldingTypeEnum.RunOfSameKind)
                    {
                        if (DMCard.isRunOfSameKind(testCards))
                        {
                            return m;
                        }
                    }
                }
            }
            return null;
        }
        public void AddScore(int s)
        {
            score += s;
            addScore += s;
        }

        public void AddCountByType(typeScore type)
        {
            mCountTypeScore[type] += 1;
        }

        public void RemoveScore(int s)
        {
            score -= s;
            removeScore -= s;
        }

        public byte GetAndResetCondition(typeScore type)
        {
            byte condition = 0;
            if (mWhatAreYouDone[(int)type])
            {
                setWhatAreYouDone(type, false);
                condition = (byte)type;
            }
            return condition;
        }


        public byte GetAndSetScoreConditionFormType(typeScore type)
        {
            byte condition = 0;
            if (mWhatAreYouDone[(int)type])
            {
                setWhatAreYouDone(type, false);
                setFailConditionScore(type);
                condition = (byte)type;
            }
            return condition;
        }

        public static void ResetAndGetTargetCondition(Dictionary<DMPlayer, DMScoreController> scores, typeScore type, out byte targetCondition)
        {
            targetCondition = 0;
            foreach (var scon in scores)
            {
                if (scon.Value.getWhatAreYourDone(type))
                {
                    targetCondition = (byte)type;
                    scon.Value.setFailConditionScore(type);
                    scon.Value.setWhatAreYouDone(type, false);
                    break;
                }
            }
        }

        public static void GetResetMeldCondition(List<DMCard> cards, Dictionary<DMPlayer, DMScoreController> mScoreControllers, List<DMPlayer> mPlayers, out string targetConditions, out string targetIndexs)
        {
            targetConditions = "";
            targetIndexs = "";
            bool isHeadCard = false;
            bool isSpecialCard = false;

            foreach (var c in cards)
            {
                if (c.isSpecialCard())
                {
                    isSpecialCard = true;
                }
                if (c.isHeadCard)
                {
                    isHeadCard = true;
                }
            }
            foreach (var c in cards)
            {
                if (c.playerIndex == DMCard.unPlayerIndex)
                {
                    continue;
                }
                byte condition = 0;
                if (isHeadCard)
                {
                    condition = mScoreControllers[mPlayers[c.playerIndex]].GetAndResetCondition(DMScoreController.typeScore.dropHead);
                    if (condition > 0)
                    {
                        mScoreControllers[mPlayers[c.playerIndex]].setFailConditionScore(DMScoreController.typeScore.dropHead);
                        targetConditions += condition + ",";
                        targetIndexs += c.playerIndex + ",";
                    }
                }

                if (isSpecialCard)
                {
                    condition = mScoreControllers[mPlayers[c.playerIndex]].GetAndResetCondition(DMScoreController.typeScore.dropSpeto);
                    if (condition > 0)
                    {
                        mScoreControllers[mPlayers[c.playerIndex]].setFailConditionScore(DMScoreController.typeScore.dropSpeto);
                        targetConditions += condition + ",";
                        targetIndexs += c.playerIndex + ",";
                    }
                }
            }
            if (targetConditions.Length > 0)
            {
                targetConditions = targetConditions.Remove(targetConditions.Length - 1);
                targetIndexs = targetIndexs.Remove(targetIndexs.Length - 1);
            }
        }

        public void checkSetDropFool(byte playerIndex, byte cardTargetIndex, bool trunFollowClock)
        {
            if (cardTargetIndex != playerIndex)
            {
                int targetIndex = playerIndex;
                if (trunFollowClock)
                {
                    //คนก่อนหน้า ตามเข็ม
                    targetIndex--;
                    if (targetIndex < 0)
                    {
                        targetIndex = mGameRoom.players.Count - 1;
                    }
                }
                else
                {
                    //คนก่อนหน้า ถวนเข็ม
                    targetIndex++;
                    if (targetIndex >= mGameRoom.players.Count)
                    {
                        targetIndex = 0;
                    }
                }
                if (cardTargetIndex == targetIndex)
                {
                    mGameRoom.mScoreControllers[mGameRoom.players[cardTargetIndex]].setWhatAreYouDone(DMScoreController.typeScore.dropFool);
                }
            }
        }

        public void getDropFool(bool trunFollowClock, out byte targetIndex, out byte targetCondition)
        {
            int temptargetIndex = (int)player.playerIndex;
            targetIndex = player.playerIndex;

            targetCondition = 0;
            if (trunFollowClock)
            {
                //คนก่อนหน้า ตามเข็ม
                temptargetIndex--;
                if (temptargetIndex < 0)
                {
                    temptargetIndex = mGameRoom.players.Count - 1;
                }
            }
            else
            {
                //คนก่อนหน้า ถวนเข็ม
                temptargetIndex++;
                if (temptargetIndex >= mGameRoom.players.Count)
                {
                    temptargetIndex = 0;
                }
            }

            targetCondition = mGameRoom.mScoreControllers[mGameRoom.players[temptargetIndex]].GetAndResetCondition(typeScore.dropFool);
            targetIndex = (byte)temptargetIndex;
        }
    }
}