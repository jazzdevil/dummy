﻿#define ADD_BOTS_AFTER_PLAYER_JOINED_ROOM

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Dummy.DMProtocol;
using Parse;
using UnityEngine;
using UnityEngine.Networking;
using System.Net;


namespace Dummy.Server
{
    public class CasinoServer : MonoBehaviour
    {

        public const int kMaxConnection = 5000;
        public const int kSocketPort = 8888;
        public const float kHeartbeatInterval = 5f;

        public bool verbose;

        int mGameMessageChannelId = -1;
        int mSocketId = -1;
        int mNumberOfConnection = 0;

        List<DMConnection> mConnections;
        List<DMGameRoom> mGameRooms;

        float mHeartbeatTimer;

        public int numberOfConnection()
        {
            return mNumberOfConnection;
        }

        void Awake()
        {
            Application.runInBackground = true;
            mHeartbeatTimer = 0;

            mConnections = new List<DMConnection>();
            mGameRooms = new List<DMGameRoom>();
        }

        // Use this for initialization
        void Start()
        {
            NetworkTransport.Init();
            ConnectionConfig config = new ConnectionConfig();
            mGameMessageChannelId = config.AddChannel(QosType.Reliable);
            config.DisconnectTimeout = 5 * 60 * 1000;
            HostTopology topology = new HostTopology(config, kMaxConnection);

            mSocketId = NetworkTransport.AddHost(topology, kSocketPort);
            Debug.LogFormat("Socket[id:{0}] open at port: {1} , IP : {2}", mSocketId, kSocketPort, GetIP());
            Debug.unityLogger.logEnabled = false;

        }

        public string GetIP()
        {
            string strHostName = "";
            strHostName = System.Net.Dns.GetHostName();

            IPHostEntry ipEntry = System.Net.Dns.GetHostEntry(strHostName);

            IPAddress[] addr = ipEntry.AddressList;

            return addr[addr.Length - 1].ToString();

        }

        void Update()
        {
            //rec == receive
            int recSocketId;
            int recConnectionId;
            int recChannelId;
            int bufferSize = 1024;
            int dataSize;
            byte err;
            byte[] recBuffer = new byte[bufferSize];
            NetworkEventType recNetworkEvent = NetworkTransport.Receive(out recSocketId, out recConnectionId,
                out recChannelId, recBuffer, bufferSize,
                out dataSize, out err);
            switch (recNetworkEvent)
            {
                case NetworkEventType.Nothing:
                    break;
                case NetworkEventType.ConnectEvent:
                    {
                        if (verbose)
                        {
                            StringBuilder sb = new StringBuilder();
                            sb.AppendFormat("<color=blue>Incoming connection event received</color>\n");
                            sb.AppendFormat("Socket ID: {0}\n", recSocketId);
                            sb.AppendFormat("Connection ID: {0}\n", recConnectionId);
                            sb.AppendFormat("Channel ID: {0}\n", recChannelId);
                            Debug.Log(sb.ToString());
                        }

                        ManageConnectEvent(recConnectionId, recChannelId);
                        break;
                    }
                case NetworkEventType.DataEvent:
                    {
                        if (verbose)
                        {
                            StringBuilder sb = new StringBuilder();
                            sb.AppendFormat("<color=green>Remote client send data</color>\n");
                            sb.AppendFormat("Socket ID: {0}\n", recSocketId);
                            sb.AppendFormat("Connection ID: {0}\n", recConnectionId);
                            sb.AppendFormat("Channel ID: {0}\n", recChannelId);
                            Debug.Log(sb.ToString());
                        }
                        DispatchReceivedMessage(recConnectionId, recBuffer);
                        break;
                    }
                case NetworkEventType.DisconnectEvent:
                    {
                        if (verbose)
                        {
                            StringBuilder sb = new StringBuilder();
                            sb.AppendFormat("<color=yellow>Remote client event disconnected</color>\n");
                            sb.AppendFormat("Socket ID: {0}\n", recSocketId);
                            sb.AppendFormat("Connection ID: {0}\n", recConnectionId);
                            sb.AppendFormat("Channel ID: {0}\n", recChannelId);
                            Debug.Log(sb.ToString());
                        }

                        ManageDisconnectEvent(recConnectionId);
                        break;
                    }
            }

            // Process all Game rooms
            for (int i = 0; i < mGameRooms.Count; i++)
            {
                mGameRooms[i].Process();
                mGameRooms[i].UpdateGameState();
            }

            mHeartbeatTimer += Time.deltaTime;
            if (mHeartbeatTimer > kHeartbeatInterval)
            {
                mHeartbeatTimer = 0;
                Debug.Log("<color=green>[DMServer] Send Heartbeat Message</color>");
                ReqClientHeartbeat hbMsg = new ReqClientHeartbeat();
                for (int i = 0; i < mConnections.Count; i++)
                {
                    Send(mConnections[i], hbMsg);
                }
            }
        }


        void ManageConnectEvent(int connectionId, int channelId)
        {
            DMConnection newConnection = new DMConnection();
            newConnection.player = null;
            newConnection.connectionId = connectionId;
            newConnection.channelId = channelId;
            mConnections.Add(newConnection);
            mNumberOfConnection++;

            if (verbose)
            {
                Debug.Log("Send ReqWhoAreYou");
            }

            // Send ReqWhoAreYou to ask userId from client
            Send(newConnection, new ReqWhoAreYouMessage());
        }

        void ManageDisconnectEvent(int connectionId)
        {
            for (int i = 0; i < mConnections.Count; i++)
            {
                if (mConnections[i].connectionId == connectionId)
                {
                    mConnections[i].player.isConnect = false;
                    mConnections.RemoveAt(i);
                    mNumberOfConnection--;
                    break;
                }
            }
        }

        void DispatchReceivedMessage(int connectionId, byte[] rawMessage)
        {
            byte[] b = new byte[2];
            DMProtocol.DMProtocolUtility.CopyBytes(rawMessage, 0, b, 0, 2);
            ushort msgId = BitConverter.ToUInt16(b, 0);
            if (verbose)
            {
                Debug.LogFormat("Dispatching message: {0}", DMProtocolConstants.MsgIdAsString(msgId));
            }

            switch (msgId)
            {
                case DMProtocolConstants.kMsgId_RespWhoAreYou:
                    {
                        var msg = DMProtocolUtility.CreateDMMessageFromBuffer<RespWhoAreYouMessage>(rawMessage);
                        DMConnection conn = FindConnectionById(connectionId);
                        if (conn.player == null)
                        {
                            conn.player = new DMPlayer();
                            conn.player.userId = msg.userId;
                        }

                        if (verbose)
                        {
                            StringBuilder sb = new StringBuilder();
                            sb.AppendFormat("Receive RespWhoAreYouMessage\n");
                            sb.AppendFormat("userId: {0}", msg.userId);
                            Debug.Log(sb.ToString());
                        }
                    }
                    break;
                case DMProtocolConstants.kMsgId_ReqJoinRoom:
                    {
                        var msg = DMProtocolUtility.CreateDMMessageFromBuffer<ReqJoinRoomMessage>(rawMessage);
                        string roomId = msg.roomId;
                        // DMGameRoom room = FindGameRoomByRoomId(roomId);
                        DMGameRoom room = FindEmptyAndNotStartGameRoom();
                        if (room == null)
                        {
                            // If room with roomId is not found, create a new one with that roomId.
                            if (verbose)
                            {
                                Debug.LogFormat("<color=green>[DMserver] create a new Room: {0}</color>", roomId);
                            }
                            room = new DMGameRoom(this, roomId);
                            mGameRooms.Add(room);
                        }
                        DMConnection conn = FindConnectionById(connectionId);
                        DMPlayer player = conn.player;
                        bool joinRoomSuccess = false;
                        // #if ADD_BOTS_AFTER_PLAYER_JOINED_ROOM
                        //                         room.AddBots (2);
                        // #endif
                        RespJoinRoomMessage respMsg = new RespJoinRoomMessage();
                        respMsg.errMsg = "";
                        if (room.Join(player))
                        {
                            respMsg.roomID = room.roomId;
                            joinRoomSuccess = true;
                            conn.gameRoom = room;
                            // TODO: Update LobbyServer about room Information by using Parse API
                            // 
                            // 
                        }
                        else
                        {
                            respMsg.errMsg = "room is full";
                        }

                        // #if ADD_BOTS_AFTER_PLAYER_JOINED_ROOM
                        //                         room.AddBots();
                        // #endif
                        respMsg.playerIndex = player.playerIndex;
                        respMsg.success = joinRoomSuccess;
                        Send(conn, respMsg);
                    }
                    break;
                case DMProtocolConstants.kMsgId_leaveAndRemoveRoom:
                    {
                        var msg = DMProtocolUtility.CreateDMMessageFromBuffer<leaveAndRemoveRoom>(rawMessage);
                        string roomId = msg.roomID;
                        DMGameRoom room = FindGameRoomByRoomId(roomId);
                        if (room != null)
                        {
                            mGameRooms.Remove(room);
                            Debug.LogFormat("<color=red>[DMserver] remove a Room: {0}</color>", roomId);
                        }

                        break;
                    }
                default:
                    {
                        ushort messageMask = DMProtocolConstants.GetMessageIdMask(msgId);

                        //Game Room Message
                        if (messageMask == DMProtocolConstants.kMsgIdMask_GameRoom)
                        {
                            DMConnection conn = FindConnectionById(connectionId);
                            if (conn != null && conn.gameRoom != null)
                            {
                                conn.gameRoom.Receive(conn, msgId, rawMessage);
                            }
                        }
                    }
                    break;
            }
        }
        public void RemoveRoomByRoomId(string roomId)
        {
            DMGameRoom room = FindGameRoomByRoomId(roomId);
            if (room != null)
            {
                mGameRooms.Remove(room);
                Debug.LogFormat("<color=red>[DMserver] remove a Room: {0}</color>", roomId);
            }
        }

        public void Send(DMConnection connection, DMBaseMessage message)
        {
            byte[] bytes;
            using (MemoryStream stream = new MemoryStream(1024))
            {
                using (BinaryWriter writer = new BinaryWriter(stream))
                {
                    message.WriteBytes(writer);
                }
                bytes = stream.ToArray();
            }

            byte err;
            NetworkTransport.Send(mSocketId,
                connection.connectionId,
                connection.channelId,
                bytes,
                DMProtocolConstants.kProtocolBufferSize,
                out err);

            if (verbose)
            {
                Debug.LogFormat("[Server] Send message: {0} to connection {1} [{2}] | id : {3}",
                    DMProtocolConstants.MsgIdAsString(message.messageID),
                    connection.connectionId,
                    connection.player != null ? connection.player.userId : "-",
                    message.messageID);
            }
        }

        #region Connection Querying

        public DMConnection FindConnectionById(int connectionId)
        {
            for (int i = 0; i < mConnections.Count; i++)
            {
                if (mConnections[i].connectionId == connectionId)
                {
                    return mConnections[i];
                }
            }
            return null;
        }

        public DMConnection FindConnectionByDMPlayer(DMPlayer player)
        {
            if (player != null)
            {
                for (int i = 0; i < mConnections.Count; i++)
                {
                    if (mConnections[i].player == player)
                    {
                        RemovePlayerInGameRoom(player);
                        return mConnections[i];
                    }
                }
            }
            return null;
        }

        public void RemovePlayerInGameRoom(DMPlayer player)
        {
            foreach (var gameRoom in mGameRooms)
            {
                foreach (var map in gameRoom.mPlayerToConnectionMapping)
                {
                    if (map.Key == player)
                    {
                        map.Value.player.isConnect = false;
                        return;
                    }
                }
            }
        }

        #endregion

        #region Room Querying

        public DMGameRoom FindGameRoomByRoomId(string roomId)
        {
            for (int i = 0; i < mGameRooms.Count; i++)
            {
                if (mGameRooms[i].roomId == roomId)
                {
                    return mGameRooms[i];
                }
            }
            return null;
        }

        public DMGameRoom FindEmptyAndNotStartGameRoom()
        {
            for (int i = 0; i < mGameRooms.Count; i++)
            {
                if (mGameRooms[i].GetNumberOfPlayer() != DMGameRoom.kMaxPlayer && mGameRooms[i].IsStartGame())
                {
                    return mGameRooms[i];
                }
            }
            return null;
        }

        #endregion

    }

    public class DMConnection
    {
        public int channelId;
        public int connectionId;
        public DMPlayer player;
        public DMGameRoom gameRoom;
    }
}