﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Dummy.StressTest {

    public class StressTester : MonoBehaviour {
        public GameObject clientPrefab;
        public int numberOfTestClients;
        List<StressTestClient> mClients;

        public IEnumerator BeginTest(int startPortNumber) {
            int port = startPortNumber;
            mClients = new List<StressTestClient>();
            for (int i = 0; i < numberOfTestClients; i++) {
                GameObject clientGO = Instantiate(clientPrefab);
                clientGO.name = string.Format("Client-{0}", i);
                StressTestClient client = clientGO.GetComponent<StressTestClient>();
                client.clientName = clientGO.name;
                client.clientPort = ++port;
                mClients.Add(client);
            }

            yield return new WaitForSeconds(1f);

            while (true) {
                foreach (var client in mClients) {
                    client.Connect();
                }
                yield return new WaitForSeconds(1f);

                foreach (var client in mClients) {
                    client.StartTestSendMessage();
                }
                yield return new WaitForSeconds(Random.value * Random.Range(5, 10));

                foreach (var client in mClients) {
                    client.StopTestSendMessage();
                }
                yield return new WaitForSeconds(Random.value);

                foreach (var client in mClients) {
                    client.Disconnect();
                }
                yield return new WaitForSeconds(Random.value);
            }
        }
    }
}
