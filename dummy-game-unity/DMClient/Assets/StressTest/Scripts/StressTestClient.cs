﻿using UnityEngine;
using UnityEngine.Networking;

using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections;
using System.Text;

namespace Dummy.StressTest {

    public class StressTestClient : MonoBehaviour {

        public const int kMaxConnection = 2;
        public const int kServerPort = 8888;

        public string serverURL = "127.0.0.1";
        public string clientName;
        public int clientPort = 9001;
        
        int mGameMessageChannelId = -1;
        int mSocketId = -1;
        int mConnectionId;

        // Use this for initialization
        void Start() {
            Application.runInBackground = true;

            NetworkTransport.Init();
            ConnectionConfig config = new ConnectionConfig();
            mGameMessageChannelId = config.AddChannel(QosType.Reliable);
            HostTopology topology = new HostTopology(config, kMaxConnection);
            mSocketId = NetworkTransport.AddHost(topology, clientPort);
            StressTesterUI.Instance.appendLog(string.Format("Client[{0}] established connection on port: {1}", clientName, clientPort));
            Debug.LogFormat("Client[{0}] established connection on port: {1}", clientName, clientPort);
        }

        public void Connect() {
            byte err;
            mConnectionId = NetworkTransport.Connect(mSocketId, serverURL, kServerPort, 0, out err);
            StressTesterUI.Instance.appendLog(string.Format("Connected to server. ConnectionId: {0} Error: {1}", mConnectionId, NetworkErrorAsString(err)));
            Debug.LogFormat("Connected to server. ConnectionId: {0} Error: {1}", mConnectionId, NetworkErrorAsString(err));
        }

        public void Disconnect() {
            byte err;
            NetworkTransport.Disconnect(mSocketId, mConnectionId, out err);
            StressTesterUI.Instance.appendLog(string.Format("Disconnect from server. ConnectionId: {0} Error: {1}", mConnectionId, NetworkErrorAsString(err)));
        }
        
        public void SendSocketMessageToServer(string msg, out byte networkErr) {
            int bufferSize = 1024;
            byte[] buffer = new byte[bufferSize];
            Stream stream = new MemoryStream(buffer);
            BinaryFormatter formatter = new BinaryFormatter();
            formatter.Serialize(stream, msg);
            NetworkTransport.Send(mSocketId, mConnectionId, mGameMessageChannelId, buffer, bufferSize, out networkErr);
        }

        void Update() {
            ManageNetworkReceive();
        }

        void ManageNetworkReceive() {
            int recSocketId;
            int recConnectionId;
            int recChannelId;
            int bufferSize = 1024;
            int dataSize;
            byte err;
            byte[] recBuffer = new byte[bufferSize];
            NetworkEventType recNetworkEvent = NetworkTransport.Receive(out recSocketId, out recConnectionId,
                                                                        out recChannelId, recBuffer, bufferSize,
                                                                        out dataSize, out err);
            switch (recNetworkEvent) {
                case NetworkEventType.Nothing: {
                        break;
                    }
                case NetworkEventType.ConnectEvent: {
                        StringBuilder sb = new StringBuilder();
                        sb.AppendFormat("<color=blue>Client[{0}] Incoming connection event received</color>\n", clientName);
                        sb.AppendFormat("Socket ID: {0}\n", recSocketId);
                        sb.AppendFormat("Connection ID: {0}\n", recConnectionId);
                        sb.AppendFormat("Channel ID: {0}\n", recChannelId);
                        StressTesterUI.Instance.appendLog(sb.ToString());
                        Debug.Log(sb.ToString());
                        break;
                    }
                case NetworkEventType.DataEvent: {
                        Stream stream = new MemoryStream(recBuffer);
                        BinaryFormatter formatter = new BinaryFormatter();
                        string message = formatter.Deserialize(stream) as string;

                        StringBuilder sb = new StringBuilder();
                        sb.AppendFormat("<color=green>Client[{0}] Remote client send message: {1}\n</color>", clientName, message);
                        sb.AppendFormat("Socket ID: {0}\n", recSocketId);
                        sb.AppendFormat("Connection ID: {0}\n", recConnectionId);
                        sb.AppendFormat("Channel ID: {0}\n", recChannelId);
                        StressTesterUI.Instance.appendLog(sb.ToString());
                        Debug.Log(sb.ToString());
                        break;
                    }
                case NetworkEventType.DisconnectEvent: {
                        StringBuilder sb = new StringBuilder();
                        sb.AppendFormat("<color=yellow>Client[{0}] Remote client event disconnected</color>\n", clientName);
                        sb.AppendFormat("Socket ID: {0}\n", recSocketId);
                        sb.AppendFormat("Connection ID: {0}\n", recConnectionId);
                        sb.AppendFormat("Channel ID: {0}\n", recChannelId);
                        StressTesterUI.Instance.appendLog(sb.ToString());
                        Debug.Log(sb.ToString());
                        break;
                    }
            }
        }

        string NetworkErrorAsString(byte err) {
            return ((NetworkError)err).ToString();
        }

        IEnumerator TestSendMessage() {
            int count = 0;
            while (true) {
                yield return new WaitForSeconds(0.5f * Random.value);
                byte err;
                SendSocketMessageToServer(string.Format("Stress test server: {0}", ++count), out err);
                StressTesterUI.Instance.appendLog(string.Format("Client [{0}] send message Error: {1}", clientName, NetworkErrorAsString(err)));
                Debug.LogFormat("Client [{0}] send message Error: {1}", clientName, NetworkErrorAsString(err));

                if (isTerminateTestSendMessage) {
                    break;
                }
            }
        }

        bool isTerminateTestSendMessage = false;
        public void StartTestSendMessage() {
            isTerminateTestSendMessage = false;
            StartCoroutine(TestSendMessage());
        }

        public void StopTestSendMessage() {
            isTerminateTestSendMessage = true;
        }
    }

}