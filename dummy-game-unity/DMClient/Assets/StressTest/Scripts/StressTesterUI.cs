﻿using UnityEngine;
using UnityUI = UnityEngine.UI;

using System.Collections;
using System.Collections.Generic;

namespace Dummy.StressTest {

    public class StressTesterUI : MonoBehaviour {

        public int maximumDisplayMessages = 100;
        List<string> mLogMessages;

        public static StressTesterUI Instance;
        public UnityUI.Text logText;
        public UnityUI.InputField inputStartPort;
        public UnityUI.Button bttnBegin;

        public StressTester tester;

        void Awake() {
            if (Instance != null) {
                Destroy(gameObject);
                return;
            }
            Instance = this;

            mLogMessages = new List<string>();
            bttnBegin.onClick.AddListener(() => {
                StartCoroutine(tester.BeginTest(int.Parse(inputStartPort.text)));
            });
        }

        public void appendLog(string message) {
            mLogMessages.Insert(0, message);
            if (mLogMessages.Count > maximumDisplayMessages) {
                mLogMessages.RemoveAt(maximumDisplayMessages - 1);
            }

            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            for (int i = 0; i < mLogMessages.Count; i++) {
                sb.AppendLine(mLogMessages[i]);
            }
            logText.text = sb.ToString();
        }
    }
}
