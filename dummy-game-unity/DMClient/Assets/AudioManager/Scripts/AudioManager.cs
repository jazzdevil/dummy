﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Dummy.Client.Audio
{
    public class AudioManager : MonoBehaviour
    {
        #region instance field
        public static AudioManager instance;
        private void initializedInstance()
        {
            if (!instance) instance = this;
            else if (instance != this) Destroy(gameObject);
            DontDestroyOnLoad(instance);
        }
        #endregion

        #region audio file field
        Dictionary<string, AudioClip> _Audio = new Dictionary<string, AudioClip>();
        private void initializedAudioFile()
        {
            Object[] audios = Resources.LoadAll("Audio", typeof(AudioClip));

            foreach (var item in audios)
            {
                _Audio.Add(item.name, (AudioClip)item);
            }

            //debug
            //foreach (var item in _Audio)
            //{
            //    Debug.Log(item.Key + " " + item.Value);
            //}
        }
        #endregion

        #region audio source field
        public Dictionary<string, AudioSource> _audioSource = new Dictionary<string, AudioSource>();
        private void initializedAudioSource()
        {
            foreach (Transform item in transform)
            {
                if (item.GetComponent<AudioSource>())
                {
                    _audioSource.Add(item.name.Split(' ')[0], item.GetComponent<AudioSource>());
                }
            }

            //debug
            //foreach (var item in _audioSource)
            //{
            //    Debug.Log(item.Key + " " + item.Value);
            //}
        }
        #endregion

        #region play sound
        public void PlaySFX(string audio)
        {
            _audioSource["SFX"].PlayOneShot(_Audio[audio]);
        }

        public void PlayBGM(string audio)
        {
            _audioSource["BGM"].clip = _Audio[audio];
            _audioSource["BGM"].Play();
        }

        public void Play(string channel, string audio)
        {
            switch (channel)
            {
                case "BGM":
                default:
                    _audioSource[channel].clip = _Audio[audio];
                    _audioSource[channel].Play();
                    break;

                case "SFX":
                    _audioSource[channel].PlayOneShot(_Audio[audio]);
                    break;
            }
        }
        #endregion

        private void Awake()
        {
            initializedInstance();

            initializedAudioFile();

            initializedAudioSource();

            //Test
            //PlayBGM("bgm01");
            //PlaySFX("button_sfx");
        }

        //More test
        //private void Update()
        //{
        //    if(Input.GetKeyDown(KeyCode.Space))
        //    {
        //        PlaySFX("button_sfx");
        //    }
        //}
    }
}
