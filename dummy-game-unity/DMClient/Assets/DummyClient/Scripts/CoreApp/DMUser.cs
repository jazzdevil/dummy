﻿// using System;
// using System.Collections;
// using System.Collections.Generic;
// using UnityEngine;
// using XSystem;

// namespace Dummy.Client {

//     [Serializable]
//     public class DMUser : XUser {
//         public byte userType;
//         public string gender;
//         public int level;
//         public int gold;
//         public int chip;

//         public DMUser() : base() {
//             userType = 1;
//             gender = "";
//             level = 1;
//             gold = 0;
//             chip = 0;
//         }

//         public override IEnumerator SyncUserData(Action<bool> callback) {
//             WWW www = null;
//             yield return XCore.S(XCore.GET(
//                 apiPath: "auth/me",
//                 headers: null,
//                 callback: (www_) => { www = www_; }
//             ));

//             if (!string.IsNullOrEmpty(www.error)) {
//                 if (callback != null) {
//                     callback(false);
//                 }
//                 yield break;
//             }

//             var result = JsonUtility.FromJson<SyncUserDataResult<DMUser>>(www.text);
//             if (result.response == false) {
//                 if (callback != null) {
//                     callback(false);
//                 }
//                 yield break;
//             }

//             var u = result.user;
//             this.email = u.email;
//             this.name = u.name;
//             this.username = u.username;
//             this.fbIdentity = u.fbIdentity;
//             this.userType = u.userType;
//             this.gender = u.gender;
//             this.level = u.level;
//             this.gold = u.gold;
//             this.chip = u.chip;
//             if (callback != null) {
//                 callback(true);
//             }
//         }
//     }

// }