﻿// using System;
// using System.Collections;
// using System.Collections.Generic;
// using UnityEngine;

// using XSystem;

// namespace Dummy.Client {

//     public class DMStore {

//         public static IEnumerator PayGold(int amount, Action<PayOrEarnGoldResult> callback) {
//             if (XSession.Current() == null) {
//                 if (callback != null) {
//                     callback(new PayOrEarnGoldResult() {
//                         response = false,
//                         error = "Invalid Session",
//                         rawResult = "",
//                         gold = 0
//                     });
//                 }
//                 yield break;
//             }

//             WWWForm form = new WWWForm();
//             form.AddField("amount", amount);

//             WWW www = null;
//             yield return XCore.S(XCore.POST(
//                 apiPath: "/store/paygold",
//                 headers: null,
//                 postData: form,
//                 callback: (www_)  => { www = www_; }
//             ));

//             if (!string.IsNullOrEmpty(www.error)) {
//                 Debug.LogErrorFormat("Paying gold failed... {0}", www.error);
//                 if (callback != null) {
//                     callback(new PayOrEarnGoldResult() {
//                         response = false,
//                         error = www.error,
//                         rawResult = "",
//                         gold = 0
//                     });
//                 }
//                 yield break;
//             }

//             if (callback != null) {
//                 callback(JsonUtility.FromJson<PayOrEarnGoldResult>(www.text));
//             } 
//         }

//         public static IEnumerator EarnGold(int amount, Action<PayOrEarnGoldResult> callback) {
//             if (XSession.Current() == null) {
//                 if (callback != null) {
//                     callback(new PayOrEarnGoldResult() {
//                         response = false,
//                         error = "Invalid Session",
//                         rawResult = "",
//                         gold = 0
//                     });
//                 }
//                 yield break;
//             }

//             WWWForm form = new WWWForm();
//             form.AddField("amount", amount);

//             WWW www = null;
//             yield return XCore.S(XCore.POST(
//                 apiPath: "/store/earngold",
//                 headers: null,
//                 postData: form,
//                 callback: (www_) => { www = www_; }
//             ));

//             if (!string.IsNullOrEmpty(www.error)) {
//                 Debug.LogErrorFormat("Earning gold failed... {0}", www.error);
//                 if (callback != null) {
//                     callback(new PayOrEarnGoldResult() {
//                         response = false,
//                         error = www.error,
//                         rawResult = "",
//                         gold = 0
//                     });
//                 }
//                 yield break;
//             }

//             if (callback != null) {
//                 callback(JsonUtility.FromJson<PayOrEarnGoldResult>(www.text));
//             }
//         }

//         public static IEnumerator PayChip(int amount, Action<PayOrEarnChipResult> callback) {
//             if (XSession.Current() == null) {
//                 if (callback != null) {
//                     callback(new PayOrEarnChipResult() {
//                         response = false,
//                         error = "Invalid Session",
//                         rawResult = "",
//                         chip = 0
//                     });
//                 }
//                 yield break;
//             }

//             WWWForm form = new WWWForm();
//             form.AddField("amount", amount);

//             WWW www = null;
//             yield return XCore.S(XCore.POST(
//                 apiPath: "/store/paychip",
//                 headers: null,
//                 postData: form,
//                 callback: (www_) => { www = www_; }
//             ));

//             if (!string.IsNullOrEmpty(www.error)) {
//                 Debug.LogErrorFormat("Paying chip failed... {0}", www.error);
//                 if (callback != null) {
//                     callback(new PayOrEarnChipResult() {
//                         response = false,
//                         error = www.error,
//                         rawResult = "",
//                         chip = 0
//                     });
//                 }
//                 yield break;
//             }

//             if (callback != null) {
//                 callback(JsonUtility.FromJson<PayOrEarnChipResult>(www.text));
//             }
//         }

//         public static IEnumerator EarnChip(int amount, Action<PayOrEarnChipResult> callback) {
//             if (XSession.Current() == null) {
//                 if (callback != null) {
//                     callback(new PayOrEarnChipResult() {
//                         response = false,
//                         error = "Invalid Session",
//                         rawResult = "",
//                         chip = 0
//                     });
//                 }
//                 yield break;
//             }

//             WWWForm form = new WWWForm();
//             form.AddField("amount", amount);

//             WWW www = null;
//             yield return XCore.S(XCore.POST(
//                 apiPath: "/store/earnchip",
//                 headers: null,
//                 postData: form,
//                 callback: (www_) => { www = www_; }
//             ));

//             if (!string.IsNullOrEmpty(www.error)) {
//                 Debug.LogErrorFormat("Earning chip failed... {0}", www.error);
//                 if (callback != null) {
//                     callback(new PayOrEarnChipResult() {
//                         response = false,
//                         error = www.error,
//                         rawResult = "",
//                         chip = 0
//                     });
//                 }
//                 yield break;
//             }

//             if (callback != null) {
//                 callback(JsonUtility.FromJson<PayOrEarnChipResult>(www.text));
//             }
//         }

//         public static IEnumerator RecordIAPTRansaction(
//             string receiptNumber, string productID, 
//             string currencyCode, string payment,
//             Action<RecordIAPTransactionResult> callback) {
//             WWWForm form = new WWWForm();
//             string platform = "standalone";
// #if UNITY_ANDROID
//             platform = "android";
// #elif UNITY_IOS
//             platform = "ios";
// #endif
//             form.AddField("platform", platform);
//             form.AddField("receiptNumber", receiptNumber);
//             form.AddField("productID", productID);
//             form.AddField("paymentCurrency", currencyCode);
//             form.AddField("paymentAmount", payment);
//             WWW www = null;
//             yield return XCore.S(XCore.POST(
//                 apiPath: "/store/iaptransaction",
//                 postData: form,
//                 headers: null,
//                 callback: (www_) => { www = www_; }
//             ));

//             if (string.IsNullOrEmpty(www.error)) {
//                 if (callback != null) {
//                     callback(new RecordIAPTransactionResult() {
//                         response = false,
//                         error = www.error,
//                         rawResult = "",
//                         transactionID = ""
//                     });
//                 }
//                 yield break;
//             }

//             if (callback != null) {
//                 callback(JsonUtility.FromJson<RecordIAPTransactionResult>(www.text));
//             }
//         }

// #region Results

//         [Serializable]
//         public class PayOrEarnGoldResult : BaseWSResult {
//             public int gold;
//         }

//         [Serializable]
//         public class PayOrEarnChipResult : BaseWSResult {
//             public int chip;
//         }

//         [Serializable]
//         public class RecordIAPTransactionResult : BaseWSResult {
//             public string transactionID;
//         }

// #endregion

//     }
// }