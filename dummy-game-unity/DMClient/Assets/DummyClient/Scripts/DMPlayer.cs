﻿using UnityEngine;
using System.Collections;


namespace Dummy.Client {

    public class DMPlayer {
        
        public string userId;
        public string displayName;
        public int score;
        public int playerIndex;
        public int numberOfOnHandCards;
    }

}