﻿using UnityEngine;
using System.Collections;

using Dummy.DMProtocol;
using Dummy.Client.UI;


namespace Dummy.Client.Test {

    public class PlayerOnHandCardTester : MonoBehaviour {
        public PlayerOnHandCardsController onHandCardController;

        void Start() {
            for (int i = 0; i < 7; i++) {
                onHandCardController.AddCard(new DMCard() {
                    rank = (byte)Random.Range(2, 15),
                    kind = (DMCard.CardKindEnum)Random.Range(1, 4)
                });
            }
            onHandCardController.RearrangeCards();
        }

        void Update() {
            if (Input.GetKeyDown(KeyCode.I)) {
                onHandCardController.AddCard(new DMCard() {
                    rank = (byte)Random.Range(2, 15),
                    kind = (DMCard.CardKindEnum)Random.Range(1, 4)
                });
                onHandCardController.RearrangeCards();
            }
        }
    }

}