﻿using UnityEngine;
using UnityUI = UnityEngine.UI;

using System.Collections;
using System.Collections.Generic;

using Dummy.Client.UI;
using Dummy.DMProtocol;


namespace Dummy.Client.Test
{

    public class PlayerCardsOnHandControllerTester : MonoBehaviour,
        PlayerOnHandCardsController.PlayerOnHandCardsControllerRequesterDelegate
    {

        [Header("UI - Components")]
        public PlayerOnHandCardsController onHandCardsController;
        public UnityUI.Button bttnResetOnHandcards;
        public CardTableController cardTable;

        List<DMCard> mStockCard;

        void Awake()
        {
            bttnResetOnHandcards.onClick.AddListener(() =>
            {
                ResetCards();
                onHandCardsController.ShowControlPanel();
            });
        }

        void Start()
        {
            ResetCards();
            onHandCardsController.ShowControlPanel();
            onHandCardsController.delegateObject = this;
        }

        void ResetCards()
        {
            mStockCard = DMCard.GenerateFullDeckOfCard();
            DMCard.Shuffle(mStockCard);

            onHandCardsController.RemoveAllCards();
            for (int i = 14; i >= 0; i--)
            {
                onHandCardsController.AddCard(mStockCard[i]);
                mStockCard.RemoveAt(i);
            }
            onHandCardsController.RearrangeCards();
        }

        public void PlayerOnHandCardsControllerRequestDiscard(DMCard card)
        {
            onHandCardsController.ResponseToRequestDiscard(card);
        }

        public void PlayerOnHandCardsControllerRequestPickupDiscardCardAndMeld(DMCard card, DMMeldingCard meldingCard)
        {
            throw new System.NotImplementedException();
        }

        public void PlayerOnHandCardsControllerRequestDrawStockCard()
        {
            DMCard card = mStockCard[0];
            mStockCard.RemoveAt(0);
            onHandCardsController.ResponseToRequestDrawStockCard(card);
        }

        public void PlayerOnHandCardsControllerRequestLayoff(DMCard card, DMMeldingCard targetMeldingCard)
        {
            throw new System.NotImplementedException();
        }

        public void PlayerOnHandCardsControllerRequestMeldCard(DMCard[] cards)
        {
            throw new System.NotImplementedException();
        }

        public void leaveAndRemoveRoom(string roomID)
        {

        }

        public void PlayerOnHandCardsControllerRequestKnockCard(DMCard card)
        {

        }

        public void PlayerOnHandCardsControllerPassLastTurn()
        {

        }

        public void PlayerOnHandCardsControllerRequestLayoffs(DMCard[] card, ushort targetMeldingCard)
        {

        }

    }
}