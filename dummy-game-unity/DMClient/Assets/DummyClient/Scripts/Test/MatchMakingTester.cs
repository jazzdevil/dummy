﻿using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using StopWatch = System.Diagnostics.Stopwatch;

using UnityEngine;
using Protocol.DMProtocol;

public class MatchMakingTester : MonoBehaviour {

    public string serverUrl = "localhost";
    public int serverPort = 3333;
    public string message = "Hello world";
    const int kMaxMessageLength = 2048;

    DMPacketProtocol mPacketProtocol;
    Queue<byte[]> mDataQueue;
    
    TcpClient mClient;

    void Start() {
        Application.runInBackground = true;
        mDataQueue = new Queue<byte[]>();
        mPacketProtocol = new DMPacketProtocol(kMaxMessageLength);
        mPacketProtocol.OnMessageReceived = (data) => {
            mDataQueue.Enqueue(data);
        };
        Connect(serverUrl, serverPort);
    }

    void Update() {
        if (Input.GetKeyDown(KeyCode.N)) {
            CreateOrJoinRoom();
        }
    }

    void Connect(string host, int port) {
        mClient = new TcpClient();
        try {
            mClient.Connect(host, port);
        }
        catch (System.ArgumentNullException ex) {
            Debug.LogFormat("ArgumentNullException:{0}", ex);
        }
        catch (SocketException ex) {
            Debug.LogFormat("SocketException:{0}", ex);
        }
        Debug.LogFormat("Connected to server {0}:{1}", host, port);

        StartCoroutine(ReadLoop(mClient));
        StartCoroutine(ProcessingLoop());
    }

    void CreateOrJoinRoom() {
        JoinOrCreateRoomRequestMessage req = new JoinOrCreateRoomRequestMessage();
        req.userID = System.Guid.NewGuid().ToString();
        req.maxPlayers = 4;
        req.level = 1;
        string json = JsonUtility.ToJson(req);
        Send(json);
    }

    void Send(string message) {
        var data = System.Text.Encoding.ASCII.GetBytes(message);
        var wrappedMessage = DMPacketProtocol.WrapMessage(data);
        Debug.LogFormat("[MatchMakingTester] begin sending... {0}(len {1}) to {2}", message, data.Length, serverUrl);
        var stream = mClient.GetStream();
        Debug.LogFormat("[MatchMakerTester] data len={0} message len={1}", data.Length, wrappedMessage.Length);
        stream.Write(wrappedMessage, 0, wrappedMessage.Length);
        Debug.LogFormat("[MatchMakingTester] sent...");
    }

    IEnumerator ReadLoop(TcpClient client) {
        var stream = mClient.GetStream();

        while (true) {
            while (!stream.DataAvailable) {
                yield return null;
            }

            while (stream.DataAvailable) {
                StopWatch watch = new StopWatch();
                watch.Start();
                var buffer = new byte[kMaxMessageLength];
                int len = stream.Read(buffer, 0, buffer.Length);
                var data = new byte[len];
                System.Array.Copy(buffer, 0, data, 0, len);
                Debug.LogFormat("[MatchMakingTester] Received data");
                mPacketProtocol.DataReceived(data);
            }
        }
    }

    IEnumerator ProcessingLoop() {
        while (true) {
            if (mDataQueue.Count > 0) {
                var data = mDataQueue.Dequeue();
                var responseData = System.Text.Encoding.ASCII.GetString(data, 0, data.Length);
                Debug.LogFormat("[MatchMakingTester] received: {0}", responseData);
            }
            yield return null;
        }
    }

    #region Message

    const ushort MsgID_JoinOrCreateRoomRequest = 1;
    const ushort MsgID_JoinOrCreateRoomResponse = 2;
    const ushort MsgID_RoomStatusRequest = 3;
    const ushort MsgID_RoomStatusResponse = 4;

    [System.Serializable]
    public class MatchMakingMessage {
        public ushort msgID;
        public MatchMakingMessage(ushort msgID) {
            this.msgID = msgID;
        }
    }
 
    [System.Serializable]
    public class JoinOrCreateRoomRequestMessage : MatchMakingMessage {
        public string userID;
        public ushort level;
        public string roomType;
        public byte maxPlayers;
        public JoinOrCreateRoomRequestMessage() : base(MsgID_JoinOrCreateRoomRequest) { }
    }

    [System.Serializable]
    public class JoinOrCreateRoomResponseMessage : MatchMakingMessage {
        public string roomID;
        public JoinOrCreateRoomResponseMessage() : base(MsgID_JoinOrCreateRoomResponse) { }
    }

    [System.Serializable]
    public class RoomStatusRequestMessage : MatchMakingMessage {
        public string userID;
        public string roomID;
        public byte numberOfPlayer;
        public RoomStatusRequestMessage() : base(MsgID_RoomStatusRequest) { }
    }

    [System.Serializable]
    public class RoomStatusResponseMessage : MatchMakingMessage {
        public string roomID;
        public byte numberOfPlayer;
        public byte maxPlayers;
        public RoomStatusResponseMessage() : base(MsgID_RoomStatusResponse) { }
    }
    
    #endregion

}
