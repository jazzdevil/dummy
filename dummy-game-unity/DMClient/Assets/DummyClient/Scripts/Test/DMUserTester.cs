﻿// using System.Collections;
// using System.Collections.Generic;
// using UnityEngine;
// using XSystem;

// namespace Dummy.Client.Test {

//     public class DMUserTester : MonoBehaviour {

//         void Awake() {
//             Application.runInBackground = true;
//             XConfiguration.Initialize();
//             XUnityDispatcher.Initialize();
//         }

//         // Use this for initialization
//         IEnumerator Start() {
//             yield return StartCoroutine(Test_NormalLoginFlow());
//         }
        
//         IEnumerator Test_NormalLoginFlow() {
//             string username = System.Guid.NewGuid().ToString().Split('-')[0];
//             string password = "my-password-01";

//             // 1. Sign up
//             SignUpResult<DMUser> signUpResult = null;
//             Debug.LogFormat("begin signup with username:{0} password:{1}", username, password);
//             yield return XCore.S(DMUser.SignUp<DMUser>(username, password,
//                 (r) => { signUpResult = r; }
//             ));
//             if (signUpResult.response == false) {
//                 Debug.LogErrorFormat("signup failed: {0}", signUpResult.error);
//                 yield break;
//             }
//             Debug.Log("signup success");

//             // 2. Login
//             LoginResult<DMUser> loginResult = null;
//             Debug.LogFormat("begin logging in with username:{0} password:{1}", username, password);
//             yield return XCore.S(DMUser.Login<DMUser>(username, password,
//                 (r) => { loginResult = r; }
//             ));
//             if (loginResult.response == false) {
//                 Debug.LogErrorFormat("login failed: {0}", loginResult.error);
//                 yield break;
//             }

//             // 3. Sync user data
//             DMUser me = XUser.CurrentUser() as DMUser;
//             bool syncUserSuccess = false;
//             Debug.Log("sync user data");
//             yield return XCore.S(me.SyncUserData((r) => { syncUserSuccess = r; }));
//             if (!syncUserSuccess) {
//                 Debug.LogError("syncUserData failed!!");
//                 yield break;
//             }
//             Debug.LogFormat("login success with user: {0}", DMUser.CurrentUser().username);
//             Debug.LogFormat("session token: {0}", XSession.Current().Token());

//             // 4. Logout
//             LogoutResult logoutResult = null;
//             yield return XCore.S(DMUser.Logout((r) => { logoutResult = r; }));
//             if (logoutResult.response == false) {
//                 Debug.LogErrorFormat("logout failed: {0}", logoutResult.error);
//                 yield break;
//             }
//             Debug.LogFormat("logout success");

//             // 5. Test Session perishes after logged out
//             if (XSession.Current() != null) {
//                 Debug.LogErrorFormat("session is not null after logged out... [failed]");
//                 yield break;
//             }

//             // 6. Test SyncUserData, after logged out, is not allowed
//             syncUserSuccess = false;
//             Debug.Log("sync user data");
//             yield return XCore.S(me.SyncUserData((r) => { syncUserSuccess = r; }));
//             if (syncUserSuccess) {
//                 Debug.LogError("After logout SyncUserData must fail!!");
//                 yield break;
//             }

//             // 7. Done - all cases success
//             Debug.Log("Test... success");
//         }

//     }
// }