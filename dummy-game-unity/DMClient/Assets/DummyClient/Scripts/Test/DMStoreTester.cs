﻿// using System.Collections;
// using System.Collections.Generic;
// using UnityEngine;
// using XSystem;

// namespace Dummy.Client.Test {

//     public class DMStoreTester : MonoBehaviour {

//         void Awake() {
//             Application.runInBackground = true;
//             XConfiguration.Initialize();
//             XUnityDispatcher.Initialize();
//         }

//         IEnumerator Start() {
//             yield return StartCoroutine(Test_NormalCase());
//         }

//         IEnumerator Test_NormalCase() {
//             string username = System.Guid.NewGuid().ToString().Split('-')[0];
//             string password = "my-password-01";

//             // 1. Sign up
//             SignUpResult<DMUser> signUpResult = null;
//             Debug.LogFormat("begin signup with username:{0} password:{1}", username, password);
//             yield return XCore.S(DMUser.SignUp<DMUser>(username, password,
//                 (r) => { signUpResult = r; }
//             ));
//             if (signUpResult.response == false) {
//                 Debug.LogErrorFormat("signup failed: {0}", signUpResult.error);
//                 yield break;
//             }
//             Debug.Log("signup success");

//             // 2. Login
//             LoginResult<DMUser> loginResult = null;
//             Debug.LogFormat("begin logging in with username:{0} password:{1}", username, password);
//             yield return XCore.S(DMUser.Login<DMUser>(username, password,
//                 (r) => { loginResult = r; }
//             ));
//             if (loginResult.response == false) {
//                 Debug.LogErrorFormat("login failed: {0}", loginResult.error);
//                 yield break;
//             }

//             DMUser me = DMUser.CurrentUser() as DMUser;


//             // 3. Earn chip 
//             {
//                 int beforeChip = me.chip;
//                 int amount = 100;
//                 DMStore.PayOrEarnChipResult result = null;
//                 yield return DMStore.EarnChip(amount, (r) => { result = r; });

//                 if (beforeChip + amount != result.chip) {
//                     Debug.LogErrorFormat("earn chip failed, beforeChip+amount={0} result.chip={1}", beforeChip + amount, result.chip);
//                     yield break;
//                 }

//                 bool syncUSerDataSuccess = false;
//                 yield return me.SyncUserData((r) => { syncUSerDataSuccess = r; });
//                 if (!syncUSerDataSuccess) {
//                     yield break;
//                 }
//                 if (me.chip != result.chip) {
//                     Debug.LogErrorFormat("earn chip failed, user.chip={0} result.chip={1}", me.chip, result.chip);
//                     yield break;
//                 }
//             }

//             // 4. Pay chip 
//             {
//                 int beforeChip = me.chip;
//                 int amount = 50;
//                 DMStore.PayOrEarnChipResult result = null;
//                 yield return DMStore.PayChip(amount, (r) => { result = r; });

//                 if (beforeChip - amount != result.chip) {
//                     Debug.LogErrorFormat("pay chip failed, beforeChip-amount={0} result.chip={1}", beforeChip - amount, result.chip);
//                     yield break;
//                 }

//                 bool syncUSerDataSuccess = false;
//                 yield return me.SyncUserData((r) => { syncUSerDataSuccess = r; });
//                 if (!syncUSerDataSuccess) {
//                     yield break;
//                 }
//                 if (me.chip != result.chip) {
//                     Debug.LogErrorFormat("pay chip failed, user.chip={0} result.chip={1}", me.chip, result.chip);
//                     yield break;
//                 }
//             }
            
//             // 5. Earn gold 
//             {
//                 int beforeGold = me.gold;
//                 int amount = 100;
//                 DMStore.PayOrEarnGoldResult result = null;
//                 yield return DMStore.EarnGold(amount, (r) => { result = r; });

//                 if (beforeGold + amount != result.gold) {
//                     Debug.LogErrorFormat("earn gold failed, beforeGold+amount={0} result.gold={1}", beforeGold + amount, result.gold);
//                     yield break;
//                 }

//                 bool syncUSerDataSuccess = false;
//                 yield return me.SyncUserData((r) => { syncUSerDataSuccess = r; });
//                 if (!syncUSerDataSuccess) {
//                     yield break;
//                 }
//                 if (me.gold != result.gold) {
//                     Debug.LogErrorFormat("earn gold failed, user.gold={0} result.gold={1}", me.gold, result.gold);
//                     yield break;
//                 }
//             }

//             // 6. Pay chip 
//             {
//                 int beforeGold = me.gold;
//                 int amount = 50;
//                 DMStore.PayOrEarnGoldResult result = null;
//                 yield return DMStore.PayGold(amount, (r) => { result = r; });

//                 if (beforeGold - amount != result.gold) {
//                     Debug.LogErrorFormat("pay gold failed, beforeGold-amount={0} result.gold={1}", beforeGold - amount, result.gold);
//                     yield break;
//                 }

//                 bool syncUSerDataSuccess = false;
//                 yield return me.SyncUserData((r) => { syncUSerDataSuccess = r; });
//                 if (!syncUSerDataSuccess) {
//                     yield break;
//                 }
//                 if (me.chip != result.gold) {
//                     Debug.LogErrorFormat("pay gold failed, user.gold={0} result.gold={1}", me.gold, result.gold);
//                     yield break;
//                 }
//             }


//             Debug.Log("Test... success");
//         }
//     }

// }