﻿// using Facebook.Unity;
// using System.Collections;
// using System.Collections.Generic;
// using UnityEngine;
// using UnityEngine.Purchasing;
// using UnityUI = UnityEngine.UI;
// using Dummy.Client;
// using System;
// using XSystem;

// public class WebServiceTester : MonoBehaviour {

//     public class Session {
//         private static Session mCurrentSession;
//         public static Session Current() {
//             return mCurrentSession;
//         }

//         public static void NewSession(string token) {
//             mCurrentSession = new Session(token);
//         }

//         public static void Terminate() {
//             mCurrentSession = null;
//         }

//         private string mToken;
//         public string Token() {
//             return mToken;
//         }

//         private Session(string token) {
//             mToken = token;
//         }
//     }

//     [System.Serializable]
//     public class User {
//         public string name;
//         public string username;
//         public string email;
//         public byte userType;
//         public string gender;
//         public int level;
//         public int gold;
//         public int chip;
//         public FacebookIdentity fbIdentity;
//     }

//     [System.Serializable]
//     public class FacebookIdentity {
//         public string fbID;
//         public string name;
//         public string email;
//         public string accessToken;
//     }

//     [System.Serializable]
//     public class LoginResponse {
//         public bool response;
//         public string error;
//         public string token;
//         public User user;
//     }

//     [System.Serializable]
//     public class GetCurrentUserResponse {
//         public bool response;
//         public User user;
//     }

//     [System.Serializable]
//     public class CheckSessionResponse {
//         public string error;
//         public bool valid;
//     }

//     [System.Serializable]
//     public class RecordTransactionResponse {
//         public bool response;
//         public string error;
//         public string transactionID;
//     }

//     string kAPIHost = "http://192.168.1.103:8080/api/v1";
//     static string currentFBAccessToken = "";
//     Dummy.Client.IAPPurchaser mIAPPurchaser;

//     public UnityUI.InputField mInputIPAddress;
//     public UnityUI.Button mButtonStartTesting;

//     void Awake() {
//         Application.runInBackground = true;

//         GameObject newGO = new GameObject("IAPPurchaser");
//         mIAPPurchaser = newGO.AddComponent<Dummy.Client.IAPPurchaser>();
//         mInputIPAddress.text = "192.168.1.103";
//         mButtonStartTesting.onClick.AddListener(() => {
//             kAPIHost = string.Format("http://{0}:8080/api/v1", mInputIPAddress.text);
//             StartCoroutine(StartTesting());
//         });
//     }

//     IEnumerator StartTesting() {
//         //yield return StartCoroutine(TestAuthenticationFlow());
//         //yield return StartCoroutine(TestRegisterWithFacebookFlow());
//         yield return StartCoroutine(TestLoginWithFacebookFlow());
//         //yield return StartCoroutine(TestGoldEarningAndPayment());
//         //yield return StartCoroutine(TestChipEarningAndPayment());
//         yield return StartCoroutine(TestPurchasing());
//     }
    
//     /// <summary>
//     /// Testing Register -> Login -> CheckSessionValid -> Logout
//     /// </summary>
//     /// <returns></returns>
//     IEnumerator TestAuthenticationFlow() {
//         string username = System.Guid.NewGuid().ToString().Split('-')[0];
//         string password = System.Guid.NewGuid().ToString().Split('-')[0];
//         string sessionToken = "";

//         // register
//         Debug.LogFormat("Begin register...");
//         yield return StartCoroutine(Register(username, password));
        
//         // login
//         Debug.LogFormat("Begin login...");
//         bool loginSuccess = false;
//         yield return StartCoroutine(Login(username, password, (response) => {
//             if (response.response == true) {
//                 loginSuccess = true;
//                 sessionToken = response.token;
//                 Debug.LogFormat("Token: {0}", response.token);
//             }
//             else {
//                 Debug.LogErrorFormat("Login failed due to error: {0}", response.error);
//             }
//         }));
//         if (!loginSuccess) {
//             yield break;
//         }

//         // check session
//         Debug.LogFormat("Begin check session");
//         yield return StartCoroutine(CheckSession((response) => {
//             if (string.IsNullOrEmpty(response.error)) {
//                 Debug.LogFormat("Token valid: {0}", response.valid);
//             }
//             else {
//                 Debug.LogFormat("Error: {0}", response.error);
//             }
//         }));

//         // logout
//         Debug.LogFormat("Begin logout...");
//         yield return StartCoroutine(Logout());
//     }

//     IEnumerator TestLoginWithFacebookFlow() {
//         if (string.IsNullOrEmpty(currentFBAccessToken)) {
//             var fbLogin = (new GameObject()).AddComponent<Dummy.Test.FBLoginTester>();
//             while (!Dummy.Test.FBLoginTester.WasInitialized()) {
//                 yield return null;
//             }

//             bool waitForLoggingIn = true;
//             ILoginResult loginResult = null;
//             fbLogin.Login((result) => {
//                 loginResult = result;
//                 waitForLoggingIn = false;
//             });
//             while (waitForLoggingIn) {
//                 yield return null;
//             }
//             Destroy(fbLogin.gameObject);

//             if (loginResult.Cancelled || !string.IsNullOrEmpty(loginResult.Error)) {
//                 Debug.LogErrorFormat("login with facebook failed with reason {0}", loginResult.Error);
//                 yield break;
//             }

//             if (loginResult == null) {
//                 Debug.LogError("loginresult = null");
//             }
//             else if (loginResult.AccessToken == null) {
//                 Debug.LogError("accesstoken = null");
//             }
//             currentFBAccessToken = loginResult.AccessToken.TokenString;
//         }

//         Debug.LogFormat("login facebook flow - get access token: {0}", currentFBAccessToken);

//         WWWForm form = new WWWForm();
//         form.AddField("access_token", currentFBAccessToken);

//         WWW www = new WWW(kAPIHost + "/auth/loginWithFB", form);
//         yield return www;

//         if (www.error != null) {
//             Debug.LogErrorFormat("Error: {0}", www.error);
//         }
//         else {
//             Debug.LogFormat("RawResult: {0}", www.text);
//             var loginResponse = JsonUtility.FromJson<LoginResponse>(www.text);
//             Session.NewSession(loginResponse.token);
//             Debug.LogFormat("Loggedin success with user: {0}", loginResponse.user.username);
//         }
//     }

//     IEnumerator TestRegisterWithFacebookFlow() {
//         if (string.IsNullOrEmpty(currentFBAccessToken)) {
//             var fbLogin = (new GameObject()).AddComponent<Dummy.Test.FBLoginTester>();
//             while (!Dummy.Test.FBLoginTester.WasInitialized()) {
//                 yield return null;
//             }

//             bool waitForLoggingIn = true;
//             ILoginResult loginResult = null;
//             fbLogin.Login((result) => {
//                 if (!(result.Cancelled && string.IsNullOrEmpty(result.Error))) {
//                     loginResult = result;
//                 }
//                 waitForLoggingIn = false;
//             });
//             while (waitForLoggingIn) {
//                 yield return null;
//             }
//             Destroy(fbLogin.gameObject);
//             if (loginResult == null) {
//                 Debug.LogErrorFormat("log in facebook failed... terminated!!");
//                 yield break;
//             }

//             currentFBAccessToken = loginResult.AccessToken.TokenString;
//         }

//         string username = System.Guid.NewGuid().ToString().Split('-')[0];
//         string password = "password";
//         string sessionToken = "";
//         yield return StartCoroutine(Register(username, password));
//         Debug.LogFormat("registered new user: {0}", username);
//         yield return StartCoroutine(Login(username, password, (response) => {
//             if (response.response) {
//                 sessionToken = response.token;
//             }
//             else {
//                 Debug.LogErrorFormat("register failed... {0}", response.error);
//             }
//         }));
//         Session.NewSession(sessionToken);

//         if (string.IsNullOrEmpty(sessionToken)) {
//             Debug.LogErrorFormat("log in failed... terminated!!");
//             yield break;
//         }
//         Debug.LogFormat("logged in with user: {0}", username);

//         var form = new WWWForm();
//         form.AddField("access_token", currentFBAccessToken);
//         form.AddField("session_token", sessionToken);
//         WWW www = new WWW(kAPIHost + "/auth/linkFacebook", form);
//         yield return www;

//         if (!string.IsNullOrEmpty(www.error)) {
//             Debug.LogErrorFormat("Error: {0}", www.error);
//         }
//         else {
//             Debug.LogFormat("Result: {0}", www.text);
//         }
//     }

//     public IEnumerator Register(string username, string password) {
//         Debug.LogFormat("Registering with username: {0} and password: {1}", username, password);

//         WWWForm form = new WWWForm();
//         form.AddField("username", username);
//         form.AddField("password", password);
//         WWW www = new WWW(kAPIHost + "/auth/register", form);
//         yield return www;

//         if (www.error != null) {
//             Debug.LogErrorFormat("Error: {0}", www.error);
//         }
//         else {
//             Debug.LogFormat("Result: {0}", www.text);
//         }
//     }

//     public IEnumerator Login(string username, string password, System.Action<LoginResponse> callback) {
//         WWWForm form = new WWWForm();
//         form.AddField("username", username);
//         form.AddField("password", password);

//         WWW www = new WWW(kAPIHost + "/auth/login", form);
//         yield return www;

//         if (www.error != null) {
//             Debug.LogFormat("Error: {0}", www.error);
//             if (callback != null) {
//                 callback(new LoginResponse() {
//                     error = www.error,
//                     response = false,
//                     token = "",
//                     user = null
//                 });
//             }
//         }
//         else {
//             Debug.LogFormat("Result: {0}", www.text);
//             if (callback != null) {
//                 callback(JsonUtility.FromJson<LoginResponse>(www.text));
//             }
//         }
//     }

//     public IEnumerator Logout() {
//         Dictionary<string, string> headers = new Dictionary<string, string>();
//         headers["session_token"] = Session.Current().Token();
//         WWW www = new WWW(kAPIHost + "/auth/logout", new byte[] { 0 }, headers);
//         yield return www;

//         if (!string.IsNullOrEmpty(www.error)) {
//             Debug.LogErrorFormat("Logout error: {0}", www.error);
//             yield break;
//         }

//         Session.Terminate();
//     }

//     public IEnumerator CheckSession(System.Action<CheckSessionResponse> callback) {
//         Dictionary<string, string> headers = new Dictionary<string, string>();
//         headers["session_token"] = Session.Current().Token();
//         WWW www = new WWW(kAPIHost + "/auth/session", null, headers);

//         yield return www;

//         if (www.error != null) {
//             Debug.LogFormat("Error: {0}", www.error);
//             if (callback != null) {
//                 callback(new CheckSessionResponse() {
//                     error = www.error
//                 });
//             }
//         }
//         else {
//             Debug.LogFormat("Result: {0}", www.text);
//             if (callback != null) {
//                 callback(JsonUtility.FromJson<CheckSessionResponse>(www.text));
//             }
//         }
//     }
    
//     public IEnumerator GetcurrentUser(System.Action<User> callback) {
//         if (Session.Current() == null) {
//             Debug.LogFormat("Session invalid, please login before earn or pay gold");
//             yield break;
//         }

//         Dictionary<string, string> headers = new Dictionary<string, string>();
//         headers["session_token"] = Session.Current().Token();
//         WWW www = new WWW(kAPIHost + "/auth/me", null, headers);
//         yield return www;

//         if (!string.IsNullOrEmpty(www.error)) {
//             Debug.LogFormat("GetCurrentUser failed... {0}", www.error);
//             if (callback != null) {
//                 callback(null);
//             }
//             yield break;
//         }

//         if (callback != null) {
//             var response = JsonUtility.FromJson<GetCurrentUserResponse>(www.text);
//             callback(response.user);
//         }
//     }

//     public IEnumerator TestGoldEarningAndPayment() {
//         if (Session.Current() == null) {
//             Debug.LogFormat("Session invalid, please login before earn or pay gold");
//             yield break;
//         }

//         Dictionary<string, string> headers = new Dictionary<string, string>();
//         headers["session_token"] = Session.Current().Token();

//         User user1 = null;
//         yield return StartCoroutine(GetcurrentUser((u) => {
//             user1 = u;
//         }));
//         if (user1 == null) {
//             Debug.LogFormat("Cannot get current user");
//             yield break;
//         }

//         int goldAmount = 12;

//         // Earning Gold
//         Debug.LogFormat("earning gold {0}", goldAmount);
//         WWWForm form = new WWWForm();
//         form.AddField("session_token", Session.Current().Token());
//         form.AddField("amount", goldAmount);
//         WWW www = new WWW(kAPIHost + "/store/earngold", form);
//         yield return www;

//         if (!string.IsNullOrEmpty(www.error)) {
//             Debug.LogErrorFormat("Earning gold failed... {0}", www.error);
//             yield break;
//         }

//         User user2 = null;
//         yield return StartCoroutine(GetcurrentUser((u) => {
//             user2 = u;
//         }));
//         if (user2 == null) {
//             Debug.LogFormat("Cannot get current user");
//             yield break;
//         }

//         if (user1.gold + goldAmount != user2.gold) {
//             Debug.LogErrorFormat("Earning gold incorrectly expected: {0} actual: {1}", user1.gold + goldAmount, user2.gold);
//             yield break;
//         }

//         Debug.LogFormat("Earning gold: {0} total: {1} success!", goldAmount, user2.gold);

//         // Paying gold

//         Debug.LogFormat("paying gold {0}", goldAmount);
//         www = new WWW(kAPIHost + "/store/paygold", form);
//         yield return www;

//         if (!string.IsNullOrEmpty(www.error)) {
//             Debug.LogErrorFormat("Paying gold failed... {0}", www.error);
//             yield break;
//         }

//         User user3 = null;
//         yield return StartCoroutine(GetcurrentUser((u) => {
//             user3 = u;
//         }));
//         if (user3 == null) {
//             Debug.LogFormat("Cannot get current user");
//             yield break;
//         }

//         if (user2.gold - goldAmount != user3.gold) {
//             Debug.LogErrorFormat("Paying gold incorrectly expected: {0} actual: {1}", user2.gold - goldAmount, user3.gold);
//             yield break;
//         }

//         Debug.LogFormat("Paying gold: {0} total: {1} success!", goldAmount, user3.gold);
//     }

//     public IEnumerator TestChipEarningAndPayment() {
//         if (Session.Current() == null) {
//             Debug.LogFormat("Session invalid, please login before earn or pay chip");
//             yield break;
//         }

//         Dictionary<string, string> headers = new Dictionary<string, string>();
//         headers["session_token"] = Session.Current().Token();

//         User user1 = null;
//         yield return StartCoroutine(GetcurrentUser((u) => {
//             user1 = u;
//         }));
//         if (user1 == null) {
//             Debug.LogFormat("Cannot get current user");
//             yield break;
//         }

//         int chipAmount = 12;

//         // Earning Chip
//         Debug.LogFormat("earning chip {0}", chipAmount);
//         WWWForm form = new WWWForm();
//         form.AddField("session_token", Session.Current().Token());
//         form.AddField("amount", chipAmount);
//         WWW www = new WWW(kAPIHost + "/store/earnchip", form);
//         yield return www;

//         if (!string.IsNullOrEmpty(www.error)) {
//             Debug.LogErrorFormat("Earning chip failed... {0}", www.error);
//             yield break;
//         }

//         User user2 = null;
//         yield return StartCoroutine(GetcurrentUser((u) => {
//             user2 = u;
//         }));
//         if (user2 == null) {
//             Debug.LogFormat("Cannot get current user");
//             yield break;
//         }

//         if (user1.chip + chipAmount != user2.chip) {
//             Debug.LogErrorFormat("Earning chip incorrectly expected: {0} actual: {1}", user1.chip + chipAmount, user2.chip);
//             yield break;
//         }

//         Debug.LogFormat("Earning chip: {0} total: {1} success!", chipAmount, user2.chip);

//         // Paying gold

//         Debug.LogFormat("paying chip {0}", chipAmount);
//         www = new WWW(kAPIHost + "/store/paychip", form);
//         yield return www;

//         if (!string.IsNullOrEmpty(www.error)) {
//             Debug.LogErrorFormat("Paying chip failed... {0}", www.error);
//             yield break;
//         }

//         User user3 = null;
//         yield return StartCoroutine(GetcurrentUser((u) => {
//             user3 = u;
//         }));
//         if (user3 == null) {
//             Debug.LogFormat("Cannot get current user");
//             yield break;
//         }

//         if (user2.chip - chipAmount != user3.chip) {
//             Debug.LogErrorFormat("Paying gold incorrectly expected: {0} actual: {1}", user2.chip - chipAmount, user3.chip);
//             yield break;
//         }

//         Debug.LogFormat("Paying chip: {0} total: {1} success!", chipAmount, user3.chip);
//     }

//     IEnumerator TestPurchasing() {
//         Dictionary<string, string> headers = new Dictionary<string, string>();
//         headers["session_token"] = Session.Current().Token();

//         bool isWaitingForPurchasing = true;
//         bool purchaseSuccess = false;
//         string purchaseFailedReason = "";
//         string productID = IAPPurchaser.kAPIId_GoldPack01;
//         string productReceipt = "";
//         string currencyCode = "";
//         decimal price = 0;
//         int goldAmount = 0;

//         bool bypassRealIAP = true;

//         if (bypassRealIAP) {
//             purchaseSuccess = true;
//             productReceipt = Guid.NewGuid().ToString();
//             currencyCode = "BHT";
//             price = 10;
//             goldAmount = 100;
//         }
//         else {
//             mIAPPurchaser.BuyConsumable(consumableProductID: productID,
//                 onSuccess: (purchase) => {
//                     purchaseSuccess = true;
//                     var p = purchase.purchasedProduct;
//                     productReceipt = p.receipt;
//                     currencyCode = p.metadata.isoCurrencyCode;
//                     price = p.metadata.localizedPrice;

//                     if (String.Equals(purchase.purchasedProduct.definition.id, IAPPurchaser.kAPIId_GoldPack01, StringComparison.Ordinal)) {
//                         goldAmount = 100;
//                     }
//                     else if (String.Equals(purchase.purchasedProduct.definition.id, IAPPurchaser.kAPIId_GoldPack02, StringComparison.Ordinal)) {
//                         goldAmount = 500;
//                     }
//                     else if (String.Equals(purchase.purchasedProduct.definition.id, IAPPurchaser.kAPIId_GoldPack03, StringComparison.Ordinal)) {
//                         goldAmount = 1000;
//                     }

//                     return PurchaseProcessingResult.Complete;
//                 },
//                 onFailed: (product, reasons) => {
//                     purchaseSuccess = false;
//                     purchaseFailedReason = reasons.ToString();
//                 });
//             while (isWaitingForPurchasing) {
//                 yield return null;
//             }
//         }

//         if (!purchaseSuccess) {
//             Debug.LogErrorFormat("Purchasing failed with reason: {0}", purchaseFailedReason);
//             yield break;
//         }

//         User currentUser = null;
//         yield return StartCoroutine(GetcurrentUser((u) => {
//             currentUser = u;
//         }));
//         if (currentUser == null) {
//             Debug.LogFormat("Cannot get current user");
//             yield break;
//         }

//         // Earning gold 
//         {
//             Debug.LogFormat("earning gold {0}", goldAmount);
//             WWWForm form = new WWWForm();
//             form.AddField("session_token", Session.Current().Token());
//             form.AddField("amount", goldAmount);
//             WWW www = new WWW(kAPIHost + "/store/earngold", form);
//             yield return www;
//             if (!string.IsNullOrEmpty(www.error)) {
//                 Debug.LogErrorFormat("Earning gold failed... {0}", www.error);
//                 yield break;
//             }
//         }

//         // Record IAP transaction
//         {
//             WWWForm form = new WWWForm();
//             form.AddField("platform", "android");
//             form.AddField("receiptNumber", productReceipt);
//             form.AddField("productID", productID);
//             form.AddField("paymentCurrency", currencyCode);
//             form.AddField("paymentAmount", price.ToString());
//             form.AddField("session_token", Session.Current().Token());
//             WWW www = new WWW(kAPIHost + "/store/iapTransaction", form);
//             yield return www;
//             if (!string.IsNullOrEmpty(www.error)) {
//                 Debug.LogFormat("Record IAP transaction failed with reason: {0}", www.error);
//                 yield break;
//             }
            
//             var response = JsonUtility.FromJson<RecordTransactionResponse>(www.text);
//             if (response.response) {
//                 Debug.LogFormat("Record IAP Transaction success: transaction ID: {0}", response.transactionID);
//             }
//             else {
//                 Debug.LogErrorFormat("Record IAP Transaction failed: {0}", response.error);
//             }
//         }
//     }
// }
