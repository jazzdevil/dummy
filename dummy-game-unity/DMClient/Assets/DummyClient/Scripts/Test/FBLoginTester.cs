﻿// using System.Collections;
// using System.Collections.Generic;
// using UnityEngine;
// using FBUnity = Facebook.Unity;

// namespace Dummy.Test {

//     public class FBLoginTester : MonoBehaviour {
//         static bool mWasInitialized = false;
//         public static bool WasInitialized() {
//             return mWasInitialized;
//         }

//         // Use this for initialization
//         void Start() {
//             if (!mWasInitialized) {
//                 FBUnity.FB.Init(this.OnFBInitComplete, this.OnHideUnity);
//             }
//         }
        
//         public void Login(System.Action<FBUnity.ILoginResult> callback) {
//             FBUnity.FB.LogInWithReadPermissions(new string[] {
//                 "public_profile", "email", "user_friends" },
//                 (result) => {
//                     callback(result);
//                 });
//         }

//         void OnFBInitComplete() {
//             Debug.Log("FB Init Success");
//             Debug.LogFormat("IsLoggedIn: {0} IsInitialized: {1}", FBUnity.FB.IsLoggedIn, FBUnity.FB.IsInitialized);
//             if (FBUnity.AccessToken.CurrentAccessToken != null) {
//                 Debug.LogFormat("AccessToken: {0} UserID: {1}", 
//                     FBUnity.AccessToken.CurrentAccessToken.TokenString,
//                     FBUnity.AccessToken.CurrentAccessToken.UserId);
//             }
//             mWasInitialized = true;
//         }

//         void OnHideUnity(bool isGameShown) {
//             Debug.LogFormat("OnHideUnity: {0}", isGameShown);
//         }

//         void LoginResult(FBUnity.IResult result) {
//             if (result == null) {
//                 Debug.LogFormat("Login result: NONE");
//                 return;
//             }
//             if (!string.IsNullOrEmpty(result.Error)) {
//                 Debug.LogFormat("Login Error: {0}", result.Error);
//             }
//             else if (result.Cancelled) {
//                 Debug.LogFormat("Login Cancelled");
//             }
//             else if (!string.IsNullOrEmpty(result.RawResult)) {
//                 Debug.LogFormat("Login success: {0}", result.RawResult);
//             }
//         }
//     }

// }