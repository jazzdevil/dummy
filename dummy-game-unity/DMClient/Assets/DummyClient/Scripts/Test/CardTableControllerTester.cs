﻿using UnityEngine;
using System.Collections;

using Dummy.Client.UI;
using Dummy.DMProtocol;

namespace Dummy.Client.Test {

    public class CardTableControllerTester : MonoBehaviour {

        public CardTableController cardTable;

        // Update is called once per frame
        void Update() {
            if (Input.GetKeyDown(KeyCode.I)) {
                cardTable.discardCardTable.AddCard(new DMCard() {
                    rank = (byte)Random.Range(2, 15),
                    kind = (DMCard.CardKindEnum)Random.Range(1, 5)
                });
                cardTable.discardCardTable.RearrangeCards();
            }
            if (Input.GetKey(KeyCode.RightShift) || Input.GetKey(KeyCode.LeftShift)) {
                MeldingAreaController meldingArea = null;
                if (Input.GetKeyDown(KeyCode.Alpha1)) meldingArea = cardTable.localPlayerMeldingArea;
                else if (Input.GetKeyDown(KeyCode.Alpha2)) meldingArea = cardTable.remotePlayerMeldingAreas[0];
                else if (Input.GetKeyDown(KeyCode.Alpha3)) meldingArea = cardTable.remotePlayerMeldingAreas[1];
                else if (Input.GetKeyDown(KeyCode.Alpha4)) meldingArea = cardTable.remotePlayerMeldingAreas[2];

                if (meldingArea != null) {
                    DMMeldingCard.MeldingTypeEnum meldingType = DMMeldingCard.MeldingTypeEnum.RunOfSameKind;
                    if (Random.value > 0.5f) meldingType = DMMeldingCard.MeldingTypeEnum.SameRank;
                    meldingArea.Put(CreateRandomMeldingCard(meldingType));
                    meldingArea.Rearrange();
                }
            }
        }

        DMMeldingCard CreateRandomMeldingCard(DMMeldingCard.MeldingTypeEnum meldingType) {
            DMMeldingCard meldingCard = new DMMeldingCard();
            meldingCard.id = (System.UInt16)Random.Range(0, 1000);
            meldingCard.meldingType = meldingType;
            if (meldingType == DMMeldingCard.MeldingTypeEnum.RunOfSameKind) {
                int numberOfCard = Random.Range(3, 6);
                int startRank = Random.Range(DMCard.kMinRank, DMCard.kMaxRank - numberOfCard - 1);
                int kind = Random.Range(1, 5);
                for (int i = 0; i < numberOfCard; i++) {
                    meldingCard.cards.Add(new DMCard() {
                        kind = (DMCard.CardKindEnum)kind,
                        rank = (byte)(startRank + i)
                    });
                }
            }
            else if (meldingType == DMMeldingCard.MeldingTypeEnum.SameRank) {
                int rank = Random.Range(DMCard.kMinRank, DMCard.kMaxRank + 1);
                int kind = Random.Range(1, 3);
                for (; kind <= 4; kind++) {
                    meldingCard.cards.Add(new DMCard() {
                        kind = (DMCard.CardKindEnum)kind,
                        rank = (byte)rank
                    });
                }
            }
            
            return meldingCard;
        }
    }

}