﻿using UnityEngine;
using UnityUI = UnityEngine.UI;
using UnityEngine.SceneManagement;

using System.Collections;
using System.Collections.Generic;
using Dummy.DMProtocol;

namespace Dummy.Client.UI
{

    public class PlayerOnHandCardsControllerBackUp : MonoBehaviour
    {

        public interface PlayerOnHandCardsControllerRequesterDelegate
        {
            // Draw action
            void PlayerOnHandCardsControllerRequestDrawStockCard();

            // Pooh
            void PlayerOnHandCardsControllerRequestPickupDiscardCardAndMeld(DMCard card, DMMeldingCard meldingCard);

            // Play action
            void PlayerOnHandCardsControllerRequestMeldCard(DMCard[] cards);
            void PlayerOnHandCardsControllerRequestDiscard(DMCard card);
            void PlayerOnHandCardsControllerRequestLayoff(DMCard card, DMMeldingCard targetMeldingCard);
            void leaveAndRemoveRoom(string roomID);
            void PlayerOnHandCardsControllerRequestKnockCard(DMCard card);
            void PlayerOnHandCardsControllerPassLastTurn();
        }

        enum ActionStateEnum
        {
            Draw = 0,
            Play,
            Knock
        }

        enum ActionDrawStateEnum
        {
            DrawStockCard = 0,
            PickupDiscardCardAndMeld
        }

        enum ActionPlayStateEnum
        {
            Normal = 0,
            Meld,
            Discard,
            Layoff
        }

        [Header("Card table")]
        public CardTableController cardTable;

        [Header("UI Component")]
        public RectTransform cardContainer;
        public GameObject cardPrefab;
        public GameObject controlPanel;

        [Header("Action-State-Draw-Buttons")]
        public GameObject actionStateDrawButtonsGroup;
        public UnityUI.Button bttnDrawStockCard;
        public UnityUI.Button bttnDrawDiscardCardAndMeld;
        public UnityUI.Button bttnPassStateDraw;

        [Header("Action-State-Play-Buttons")]
        public GameObject actionStatePlayButtonsGroup;
        public UnityUI.Button bttnDiscard;
        public UnityUI.Button bttnMeld;
        public UnityUI.Button bttnLayoff;
        public UnityUI.Button bttnPassStatePlay;

        [Header("Action-State-Knock-Buttons")]
        public GameObject actionStateKnockButtonsGroup;
        public UnityUI.Button bttnKnock;

        [Header("Confirm-Buttons")]
        public GameObject confirmButtonsGroup;
        public UnityUI.Button bttnOk;
        public UnityUI.Button bttnCancel;

        [Header("Reset-Buttons")]

        public GameObject resetButtonsGroup;
        public UnityUI.Button bttnLeave;

        [Header("Configuration")]
        public float minHSpace;
        public float maxHSpace;

        public PlayerOnHandCardsControllerRequesterDelegate delegateObject;

        List<DMCard> mCards;
        List<DMCardController> mCardControllers;
        List<DMCardController> mSelectedCardControllers;

        float mHSpace;
        ActionStateEnum mActionState;
        ActionDrawStateEnum mActionDrawState;
        ActionPlayStateEnum mActionPlayState;

        void Awake()
        {
            mCards = new List<DMCard>();
            mCardControllers = new List<DMCardController>();
            mSelectedCardControllers = new List<DMCardController>();

            mHSpace = 0;
            mActionState = ActionStateEnum.Draw;
            mActionDrawState = ActionDrawStateEnum.DrawStockCard;
            mActionPlayState = ActionPlayStateEnum.Normal;

            SetupControlPanelUI();
        }

        void SetupControlPanelUI()
        {


            resetButtonsGroup.SetActive(true);
            //bttnLeave.
            bttnLeave.onClick.AddListener(() =>
            {
                delegateObject.leaveAndRemoveRoom(DMClient.GetInstance().mockupRoomId);
                SceneManager.LoadScene("Login");

            });

            #region Action Draw State

            // Draw stock card
            bttnDrawStockCard.onClick.AddListener(() =>
            {
                mActionDrawState = ActionDrawStateEnum.DrawStockCard;
                delegateObject.PlayerOnHandCardsControllerRequestDrawStockCard();
            });

            bttnPassStateDraw.onClick.AddListener(() =>
            {
                delegateObject.PlayerOnHandCardsControllerPassLastTurn();
            });

            // Pickup discarded card and meld
            bttnDrawDiscardCardAndMeld.onClick.AddListener(() =>
            {
                CardPileController discardCardPile = cardTable.discardCardTable;

                // Get all possible melding cards
                DMCard[] discardedCards = discardCardPile.GetCopyOfAllCards();
                List<DMCard> allCards = new List<DMCard>();
                allCards.AddRange(discardedCards);
                allCards.AddRange(mCards);
                var allPossibleMeldingCards = DMMeldingCard.FindAllPossibleMeldingCard(allCards.ToArray());

                // Find out the combination of melding cards contain both card from Discard pile and OnHand card
                DMMeldingCard validMeldingCard = null;
                for (int idx = 0; idx < allPossibleMeldingCards.Count; idx++)
                {
                    bool hasCardFromDiscardPile = false;
                    bool hasCardFromOnHandCards = false;
                    DMMeldingCard meldingCard = allPossibleMeldingCards[idx];
                    for (int i = 0; i < meldingCard.cards.Count; i++)
                    {
                        DMCard c = meldingCard.cards[i];
                        if (!hasCardFromDiscardPile)
                        {
                            // Find card in Discarded card pile
                            for (int j = 0; j < discardedCards.Length; j++)
                            {
                                if (c.IsIdentical(discardedCards[j]))
                                {
                                    hasCardFromDiscardPile = true;
                                    break;
                                }
                            }
                        }
                        if (!hasCardFromOnHandCards)
                        {
                            // Find card in OnHand cards
                            for (int j = 0; j < mCards.Count; j++)
                            {
                                if (c.IsIdentical(mCards[j]))
                                {
                                    hasCardFromOnHandCards = true;
                                    break;
                                }
                            }
                        }
                        if (hasCardFromDiscardPile && hasCardFromOnHandCards)
                        {
                            break;
                        }
                    }

                    if (hasCardFromDiscardPile && hasCardFromOnHandCards)
                    {
                        validMeldingCard = meldingCard;
                        break;
                    }
                }

                if (validMeldingCard == null)
                {
                    Debug.Log("[PlayerOnHandCardController] Cannot find any possible melding card");
                    return;
                }

                mActionDrawState = ActionDrawStateEnum.PickupDiscardCardAndMeld;
                confirmButtonsGroup.SetActive(true);
                actionStateDrawButtonsGroup.SetActive(false);

                discardCardPile.isInteractable = true;
                discardCardPile.isSelectable = true;
                discardCardPile.DeselectAllCards(false);

                foreach (var card in validMeldingCard.cards)
                {
                    for (int i = 0; i < discardedCards.Length; i++)
                    {
                        if (card.IsIdentical(discardedCards[i]))
                        {
                            discardCardPile.SelectCard(card, notifyEvent: false);
                            break;
                        }
                    }
                    for (int i = 0; i < mCardControllers.Count; i++)
                    {
                        if (card.IsIdentical(mCardControllers[i].GetCard()))
                        {
                            SelectCard(mCardControllers[i]);
                            break;
                        }
                    }
                }

            });

            #endregion

            #region Action Play State

            // Discard
            bttnDiscard.onClick.AddListener(() =>
            {
                mActionPlayState = ActionPlayStateEnum.Discard;
                confirmButtonsGroup.SetActive(true);
                actionStatePlayButtonsGroup.SetActive(false);
            });

            bttnPassStatePlay.onClick.AddListener(() =>
            {
                delegateObject.PlayerOnHandCardsControllerPassLastTurn();
            });

            // Melding
            bttnMeld.onClick.AddListener(() =>
            {
                // Pooh | can't meld when you draw card
                if (mActionDrawState == ActionDrawStateEnum.DrawStockCard)
                {
                    Debug.LogFormat("[PlayerController] cannot meld when draw card");
                    return;
                }

                mActionPlayState = ActionPlayStateEnum.Meld;
                confirmButtonsGroup.SetActive(true);
                actionStatePlayButtonsGroup.SetActive(false);

                DeselectAllCards();
                List<DMMeldingCard> meldingCards = DMMeldingCard.FindAllPossibleMeldingCard(mCards.ToArray());
                if (meldingCards.Count == 0) return;

                DMMeldingCard firstMeldingCard = meldingCards[0];
                for (int i = 0; i < firstMeldingCard.cards.Count; i++)
                {
                    foreach (var cardController in mCardControllers)
                    {
                        if (cardController.GetCard().IsIdentical(firstMeldingCard.cards[i]))
                        {
                            SelectCard(cardController);
                            break;
                        }
                    }
                }
            });

            // Layoff
            bttnLayoff.onClick.AddListener(() =>
            {
                mActionPlayState = ActionPlayStateEnum.Layoff;
                confirmButtonsGroup.SetActive(true);
                actionStatePlayButtonsGroup.SetActive(false);

                // Find and select card that can be layed off
                DeselectAllCards();
                for (int i = 0; i < mCards.Count; i++)
                {
                    DMCard card = mCards[i];
                    if (this.cardTable.FindMeldingCardForLayOffCard(card) != null)
                    {
                        DMCardController cardController = null;
                        for (int j = 0; j < mCardControllers.Count; j++)
                        {
                            if (mCardControllers[j].GetCard().IsIdentical(card))
                            {
                                cardController = mCardControllers[j];
                                break;
                            }
                        }
                        if (cardController != null)
                        {
                            SelectCard(cardController);
                            break;
                        }
                    }
                }
            });

            #endregion

            #region Action Knock State
            bttnKnock.onClick.AddListener(() =>
            {
                delegateObject.PlayerOnHandCardsControllerRequestKnockCard(mCards[0]);
                actionStateKnockButtonsGroup.SetActive(false);
            });

            #endregion Action Knock State

            // Confirm - Ok
            bttnOk.onClick.AddListener(() =>
            {
                Debug.LogFormat("{0}   {1}", mActionState, mActionDrawState);
                if (mActionState == ActionStateEnum.Draw)
                {
                    if (mActionDrawState == ActionDrawStateEnum.PickupDiscardCardAndMeld)
                    {
                        // Assemble cards from the selected cards from OnHand and the selected cards from discarded pile
                        CardPileController discardCardPile = cardTable.discardCardTable;
                        List<DMCard> selectedCardsForMelding = new List<DMCard>();
                        selectedCardsForMelding.AddRange(discardCardPile.GetCopyOfSelectedCards());
                        foreach (var cardController in mSelectedCardControllers)
                        {
                            selectedCardsForMelding.Add(cardController.GetCard());
                        }

                        // Try out, the selected card can be melded
                        DMMeldingCard meldingCard = null;
                        try
                        {
                            meldingCard = new DMMeldingCard(selectedCardsForMelding.ToArray());
                        }
                        catch (System.InvalidOperationException)
                        {
                            Debug.LogFormat("Cannot create a melding card with the selected cards");
                        }
                        if (meldingCard == null)
                        {
                            return;
                        }

                        // Find out which discarded card would be picked up
                        bool isPickValid = false;

                        DMCard pickupDiscardedCard = discardCardPile.CardAtIndex(0);
                        for (int i = 0; i < discardCardPile.NumberOfCards(); i++)
                        {
                            DMCard card = discardCardPile.CardAtIndex(i);
                            bool isInMeldingCard = false;
                            foreach (var c in meldingCard.cards)
                            {
                                if (card.IsIdentical(c))
                                {
                                    isPickValid = true;
                                    isInMeldingCard = true;
                                    break;
                                }
                            }
                            if (isInMeldingCard)
                            {
                                pickupDiscardedCard = card;
                                break;
                            }
                        }

                        if (isPickValid)
                        {
                            int indexPickCard = discardCardPile.IndexOfCard(pickupDiscardedCard);
                            DMCard[] discardCards = discardCardPile.GetCopyOfAllCards();
                            int countPickUpDiscardCard = discardCards.Length - indexPickCard;
                            if (meldingCard.cards.Count > (countPickUpDiscardCard + mCards.Count) - 1)
                            {
                                confirmButtonsGroup.SetActive(false);
                                actionStateDrawButtonsGroup.SetActive(true);
                                discardCardPile.DeselectAllCards(false);
                                DeselectAllCards();
                                Debug.LogErrorFormat("[PlayerOnHandCardsController] cannot meld all card");
                                return;
                            }
                        }

                        delegateObject.PlayerOnHandCardsControllerRequestPickupDiscardCardAndMeld(pickupDiscardedCard, meldingCard);

                        confirmButtonsGroup.SetActive(false);
                        //actionStatePlayButtonsGroup.SetActive(true);
                    }
                }
                else if (mActionState == ActionStateEnum.Play)
                {
                    if (mActionPlayState == ActionPlayStateEnum.Discard)
                    {
                        delegateObject.PlayerOnHandCardsControllerRequestDiscard(mSelectedCardControllers[0].GetCard());
                    }
                    else if (mActionPlayState == ActionPlayStateEnum.Meld)
                    {
                        if (GetSelectedCards().Length > mCards.Count - 1)
                        {
                            Debug.LogErrorFormat("[PlayerOnHandCardsController] cannot meld all card");
                        }
                        else
                        {
                            delegateObject.PlayerOnHandCardsControllerRequestMeldCard(GetSelectedCards());
                        }
                    }
                    else if (mActionPlayState == ActionPlayStateEnum.Layoff)
                    {
                        if (GetNumberOfSelectedCards() == 1)
                        {
                            DMCard card = GetSelectedCardAtIndex(0);
                            DMMeldingCard targetMeldingCard = this.cardTable.FindMeldingCardForLayOffCard(card);
                            if (targetMeldingCard == null)
                            {
                                Debug.LogWarning("[PlayerOnHandCardsController] layoff card: Not find MeldingCard");
                            }
                            else
                            {
                                Debug.LogFormat("[PlayerOnHandCardsController] layoff card: {0} to meldingCard id: {1}", card.CardName(), targetMeldingCard.id);
                                delegateObject.PlayerOnHandCardsControllerRequestLayoff(card, targetMeldingCard);
                            }
                        }
                        else
                        {
                            Debug.LogWarning("[PlayerOnHandCardsController] cannot layoff more than 1 card");
                        }
                    }

                    mActionPlayState = ActionPlayStateEnum.Normal;
                    DeselectAllCards();

                    confirmButtonsGroup.SetActive(false);
                    //actionStatePlayButtonsGroup.SetActive(true);
                }
                else if (mActionState == ActionStateEnum.Knock)
                {

                }
            });

            // Confirm - Cancel
            bttnCancel.onClick.AddListener(() =>
            {
                confirmButtonsGroup.SetActive(false);

                if (mActionState == ActionStateEnum.Draw)
                {
                    CardPileController discardCardPile = cardTable.discardCardTable;
                    actionStateDrawButtonsGroup.SetActive(true);
                    if (mActionDrawState == ActionDrawStateEnum.PickupDiscardCardAndMeld)
                    {
                        DeselectAllCards();
                        discardCardPile.DeselectAllCards(notifyEvent: false);
                    }
                }
                else if (mActionState == ActionStateEnum.Play)
                {
                    DeselectAllCards();
                    mActionPlayState = ActionPlayStateEnum.Normal;
                    actionStatePlayButtonsGroup.SetActive(true);
                }
            });
        }

        public void setLastTurn()
        {
            bttnPassStateDraw.gameObject.SetActive(true);
            bttnPassStatePlay.gameObject.SetActive(true);
            bttnDrawStockCard.gameObject.SetActive(false);
            bttnMeld.gameObject.SetActive(false);
        }

        void checkKnock()
        {
            if (mCards.Count == 1)
            {
                mActionState = ActionStateEnum.Knock;
                actionStatePlayButtonsGroup.SetActive(false);
                actionStateKnockButtonsGroup.SetActive(true);
            }
            else
            {
                mActionState = ActionStateEnum.Play;
                actionStatePlayButtonsGroup.SetActive(true);
            }
        }
        bool CanSelectCard()
        {
            return true;
            //if (mActionState == ActionStateEnum.Play && (
            //        mActionPlayState == ActionPlayStateEnum.Discard ||
            //        mActionPlayState == ActionPlayStateEnum.Layoff ||
            //        mActionPlayState == ActionPlayStateEnum.Meld))
            //{
            //    return true;
            //}
            //else if (mActionState == ActionStateEnum.Draw &&
            //  mActionDrawState == ActionDrawStateEnum.PickupDiscardCardAndMeld)
            //{
            //    return true;
            //}
            //return false;
        }

        public void AddCard(DMCard card)
        {
            foreach (var c in mCards)
            {
                if (c.ToUInt16() == card.ToUInt16())
                {
                    Debug.LogWarning("[PlayerOnHandCardsController] trying to add a duplicated card -- ignore");
                    return;
                }
            }

            mCards.Add(card);
            GameObject newCard = Instantiate(cardPrefab);
            RectTransform r = newCard.GetComponent<RectTransform>();
            r.SetParent(cardContainer.GetComponent<RectTransform>());
            r.transform.localScale = new Vector3(0.83f, 0.83f, 0.83f);

            DMCardController cardController = newCard.GetComponent<DMCardController>();
            cardController.SetCard(card);
            mCardControllers.Add(cardController);

            cardController.onClicked += (c) =>
            {
                if (CanSelectCard())
                {
                    if (!mSelectedCardControllers.Contains(c))
                    {
                        SelectCard(c);
                    }
                    else
                    {
                        DeselectCard(c);
                    }
                }
            };
        }

        public void RemoveCard(DMCard card)
        {
            bool found = false;
            foreach (var c in mCards)
            {
                if (c.IsIdentical(card))
                {
                    found = true;
                    break;
                }
            }
            if (!found)
            {
                Debug.LogWarningFormat("[PlayerOnHandCardsController] trying to remove non-existing card {0} - ignore", card.CardName());
                return;
            }

            mCards.Remove(card);
            for (int i = mCardControllers.Count - 1; i >= 0; i--)
            {
                if (mCardControllers[i].GetCard().IsIdentical(card))
                {
                    var c = mCardControllers[i];
                    mCardControllers.Remove(c);
                    if (mSelectedCardControllers.Contains(c))
                    {
                        mSelectedCardControllers.Remove(c);
                    }
                    Destroy(c.gameObject);
                }
            }
        }

        public void RemoveAllCards()
        {
            for (int i = mCards.Count - 1; i >= 0; i--)
            {
                RemoveCard(mCards[i]);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="theirCards"></param>
        /// <returns>True if cards has been changed, otherwise return false. So if this function return true, it might be need to Rearrange</returns>
        public bool SyncCards(DMCard[] theirCards)
        {
            bool cardsHaveBeenChanged = false;

            // If my card does not exist in theirCards then remove it.
            for (int i = mCards.Count - 1; i >= 0; i--)
            {
                bool found = false;
                DMCard myCard = mCards[i];
                for (int j = 0; j < theirCards.Length; j++)
                {
                    DMCard theirCard = theirCards[j];
                    if (myCard.IsIdentical(theirCard))
                    {
                        found = true;
                        break;
                    }
                }
                if (!found)
                {
                    RemoveCard(myCard);
                    cardsHaveBeenChanged = true;
                }
            }

            // If their card is still not in my cards, then add it
            for (int i = 0; i < theirCards.Length; i++)
            {
                bool found = false;
                DMCard theirCard = theirCards[i];
                for (int j = 0; j < mCards.Count; j++)
                {
                    DMCard myCard = mCards[j];
                    if (theirCard.IsIdentical(myCard))
                    {
                        found = true;
                        break;
                    }
                }

                if (!found)
                {
                    AddCard(theirCard);
                    cardsHaveBeenChanged = true;
                }
            }

            return cardsHaveBeenChanged;
        }

#if UNITY_EDITOR
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
                Debug.Log("[Editor-Mode] PlayerOnHandCardsController - Reaarange cards");
                RearrangeCards();
            }
        }
#endif

        public void RearrangeCards()
        {
            float totalWidth = cardContainer.rect.width;
            int n = mCardControllers.Count;
            mHSpace = Mathf.Clamp(totalWidth / n, minHSpace, maxHSpace);

            float localX = 0;
            for (int i = 0; i < mCardControllers.Count; i++)
            {
                RectTransform r = mCardControllers[i].GetComponent<RectTransform>();
                Vector3 p = r.localPosition;
                p.x = localX;
                p.y = 0;
                r.localPosition = p;
                localX += mHSpace;
            }
        }

        public void ShowControlPanel()
        {
            controlPanel.SetActive(true);
            mActionState = ActionStateEnum.Draw;
            actionStateDrawButtonsGroup.SetActive(true);
            actionStatePlayButtonsGroup.SetActive(false);
            confirmButtonsGroup.SetActive(false);
        }

        public void HideControlPanel()
        {
            controlPanel.SetActive(false);
        }

        public int GetNumberOfSelectedCards()
        {
            return mSelectedCardControllers.Count;
        }

        public DMCard GetSelectedCardAtIndex(int index)
        {
            return mSelectedCardControllers[index].GetCard();
        }

        public DMCard[] GetSelectedCards()
        {
            DMCard[] cards = new DMCard[mSelectedCardControllers.Count];
            for (int i = 0; i < mSelectedCardControllers.Count; i++)
            {
                cards[i] = mSelectedCardControllers[i].GetCard();
            }
            return cards;
        }

        public void SelectCard(DMCardController card)
        {
            mSelectedCardControllers.Add(card);
            MoveUp(card);
        }

        public void DeselectCard(DMCardController card)
        {
            mSelectedCardControllers.Remove(card);
            MoveDown(card);
        }

        public void DeselectAllCards()
        {
            for (int i = mSelectedCardControllers.Count - 1; i >= 0; i--)
            {
                var card = mSelectedCardControllers[i];
                mSelectedCardControllers.Remove(card);
                MoveDown(card);
            }
        }

        void MoveUp(DMCardController cardController)
        {
            RectTransform r = cardController.GetComponent<RectTransform>();
            Vector3 p = r.localPosition;
            p.y = 20;
            r.localPosition = p;
        }

        void MoveDown(DMCardController cardController)
        {
            RectTransform r = cardController.GetComponent<RectTransform>();
            Vector3 p = r.localPosition;
            p.y = 0;
            r.localPosition = p;
        }

        #region Response to delegate

        public void ResponseToRequestDrawStockCard(DMCard card)
        {
            AddCard(card);
            RearrangeCards();

            mActionState = ActionStateEnum.Play;
            actionStateDrawButtonsGroup.SetActive(false);

            mActionPlayState = ActionPlayStateEnum.Normal;
            actionStatePlayButtonsGroup.SetActive(true);
        }

        public void ResponseToRequestPickupAndMeldingCard(DMCard[] pickupCards, DMMeldingCard meldingCard, bool valid = true)
        {
            // Remove pickup card from discard card pile
            for (int i = 0; i < pickupCards.Length; i++)
            {
                this.cardTable.discardCardTable.RemoveCard(pickupCards[i]);
            }

            // Add pickup cards to onhand cards except cards that include in meldingCard
            for (int i = 0; i < pickupCards.Length; i++)
            {
                DMCard c = pickupCards[i];
                bool isInMeldidngCard = false;
                for (int j = 0; j < meldingCard.cards.Count; j++)
                {
                    if (c.IsIdentical(meldingCard.cards[j]))
                    {
                        isInMeldidngCard = true;
                        break;
                    }
                }
                if (!isInMeldidngCard)
                {
                    AddCard(c);
                }
            }

            // Remove cards in MeldingCard from OnHand cards
            for (int i = 0; i < meldingCard.cards.Count; i++)
            {
                DMCard c = meldingCard.cards[i];
                for (int j = 0; j < mCards.Count; j++)
                {
                    if (c.IsIdentical(mCards[j]))
                    {
                        RemoveCard(c);
                    }
                }
            }

            RearrangeCards();

            mActionState = ActionStateEnum.Play;
            actionStateDrawButtonsGroup.SetActive(false);
            checkKnock();
        }

        public void ResponsetoRequestMeldCard(DMMeldingCard meldingCard, bool valid = true)
        {
            if (!valid)
            {
                Debug.LogFormat("[PlayerOnHandCardController] response invalid meldingcard");
                return;
            }

            for (int i = 0; i < meldingCard.cards.Count; i++)
            {
                RemoveCard(meldingCard.cards[i]);
            }
            RearrangeCards();
            checkKnock();
        }

        public void ResponseToRequestDiscard(DMCard card)
        {
            RemoveCard(card);
            RearrangeCards();

        }

        public void ResponseToRequestLayoff(DMCard card, System.UInt16 targetMeldingCardId)
        {
            RemoveCard(card);
            RearrangeCards();
            checkKnock();
        }

        #endregion
    }

}