﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Dummy.DMProtocol;
using Dummy.Client.Audio;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

namespace Dummy.Client
{
    public class DMClient : MonoBehaviour
    {

        public const int kMaxConnection = 2;
        public const int kServerPort = 8888;

        public bool verbose;
        public string serverURL = "34.87.141.103";
        public bool isLocal = false;
        public string clientName;
        public int clientPort = 9001;
        public bool isGuest = false;

        [Header("Mockup data")]
        public string mockupUserId = "Mock";
        public bool randomMockupRoomId = true;
        public string mockupRoomId = "AB90C112DE33";

        int mGameMessageChannelId = -1;
        int mSocketId = -1;
        int mConnectionId = -1;

        Dictionary<int, List<IMessageReceivable>> mMessageReceivers;

        #region Singleton
        static DMClient s_instance;
        public static DMClient GetInstance()
        {
            return s_instance;
        }
        #endregion

        public bool isConnect()
        {
            if (mConnectionId != -1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        void OnApplicationQuit()
        {
            Disconnect();
            Debug.Log("Application ending after " + Time.time + " seconds");
        }

        void Awake()
        {
            // if (s_instance == null)
            // {
            //     s_instance = this;
            //     DontDestroyOnLoad(gameObject);
            //     mMessageReceivers = new Dictionary<int, List<IMessageReceivable>>();
            //     Debug.Log("Create Client");

            //     //Initialization code goes here[/INDENT]
            // }
            // else
            // {
            //     Destroy(gameObject);
            // }
            if (s_instance != null)
            {
                Destroy(this.gameObject);
                return;
            }
            mMessageReceivers = new Dictionary<int, List<IMessageReceivable>>();
            DontDestroyOnLoad(this.gameObject);

            s_instance = this;

            if (isLocal)
            {
                serverURL = "127.0.0.1";
            }

        }

        // Use this for initialization
        void Start()
        {
            Application.runInBackground = true;

            NetworkTransport.Init();
            ConnectionConfig config = new ConnectionConfig();
            mGameMessageChannelId = config.AddChannel(QosType.Reliable);
            config.DisconnectTimeout = 5 * 60 * 1000;
            HostTopology topology = new HostTopology(config, kMaxConnection);
            mSocketId = NetworkTransport.AddHost(topology);
            Debug.LogFormat("Client[{0}] established connection on port: {1} {2}", clientName, clientPort, mSocketId);

            // if (randomMockupRoomId)
            // {
            //     mockupRoomId = Guid.NewGuid().ToString();
            // }

            //GUEST ID
            mockupUserId = Guid.NewGuid().ToString();
        }

        public void RandomMockupRoomId()
        {
            mockupRoomId = Guid.NewGuid().ToString();
        }

        void Update()
        {
            ManageNetworkReceive();
            foreach (var kvp in mMessageReceivers)
            {
                List<IMessageReceivable> receivers = kvp.Value;
                if (receivers == null) continue;
                for (int i = 0; i < receivers.Count; i++)
                {
                    receivers[i].Process();
                }
            }
        }


        public void Connect()
        {
            byte err; 
            Debug.LogFormat("ip : {0}",serverURL);
       
            mConnectionId = NetworkTransport.Connect(0, serverURL, kServerPort, 0, out err);
            Debug.LogFormat("Connected to server. ConnectionId: {0} Error: {1}", mConnectionId, NetworkErrorAsString(err));
        }

        public void Disconnect()
        {
            byte err;
            NetworkTransport.Disconnect(mSocketId, mConnectionId, out err);
            Debug.LogFormat("Disconnect from server. ConnectionId: {0} Error: {1}", mConnectionId, NetworkErrorAsString(err));
            mConnectionId = -1;
        }

        void ManageNetworkReceive()
        {
            int recSocketId;
            int recConnectionId;
            int recChannelId;
            int bufferSize = 1024;
            int dataSize;
            byte err;
            byte[] recBuffer = new byte[bufferSize];
            NetworkEventType recNetworkEvent = NetworkTransport.Receive(out recSocketId, out recConnectionId,
                out recChannelId, recBuffer, bufferSize,
                out dataSize, out err);
            switch (recNetworkEvent)
            {
                case NetworkEventType.Nothing:
                    {
                        break;
                    }
                case NetworkEventType.ConnectEvent:
                    {
                        if (verbose)
                        {
                            StringBuilder sb = new StringBuilder();
                            sb.AppendFormat("<color=blue>Client[{0}] Incoming connection event received</color>\n", clientName);
                            sb.AppendFormat("Socket ID: {0}\n", recSocketId);
                            sb.AppendFormat("Connection ID: {0}\n", recConnectionId);
                            sb.AppendFormat("Channel ID: {0}\n", recChannelId);
                            Debug.Log(sb.ToString());
                        }
                        break;
                    }
                case NetworkEventType.DataEvent:
                    {
                        if (verbose)
                        {
                            StringBuilder sb = new StringBuilder();
                            sb.AppendFormat("<color=green>Client[{0}] Remote client send message\n</color>", clientName);
                            sb.AppendFormat("Socket ID: {0}\n", recSocketId);
                            sb.AppendFormat("Connection ID: {0}\n", recConnectionId);
                            sb.AppendFormat("Channel ID: {0}\n", recChannelId);
                            Debug.Log(sb.ToString());
                        }

                        DispatchReceivedMessage(recConnectionId, recBuffer);
                        break;
                    }
                case NetworkEventType.DisconnectEvent:
                    {
                        if (verbose)
                        {
                            StringBuilder sb = new StringBuilder();
                            sb.AppendFormat("<color=yellow>Client[{0}] Remote client event disconnected</color>\n", clientName);
                            sb.AppendFormat("Socket ID: {0}\n", recSocketId);
                            sb.AppendFormat("Connection ID: {0}\n", recConnectionId);
                            sb.AppendFormat("Channel ID: {0}\n", recChannelId);
                            Debug.Log(sb.ToString());
                        }
                        break;
                    }
            }
        }

        void DispatchReceivedMessage(int connectionId, byte[] buffer)
        {
            byte[] b = new byte[2];
            DMProtocol.DMProtocolUtility.CopyBytes(buffer, 0, b, 0, 2);
            ushort msgId = BitConverter.ToUInt16(b, 0);
            if (verbose)
            {
                Debug.LogFormat("Dispathcing message type: {0}", DMProtocolConstants.MsgIdAsString(msgId));
            }

            switch (msgId)
            {
                case DMProtocolConstants.kMsgId_ReqWhoAreYou:
                    {
                        if (verbose)
                        {
                            Debug.Log("Receive ReqWhoAreYouMessage");
                        }

                        RespWhoAreYouMessage msg = new RespWhoAreYouMessage();
                        msg.userId = mockupUserId;
                        Send(msg);

                        if (verbose)
                        {
                            Debug.LogFormat("Send RespWhoAreYouMessage userId: {0}", msg.userId);
                        }
                    }
                    break;
                case DMProtocolConstants.kMsgId_ReqClientHeartbeat:
                    {
                        RespClientHeartbeat msg = new RespClientHeartbeat();
                        Send(msg);
                        if (verbose)
                        {
                            Debug.LogFormat("Send RespClientHeartbeat");
                        }
                    }
                    break;
                default:
                    {
                        // TODO: Dispatch message to appropriate receiver
                        if (!mMessageReceivers.ContainsKey(msgId))
                        {
                            if (verbose)
                            {
                                Debug.LogWarningFormat("Cannot find Receiver for messageId: {0}", DMProtocolConstants.MsgIdAsString(msgId));
                            }
                            break;
                        }

                        List<IMessageReceivable> receivers = mMessageReceivers[msgId];
                        for (int i = 0; i < receivers.Count; i++)
                        {
                            receivers[i].Receive(msgId, buffer);
                        }
                        break;
                    }
            }
        }

        public void Send(DMBaseMessage message)
        {
            byte[] bytes;
            using (MemoryStream stream = new MemoryStream(1024))
            {
                using (BinaryWriter writer = new BinaryWriter(stream))
                {
                    message.WriteBytes(writer);
                }
                bytes = stream.ToArray();
            }

            byte err;
            NetworkTransport.Send(mSocketId,
                mConnectionId,
                mGameMessageChannelId,
                bytes,
                DMProtocolConstants.kProtocolBufferSize,
                out err);
            if ((NetworkError)err != NetworkError.Ok)
            {
                SceneManager.LoadScene("Login"); 
                AudioManager.instance.PlayBGM("bgm02");
                Debug.Log("<color=magenta> work from change bgm to bgm02 </color>");
            }
        }

        string NetworkErrorAsString(byte err)
        {
            return ((NetworkError)err).ToString();
        }

        public void RegisterMessageReceiver(int messageId, IMessageReceivable receiver)
        {
            if (verbose)
            {
                Debug.LogFormat("Register Receiver: {0} for messageId: {1}", receiver.Tag(), DMProtocolConstants.MsgIdAsString((ushort)messageId));
            }
            if (!mMessageReceivers.ContainsKey(messageId))
            {
                mMessageReceivers.Add(messageId, new List<IMessageReceivable>());

            }

            mMessageReceivers[messageId].Add(receiver);

        }

        public void RemoveMessageReceiver()
        {
            mMessageReceivers.Clear();
        }
    }

}