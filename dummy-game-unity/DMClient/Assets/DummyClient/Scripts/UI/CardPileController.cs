﻿using System;
using System.Collections;
using System.Collections.Generic;
using Dummy.DMProtocol;
using UnityEngine;
using DigitalRuby.Tween;

namespace Dummy.Client.UI
{

    public class CardPileController : MonoBehaviour
    {

        [Header("UI Component")]
        public RectTransform cardContainer;
        public GameObject cardPrefab;

        [Header("Configuration")]
        public bool isInteractable = true; // Allow each card controller accept click event
        public bool isSelectable = false; // The clicked card will be selected or not. This flag will be considered only if the isInteractable is set as true
        public float minHSpace;
        public float maxHSpace;

        public event Action<DMCard> onCardDidSelected;
        public event Action<DMCard> onCardDidDeselected;

        protected List<DMCard> mCards;
        protected List<DMCardController> mCardControllers;
        protected List<DMCardController> mSelectedCardControllers;

        float mHSpace;

        void Awake()
        {
            mCards = new List<DMCard>();
            mCardControllers = new List<DMCardController>();
            mSelectedCardControllers = new List<DMCardController>();

            mHSpace = 0;
        }

        public void AddCard(DMCard card, bool rearrange = false, float delay = 0f)
        {
            foreach (var c in mCards)
            {
                if (c.ToUInt16() == card.ToUInt16())
                {
                    Debug.LogWarning("[PlayerOnHandCardsController] trying to add a duplicated card -- ignore");
                    return;
                }
            }

            mCards.Add(card);
            GameObject newCard = Instantiate(cardPrefab);
            RectTransform cardRect = newCard.GetComponent<RectTransform>();
            RectTransform containerRect = cardContainer.GetComponent<RectTransform>();
            cardRect.SetParent(containerRect);
            cardRect.localScale = Vector3.one;

            float w1 = cardRect.rect.width;
            float h1 = cardRect.rect.height;
            float h2 = containerRect.rect.height;
            cardRect.sizeDelta = new Vector2(w1 * (h2 / h1), h2);

            DMCardController cardController = newCard.GetComponent<DMCardController>();
            cardController.SetCard(card);
            mCardControllers.Add(cardController);

            cardController.onClicked += OnCardControllerDidClicked;

            if (rearrange)
            {
                float totalWidth = cardContainer.rect.width;
                int n = mCardControllers.Count;
                var mHSpace = Mathf.Clamp(totalWidth / n, minHSpace, maxHSpace);

                Vector3 p = cardRect.anchoredPosition;
                p.x = mHSpace * (n - 1);
                p.y = 0;
                //cardRect.anchoredPosition = p;

                cardRect.anchoredPosition = new Vector3(p.x, p.y + 10);
                System.Action<ITween<Vector3>> updateTween = (t) =>
                {
                    cardRect.anchoredPosition = t.CurrentValue;
                    cardRect.GetComponent<CanvasGroup>().alpha = t.CurrentProgress;
                };
                System.Action<ITween<Vector3>> completeTween = (t) =>
                {
                    n = mCardControllers.Count;
                    mHSpace = Mathf.Clamp(totalWidth / n, minHSpace, maxHSpace);
                    p = cardRect.anchoredPosition;
                    p.x = mHSpace * (n - 1);
                    p.y = 0;
                    cardRect.anchoredPosition = p;
                };
                //tween effect
                var tw = cardRect.gameObject.Tween(null, cardRect.anchoredPosition, p, 0.2f, TweenScaleFunctions.CubicEaseIn, updateTween);
                tw.Delay = delay;
            }
        }

        public void InsertCard(int index, DMCard card, int playerIndex = -1)
        {
            foreach (var c in mCards)
            {
                if (c.ToUInt16() == card.ToUInt16())
                {
                    Debug.LogWarning("[PlayerOnHandCardsController] trying to add a duplicated card -- ignore");
                    return;
                }
            }

            mCards.Insert(index, card);
            GameObject newCard = Instantiate(cardPrefab);
            RectTransform cardRect = newCard.GetComponent<RectTransform>();
            RectTransform containerRect = cardContainer.GetComponent<RectTransform>();
            cardRect.SetParent(containerRect);
            cardRect.SetSiblingIndex(index);
            cardRect.localScale = Vector3.one;

            float w1 = cardRect.rect.width;
            float h1 = cardRect.rect.height;
            float h2 = containerRect.rect.height;
            cardRect.sizeDelta = new Vector2(w1 * (h2 / h1), h2);

            DMCardController cardController = newCard.GetComponent<DMCardController>();
            cardController.SetCard(card);
            mCardControllers.Insert(index, cardController);

            cardController.onClicked += OnCardControllerDidClicked;
            
            if (playerIndex != -1)
            {
                //must be the layoffcard action
                cardController.assignLayoffMark(playerIndex);
            }
        }

        public virtual void RemoveCard(DMCard card)
        {
            bool found = false;
            foreach (var c in mCards)
            {
                if (c.IsIdentical(card))
                {
                    found = true;
                    break;
                }
            }
            if (!found)
            {
                Debug.LogWarningFormat("[PlayerOnHandCardsController] trying to remove non-existing card {0} - ignore", card.CardName());
                return;
            }

            for (int i = mSelectedCardControllers.Count - 1; i >= 0; i--)
            {
                if (mSelectedCardControllers[i].GetCard().IsIdentical(card))
                {
                    DeselectCard(mSelectedCardControllers[i], false);
                }
            }

            for (int i = mCardControllers.Count - 1; i >= 0; i--)
            {
                if (mCardControllers[i].GetCard().IsIdentical(card))
                {
                    var c = mCardControllers[i];
                    mCardControllers.Remove(c);
                    RectTransform r = c.GetComponent<RectTransform>();
                    Vector3 p = r.anchoredPosition;
                    p.y += 20;
                    //r.anchoredPosition = p;
                    System.Action<ITween<Vector3>> updateTween = (t) =>
                    {
                        r.anchoredPosition = t.CurrentValue;
                        c.GetComponent<CanvasGroup>().alpha = t.CurrentProgress.Remap(0, 1, 1, 0);
                    };
                    System.Action<ITween<Vector3>> completeTween = (t) =>
                    {
                        Destroy(c.gameObject);
                    };
                    var tw = r.gameObject.Tween(null, r.anchoredPosition, p, 0.1f, TweenScaleFunctions.CubicEaseIn, updateTween, completeTween);
                }
            }

            mCards.Remove(card);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="theirCards"></param>
        /// <returns>True if cards has been changed, otherwise return false. So if this function return true, it might be need to Rearrange</returns>
        public bool SyncCards(DMCard[] theirCards)
        {
            bool cardsHaveBeenChanged = false;

            // If my card does not exist in theirCards then remove it.
            for (int i = mCards.Count - 1; i >= 0; i--)
            {
                bool found = false;
                DMCard myCard = mCards[i];
                for (int j = 0; j < theirCards.Length; j++)
                {
                    DMCard theirCard = theirCards[j];
                    if (myCard.IsIdentical(theirCard))
                    {
                        found = true;
                        break;
                    }
                }
                if (!found)
                {
                    RemoveCard(myCard);
                    cardsHaveBeenChanged = true;
                }
            }

            // If their card is still not in my cards, then add it
            for (int i = 0; i < theirCards.Length; i++)
            {
                bool found = false;
                DMCard theirCard = theirCards[i];
                for (int j = 0; j < mCards.Count; j++)
                {
                    DMCard myCard = mCards[j];
                    if (theirCard.IsIdentical(myCard))
                    {
                        found = true;
                        break;
                    }
                }

                if (!found)
                {
                    AddCard(theirCard, true);
                    cardsHaveBeenChanged = true;
                }
            }

            return cardsHaveBeenChanged;
        }

        public void RearrangeCards()
        {
            float totalWidth = cardContainer.rect.width;
            int n = mCardControllers.Count;
            mHSpace = Mathf.Clamp(totalWidth / n, minHSpace, maxHSpace);

            float localX = 0;
            for (int i = 0; i < mCardControllers.Count; i++)
            {
                RectTransform r = mCardControllers[i].GetComponent<RectTransform>();
                Vector2 p = r.anchoredPosition;
                p.x = localX;
                p.y = 0;
                r.anchoredPosition = p;
                localX += mHSpace;
            }
        }

        public DMCard[] GetCopyOfAllCards()
        {
            return mCards.ToArray();
        }
        public DMCard[] GetCopyOfSelectedCards()
        {
            List<DMCard> cards = new List<DMCard>();
            foreach (var cardController in mSelectedCardControllers)
            {
                cards.Add(cardController.GetCard());
            }
            return cards.ToArray();
        }

        public int NumberOfCards()
        {
            return mCards.Count;
        }

        public DMCard CardAtIndex(int index)
        {
            if (index < 0 || index >= mCardControllers.Count)
            {
                throw new IndexOutOfRangeException("Card index out of range");
            }
            return mCardControllers[index].GetCard();
        }

        public int IndexOfCard(DMCard card)
        {
            for (int i = 0; i < mCardControllers.Count; i++)
            {
                if (mCardControllers[i].GetCard().IsIdentical(card))
                {
                    return i;
                }
            }
            return -1;
        }

        public void SelectCard(DMCard card, bool notifyEvent)
        {
            for (int i = 0; i < mCardControllers.Count; i++)
            {
                if (card.IsIdentical(mCardControllers[i].GetCard()))
                {
                    SelectCard(mCardControllers[i], notifyEvent);
                    break;
                }
            }
        }
        public void SelectCard(DMCardController card, bool notifyEvent)
        {
            mSelectedCardControllers.Add(card);
            MoveUp(card);
            if (notifyEvent)
            {
                if (onCardDidSelected != null)
                {
                    onCardDidSelected(card.GetCard());
                }
            }
        }

        public void DeselectCard(DMCard card, bool notifyEvent)
        {
            for (int i = 0; i < mCardControllers.Count; i++)
            {
                if (card.IsIdentical(mCardControllers[i].GetCard()))
                {
                    DeselectCard(mCardControllers[i], notifyEvent);
                    break;
                }
            }
        }
        public void DeselectCard(DMCardController card, bool notifyEvent)
        {
            mSelectedCardControllers.Remove(card);
            MoveDown(card);
            if (notifyEvent)
            {
                if (onCardDidDeselected != null)
                {
                    onCardDidDeselected(card.GetCard());
                }
            }
        }

        public void DeselectAllCards(bool notifyEvent)
        {
            for (int i = mSelectedCardControllers.Count - 1; i >= 0; i--)
            {
                var card = mSelectedCardControllers[i];
                DeselectCard(card, notifyEvent);
            }
        }

        private void OnCardControllerDidClicked(DMCardController cardController)
        {
            if (!isInteractable)
            {
                return;
            }

            if (isSelectable)
            {
                if (!mSelectedCardControllers.Contains(cardController))
                {
                    SelectCard(cardController, notifyEvent: true);
                }
                else
                {
                    DeselectCard(cardController, notifyEvent: true);
                }
            }
        }

        protected void MoveUp(DMCardController cardController)
        {
            RectTransform r = cardController.GetComponent<RectTransform>();
            Vector2 p = r.anchoredPosition;
            p.y = 20;
            //r.localPosition = p;
            System.Action<ITween<Vector3>> updateTween = (t) =>
            {
                r.localPosition = t.CurrentValue;
            };
            r.gameObject.Tween(null, r.localPosition, p, 0.1f, TweenScaleFunctions.CubicEaseOut, updateTween);

            Debug.LogFormat("[CardPileController] MoveUp: {0}", cardController.GetCard().CardName());
        }

        protected void MoveDown(DMCardController cardController)
        {
            RectTransform r = cardController.GetComponent<RectTransform>();
            Vector3 p = r.anchoredPosition;
            p.y = 0;
            r.anchoredPosition = p;
            Debug.LogFormat("[CardPileController] MoveDown: {0}", cardController.GetCard().CardName());
        }

    }

}