﻿using UnityEngine;
using UnityUI = UnityEngine.UI;

using System;
using System.Collections;
using System.Collections.Generic;
using Dummy.DMProtocol;

namespace Dummy.Client.UI
{

    public class MeldingAreaController : MonoBehaviour
    {

        [Header("Prefabs")]
        public GameObject cardPilePrefab;

        [Header("UI Components")]
        public RectTransform area;

        [HideInInspector]
        public byte playerIndex;

        RectTransform mRectTransform;
        List<DMMeldingCard> mMeldingCards;
        Dictionary<UInt16, CardPileController> mMeldingCardPiles;

        void Awake()
        {
            mRectTransform = GetComponent<RectTransform>();
            mMeldingCards = new List<DMMeldingCard>();
            mMeldingCardPiles = new Dictionary<UInt16, CardPileController>();
        }

        public int getMeldingCount()
        {
            return mMeldingCards.Count;
        }

        public bool Put(DMMeldingCard meldingCard)
        {
            for (int i = 0; i < mMeldingCards.Count; i++)
            {
                if (mMeldingCards[i].id == meldingCard.id)
                {
                    Debug.LogWarningFormat("[MeldingCardAreaController] you are trying to put the melding card with same id ({0}) as the one that already exists.", meldingCard.id);
                    return false;
                }
            }

            mMeldingCards.Add(meldingCard);

            GameObject meldingCardPile = Instantiate(cardPilePrefab);
            RectTransform rect = meldingCardPile.GetComponent<RectTransform>();
            rect.SetParent(this.area);
            rect.localScale = 1.25f * Vector3.one;
            rect.anchoredPosition = new Vector2(0, 0);

            CardPileController meldingCardPileController = meldingCardPile.GetComponent<CardPileController>();
            mMeldingCardPiles.Add(meldingCard.id, meldingCardPileController);
            meldingCardPileController.isInteractable = false;

            for (int i = 0; i < meldingCard.cards.Count; i++)
            {
                meldingCardPileController.AddCard(meldingCard.cards[i], true, i * 0.1f);
            }
            meldingCardPileController.RearrangeCards();

            return true;
        }

        public float vSpace = 10f;
        public void Rearrange()
        {
            Rect areaRect = this.area.rect;
            float x = 0;
            float y = 0;
            for (int i = 0; i < mMeldingCards.Count; i++)
            {
                RectTransform meldingCardPileRect = mMeldingCardPiles[mMeldingCards[i].id].GetComponent<RectTransform>();
                meldingCardPileRect.anchoredPosition = new Vector2(x, y);
                if (x + meldingCardPileRect.rect.width > areaRect.width)
                {
                    x = 0;
                    y -= (meldingCardPileRect.rect.height + vSpace);
                }
                else
                {
                    x += meldingCardPileRect.rect.width;
                }
            }

        }

        public bool InsertLayoffCard(DMCard card, UInt16 meldingCardId, int targetPlayerID)
        {
            DMMeldingCard meldingCard = null;
            for (int i = 0; i < mMeldingCards.Count; i++)
            {
                if (mMeldingCards[i].id == meldingCardId)
                {
                    meldingCard = mMeldingCards[i];
                    break;
                }
            }
            if (meldingCard == null)
            {
                return false;
            }

            int insertAtIndex = -1;
            if (meldingCard.InsertLayoffCard(card, out insertAtIndex))
            {
                var cardPile = mMeldingCardPiles[meldingCard.id];
                cardPile.InsertCard(insertAtIndex, card, targetPlayerID);
                cardPile.RearrangeCards();
                return true;
            }

            // Fallback if it does not match any case => return false
            return false;
        }

        public DMMeldingCard FindMeldingCardForLayoff(DMCard layoffCard)
        {
            List<DMCard> testCards = new List<DMCard>();
            for (int i = 0; i < mMeldingCards.Count; i++)
            {
                DMMeldingCard m = mMeldingCards[i];
                testCards.Clear();
                testCards.AddRange(m.cards);
                testCards.Add(layoffCard);
                if (m.meldingType == DMMeldingCard.MeldingTypeEnum.SameRank)
                {
                    if (DMCard.isSameRank(testCards))
                    {
                        return m;
                    }
                }
                else if (m.meldingType == DMMeldingCard.MeldingTypeEnum.RunOfSameKind)
                {
                    if (DMCard.isRunOfSameKind(testCards))
                    {
                        return m;
                    }
                }
            }

            return null;
        }

        public DMMeldingCard FindMeldingCardForLayoff(List<DMCard> layoffCards)
        {
            List<DMCard> testCards = new List<DMCard>();
            for (int i = 0; i < mMeldingCards.Count; i++)
            {
                DMMeldingCard m = mMeldingCards[i];
                testCards.Clear();
                testCards.AddRange(m.cards);
                testCards.AddRange(layoffCards);
                if (m.meldingType == DMMeldingCard.MeldingTypeEnum.SameRank)
                {
                    if (DMCard.isSameRank(testCards))
                    {
                        return m;
                    }
                }
                else if (m.meldingType == DMMeldingCard.MeldingTypeEnum.RunOfSameKind)
                {
                    if (DMCard.isRunOfSameKind(testCards))
                    {
                        return m;
                    }
                }
            }

            return null;
        }
    }

}