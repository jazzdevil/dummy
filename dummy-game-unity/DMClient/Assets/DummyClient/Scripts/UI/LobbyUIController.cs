﻿using UnityEngine;
using UnityUI = UnityEngine.UI;
using Dummy.Client.Audio;
using System.Collections;

namespace Dummy.Client.UI
{

    public class LobbyUIController : MonoBehaviour
    {

        [Header("UI Components")]
        public GameObject JoinRoomPanel;
        public GameObject ConnectionPanel;

        [Header("UI - Room")]
        public UnityUI.Button bttnJoin;

        [Header("UI - Connection")]
        public UnityUI.InputField inputServerIP;
        public UnityUI.Button bttnConnect;

        public UnityUI.Button bttnFbConnect;

        [Header("Configuration")]
        public DMLobbyController lobbyController;

        void Awake()
        {
            bttnJoin?.onClick.AddListener(() =>
            {
                lobbyController.JoinRoom();
                AudioManager.instance.PlaySFX("button_sfx");
                Debug.Log("<color=yellow> work from bttnJoin </color>");
            });
            bttnConnect?.onClick.AddListener(() =>
            {
                //DMClient.GetInstance().serverURL = string.IsNullOrEmpty(inputServerIP.text) ? "127.0.0.1" : inputServerIP.text;
                AudioManager.instance.PlaySFX("button_sfx");
                Debug.Log("<color=yellow> work from bttnConnect </color>");
                DMClient.GetInstance().Connect();
                DMClient.GetInstance().isGuest = true;
                JoinRoomPanel.SetActive(true);
                ConnectionPanel.SetActive(false);
            });

            bttnFbConnect?.onClick.AddListener(() =>
            {
                Debug.LogFormat("{0}", DMClient.GetInstance().isConnect());
            });
        }

        void Start()
        {
            if (DMClient.GetInstance().isConnect())
            {
                JoinRoomPanel.SetActive(true);
                ConnectionPanel.SetActive(false);
            }
            else
            {
                JoinRoomPanel.SetActive(false);
                ConnectionPanel.SetActive(true);
            }
        }
    }

}