﻿using UnityEngine;
using UnityUI = UnityEngine.UI;
using UnityEngine.SceneManagement;

using System.Collections;
using System.Collections.Generic;
using Dummy.DMProtocol;
using Dummy.Client.Audio;
using UnityEngine.UI;
using System.Linq;

using DigitalRuby.Tween;

namespace Dummy.Client.UI
{

    public class PlayerOnHandCardsController : MonoBehaviour
    {

        public interface PlayerOnHandCardsControllerRequesterDelegate
        {
            // Draw action
            void PlayerOnHandCardsControllerRequestDrawStockCard();

            // Pooh
            void PlayerOnHandCardsControllerRequestPickupDiscardCardAndMeld(DMCard card, DMMeldingCard meldingCard);

            // Play action
            void PlayerOnHandCardsControllerRequestMeldCard(DMCard[] cards);
            void PlayerOnHandCardsControllerRequestDiscard(DMCard card);
            void PlayerOnHandCardsControllerRequestLayoff(DMCard card, DMMeldingCard targetMeldingCard);
            void PlayerOnHandCardsControllerRequestLayoffs(DMCard[] card, ushort targetMeldingCard);
            void leaveAndRemoveRoom(string roomID);
            void PlayerOnHandCardsControllerRequestKnockCard(DMCard card);
            void PlayerOnHandCardsControllerPassLastTurn();
        }

        enum ActionStateEnum
        {
            Draw = 0,
            Play,
            Knock
        }

        enum ActionDrawStateEnum
        {
            DrawStockCard = 0,
            PickupDiscardCardAndMeld
        }

        enum ActionPlayStateEnum
        {
            Normal = 0,
            Meld,
            Discard,
            Layoff
        }

        [Header("Card table")]
        public CardTableController cardTable;

        [Header("UI Component")]
        public RectTransform cardContainer;
        public GameObject cardPrefab;
        public GameObject controlPanel;

        [Header("Action-State-Draw-Buttons")]
        public GameObject actionStateDrawButtonsGroup;
        public UnityUI.Button bttnDrawStockCard;
        public UnityUI.Button bttnPickAndMeldCard;
        public UnityUI.Button bttnHelp;
        public UnityUI.Button bttnPassStateDraw;

        [Header("Action-State-Play-Buttons")]
        public GameObject actionStatePlayButtonsGroup;
        public UnityUI.Button bttnDiscard;
        public UnityUI.Button bttnMeld;
        public UnityUI.Button bttnLayoff;
        public UnityUI.Button bttnPassStatePlay;
        public UnityUI.Button bttnHelpPlayState;

        [Header("Action-State-Knock-Buttons")]
        public GameObject actionStateKnockButtonsGroup;
        public UnityUI.Button bttnKnock;

        [Header("Confirm-Buttons")]
        public GameObject confirmButtonsGroup;
        public UnityUI.Button bttnOk;
        public UnityUI.Button bttnCancel;

        [Header("Reset-Buttons")]
        public GameObject resetButtonsGroup;
        public UnityUI.Button bttnLeave;

        [Header("Sort-Buttons")]
        //public UnityUI.Button bttnSortKind;
        //public UnityUI.Button bttnSortRank;
        public UnityUI.Button btnSort;

        [Header("Configuration")]
        public float minHSpace;
        public float maxHSpace;

        private bool isFirstPickAndMeld;
        private bool mToggleSort;

        public PlayerOnHandCardsControllerRequesterDelegate delegateObject;

        List<DMCard> mCards;
        List<DMCardController> mCardControllers;
        List<DMCardController> mSelectedCardControllers;

        float mHSpace;
        ActionStateEnum mActionState;
        ActionDrawStateEnum mActionDrawState;
        ActionPlayStateEnum mActionPlayState;

        private bool isPlayerTurn = false;

        void Awake()
        {
            mCards = new List<DMCard>();
            mCardControllers = new List<DMCardController>();
            mSelectedCardControllers = new List<DMCardController>();

            mHSpace = 0;
            mActionState = ActionStateEnum.Draw;
            mActionDrawState = ActionDrawStateEnum.DrawStockCard;
            mActionPlayState = ActionPlayStateEnum.Normal;

            isFirstPickAndMeld = false;
            mToggleSort = false;

            SetupControlPanelUI();
        }

        public void SetDiscardSelectable(bool b)
        {
            cardTable.discardCardTable.isInteractable = b;
            cardTable.discardCardTable.isSelectable = b;
            cardTable.discardCardTable.DeselectAllCards(false);
        }

        public void SetBttnDraw()
        {
            if (isFirstPickAndMeld)
            {
                actionStateDrawButtonsGroup.SetActive(false);
                bttnDiscard.gameObject.SetActive(true);
                bttnLayoff.gameObject.SetActive(true);
                bttnMeld.gameObject.SetActive(true);
                bttnHelpPlayState.gameObject.SetActive(true);
                actionStatePlayButtonsGroup.SetActive(true);
            }
            else
            {
                actionStateDrawButtonsGroup.SetActive(false);
                bttnDiscard.gameObject.SetActive(true);
                bttnLayoff.gameObject.SetActive(false);
                bttnMeld.gameObject.SetActive(false);
                bttnHelpPlayState.gameObject.SetActive(false);
                actionStatePlayButtonsGroup.SetActive(true);
            }

        }

        public void SetBttnPickUpAndMeld()
        {
            actionStateDrawButtonsGroup.SetActive(false);
            bttnDiscard.gameObject.SetActive(true);
            bttnLayoff.gameObject.SetActive(true);
            bttnMeld.gameObject.SetActive(true);
            bttnHelpPlayState.gameObject.SetActive(true);
            actionStatePlayButtonsGroup.SetActive(true);
        }

        void SetupControlPanelUI()
        {
            resetButtonsGroup.SetActive(true);

            btnSort.onClick.AddListener(() =>
            {
                AudioManager.instance.PlaySFX("table_deal");
                Debug.Log("<color=red> work from btnSort </color>");

                System.Action<ITween<Vector3>> completeTween = (t) =>
                {
                    if (mToggleSort)
                    {
                        for (int i = 0; i < mCardControllers.Count; i++)
                        {
                            for (int j = i + 1; j < mCardControllers.Count; j++)
                            {
                                if (mCardControllers[j].GetCard().ToUint16Kind() < mCardControllers[i].GetCard().ToUint16Kind())
                                {
                                    DMCardController temp = mCardControllers[i];
                                    mCardControllers[i] = mCardControllers[j];
                                    mCardControllers[j] = temp;
                                    mCardControllers[i].GetComponent<Transform>().SetSiblingIndex(i);
                                    mCardControllers[j].GetComponent<Transform>().SetSiblingIndex(j);
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < mCardControllers.Count; i++)
                        {
                            for (int j = i + 1; j < mCardControllers.Count; j++)
                            {
                                if (mCardControllers[j].GetCard().ToUint16Rank() < mCardControllers[i].GetCard().ToUint16Rank())
                                {
                                    DMCardController temp = mCardControllers[i];
                                    mCardControllers[i] = mCardControllers[j];
                                    mCardControllers[j] = temp;
                                    mCardControllers[i].GetComponent<Transform>().SetSiblingIndex(i);
                                    mCardControllers[j].GetComponent<Transform>().SetSiblingIndex(j);
                                }
                            }
                        }
                    }
                    mToggleSort = !mToggleSort;
                    RearrangeCards(true);
                };
                for (int i = 0; i < mCardControllers.Count; i++)
                {
                    RectTransform r = mCardControllers[i].GetComponent<RectTransform>();
                    System.Action<ITween<Vector3>> updatePos = (t) =>
                    {
                        r.localPosition = t.CurrentValue;
                    };
                    if (i != mCardControllers.Count - 1)
                    {
                        r.gameObject.Tween("rearrange card " + i, r.localPosition, Vector3.zero, 0.2f, TweenScaleFunctions.CubicEaseIn, updatePos);
                    }
                    else
                    {
                        r.gameObject.Tween("rearrange card " + i, r.localPosition, Vector3.zero, 0.2f, TweenScaleFunctions.CubicEaseIn, updatePos, completeTween);
                    }
                }
            });


            //bttnSortKind.onClick.AddListener(() =>
            //{
            //    for (int i = 0; i < mCardControllers.Count; i++)
            //    {
            //        for (int j = i + 1; j < mCardControllers.Count; j++)
            //        {
            //            if (mCardControllers[j].GetCard().ToUint16Kind() < mCardControllers[i].GetCard().ToUint16Kind())
            //            {
            //                DMCardController temp = mCardControllers[i];
            //                mCardControllers[i] = mCardControllers[j];
            //                mCardControllers[j] = temp;
            //                mCardControllers[i].GetComponent<Transform>().SetSiblingIndex(i);
            //                mCardControllers[j].GetComponent<Transform>().SetSiblingIndex(j);
            //            }
            //        }
            //    }
            //    RearrangeCards();
            //});
            //bttnSortRank.onClick.AddListener(() =>
            //{
            //    for (int i = 0; i < mCardControllers.Count; i++)
            //    {
            //        for (int j = i + 1; j < mCardControllers.Count; j++)
            //        {
            //            if (mCardControllers[j].GetCard().ToUint16Rank() < mCardControllers[i].GetCard().ToUint16Rank())
            //            {
            //                DMCardController temp = mCardControllers[i];
            //                mCardControllers[i] = mCardControllers[j];
            //                mCardControllers[j] = temp;
            //                mCardControllers[i].GetComponent<Transform>().SetSiblingIndex(i);
            //                mCardControllers[j].GetComponent<Transform>().SetSiblingIndex(j);
            //            }
            //        }
            //    }
            //    RearrangeCards();
            //});
            //bttnLeave.
            bttnLeave.onClick.AddListener(() =>
            {
                AudioManager.instance.PlaySFX("button_sfx");
                Debug.Log("<color=yellow> work from bttnLeave </color>");
                delegateObject.leaveAndRemoveRoom(DMClient.GetInstance().mockupRoomId);
                SceneManager.LoadScene("Login");
                AudioManager.instance.PlayBGM("bgm02");
                Debug.Log("<color=magenta> work from change bgm to bgm02 </color>");
            });

            #region Action Draw State

            // Draw stock card
            bttnDrawStockCard.onClick.AddListener(() =>
            {
                AudioManager.instance.PlaySFX("table_deal");
                Debug.Log("<color=red> work from bttnDrawStockCard </color>");
                mActionDrawState = ActionDrawStateEnum.DrawStockCard;
                delegateObject.PlayerOnHandCardsControllerRequestDrawStockCard();
                SetDiscardSelectable(false);
                DeselectAllCards();
            });

            bttnPassStateDraw.onClick.AddListener(() =>
            {
                AudioManager.instance.PlaySFX("table_deal");
                Debug.Log("<color=red> work from bttnPassStateDraw </color>");
                delegateObject.PlayerOnHandCardsControllerPassLastTurn();
            });

            bttnPickAndMeldCard.onClick.AddListener(() =>
            {
                AudioManager.instance.PlaySFX("table_deal");
                Debug.Log("<color=red> work from bttnPickAndMeldCard </color>");
                if (PickAndMeldCard())
                {
                    SetDiscardSelectable(false);
                    SetBttnPickUpAndMeld();
                    if (!isFirstPickAndMeld)
                    {
                        isFirstPickAndMeld = true;
                        //bttnPickAndMeldCard.GetComponentInChildren<Text>().text = "ลงไพ่";
                    }
                    cardTable.discardCardTable.DeselectAllCards(false);
                    DeselectAllCards();
                }
                else
                {
                    cardTable.discardCardTable.DeselectAllCards(false);
                    DeselectAllCards();
                }
            });

            // Pickup discarded card and meld
            bttnHelp.onClick.AddListener(() =>
            {
                AudioManager.instance.PlaySFX("table_deal");
                CardPileController discardCardPile = cardTable.discardCardTable;

                // Get all possible melding cards
                DMCard[] discardedCards = discardCardPile.GetCopyOfAllCards();
                List<DMCard> allCards = new List<DMCard>();
                allCards.AddRange(discardedCards);
                allCards.AddRange(mCards);
                var allPossibleMeldingCards = DMMeldingCard.FindAllPossibleMeldingCard(allCards.ToArray());

                // Find out the combination of melding cards contain both card from Discard pile and OnHand card
                DMMeldingCard validMeldingCard = null;
                for (int idx = 0; idx < allPossibleMeldingCards.Count; idx++)
                {
                    bool hasCardFromDiscardPile = false;
                    bool hasCardFromOnHandCards = false;
                    DMMeldingCard meldingCard = allPossibleMeldingCards[idx];
                    for (int i = 0; i < meldingCard.cards.Count; i++)
                    {
                        DMCard c = meldingCard.cards[i];
                        if (!hasCardFromDiscardPile)
                        {
                            // Find card in Discarded card pile
                            for (int j = 0; j < discardedCards.Length; j++)
                            {
                                if (c.IsIdentical(discardedCards[j]))
                                {
                                    hasCardFromDiscardPile = true;
                                    break;
                                }
                            }
                        }
                        if (!hasCardFromOnHandCards)
                        {
                            // Find card in OnHand cards
                            for (int j = 0; j < mCards.Count; j++)
                            {
                                if (c.IsIdentical(mCards[j]))
                                {
                                    hasCardFromOnHandCards = true;
                                    break;
                                }
                            }
                        }
                        if (hasCardFromDiscardPile && hasCardFromOnHandCards)
                        {
                            break;
                        }
                    }

                    if (hasCardFromDiscardPile && hasCardFromOnHandCards)
                    {
                        validMeldingCard = meldingCard;
                        break;
                    }
                }

                if (validMeldingCard == null)
                {
                    Debug.Log("[PlayerOnHandCardController] Cannot find any possible melding card");
                    return;
                }

                mActionDrawState = ActionDrawStateEnum.PickupDiscardCardAndMeld;

                foreach (var card in validMeldingCard.cards)
                {
                    for (int i = 0; i < discardedCards.Length; i++)
                    {
                        if (card.IsIdentical(discardedCards[i]))
                        {
                            discardCardPile.SelectCard(card, notifyEvent: false);
                            break;
                        }
                    }
                    for (int i = 0; i < mCardControllers.Count; i++)
                    {
                        if (card.IsIdentical(mCardControllers[i].GetCard()))
                        {
                            SelectCard(mCardControllers[i]);
                            break;
                        }
                    }
                }

            });

            #endregion

            #region Action Play State

            // Discard
            bttnDiscard.onClick.AddListener(() =>
            {
                AudioManager.instance.PlaySFX("table_deal");
                Debug.Log("<color=red> work from bttnDiscard </color>");
                mActionPlayState = ActionPlayStateEnum.Discard;
                delegateObject.PlayerOnHandCardsControllerRequestDiscard(mSelectedCardControllers[0].GetCard());
                actionStatePlayButtonsGroup.SetActive(false);
            });

            bttnPassStatePlay.onClick.AddListener(() =>
            {
                AudioManager.instance.PlaySFX("table_deal");
                Debug.Log("<color=red> work from bttnPassStatePlay </color>");
                delegateObject.PlayerOnHandCardsControllerPassLastTurn();
            });

            // Melding
            bttnHelpPlayState.onClick.AddListener(() =>
            {
                AudioManager.instance.PlaySFX("table_deal");
                Debug.Log("<color=red> work from bttnHelpPlayState </color>");
                mActionPlayState = ActionPlayStateEnum.Meld;

                DeselectAllCards();
                List<DMMeldingCard> meldingCards = DMMeldingCard.FindAllPossibleMeldingCard(mCards.ToArray());
                if (meldingCards.Count == 0) return;

                DMMeldingCard firstMeldingCard = meldingCards[0];
                for (int i = 0; i < firstMeldingCard.cards.Count; i++)
                {
                    foreach (var cardController in mCardControllers)
                    {
                        if (cardController.GetCard().IsIdentical(firstMeldingCard.cards[i]))
                        {
                            SelectCard(cardController);
                            break;
                        }
                    }
                }
            });

            bttnMeld.onClick.AddListener(() =>
            {
                AudioManager.instance.PlaySFX("table_deal");
                Debug.Log("<color=red> work from bttnMeld </color>");
                if (GetSelectedCards().Length > mCards.Count - 1)
                {
                    Debug.LogErrorFormat("[PlayerOnHandCardsController] cannot meld all card");
                    DeselectAllCards();
                }
                else
                {
                    delegateObject.PlayerOnHandCardsControllerRequestMeldCard(GetSelectedCards());
                }
            });

            // Layoff
            bttnLayoff.onClick.AddListener(() =>
            {
                AudioManager.instance.PlaySFX("table_deal");
                Debug.Log("<color=red> work from bttnLayoff </color>");
                //mActionPlayState = ActionPlayStateEnum.Layoff;
                //confirmButtonsGroup.SetActive(true);
                //actionStatePlayButtonsGroup.SetActive(false);

                //// Find and select card that can be layed off
                //DeselectAllCards();
                //for (int i = 0; i < mCards.Count; i++)
                //{
                //    DMCard card = mCards[i];
                //    if (this.cardTable.FindMeldingCardForLayOffCard(card) != null)
                //    {
                //        DMCardController cardController = null;
                //        for (int j = 0; j < mCardControllers.Count; j++)
                //        {
                //            if (mCardControllers[j].GetCard().IsIdentical(card))
                //            {
                //                cardController = mCardControllers[j];
                //                break;
                //            }
                //        }
                //        if (cardController != null)
                //        {
                //            SelectCard(cardController);
                //            break;
                //        }
                //    }
                //}
                if (GetNumberOfSelectedCards() == 1)
                {
                    DMCard card = GetSelectedCardAtIndex(0);
                    DMMeldingCard targetMeldingCard = this.cardTable.FindMeldingCardForLayOffCard(card);
                    if (targetMeldingCard == null)
                    {
                        Debug.LogWarning("[PlayerOnHandCardsController] layoff card: Not find MeldingCard");
                        DeselectAllCards();
                    }
                    else
                    {
                        Debug.LogFormat("[PlayerOnHandCardsController] layoff card: {0} to meldingCard id: {1}", card.CardName(), targetMeldingCard.id);
                        delegateObject.PlayerOnHandCardsControllerRequestLayoff(card, targetMeldingCard);
                    }
                }
                else
                {
                    Debug.LogWarning("[PlayerOnHandCardsController] cannot layoff more than 1 card");
                    DeselectAllCards();
                }
            });

            #endregion

            #region Action Knock State
            bttnKnock.onClick.AddListener(() =>
            {
                AudioManager.instance.PlaySFX("table_deal");
                Debug.Log("<color=red> work from bttnKnock </color>");
                delegateObject.PlayerOnHandCardsControllerRequestKnockCard(mCards[0]);
                actionStateKnockButtonsGroup.SetActive(false);
            });

            #endregion Action Knock State

        }

        bool loopForCheckOneTurnKnock(List<DMCard> cards, out List<DMMeldingCard> meldCards, out List<DMCard> cardsForlayoff, out List<ushort> targetMeldingCards)
        {


            List<DMCard> allCards = new List<DMCard>();
            allCards.AddRange(cards);
            var allPossibleMeldingCards = DMMeldingCard.FindAllPossibleMeldingCard(allCards.ToArray());

            foreach (var allmeldcard in allPossibleMeldingCards)
            {
                List<DMCard> leftCardsOnHard = new List<DMCard>();
                meldCards = new List<DMMeldingCard>();
                cardsForlayoff = new List<DMCard>();
                targetMeldingCards = new List<ushort>();

                leftCardsOnHard.AddRange(allCards);
                foreach (var card in allmeldcard.cards)
                {
                    for (int i = 0; i < leftCardsOnHard.Count; i++)
                    {
                        if (card.IsIdentical(leftCardsOnHard[i]))
                        {
                            leftCardsOnHard.RemoveAt(i);
                            break;
                        }
                    }
                }

                List<string> cardCanLayoff = new List<string>();
                List<ushort> cardCanLayoffMeldID = new List<ushort>();
                for (int i = 0; i < leftCardsOnHard.Count; i++)
                {
                    List<DMCard> temp_leftCardsOnHard = new List<DMCard>();
                    temp_leftCardsOnHard.AddRange(leftCardsOnHard);
                    temp_leftCardsOnHard.RemoveAt(i);
                    for (int j = temp_leftCardsOnHard.Count - 1; j >= 0; j--)
                    {

                        DMCard card = temp_leftCardsOnHard[j];
                        DMMeldingCard temp_m0 = this.cardTable.FindMeldingCardForLayOffCard(card);
                        if (temp_m0 != null)
                        {
                            List<DMCard> temp_listlayoff = new List<DMCard>();
                            temp_listlayoff.Add(card);
                            if (!cardCanLayoff.Contains(temp_leftCardsOnHard[j].CardShortName()))
                            {
                                cardCanLayoff.Add(temp_leftCardsOnHard[j].CardShortName());
                                cardCanLayoffMeldID.Add(temp_m0.id);
                                temp_leftCardsOnHard.RemoveAt(j);
                            }

                            for (int k = temp_leftCardsOnHard.Count - 1; k >= 0; k--)
                            {
                                DMCard temp_card = temp_leftCardsOnHard[k];
                                temp_listlayoff.Add(temp_card);
                                DMMeldingCard temp_m = this.cardTable.FindMeldingCardForLayOffCard(temp_listlayoff);
                                if (temp_m != null)
                                {
                                    if (!cardCanLayoff.Contains(temp_leftCardsOnHard[k].CardShortName()))
                                    {
                                        cardCanLayoffMeldID.Add(temp_m.id);
                                        cardCanLayoff.Add(temp_leftCardsOnHard[k].CardShortName());
                                        temp_leftCardsOnHard.RemoveAt(k);
                                    }
                                }
                                else
                                {
                                    temp_listlayoff.Remove(temp_card);
                                }
                            }
                        }
                    }
                }



                // -1  is a Knock Card
                if (cardCanLayoff.Count == leftCardsOnHard.Count - 1)
                {
                    for (int i = 0; i < cardCanLayoff.Count; i++)
                    {
                        foreach (var m_c in mCards)
                        {
                            DMCard temp_c = m_c;
                            if (cardCanLayoff[i] == m_c.CardShortName())
                            {
                                cardsForlayoff.Add(temp_c);
                                targetMeldingCards.Add(cardCanLayoffMeldID[i]);
                                break;
                            }
                        }
                    }
                    meldCards.Add(allmeldcard);
                    return true;
                }
                else
                {
                    if (loopForCheckOneTurnKnock(leftCardsOnHard, out meldCards, out cardsForlayoff, out targetMeldingCards))
                    {
                        meldCards.Add(allmeldcard);
                        return true;
                    }
                }
            }

            meldCards = null;
            cardsForlayoff = null;
            targetMeldingCards = null;
            return false;
        }

        bool checkOneTurnKnock()
        {
            List<DMMeldingCard> meldCards = new List<DMMeldingCard>();
            List<DMCard> cardsForlayoff = new List<DMCard>();
            List<ushort> targetMeldingCards = new List<ushort>();
            if (loopForCheckOneTurnKnock(mCards, out meldCards, out cardsForlayoff, out targetMeldingCards))
            {
                foreach (var m_meld in meldCards)
                {
                    delegateObject.PlayerOnHandCardsControllerRequestMeldCard(m_meld.cards.ToArray());
                }
                for (int i = 0; i < cardsForlayoff.Count; i++)
                {
                    Debug.LogErrorFormat("SSSSSSSSSSSSSSSSSSSSSS{0} {1}", cardsForlayoff[i], targetMeldingCards[i]);
                }


                for (int i = 0; i < cardsForlayoff.Count;)
                {
                    List<DMCard> temp_cardsForlayoff = new List<DMCard>();
                    ushort target_id = targetMeldingCards[i];
                    temp_cardsForlayoff.Add(cardsForlayoff[i]);
                    int j = i + 1;
                    for (; j < targetMeldingCards.Count;)
                    {
                        if (targetMeldingCards[i] == targetMeldingCards[j])
                        {
                            temp_cardsForlayoff.Add(cardsForlayoff[j]);
                            targetMeldingCards.RemoveAt(j);
                            cardsForlayoff.RemoveAt(j);
                        }
                        else
                        {
                            j++;
                        }
                    }
                    cardsForlayoff.RemoveAt(i);
                    targetMeldingCards.RemoveAt(i);
                    i = 0;
                    delegateObject.PlayerOnHandCardsControllerRequestLayoffs(temp_cardsForlayoff.ToArray(), target_id);
                }
                Debug.LogErrorFormat("U CAN WIN THIS ROUND WOW");
                return true;
            }
            else
            {
                Debug.LogErrorFormat("U CANNOT WIN THIS ROUND OOP");
                return false;
            }
        }

        bool PickAndMeldCard()
        {
            // Assemble cards from the selected cards from OnHand and the selected cards from discarded pile
            CardPileController discardCardPile = cardTable.discardCardTable;
            List<DMCard> selectedCardsForMelding = new List<DMCard>();

            if (discardCardPile.GetCopyOfSelectedCards().Length == 0)
            {
                Debug.LogFormat("Cannot create a melding card without discardCard");
                return false;
            }
            selectedCardsForMelding.AddRange(discardCardPile.GetCopyOfSelectedCards());
            foreach (var cardController in mSelectedCardControllers)
            {
                selectedCardsForMelding.Add(cardController.GetCard());
            }

            // Try out, the selected card can be melded
            DMMeldingCard meldingCard = null;
            try
            {
                meldingCard = new DMMeldingCard(selectedCardsForMelding.ToArray());
            }
            catch (System.InvalidOperationException)
            {
                Debug.LogFormat("Cannot create a melding card with the selected cards");
            }
            if (meldingCard == null)
            {
                return false;
            }

            // Find out which discarded card would be picked up
            bool isPickValid = false;

            DMCard pickupDiscardedCard = discardCardPile.CardAtIndex(0);
            for (int i = 0; i < discardCardPile.NumberOfCards(); i++)
            {
                DMCard card = discardCardPile.CardAtIndex(i);
                bool isInMeldingCard = false;
                foreach (var c in meldingCard.cards)
                {
                    if (card.IsIdentical(c))
                    {
                        isPickValid = true;
                        isInMeldingCard = true;
                        break;
                    }
                }
                if (isInMeldingCard)
                {
                    pickupDiscardedCard = card;
                    break;
                }
            }

            if (isPickValid)
            {
                int indexPickCard = discardCardPile.IndexOfCard(pickupDiscardedCard);
                DMCard[] discardCards = discardCardPile.GetCopyOfAllCards();
                int countPickUpDiscardCard = discardCards.Length - indexPickCard;
                if (meldingCard.cards.Count > (countPickUpDiscardCard + mCards.Count) - 1)
                {
                    Debug.LogErrorFormat("[PlayerOnHandCardsController] cannot meld all card");
                    return false;
                }
            }

            delegateObject.PlayerOnHandCardsControllerRequestPickupDiscardCardAndMeld(pickupDiscardedCard, meldingCard);
            return true;
        }

        public void setLastTurn()
        {
            bttnPassStateDraw.gameObject.SetActive(true);
            bttnPassStatePlay.gameObject.SetActive(true);
            bttnDrawStockCard.gameObject.SetActive(false);
            bttnMeld.gameObject.SetActive(false);
        }

        void checkKnock()
        {
            if (mCards.Count == 1)
            {
                mActionState = ActionStateEnum.Knock;
                actionStatePlayButtonsGroup.SetActive(false);
                actionStateKnockButtonsGroup.SetActive(true);
            }
            else
            {
                mActionState = ActionStateEnum.Play;
                actionStatePlayButtonsGroup.SetActive(true);
            }
        }
        bool CanSelectCard()
        {
            return true;
            //if (mActionState == ActionStateEnum.Play && (
            //        mActionPlayState == ActionPlayStateEnum.Discard ||
            //        mActionPlayState == ActionPlayStateEnum.Layoff ||
            //        mActionPlayState == ActionPlayStateEnum.Meld))
            //{
            //    return true;
            //}
            //else if (mActionState == ActionStateEnum.Draw &&
            //  mActionDrawState == ActionDrawStateEnum.PickupDiscardCardAndMeld)
            //{
            //    return true;
            //}
            //return false;
        }

        public void AddCard(DMCard card, bool rearrange = false)
        {
            foreach (var c in mCards)
            {
                if (c.ToUInt16() == card.ToUInt16())
                {
                    Debug.LogWarning("[PlayerOnHandCardsController] trying to add a duplicated card -- ignore");
                    return;
                }
            }

            mCards.Add(card);
            GameObject newCard = Instantiate(cardPrefab);
            RectTransform r = newCard.GetComponent<RectTransform>();
            r.SetParent(cardContainer.GetComponent<RectTransform>());
            r.transform.localScale = new Vector3(0.83f, 0.83f, 0.83f);

            DMCardController cardController = newCard.GetComponent<DMCardController>();
            cardController.SetCard(card);
            mCardControllers.Add(cardController);

            cardController.onClicked += (c) =>
            {
                AudioManager.instance.PlaySFX("table_deal");
                Debug.Log("<color=red> work from click card `cardController` </color>");
                if (CanSelectCard())
                {
                    if (!mSelectedCardControllers.Contains(c))
                    {
                        SelectCard(c);
                    }
                    else
                    {
                        DeselectCard(c);
                    }
                }
            };

            if (rearrange)
            {
                float totalWidth = cardContainer.rect.width;
                int n = mCardControllers.Count;
                var mHSpace = Mathf.Clamp(totalWidth / n, minHSpace, maxHSpace);

                Vector3 p = r.localPosition;
                p.x = mHSpace * (n - 1);
                p.y = 0;
                //r.localPosition = p;


                r.localPosition = new Vector3(p.x, p.y + 10);
                System.Action<ITween<Vector3>> updateTween = (t) =>
                {
                    r.localPosition = t.CurrentValue;
                    r.GetComponent<CanvasGroup>().alpha = t.CurrentProgress;
                };
                System.Action<ITween<Vector3>> completeTween = (t) =>
                {
                    n = mCardControllers.Count;
                    mHSpace = Mathf.Clamp(totalWidth / n, minHSpace, maxHSpace);

                    p = r.localPosition;
                    p.x = mHSpace * (n - 1);
                    p.y = 0;
                    r.localPosition = p;
                };
                //tween effect
                r.gameObject.Tween("add card", r.localPosition, p, 0.2f, TweenScaleFunctions.CubicEaseIn, updateTween, completeTween);
            }

        }

        public void RemoveCard(DMCard card)
        {
            bool found = false;
            foreach (var c in mCards)
            {
                if (c.IsIdentical(card))
                {
                    found = true;
                    break;
                }
            }
            if (!found)
            {
                Debug.LogWarningFormat("[PlayerOnHandCardsController] trying to remove non-existing card {0} - ignore", card.CardName());
                return;
            }

            mCards.Remove(card);
            for (int i = mCardControllers.Count - 1; i >= 0; i--)
            {
                if (mCardControllers[i].GetCard().IsIdentical(card))
                {
                    var c = mCardControllers[i];
                    mCardControllers.Remove(c);
                    if (mSelectedCardControllers.Contains(c))
                    {
                        mSelectedCardControllers.Remove(c);
                    }

                    RectTransform r = c.GetComponent<RectTransform>();
                    Vector3 p = r.localPosition;
                    p.y += 20;
                    //r.localPosition = p;
                    System.Action<ITween<Vector3>> updateTween = (t) =>
                    {
                        r.localPosition = t.CurrentValue;
                        c.GetComponent<CanvasGroup>().alpha = t.CurrentProgress.Remap(0, 1, 1, 0);
                    };
                    System.Action<ITween<Vector3>> completeTween = (t) =>
                    {
                        Destroy(c.gameObject);
                    };
                    r.gameObject.Tween(null, r.localPosition, p, 0.1f, TweenScaleFunctions.CubicEaseIn, updateTween, completeTween);

                }
            }
        }

        public void RemoveAllCards()
        {
            for (int i = mCards.Count - 1; i >= 0; i--)
            {
                RemoveCard(mCards[i]);
            }
        }

        public int getHandCardCount(){
            return mCards.Count;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="theirCards"></param>
        /// <returns>True if cards has been changed, otherwise return false. So if this function return true, it might be need to Rearrange</returns>
        public bool SyncCards(DMCard[] theirCards)
        {
            bool cardsHaveBeenChanged = false;

            // If my card does not exist in theirCards then remove it.
            for (int i = mCards.Count - 1; i >= 0; i--)
            {
                bool found = false;
                DMCard myCard = mCards[i];
                for (int j = 0; j < theirCards.Length; j++)
                {
                    DMCard theirCard = theirCards[j];
                    if (myCard.IsIdentical(theirCard))
                    {
                        found = true;
                        break;
                    }
                }
                if (!found)
                {
                    RemoveCard(myCard);
                    cardsHaveBeenChanged = true;
                }
            }

            // If their card is still not in my cards, then add it
            for (int i = 0; i < theirCards.Length; i++)
            {
                bool found = false;
                DMCard theirCard = theirCards[i];
                for (int j = 0; j < mCards.Count; j++)
                {
                    DMCard myCard = mCards[j];
                    if (theirCard.IsIdentical(myCard))
                    {
                        found = true;
                        break;
                    }
                }

                if (!found)
                {
                    AddCard(theirCard);
                    cardsHaveBeenChanged = true;
                }
            }

            return cardsHaveBeenChanged;
        }


        void Update()
        {
            //if (Input.GetKeyDown(KeyCode.R))
            //{
            //    Debug.Log("[Editor-Mode] PlayerOnHandCardsController - Reaarange cards");
            //    RearrangeCards();
            //}

            if (!isPlayerTurn) {
              return;
            }

            ActionPlayUIControl();
        }
        public void ActionPlayUIControl()
        {

            #region bttnHelp Calculation Setup

            CardPileController discardCardPile = cardTable.discardCardTable;

            // Get all possible melding cards
            DMCard[] discardedCards = discardCardPile.GetCopyOfAllCards();
            List<DMCard> allCards = new List<DMCard>();
            allCards.AddRange(discardedCards);
            allCards.AddRange(mCards);
            var allPossibleMeldingCards = DMMeldingCard.FindAllPossibleMeldingCard(allCards.ToArray());

            // Find out the combination of melding cards contain both card from Discard pile and OnHand card
            DMMeldingCard validMeldingCard = null;
            for (int idx = 0; idx < allPossibleMeldingCards.Count; idx++)
            {
                bool hasCardFromDiscardPile = false;
                bool hasCardFromOnHandCards = false;
                DMMeldingCard meldingCard = allPossibleMeldingCards[idx];
                for (int i = 0; i < meldingCard.cards.Count; i++)
                {
                    DMCard c = meldingCard.cards[i];
                    if (!hasCardFromDiscardPile)
                    {
                        // Find card in Discarded card pile
                        for (int j = 0; j < discardedCards.Length; j++)
                        {
                            if (c.IsIdentical(discardedCards[j]))
                            {
                                hasCardFromDiscardPile = true;
                                break;
                            }
                        }
                    }
                    if (!hasCardFromOnHandCards)
                    {
                        // Find card in OnHand cards
                        for (int j = 0; j < mCards.Count; j++)
                        {
                            if (c.IsIdentical(mCards[j]))
                            {
                                hasCardFromOnHandCards = true;
                                break;
                            }
                        }
                    }
                    if (hasCardFromDiscardPile && hasCardFromOnHandCards)
                    {
                        break;
                    }
                }

                if (hasCardFromDiscardPile && hasCardFromOnHandCards)
                {
                    validMeldingCard = meldingCard;
                    break;
                }
            }

            #endregion

            if (validMeldingCard == null)
            {
                //Debug.Log("[PlayerOnHandCardController] Cannot find any possible melding card");
                //return;
                bttnHelp.interactable = false;
            }
            else
            {
                bttnHelp.interactable = true;
            }

            bttnPickAndMeldCard.interactable = !(discardCardPile.GetCopyOfSelectedCards().Length == 0);

            bttnDiscard.interactable =
            bttnLayoff.interactable = GetNumberOfSelectedCards() == 1;

            List<DMMeldingCard> meldingCards = DMMeldingCard.FindAllPossibleMeldingCard(mCards.ToArray());
            bttnHelpPlayState.interactable = meldingCards.Count > 0;
            bttnMeld.interactable = !((GetSelectedCards().Length > mCards.Count - 1) || GetSelectedCards().Length == 0);
        }

        public void RearrangeCards(bool tween = false)
        {
            float totalWidth = cardContainer.rect.width;
            int n = mCardControllers.Count;
            mHSpace = Mathf.Clamp(totalWidth / n, minHSpace, maxHSpace);

            if (!tween)
            {
                float localX = 0;
                for (int i = 0; i < mCardControllers.Count; i++)
                {
                    RectTransform r = mCardControllers[i].GetComponent<RectTransform>();

                    Vector3 p = r.localPosition;
                    p.x = localX;
                    p.y = 0;

                    r.localPosition = p;
                    localX += mHSpace;
                }
            }
            else
            {
                for (int i = 0; i < mCardControllers.Count; i++)
                {
                    mCardControllers[i].GetComponent<RectTransform>().localPosition = Vector3.zero;
                }

                float localX = 0;
                for (int i = 0; i < mCardControllers.Count; i++)
                {
                    RectTransform r = mCardControllers[i].GetComponent<RectTransform>();

                    Vector3 p = r.localPosition;
                    p.x = localX;
                    p.y = 0;

                    System.Action<ITween<Vector3>> updatePos = (t) =>
                    {
                        r.localPosition = t.CurrentValue;
                    };

                    // completion defaults to null if not passed in
                    r.gameObject.Tween("rearrange card " + i, r.localPosition, p, 0.2f, TweenScaleFunctions.CubicEaseIn, updatePos);
                    // r.localPosition = p;
                    localX += mHSpace;
                }
            }
        }

        public void SetActiveAsPlayerTurn(bool isActive) 
        {
          isPlayerTurn = isActive;

          if (isActive) {
            ShowControlPanel();
            return;
          }

          HideControlPanel();
        }

        public void ShowControlPanel()
        {
            controlPanel.SetActive(true);
            mActionState = ActionStateEnum.Draw;
            actionStateDrawButtonsGroup.SetActive(true);
            actionStatePlayButtonsGroup.SetActive(false);
            confirmButtonsGroup.SetActive(false);
        }

        public void HideControlPanel()
        {
            controlPanel.SetActive(false);
        }

        public int GetNumberOfSelectedCards()
        {
            return mSelectedCardControllers.Count;
        }

        public DMCard GetSelectedCardAtIndex(int index)
        {
            return mSelectedCardControllers[index].GetCard();
        }

        public DMCard[] GetSelectedCards()
        {
            DMCard[] cards = new DMCard[mSelectedCardControllers.Count];
            for (int i = 0; i < mSelectedCardControllers.Count; i++)
            {
                cards[i] = mSelectedCardControllers[i].GetCard();
            }
            return cards;
        }

        public void SelectCard(DMCardController card)
        {
            mSelectedCardControllers.Add(card);
            MoveUp(card);
        }

        public void DeselectCard(DMCardController card)
        {
            mSelectedCardControllers.Remove(card);
            MoveDown(card);
        }

        public void DeselectAllCards()
        {
            for (int i = mSelectedCardControllers.Count - 1; i >= 0; i--)
            {
                var card = mSelectedCardControllers[i];
                mSelectedCardControllers.Remove(card);
                MoveDown(card);
            }
        }

        void MoveUp(DMCardController cardController)
        {
            RectTransform r = cardController.GetComponent<RectTransform>();
            Vector3 p = r.localPosition;
            p.y = 20;
            //r.localPosition = p;
            System.Action<ITween<Vector3>> updateTween = (t) =>
            {
                r.localPosition = t.CurrentValue;
            };
            r.gameObject.Tween(null, r.localPosition, p, 0.1f, TweenScaleFunctions.CubicEaseOut, updateTween);

        }

        void MoveDown(DMCardController cardController)
        {
            RectTransform r = cardController.GetComponent<RectTransform>();
            Vector3 p = r.localPosition;
            p.y = 0;
            //r.localPosition = p;
            System.Action<ITween<Vector3>> updateTween = (t) =>
            {
                r.localPosition = t.CurrentValue;
            };
            r.gameObject.Tween(null, r.localPosition, p, 0.1f, TweenScaleFunctions.CubicEaseOut, updateTween);
        }

        #region Response to delegate

        public void ResponseToRequestDrawStockCard(DMCard card)
        {
            AddCard(card, true);
            RearrangeCards();

            mActionState = ActionStateEnum.Play;
            actionStateDrawButtonsGroup.SetActive(false);

            mActionPlayState = ActionPlayStateEnum.Normal;
            actionStatePlayButtonsGroup.SetActive(true);

            if (isFirstPickAndMeld || checkOneTurnKnock())
            {
                SetBttnPickUpAndMeld();
            }
            else
            {
                SetBttnDraw();
            }

        }

        public void ResponseToRequestPickupAndMeldingCard(DMCard[] pickupCards, DMMeldingCard meldingCard, bool valid = true)
        {
            // Remove pickup card from discard card pile
            for (int i = 0; i < pickupCards.Length; i++)
            {
                this.cardTable.discardCardTable.RemoveCard(pickupCards[i]);
            }

            // Add pickup cards to onhand cards except cards that include in meldingCard
            for (int i = 0; i < pickupCards.Length; i++)
            {
                DMCard c = pickupCards[i];
                bool isInMeldidngCard = false;
                for (int j = 0; j < meldingCard.cards.Count; j++)
                {
                    if (c.IsIdentical(meldingCard.cards[j]))
                    {
                        isInMeldidngCard = true;
                        break;
                    }
                }
                if (!isInMeldidngCard)
                {
                    AddCard(c);
                }
            }

            // Remove cards in MeldingCard from OnHand cards
            for (int i = 0; i < meldingCard.cards.Count; i++)
            {
                DMCard c = meldingCard.cards[i];
                for (int j = 0; j < mCards.Count; j++)
                {
                    if (c.IsIdentical(mCards[j]))
                    {
                        RemoveCard(c);
                    }
                }
            }

            RearrangeCards();

            mActionState = ActionStateEnum.Play;
            actionStateDrawButtonsGroup.SetActive(false);
            checkKnock();
        }

        public void ResponsetoRequestMeldCard(DMMeldingCard meldingCard, bool valid = true)
        {
            if (!valid)
            {
                Debug.LogFormat("[PlayerOnHandCardController] response invalid meldingcard");
                return;
            }

            for (int i = 0; i < meldingCard.cards.Count; i++)
            {
                RemoveCard(meldingCard.cards[i]);
            }
            RearrangeCards();
            checkKnock();
        }

        public void ResponseToRequestDiscard(DMCard card)
        {
            RemoveCard(card);
            RearrangeCards();

        }

        public void ResponseToRequestLayoff(DMCard[] cards, System.UInt16 targetMeldingCardId)
        {
            foreach (var card in cards)
            {
                RemoveCard(card);
            }
            RearrangeCards();
            checkKnock();
        }

        #endregion
    }

}