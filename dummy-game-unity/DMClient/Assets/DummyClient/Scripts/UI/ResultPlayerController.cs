﻿using UnityEngine;
using UnityUI = UnityEngine.UI;

using Dummy.DMProtocol;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Dummy.Client.UI
{
    public class ResultPlayerController : MonoBehaviour
    {
        [Header("UI Component")]
        public GameObject cardPrefab;


        public RectTransform handCardContainer;
        List<DMCard> mHandCards = new List<DMCard>();
        List<DMCardController> mHandCardControllers = new List<DMCardController>();

        public RectTransform usedCardContainer;
        List<DMCard> mUsedCards = new List<DMCard>();
        List<DMCardController> mUsedCardControllers = new List<DMCardController>();

        public struct LayoffCard
        {
            public DMCard card;
            public byte playerIndex;
        }

        public List<LayoffCard> mLayoffCards = new List<LayoffCard>();

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public void SetPlayerName(string name)
        {
            transform.Find("name").GetComponent<UnityUI.Text>().text = name;
        }

        public void AddHandCard(DMCard card)
        {
            // foreach (var c in mCards)
            // {
            //     if (c.ToUInt16() == card.ToUInt16())
            //     {
            //         Debug.LogWarning("[PlayerOnHandCardsController] trying to add a duplicated card -- ignore");
            //         return;
            //     }
            // }
            mHandCards.Add(card);
            GameObject newCard = Instantiate(cardPrefab);
            RectTransform r = newCard.GetComponent<RectTransform>();
            r.SetParent(handCardContainer.GetComponent<RectTransform>());
            r.transform.localScale = new Vector3(0.83f, 0.83f, 0.83f);

            float w1 = r.rect.width;
            float h1 = r.rect.height;
            float h2 = handCardContainer.rect.height;
            r.sizeDelta = new Vector2(w1 * (h2 / h1), h2);

            DMCardController cardController = newCard.GetComponent<DMCardController>();
            cardController.SetCard(card);
            mHandCardControllers.Add(cardController);
        }
        public void AddUsedCard(DMCard card)
        {
            // foreach (var c in mCards)
            // {
            //     if (c.ToUInt16() == card.ToUInt16())
            //     {
            //         Debug.LogWarning("[PlayerOnHandCardsController] trying to add a duplicated card -- ignore");
            //         return;
            //     }
            // }
            mUsedCards.Add(card);
            GameObject newCard = Instantiate(cardPrefab);
            RectTransform r = newCard.GetComponent<RectTransform>();
            r.SetParent(usedCardContainer.GetComponent<RectTransform>());
            r.transform.localScale = new Vector3(0.83f, 0.83f, 0.83f);

            float w1 = r.rect.width;
            float h1 = r.rect.height;
            float h2 = usedCardContainer.rect.height;
            r.sizeDelta = new Vector2(w1 * (h2 / h1), h2);

            DMCardController cardController = newCard.GetComponent<DMCardController>();
            cardController.SetCard(card);
            mUsedCardControllers.Add(cardController);

            foreach (var layoffCard in mLayoffCards)
            {
                if (layoffCard.card.CardName() == card.CardName())
                {
                    cardController.assignLayoffMark(layoffCard.playerIndex);
                }
            }

        }
        float mHSpace;
        [Header("Configuration")]
        public float minHSpace = 1;
        public float maxHSpace;

        public void RearrangeCards()
        {
            float totalWidth = handCardContainer.rect.width;
            //maxHSpace = handCardContainer.rect.width;
            int n = mHandCardControllers.Count;
            mHSpace = Mathf.Clamp(totalWidth / n, minHSpace, maxHSpace);
            float localX = 5;
            for (int i = 0; i < mHandCardControllers.Count; i++)
            {
                RectTransform r = mHandCardControllers[i].GetComponent<RectTransform>();
                Vector3 p = r.localPosition;
                p.x = localX;
                p.y = -5;
                r.localPosition = p;
                localX += mHSpace;
            }

            totalWidth = usedCardContainer.rect.width;
            // maxHSpace = usedCardContainer.rect.width;
            n = mUsedCardControllers.Count;
            mHSpace = Mathf.Clamp(totalWidth / n, minHSpace, maxHSpace);
            localX = 5;
            for (int i = 0; i < mUsedCardControllers.Count; i++)
            {
                RectTransform r = mUsedCardControllers[i].GetComponent<RectTransform>();
                Vector3 p = r.localPosition;
                p.x = localX;
                p.y = -5;
                r.localPosition = p;
                localX += mHSpace;
            }
        }

        public void SetUsedCardScore()
        {
            int score = 0;
            foreach (var card in mUsedCards)
            {
                score += card.CardScore();
            }
            transform.Find("txtMieldNumber").GetComponent<UnityUI.Text>().text = score >= 0 ? "+" + score : score.ToString();
        }
        public void SetHandCardScore()
        {
            int score = 0;
            foreach (var card in mHandCards)
            {
                score += card.CardScore();
            }
            transform.Find("txtOnHandNum").GetComponent<UnityUI.Text>().text = score >= 0 ? "-" + score : score.ToString();
        }


        List<DMCard> mDiscardCards = new List<DMCard>();
        List<DMCardController> mDiscardCardControllers = new List<DMCardController>();

        public void AddDiscardPile(DMCard card)
        {
            RectTransform discardCardContainer = (RectTransform)transform.parent.Find("DiscardPile");
            mDiscardCards.Add(card);
            GameObject newCard = Instantiate(cardPrefab);
            RectTransform r = newCard.GetComponent<RectTransform>();
            r.SetParent(discardCardContainer.GetComponent<RectTransform>());
            r.transform.localScale = new Vector3(0.83f, 0.83f, 0.83f);

            float w1 = r.rect.width;
            float h1 = r.rect.height;
            float h2 = discardCardContainer.rect.height;
            r.sizeDelta = new Vector2(w1 * (h2 / h1), h2);

            DMCardController cardController = newCard.GetComponent<DMCardController>();
            cardController.SetCard(card);
            mDiscardCardControllers.Add(cardController);
        }
        public void RearrangeDiscardPile()
        {
            RectTransform discardCardContainer = (RectTransform)transform.parent.Find("DiscardPile");
            float totalWidth = discardCardContainer.rect.width;
            //maxHSpace = discardCardContainer.rect.width;
            int n = mDiscardCardControllers.Count;
            mHSpace = Mathf.Clamp(totalWidth / n, minHSpace, maxHSpace);
            float localX = 5;
            for (int i = 0; i < mDiscardCardControllers.Count; i++)
            {
                RectTransform r = mDiscardCardControllers[i].GetComponent<RectTransform>();
                Vector3 p = r.localPosition;
                p.x = localX;
                p.y = -5;
                r.localPosition = p;
                localX += mHSpace;
            }
        }

        List<DMCard> mStockCards = new List<DMCard>();
        List<DMCardController> mStockCardControllers = new List<DMCardController>();

        public void AddStockPile(DMCard card)
        {
            RectTransform stockCardContainer = (RectTransform)transform.parent.Find("StockPile");
            mStockCards.Add(card);
            GameObject newCard = Instantiate(cardPrefab);
            RectTransform r = newCard.GetComponent<RectTransform>();
            r.SetParent(stockCardContainer.GetComponent<RectTransform>());
            r.transform.localScale = new Vector3(0.83f, 0.83f, 0.83f);

            float w1 = r.rect.width;
            float h1 = r.rect.height;
            float h2 = stockCardContainer.rect.height;
            r.sizeDelta = new Vector2(w1 * (h2 / h1), h2);

            DMCardController cardController = newCard.GetComponent<DMCardController>();
            cardController.SetCard(card);
            mStockCardControllers.Add(cardController);
        }
        public void RearrangeStockPile()
        {
            RectTransform stockCardContainer = (RectTransform)transform.parent.Find("StockPile");
            float totalWidth = stockCardContainer.rect.width;
            //maxHSpace = discardCardContainer.rect.width;
            int n = mStockCardControllers.Count;
            mHSpace = Mathf.Clamp(totalWidth / n, minHSpace, maxHSpace);
            float localX = 5;
            for (int i = 0; i < mStockCardControllers.Count; i++)
            {
                RectTransform r = mStockCardControllers[i].GetComponent<RectTransform>();
                Vector3 p = r.localPosition;
                p.x = localX;
                p.y = -5;
                r.localPosition = p;
                localX += mHSpace;
            }
        }
    }
}
