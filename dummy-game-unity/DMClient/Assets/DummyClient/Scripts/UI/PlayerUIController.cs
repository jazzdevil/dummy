﻿using UnityEngine;
using UnityUI = UnityEngine.UI;

using Dummy.DMProtocol;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Dummy.Client.UI
{

    public class PlayerUIController : MonoBehaviour
    {

        public UnityUI.Image avatarImage;
        public UnityUI.Text txtPlayerName;
        public UnityUI.Text txtPlayerScore;
        public UnityUI.Image imgActionTimeIndicator;
        public UnityUI.Image tableHighlight;
        public UnityUI.Image panelTime;
        public UnityUI.Text txtTime;
        public UnityUI.Image imgHeadIcon;
        public UnityUI.Image imgHeartIcon;
        public UnityUI.Image imgCoverIcon;
        public UnityUI.Image imgItemFrame;

        public GameObject OnHandCardsGroup;
        public UnityUI.Text txtNumberOfOnHandCards;
        public UnityUI.Text txtStatus;

        public UnityUI.Button bttnTest;
        public GameObject resultPlayer;
        public ResultPlayerController resultPlayerController;
        Queue<string> txtStatusQueue;
        int mPlayerIndex = -1;
        float timeCount = 10;
        float mActionTime = 0;
        float mActionTimer = 0;

        private int scorePlayer = 0;
        public enum typeScore
        {
            None = 0,
            Knock = 1, //nomal knock
            Color = 2, //สี
            OneTurn = 3,//มืด
            ColorAndOneTurn, // สี + มืด
            NoOneTurn, //โดนมืด
            //ทิ้งเต็ม
            dropFull,

            //ปี้หัว
            dropHead,

            //ทิ้งสเปโต
            dropSpeto,

            //ทิ้งโง่
            dropFool,

            //ทิ้งดั้มมี้
            dropDummy,

            //ถูกฝากสเปโต   ต้องใช้สเปโตต่อเท่านั้น เช่น  2 => 356   แต่  2 3 => 456 ไม่ได้
            gotDummySpeto,
            playSpeto,
            playHead,

        }
        enum etxtFadeStep
        {
            fadeIn = 1,
            fadeOut = 2,
            complete = 3
        };

        float fadeStatsTime = 2f;

        bool forceTxtStatus = false;
        etxtFadeStep txtFadeStep;
        void Awake()
        {
            if (bttnTest != null)
            {
                bttnTest.onClick.AddListener(() =>
                {
                    AddStatus((byte)typeScore.dropDummy);
                });
            }

            txtStatusQueue = new Queue<string>();
            txtStatus.color = new Color(txtStatus.color.r, txtStatus.color.g, txtStatus.color.b, 0);
            imgItemFrame.gameObject.SetActive(true);

        }
        public void AddStatus(byte type)
        {
            string s = getTypeScore(type);

            s = txtPlayerName.text + ": " + s;
            StatusTextController.instance.setStatusText(s);
            //txtStatusQueue.Enqueue(s);
        }
        public void AddKnockStatus(byte type)
        {
            string s = getTypeScore(type);

            s = txtPlayerName.text + ": " + s;
            StatusTextController.instance.setStatusText(s);
            //txtStatusQueue.Enqueue(s);
        }

        public void AddDrawStatus()
        {
            var s = txtPlayerName.text + ": ได้จั่วการ์ด ";
            StatusTextController.instance.setStatusText(s);
        }
        public void AddDrawStatus(DMCard card)
        {
            var s = txtPlayerName.text + ": ได้จั่วการ์ด " + card.CardName();
            StatusTextController.instance.setStatusText(s);
        }
        public void AddDiscardStatus(DMCard card)
        {
            var s = txtPlayerName.text + ": ได้ทิ้งการ์ด " + card.CardName();
            StatusTextController.instance.setStatusText(s);
        }
        public void AddLayoffStatus(DMCard card)
        {
            var s = txtPlayerName.text + ": ได้ฝากการ์ด " + card.CardName();
            StatusTextController.instance.setStatusText(s);
        }
        public void AddLayoffStatus(DMCard[] cards)
        {
            var s = txtPlayerName.text + ": ได้ฝากการ์ด ";
            for (int i = 0; i < cards.Length; i++)
            {
                s += cards[i].CardName();
                if (i < cards.Length - 1)
                {
                    s += ", ";
                }
            }
            StatusTextController.instance.setStatusText(s);
        }



        public void ReadyToPlay()
        {
            txtStatus.text = "Ready";
            forceTxtStatus = true;
            txtStatus.color = new Color(txtStatus.color.r, txtStatus.color.g, txtStatus.color.b, 1);
        }

        public void StartGame()
        {
            txtStatus.text = "";
            forceTxtStatus = false;
            txtStatus.color = new Color(txtStatus.color.r, txtStatus.color.g, txtStatus.color.b, 0);

        }

        public void Update()
        {
            mActionTimer += Time.deltaTime;
            //set image type to filled
            //imgActionTimeIndicator.fillAmount = Mathf.Clamp((mActionTimer / mActionTime), 0, 1);
            timeCount -= Time.deltaTime;
            if (timeCount <= 0) timeCount = 0;
            txtTime.text = Math.Floor(timeCount).ToString();
            if (!forceTxtStatus)
            {
                if (txtFadeStep == etxtFadeStep.fadeIn)
                {

                    if (txtStatus.color.a < 1)
                    {

                        txtStatus.color = new Color(txtStatus.color.r, txtStatus.color.g, txtStatus.color.b, txtStatus.color.a + (Time.deltaTime / fadeStatsTime));
                    }
                    else
                    {
                        txtFadeStep = etxtFadeStep.fadeOut;
                    }
                }
                else if (txtFadeStep == etxtFadeStep.fadeOut)
                {
                    if (txtStatus.color.a > 0)
                    {
                        txtStatus.color = new Color(txtStatus.color.r, txtStatus.color.g, txtStatus.color.b, txtStatus.color.a - (Time.deltaTime / fadeStatsTime));
                    }
                    else
                    {
                        txtFadeStep = etxtFadeStep.complete;
                    }
                }
                else
                {
                    if (txtStatusQueue.Count > 0)
                    {
                        txtStatus.text = txtStatusQueue.Dequeue();
                        txtFadeStep = etxtFadeStep.fadeIn;
                    }

                }
            }

        }
        public void SetAvatarImage(string url)
        {

        }

        public void SetPlayerIndex(int playerIndex)
        {
            mPlayerIndex = playerIndex;
        }
        public int PlayerIndex()
        {
            return mPlayerIndex;
        }

        public void SetPlayerName(string name)
        {
            txtPlayerName.text = name;
            resultPlayer.transform.Find("name").GetComponent<UnityUI.Text>().text = name;
        }

        public void SetPlayerScore(int score)
        {
            txtPlayerScore.text = score.ToString();
            this.scorePlayer = score;
        }

        public void SetNumberOfOnHandCards(int numberOfCards)
        {
            txtNumberOfOnHandCards.text = numberOfCards.ToString();
            UpdateHandCardGraphic();
        }

        public void RemoveNumberOfOnHandCards(int removeCount)
        {
            int i = int.Parse(txtNumberOfOnHandCards.text);
            i -= removeCount;
            txtNumberOfOnHandCards.text = i.ToString();
            UpdateHandCardGraphic();

        }

        public void UpdateHandCardGraphic()
        {
            int cardCount = int.Parse(txtNumberOfOnHandCards.text);
            var cardcontainer = OnHandCardsGroup.transform.GetChild(0);
            if (cardcontainer.childCount < cardCount)
            {
                for (int i = 0, il = cardcontainer.childCount; i < cardCount - il; i++)
                {
                    Instantiate(cardcontainer.GetChild(0), cardcontainer);
                }
            }
            else if (cardcontainer.childCount > cardCount)
            {
                Debug.Log(cardcontainer.childCount - cardCount);
                for (int i = 0, il = cardcontainer.childCount; i < il - cardCount; i++)
                {
                    Destroy(cardcontainer.GetChild(i).gameObject);
                }
            }

            if (cardCount >= 10)
            {
                cardcontainer.GetComponent<UnityUI.HorizontalLayoutGroup>().spacing = -11;
            }
            else
            {
                cardcontainer.GetComponent<UnityUI.HorizontalLayoutGroup>().spacing = -5;

            }

        }

        public void SetOnHandCardsGroupVisible(bool visible)
        {
            OnHandCardsGroup.SetActive(visible);
        }

        public void SetActionTime(float actionTime)
        {
            mActionTime = actionTime;
            mActionTimer = 0;
            timeCount = 10;
        }
        public void SetActiveHeadIcon(bool visible)
        {
            imgHeadIcon.gameObject.SetActive(visible);
        }
        public void SetActiveSpadeIcon(bool visible)
        {
            imgHeartIcon.gameObject.SetActive(visible);
        }
        public void SetActiveClubIcon(bool visible)
        {
            imgCoverIcon.gameObject.SetActive(visible);
        }

        public void SetActionTimeIndicatorVisible(bool visible)
        {
            //imgActionTimeIndicator.gameObject.SetActive(visible);
            tableHighlight.gameObject.SetActive(visible);
        }
        public void SetActionTimeText(bool visible)
        {
            txtTime.gameObject.SetActive(visible);
            panelTime.gameObject.SetActive(visible);
        }
        public void UpdatePlayerInfo(DMPlayer player)
        {
            SetPlayerName(player.userId);
            SetPlayerScore(player.score);
            SetPlayerIndex(player.playerIndex);
        }

        string getTypeScore(byte type)
        {
            string _typeScore;

            switch (type)
            {
                case (byte)typeScore.dropFull:
                    _typeScore = "ทิ้งเต็ม";
                    break;
                case (byte)typeScore.dropHead:
                    _typeScore = "ปี้หัว";
                    break;
                case (byte)typeScore.dropSpeto:
                    _typeScore = "ทิ้งสเปโต";
                    break;
                case (byte)typeScore.dropFool:
                    _typeScore = "ทิ้งโง่";
                    break;
                case (byte)typeScore.dropDummy:
                    _typeScore = "ทิ้งดั้มมี้";
                    break;
                case (byte)typeScore.gotDummySpeto:
                    _typeScore = "ถูกฝากสเปโต";
                    break;
                case (byte)typeScore.Knock:
                    _typeScore = "น๊อค";
                    break;
                case (byte)typeScore.OneTurn:
                    _typeScore = "น๊อคมืด";
                    break;
                case (byte)typeScore.Color:
                    _typeScore = "น๊อคสี";
                    break;
                case (byte)typeScore.ColorAndOneTurn:
                    _typeScore = "น๊อคมืด และ สี";
                    break;
                case (byte)typeScore.NoOneTurn:
                    _typeScore = "ลบมืด";
                    break;
                case (byte)typeScore.playHead:
                    _typeScore = "ได้หัว";
                    break;
                case (byte)typeScore.playSpeto:
                    _typeScore = "ได้สเปโต";
                    break;
                default:
                    _typeScore = "";
                    break;
            }
            return _typeScore;
        }

        public void displayMeldStatus(string playerIndexGotCondition, string condition, PlayerUIController[] remotePlayerUIController)
        {
            if (playerIndexGotCondition.Length > 0)
            {
                string[] arrayTargetIndexs = playerIndexGotCondition.Split(',');
                string[] arrayTargetConditions = condition.Split(',');
                int i = 0;

                foreach (var item in arrayTargetIndexs)
                {
                    byte index = (byte)Int32.Parse(item);
                    if (index == this.PlayerIndex())
                    {
                        this.AddStatus((byte)Int32.Parse(arrayTargetConditions[i]));
                    }

                    foreach (var remote in remotePlayerUIController)
                    {
                        if (index == remote.PlayerIndex())
                        {
                            remote.AddStatus((byte)Int32.Parse(arrayTargetConditions[i]));
                            break;
                        }
                    }
                    i++;
                }
            }
        }
        int getScore(byte type)
        {
            int score;

            switch (type)
            {
                case (byte)typeScore.dropFull:
                    score = -50;
                    break;
                case (byte)typeScore.dropHead:
                    score = -50;
                    break;
                case (byte)typeScore.dropSpeto:
                    score = -50;
                    break;
                case (byte)typeScore.dropFool:
                    score = -50;
                    break;
                case (byte)typeScore.dropDummy:
                    score = -50;
                    break;
                case (byte)typeScore.gotDummySpeto:
                    score = -50;
                    break;
                case (byte)typeScore.Knock:
                    score = 50;
                    break;
                case (byte)typeScore.OneTurn:
                    score = 2;
                    break;
                case (byte)typeScore.Color:
                    score = 2;
                    break;
                case (byte)typeScore.ColorAndOneTurn:
                    score = 4;
                    break;
                case (byte)typeScore.NoOneTurn:
                    score = 2;
                    break;
                case (byte)typeScore.playHead:
                    score = 50;
                    break;
                case (byte)typeScore.playSpeto:
                    score = 50;
                    break;
                default:
                    score = 0;
                    break;
            }
            return score;
        }

        public void SetupResult(string _data, bool isSelf = false)
        {
            string[] data = _data.Split('@');
            string knockTxt = "";
            string knockScore = "";
            // Debug.LogErrorFormat("score : {0}", scorePlayer);

            var scoreList = new List<string>();
            var typeList = new List<string>();


            //foreach (var data_player in data)
            for (int i = 0; i < 4; i++)
            {
                var data_player = data[i];

                if (data_player.Length < 1)
                {
                    continue;
                }
                string playerindex = data_player.Split('#')[0];
                string data_scores = data_player.Split('#')[1];
                string data_handCards = data_player.Split('#')[2];
                string data_usedCards = data_player.Split('#')[3];
                if (this.PlayerIndex().ToString() == playerindex)
                {
                    foreach (var data_score in data_scores.Split('|'))
                    {
                        if (data_score.Length < 1)
                        {
                            continue;
                        }
                        string type = data_score.Split('=')[0];
                        string score = data_score.Split('=')[1];
                        if (score == "0")
                        {
                            continue;
                        }

                        if ((byte)Int32.Parse(type) == (byte)typeScore.Knock ||
                        (byte)Int32.Parse(type) == (byte)typeScore.Color ||
                        (byte)Int32.Parse(type) == (byte)typeScore.OneTurn ||
                        (byte)Int32.Parse(type) == (byte)typeScore.ColorAndOneTurn)
                        {
                            knockTxt += getTypeScore((byte)typeScore.Knock);
                            knockScore += getScore((byte)typeScore.Knock).ToString();
                            if (
                                                    (byte)Int32.Parse(type) == (byte)typeScore.Color ||
                                                    (byte)Int32.Parse(type) == (byte)typeScore.OneTurn ||
                                                    (byte)Int32.Parse(type) == (byte)typeScore.ColorAndOneTurn)
                            {
                                knockTxt += getTypeScore((byte)Int32.Parse(type));
                                knockScore += "x " + getScore((byte)Int32.Parse(type)).ToString();
                                scorePlayer /= getScore((byte)Int32.Parse(type));
                            }
                            scorePlayer -= getScore((byte)typeScore.Knock);
                            continue;
                        }
                        else if ((byte)Int32.Parse(type) == (byte)typeScore.NoOneTurn)
                        {
                            knockTxt += getTypeScore((byte)Int32.Parse(type));
                            knockScore += "x " + getScore((byte)Int32.Parse(type)).ToString();
                            scorePlayer /= getScore((byte)typeScore.NoOneTurn);
                            continue;
                        }

                        scorePlayer -= (getScore((byte)Int32.Parse(type)) * Int32.Parse(score));

                        //resultPlayer.transform.Find("type").GetComponent<UnityUI.Text>().text += getTypeScore((byte)Int32.Parse(type)) + "\n";
                        //resultPlayer.transform.Find("score").GetComponent<UnityUI.Text>().text += (getScore((byte)Int32.Parse(type)) * Int32.Parse(score)).ToString() + "\n";

                        typeList.Add(getTypeScore((byte)Int32.Parse(type)));
                        scoreList.Add((getScore((byte)Int32.Parse(type)) * Int32.Parse(score)).ToString());
                        //resultPlayer.transform.Find("score").GetComponent<UnityUI.Text>().text += (getScore((byte)Int32.Parse(type)) * Int32.Parse(score)).ToString() + "\n";
                    }

                    //Debug.Log("Add hand card:");
                    foreach (var data_handCard in data_handCards.Split('|'))
                    {
                        if (data_handCard.Length < 1)
                        {
                            continue;
                        }
                        resultPlayerController.AddHandCard(DMCard.fromUInt16(Convert.ToUInt16(data_handCard)));
                    }
                    //Debug.Log("Add used card:");
                    foreach (var data_usedCard in data_usedCards.Split('|'))
                    {
                        if (data_usedCard.Length < 1)
                        {
                            continue;
                        }
                        resultPlayerController.AddUsedCard(DMCard.fromUInt16(Convert.ToUInt16(data_usedCard)));
                    }
                    resultPlayerController.RearrangeCards();

                    resultPlayerController.SetUsedCardScore();
                    resultPlayerController.SetHandCardScore();


                    //Debug.Log("ลงไพ่\n" + resultPlayer.transform.Find("type").GetComponent<UnityUI.Text>().text);
                    //Debug.Log(scorePlayer.ToString() + "\n" + resultPlayer.transform.Find("score").GetComponent<UnityUI.Text>().text);

                    //Debug.Log(knockTxt);
                    //Debug.Log(knockScore);

                    int spacialScore = 0;

                    for (int j = 0; j < scoreList.Count; j++)
                    {
                        resultPlayer.transform.Find("type").GetComponent<UnityUI.Text>().text += typeList[j] + "\t" + scoreList[j] + "\n";
                        spacialScore += Int32.Parse(scoreList[j]);
                    }

                    if (knockTxt != "")
                    {
                        resultPlayer.transform.Find("type").GetComponent<UnityUI.Text>().text += knockTxt + "\t" + knockScore;
                    }

                    // resultPlayer.transform.Find("score").GetComponent<UnityUI.Text>().text = scorePlayer.ToString();

                    resultPlayer.transform.Find("textSpecialScore").GetComponent<UnityUI.Text>().text = spacialScore.ToString();

                    // resultPlayer.transform.Find("score").GetComponent<UnityUI.Text>().text = scorePlayer.ToString() + "\n" + resultPlayer.transform.Find("score").GetComponent<UnityUI.Text>().text;
                    // resultPlayer.transform.Find("score").GetComponent<UnityUI.Text>().text += knockScore;
                    // resultPlayer.transform.Find("score").GetComponent<UnityUI.Text>().text += "รวม " + txtPlayerScore.text + "\n";

                    resultPlayer.transform.Find("score").GetComponent<UnityUI.Text>().text = txtPlayerScore.text;
                }
            }

            if (isSelf)
            {
                var data_discardPile = data[4];
                var data_stockPile = data[5];

                foreach (var card in data_discardPile.Split('|'))
                {
                    if (card.Length < 1)
                    {
                        continue;
                    }
                    resultPlayerController.AddDiscardPile(DMCard.fromUInt16(Convert.ToUInt16(card)));
                }
                resultPlayerController.RearrangeDiscardPile();
                foreach (var card in data_stockPile.Split('|'))
                {
                    if (card.Length < 1)
                    {
                        continue;
                    }
                    resultPlayerController.AddStockPile(DMCard.fromUInt16(Convert.ToUInt16(card)));
                }
                resultPlayerController.RearrangeStockPile();
            }

        }

    }

}