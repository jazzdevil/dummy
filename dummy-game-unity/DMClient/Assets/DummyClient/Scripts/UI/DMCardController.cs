﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityUI = UnityEngine.UI;

using System.Collections;
using System.Collections.Generic;

using Dummy.DMProtocol;
using System;

namespace Dummy.Client.UI
{

    /// <summary>
    /// 
    /// </summary>
    public class DMCardController : MonoBehaviour, IPointerClickHandler
    {

        static Dictionary<ushort, Sprite> s_CardSpriteCache;

        public UnityUI.Image cardImage;
        public UnityUI.Image specialImage;
        public UnityUI.Image headImage;
        public UnityUI.Image markImage;
        public List<Sprite> markImage_sprites; //follow this order: blue red green yellow
        public event Action<DMCardController> onClicked;
        DMCard mCard;

        public void SetCard(DMCard card)
        {
            // To set sprite for Card's image, Get it from s_CardSpriteCache which will load
            // on-demand sprite and cahced it for reducing load time.
            mCard = card;
            cardImage.sprite = GetSpriteForCard(card);
            //if (mCard.isSpecialCard())
            //{
            //    specialImage.gameObject.SetActive(true);
            //}

            if (mCard.isHeadCard)
            {
                headImage.gameObject.SetActive(true);
            }
#if UNITY_EDITOR
            // Debug.Log(this.gameObject.name);
            // Debug.Log(cardImage.sprite.name);
            this.gameObject.name = string.Format("Card-{0}", cardImage.sprite.name);
#endif
        }

        public DMCard GetCard()
        {
            return mCard;
        }

        Sprite GetSpriteForCard(DMCard card)
        {
            ushort c = card.ToUInt16();
            if (s_CardSpriteCache == null)
            {
                s_CardSpriteCache = new Dictionary<ushort, Sprite>();
            }
            if (!s_CardSpriteCache.ContainsKey(c))
            {
                string spriteName = "";

                // Rank
                if (card.rank >= 2 && card.rank <= 10) spriteName = card.rank.ToString();
                else if (card.rank == 11) spriteName = "j";
                else if (card.rank == 12) spriteName = "q";
                else if (card.rank == 13) spriteName = "k";
                else if (card.rank == 14) spriteName = "a";

                // Kind
                if (card.kind == DMCard.CardKindEnum.Clubs)
                {
                    spriteName += "c";
                }
                else if (card.kind == DMCard.CardKindEnum.Diamonds)
                {
                    spriteName += "d";
                }
                else if (card.kind == DMCard.CardKindEnum.Hearts)
                {
                    spriteName += "h";
                }
                else if (card.kind == DMCard.CardKindEnum.Spades)
                {
                    spriteName += "s";
                }

                Debug.Log(spriteName);

                Sprite sp = Resources.Load<Sprite>("DummyClient/Textures/Cards/" + spriteName);
                s_CardSpriteCache[c] = sp;
            }

            return s_CardSpriteCache[c];
        }

        public void assignLayoffMark(int playerIndex)
        {
            markImage.sprite = markImage_sprites[playerIndex];
            markImage.gameObject.SetActive(true);
            //switch (playerIndex)
            //{
            //    case 0:
            //        this.transform.GetChild(0).GetComponent<UnityEngine.UI.Image>().color = Color.blue;
            //        break;
            //    case 1:
            //        this.transform.GetChild(0).GetComponent<UnityEngine.UI.Image>().color = Color.red;
            //        break;
            //    case 2:
            //        this.transform.GetChild(0).GetComponent<UnityEngine.UI.Image>().color = Color.green;
            //        break;
            //    case 3:
            //        this.transform.GetChild(0).GetComponent<UnityEngine.UI.Image>().color = Color.yellow;
            //        break;
            //}
        }
        public void OnPointerClick(PointerEventData eventData)
        {
            Debug.Log("[DMCardController] clicked at me: " + mCard.CardName());
            if (onClicked != null)
            {
                onClicked(this);
            }
        }
    }

}