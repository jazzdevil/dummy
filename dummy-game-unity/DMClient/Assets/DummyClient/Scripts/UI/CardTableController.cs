﻿using UnityEngine;
using UnityUI = UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using Dummy.DMProtocol;

namespace Dummy.Client.UI {

    public class CardTableController : MonoBehaviour {

        [Header ("UI")]
        public CardPileController discardCardTable;
        public UnityUI.Text txtNumberOfStockCard;

        [Header ("Player's area")]
        public MeldingAreaController localPlayerMeldingArea;
        public MeldingAreaController[] remotePlayerMeldingAreas;

        public void SetNumberOfStockCards (int numberOfStockCards) {
            txtNumberOfStockCard.text = numberOfStockCards.ToString ();
        }

        public int GetNumberOfStockCards () {
            return Int32.Parse (txtNumberOfStockCard.text);
        }

        public DMMeldingCard FindMeldingCardForLayOffCard (DMCard card) {
            List<MeldingAreaController> meldingAreas = new List<MeldingAreaController> ();
            meldingAreas.Add (localPlayerMeldingArea);
            meldingAreas.AddRange (remotePlayerMeldingAreas);
            for (int i = 0; i < meldingAreas.Count; i++) {
                MeldingAreaController meldingArea = meldingAreas[i];
                DMMeldingCard meldingCard = meldingArea.FindMeldingCardForLayoff (card);
                if (meldingCard != null) {
                    return meldingCard;
                }
            }
            return null;
        }

         public DMMeldingCard FindMeldingCardForLayOffCard (List<DMCard> cards) {
            List<MeldingAreaController> meldingAreas = new List<MeldingAreaController> ();
            meldingAreas.Add (localPlayerMeldingArea);
            meldingAreas.AddRange (remotePlayerMeldingAreas);
            for (int i = 0; i < meldingAreas.Count; i++) {
                MeldingAreaController meldingArea = meldingAreas[i];
                DMMeldingCard meldingCard = meldingArea.FindMeldingCardForLayoff (cards);
                if (meldingCard != null) {
                    return meldingCard;
                }
            }
            return null;
        }
    }

}