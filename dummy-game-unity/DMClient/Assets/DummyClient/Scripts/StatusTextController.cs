﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatusTextController : MonoBehaviour
{
    public static StatusTextController instance;
    public Text text_obj;
    string currentText;
    List<string> text_log = new List<string>();

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
            Destroy(this.gameObject);
        }
    }

    public void setStatusText(string text)
    {
        currentText = text;
        text_log.Add(text);

        //Debug.Log("Statustext: " + currentText);

        text_obj.text = currentText;
    }

}
