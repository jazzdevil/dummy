﻿using System;
using System.Collections;
using System.Collections.Generic;
using Dummy.DMProtocol;
using Dummy.Client.Audio;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Dummy.Client
{

    public class DMLobbyController : MonoBehaviour, IMessageReceivable
    {

        public string gameRoomSceneName = "GameRoom";
        Queue<DMBaseMessage> mMessageQueue;

        void Awake()
        {
            mMessageQueue = new Queue<DMBaseMessage>();
        }

        void Start()
        {
            var client = DMClient.GetInstance();
            client.RegisterMessageReceiver(DMProtocolConstants.kMsgId_RespJoinRoom, this);
        }

        public void JoinRoom()
        {
            var client = DMClient.GetInstance();
            client.RandomMockupRoomId();
            client.Send(new ReqJoinRoomMessage()
            {
                roomId = client.mockupRoomId,
                userId = client.mockupUserId
            });
        }

        public void Receive(ushort messageId, byte[] rawMessage)
        {
            if (messageId == DMProtocol.DMProtocolConstants.kMsgId_RespJoinRoom)
            {
                var message = DMProtocolUtility.CreateDMMessageFromBuffer<RespJoinRoomMessage>(rawMessage);

                mMessageQueue.Enqueue(message);
            }
        }

        public void Process()
        {
            while (mMessageQueue.Count > 0)
            {
                DMBaseMessage msg = mMessageQueue.Dequeue();
                switch (msg.messageID)
                {
                    case DMProtocolConstants.kMsgId_RespJoinRoom:
                        {
                            RespJoinRoomMessage message = msg as RespJoinRoomMessage;
                            if (message.success)
                            {
                                if (message.roomID != "")
                                {
                                    DMClient.GetInstance().mockupRoomId = message.roomID;
                                }
                                SceneManager.LoadScene(gameRoomSceneName);
                                AudioManager.instance.PlayBGM("bgm_title");
                                Debug.Log("<color=magenta> work from change bgm to bgm_title </color>");
                            }
                        }
                        break;

                    default:
                        break;
                }
            }
        }

        public string Tag()
        {
            return "DMLobbyController";
        }
    }

}