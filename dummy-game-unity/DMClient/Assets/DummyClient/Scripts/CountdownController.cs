﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityUI = UnityEngine.UI;

using DigitalRuby.Tween;
public class CountdownController : MonoBehaviour
{

    public enum CountdownType
    {
        Countdown = 0,
        ReadyToStart
    }
    public CountdownType countdownType;

    public GameObject ribbon;
    public GameObject text;
    public GameObject ribbon_circle;
    public GameObject ribbon_base;

    public Sprite text_img_3;
    public Sprite text_img_2;
    public Sprite text_img_1;
    public Sprite text_img_ready;
    public Sprite text_img_start;

    // Start is called before the first frame update
    void Start()
    {
        System.Action<ITween<float>> ribbon_circle_updateTween = (t) =>
        {
            ribbon_circle.GetComponent<RectTransform>().localScale = Vector3.one * t.CurrentValue;
        };
        //tween effect
        var ribbon_circle_tween = ribbon_circle.gameObject.Tween(null, 0f, 1f, 0.2f, TweenScaleFunctions.CubicEaseIn, ribbon_circle_updateTween);

        System.Action<ITween<float>> ribbon_base_updateTween = (t) =>
        {
            ribbon_base.GetComponent<RectTransform>().localScale = Vector3.one * t.CurrentValue;
        };
        //tween effect
        var ribbon_base_tween = ribbon_base.gameObject.Tween(null, 0f, 1f, 0.2f, TweenScaleFunctions.CubicEaseIn, ribbon_base_updateTween);
        ribbon_base_tween.Delay = 0.25f;

        switch (countdownType)
        {
            case CountdownType.Countdown:
                CountdownTween();
                break;
            case CountdownType.ReadyToStart:
                readyToStartTween();
                break;
        }
    }

    void CountdownTween()
    {

        text.GetComponent<UnityUI.Image>().sprite = text_img_3;

        System.Action<ITween<float>> text_updateTween = (t) =>
        {
            text.GetComponent<RectTransform>().localScale = Vector3.one * t.CurrentValue.Remap(0, 1, 3, 1);
            text.GetComponent<CanvasGroup>().alpha = t.CurrentProgress;
        };

        System.Action<ITween<float>> text_completeTween_1 = (t) =>
        {
            text.GetComponent<UnityUI.Image>().sprite = text_img_2;
        };

        System.Action<ITween<float>> text_completeTween_2 = (t) =>
        {
            text.GetComponent<UnityUI.Image>().sprite = text_img_1;
        };

        System.Action<ITween<float>> text_completeTween_3 = (t) =>
        {
            text.GetComponent<UnityUI.Image>().sprite = text_img_start;
        };


        System.Action<ITween<float>> total_updateTween = (t) =>
        {
            this.GetComponent<RectTransform>().localScale = Vector3.one * t.CurrentValue.Remap(0, 1, 1, 5);
            this.GetComponent<CanvasGroup>().alpha = t.CurrentProgress.Remap(0, 1, 1, 0);
        };

        System.Action<ITween<float>> total_completeTween = (t) =>
        {
            Destroy(this.gameObject);
        };


        text.gameObject.Tween(null, 0f, 1f, 1, TweenScaleFunctions.CubicEaseOut, text_updateTween, text_completeTween_1)
        .ContinueWith(new FloatTween().Setup(0f, 1f, 1, TweenScaleFunctions.CubicEaseOut, text_updateTween, text_completeTween_2))
        .ContinueWith(new FloatTween().Setup(0f, 1f, 1, TweenScaleFunctions.CubicEaseOut, text_updateTween, text_completeTween_3))
        .ContinueWith(new FloatTween().Setup(0f, 1f, 1, TweenScaleFunctions.CubicEaseOut, text_updateTween))
        .ContinueWith(new FloatTween().Setup(0f, 1f, 0.3f, TweenScaleFunctions.CubicEaseOut, total_updateTween, total_completeTween));
    }

    void readyToStartTween()
    {
        text.GetComponent<UnityUI.Image>().sprite = text_img_ready;

        System.Action<ITween<float>> text_updateTween = (t) =>
        {
            text.GetComponent<RectTransform>().localScale = Vector3.one * t.CurrentValue.Remap(0, 1, 3, 1);
            text.GetComponent<CanvasGroup>().alpha = t.CurrentProgress;
        };

        System.Action<ITween<float>> text_completeTween_1 = (t) =>
        {
            text.GetComponent<UnityUI.Image>().sprite = text_img_start;
        };


        System.Action<ITween<float>> total_updateTween = (t) =>
        {
            this.GetComponent<RectTransform>().localScale = Vector3.one * t.CurrentValue.Remap(0, 1, 1, 5);
            this.GetComponent<CanvasGroup>().alpha = t.CurrentProgress.Remap(0, 1, 1, 0);
        };

        System.Action<ITween<float>> total_completeTween = (t) =>
        {
            Destroy(this.gameObject);
        };


        text.gameObject.Tween(null, 0f, 1f, 1, TweenScaleFunctions.CubicEaseOut, text_updateTween, text_completeTween_1)
        .ContinueWith(new FloatTween().Setup(0f, 1f, 1, TweenScaleFunctions.CubicEaseOut, text_updateTween))
        .ContinueWith(new FloatTween().Setup(0f, 1f, 0.3f, TweenScaleFunctions.CubicEaseOut, total_updateTween, total_completeTween));

    }

    public float getTotalDuration()
    {
        switch (countdownType)
        {
            case CountdownType.Countdown:
                return 4f;
            case CountdownType.ReadyToStart:
                return 2f;
            default:
                return 0f;
        }
    }

    // Update is called once per frame
    void Update()
    {
        ribbon_circle.GetComponent<RectTransform>().Rotate(Vector3.forward * 360 * 0.75f * Time.deltaTime);
    }
}
