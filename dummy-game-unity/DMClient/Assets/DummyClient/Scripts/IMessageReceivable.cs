﻿using UnityEngine;
using System.Collections;

using Dummy.DMProtocol;

namespace Dummy.Client {
    public interface IMessageReceivable {

        string Tag();
        void Receive(ushort messageId, byte[] rawMessage);
        void Process();

    }
}