﻿using UnityEngine;
using UnityUI = UnityEngine.UI;

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Dummy.Client.UI;
using Dummy.Client.Audio;
using Dummy.DMProtocol;
using UnityEngine.SceneManagement;

namespace Dummy.Client
{

    public class DMGameRoomController : MonoBehaviour,
        IMessageReceivable,
        PlayerOnHandCardsController.PlayerOnHandCardsControllerRequesterDelegate
    {
        public enum GameRoomState
        {
            WaitForSelfReady,
            WaitForAllReady,
            WaitForSelfEndedCountdown,
            WaitForAllEndedCountdown,
            StartingCardDeal,
            Playing,
            End
        }
        DMClient mClient;
        Queue<DMBaseMessage> mMessageQueue;
        GameRoomState mState;
        GameRoomState mRequestNextState;
        int mSubState;

        DMPlayer mLocalPlayer;
        List<DMPlayer> mRemotePlayers;

        [Header("UI - Wait for ready")]
        public GameObject waitForReadyGroup;
        public UnityUI.Button bttnReady;

        [Header("UI - Player UI")]
        public GameObject playerUIControllerGroup;
        public UI.PlayerUIController playerUIController;
        public UI.PlayerUIController[] remotePlayerUIController;

        [Header("UI - On hand card controller")]
        public PlayerOnHandCardsController playerOnHandCardsController;
        public CardTableController cardTableController;

        [Header("UI - CardTable")]
        public CardTableController cardTable;

        [Header("UI - Result Screen")]
        List<DMCard> mLocalPlayerCards;
        public GameObject resultScreenGroup;
        public UnityUI.Button bttnExit;

        [Header("UI - Countdown")]
        public GameObject countdownPrefabs;
        public CountdownController.CountdownType countdownType;

        private bool isCountdownEndedTriggered = false;

        void Awake()
        {
            mClient = DMClient.GetInstance();
            mClient.RegisterMessageReceiver(DMProtocolConstants.kMsgId_InitGameRoom, this);
            mClient.RegisterMessageReceiver(DMProtocolConstants.kMsgId_RespReadyToPlay, this);
            mClient.RegisterMessageReceiver(DMProtocolConstants.kMsgId_PlayerReadyToPlay, this);
            mClient.RegisterMessageReceiver(DMProtocolConstants.kMsgId_UpdateRoomStatus, this);
            mClient.RegisterMessageReceiver(DMProtocolConstants.kMsgId_StartingCardsDeal, this);
            mClient.RegisterMessageReceiver(DMProtocolConstants.kMsgId_RespQueryTurnState, this);
            mClient.RegisterMessageReceiver(DMProtocolConstants.kMsgId_TurnChange, this);
            mClient.RegisterMessageReceiver(DMProtocolConstants.kMsgId_RespDrawStockCard, this);
            mClient.RegisterMessageReceiver(DMProtocolConstants.kMsgId_PlayerDrawStockCard, this);
            mClient.RegisterMessageReceiver(DMProtocolConstants.kMsgId_RespDiscardCard, this);
            mClient.RegisterMessageReceiver(DMProtocolConstants.kMsgId_PlayerDiscardCard, this);
            mClient.RegisterMessageReceiver(DMProtocolConstants.kMsgId_RespMeldingCard, this);
            mClient.RegisterMessageReceiver(DMProtocolConstants.kMsgId_PlayerMeldingCard, this);
            mClient.RegisterMessageReceiver(DMProtocolConstants.kMsgId_RespPickupAndMeldingCard, this);
            mClient.RegisterMessageReceiver(DMProtocolConstants.kMsgId_PlayerPickupAndMeldingCard, this);
            mClient.RegisterMessageReceiver(DMProtocolConstants.kMsgId_RespLayoffCard, this);
            mClient.RegisterMessageReceiver(DMProtocolConstants.kMsgId_PlayerLayoffCard, this);
            mClient.RegisterMessageReceiver(DMProtocolConstants.kMsgId_PlayerScore, this);
            mClient.RegisterMessageReceiver(DMProtocolConstants.kMsgId_otherPlayerScore, this);
            mClient.RegisterMessageReceiver(DMProtocolConstants.kMsgId_leaveAndRemoveRoom, this);
            mClient.RegisterMessageReceiver(DMProtocolConstants.kMsgId_RespKnockCard, this);
            mClient.RegisterMessageReceiver(DMProtocolConstants.kMsgId_PlayerKnockCard, this);
            mClient.RegisterMessageReceiver(DMProtocolConstants.kMsgId_RespPassLastTurn, this);
            mClient.RegisterMessageReceiver(DMProtocolConstants.kMsgId_PlayerPassLastTurn, this);
            mClient.RegisterMessageReceiver(DMProtocolConstants.kMsgId_PlayerCountTypeScore, this);
            mClient.RegisterMessageReceiver(DMProtocolConstants.kMsgId_RespEndCountdown, this);

            mMessageQueue = new Queue<DMBaseMessage>();
            mState = GameRoomState.WaitForSelfReady;

            mLocalPlayer = new DMPlayer();
            mRemotePlayers = new List<DMPlayer>();

            mLocalPlayerCards = new List<DMCard>();

            bttnReady.onClick.AddListener(() =>
            {
                AudioManager.instance.PlaySFX("button_sfx");
                Debug.Log("<color=yellow> work from bttnReady </color>");
                var msg = new ReqReadyToPlayMessage();
                SetRoomIdBeforeSend(msg);
            });

            bttnExit.onClick.AddListener(() =>
            {
                AudioManager.instance.PlaySFX("button_sfx");
                Debug.Log("<color=yellow> work from bttnExit </color>");
                leaveAndRemoveRoom(DMClient.GetInstance().mockupRoomId);
                SceneManager.LoadScene("Login");
                AudioManager.instance.PlayBGM("bgm02");
                Debug.Log("<color=magenta> work from change bgm to bgm02 </color>");
            });

            playerOnHandCardsController.delegateObject = this;

            ReqUpdateRoomStatusMessage m = new ReqUpdateRoomStatusMessage();
            SetRoomIdBeforeSend(m);
        }

        public string Tag()
        {
            return "GameRoom";
        }

        public void Receive(ushort messageId, byte[] rawMessage)
        {

            DMBaseMessage message = null;
            switch (messageId)
            {
                case DMProtocolConstants.kMsgId_InitGameRoom:
                    message = DMProtocolUtility.CreateDMMessageFromBuffer<InitGameRoomMessage>(rawMessage);
                    break;
                case DMProtocolConstants.kMsgId_RespReadyToPlay:
                    message = DMProtocolUtility.CreateDMMessageFromBuffer<RespReadyToPlayMessage>(rawMessage);
                    break;
                case DMProtocolConstants.kMsgId_UpdateRoomStatus:
                    message = DMProtocolUtility.CreateDMMessageFromBuffer<UpdateRoomStatusMessage>(rawMessage);
                    break;
                case DMProtocolConstants.kMsgId_StartingCardsDeal:
                    message = DMProtocolUtility.CreateDMMessageFromBuffer<StartingCardsDealMessage>(rawMessage);
                    break;
                case DMProtocolConstants.kMsgId_RespQueryTurnState:
                    message = DMProtocolUtility.CreateDMMessageFromBuffer<RespQueryTurnStateMessage>(rawMessage);
                    break;
                case DMProtocolConstants.kMsgId_TurnChange:
                    message = DMProtocolUtility.CreateDMMessageFromBuffer<TurnChangeMessage>(rawMessage);
                    break;
                case DMProtocolConstants.kMsgId_RespDrawStockCard:
                    message = DMProtocolUtility.CreateDMMessageFromBuffer<RespDrawStockCardMessage>(rawMessage);
                    break;
                case DMProtocolConstants.kMsgId_PlayerDrawStockCard:
                    message = DMProtocolUtility.CreateDMMessageFromBuffer<PlayerDrawStockCardMessage>(rawMessage);
                    break;
                case DMProtocolConstants.kMsgId_RespDiscardCard:
                    message = DMProtocolUtility.CreateDMMessageFromBuffer<RespDiscardCardMessage>(rawMessage);
                    break;
                case DMProtocolConstants.kMsgId_PlayerDiscardCard:
                    message = DMProtocolUtility.CreateDMMessageFromBuffer<PlayerDiscardCardMessage>(rawMessage);
                    break;
                case DMProtocolConstants.kMsgId_RespMeldingCard:
                    message = DMProtocolUtility.CreateDMMessageFromBuffer<RespMeldingCardMessage>(rawMessage);
                    break;
                case DMProtocolConstants.kMsgId_PlayerMeldingCard:
                    message = DMProtocolUtility.CreateDMMessageFromBuffer<PlayerMeldingCardMessage>(rawMessage);
                    break;
                case DMProtocolConstants.kMsgId_RespPickupAndMeldingCard:
                    message = DMProtocolUtility.CreateDMMessageFromBuffer<RespPickupAndMeldingCardMessage>(rawMessage);
                    break;
                case DMProtocolConstants.kMsgId_PlayerPickupAndMeldingCard:
                    message = DMProtocolUtility.CreateDMMessageFromBuffer<PlayerPickupAndMeldingCardMessage>(rawMessage);
                    break;
                case DMProtocolConstants.kMsgId_RespLayoffCard:
                    message = DMProtocolUtility.CreateDMMessageFromBuffer<RespLayoffCardMessage>(rawMessage);
                    break;
                case DMProtocolConstants.kMsgId_PlayerLayoffCard:
                    message = DMProtocolUtility.CreateDMMessageFromBuffer<PlayerLayoffCardMessage>(rawMessage);
                    break;
                case DMProtocolConstants.kMsgId_PlayerScore:
                    message = DMProtocolUtility.CreateDMMessageFromBuffer<PlayerScoreMessage>(rawMessage);
                    break;
                case DMProtocolConstants.kMsgId_otherPlayerScore:
                    message = DMProtocolUtility.CreateDMMessageFromBuffer<otherPlayerScoreMessage>(rawMessage);
                    break;
                case DMProtocolConstants.kMsgId_RespKnockCard:
                    message = DMProtocolUtility.CreateDMMessageFromBuffer<RespKnockCardMessage>(rawMessage);
                    break;
                case DMProtocolConstants.kMsgId_PlayerKnockCard:
                    message = DMProtocolUtility.CreateDMMessageFromBuffer<PlayerKnockCardMessage>(rawMessage);
                    break;
                case DMProtocolConstants.kMsgId_RespPassLastTurn:
                    message = DMProtocolUtility.CreateDMMessageFromBuffer<RespPassLastTurnMessage>(rawMessage);
                    break;
                case DMProtocolConstants.kMsgId_PlayerPassLastTurn:
                    message = DMProtocolUtility.CreateDMMessageFromBuffer<PlayerPassLastTurnMessage>(rawMessage);
                    break;
                case DMProtocolConstants.kMsgId_PlayerCountTypeScore:
                    message = DMProtocolUtility.CreateDMMessageFromBuffer<PlayerCountTypeScore>(rawMessage);
                    break;
                case DMProtocolConstants.kMsgId_RespEndCountdown:
                    message = DMProtocolUtility.CreateDMMessageFromBuffer<RespEndCountdownMessage>(rawMessage);
                    break;
                default:
                    Debug.LogErrorFormat("[DMGameRoomController] cannot receive message Id: {0}", DMProtocolConstants.MsgIdAsString(messageId));
                    break;
            }

            mMessageQueue.Enqueue(message);
        }

        public void Process()
        {
            while (mMessageQueue.Count > 0)
            {
                DMBaseMessage msg = mMessageQueue.Dequeue();
                if (msg.roomID != mClient.mockupRoomId)
                {
                    Debug.LogErrorFormat("Not This ROOM MAN!! {0} {1}", msg.roomID, msg.messageID);
                    return;
                }
                switch (msg.messageID)
                {
                    case DMProtocolConstants.kMsgId_InitGameRoom:
                        {

                        }
                        break;
                    case DMProtocolConstants.kMsgId_RespReadyToPlay:
                        {
                            RequestChangeState(GameRoomState.WaitForAllReady);
                        }
                        break;
                    case DMProtocolConstants.kMsgId_RespEndCountdown:
                        {
                            RequestChangeState(GameRoomState.WaitForAllEndedCountdown);
                        }
                        break;
                    case DMProtocolConstants.kMsgId_UpdateRoomStatus:
                        {
                            UpdateRoomStatusMessage message = msg as UpdateRoomStatusMessage;
                            int n = message.numberOfPlayers;
                            while (mRemotePlayers.Count > n - 1)
                            {
                                mRemotePlayers.RemoveAt(0);
                            }
                            while (mRemotePlayers.Count < n - 1)
                            {
                                mRemotePlayers.Add(new DMPlayer());
                            }

                            int idx = 0;
                            for (int i = 0; i < message.userIds.Length; i++)
                            {
                                if (message.userIds[i] == mClient.mockupUserId)
                                {
                                    mLocalPlayer.playerIndex = i;
                                    mLocalPlayer.userId = mClient.mockupUserId;
                                    mLocalPlayer.score = 0;
                                    if (DMClient.GetInstance().isGuest)
                                    {
                                        mLocalPlayer.userId = "Guest";
                                    }
                                    playerUIController.UpdatePlayerInfo(mLocalPlayer);
                                    playerUIController.SetOnHandCardsGroupVisible(true);
                                    if (message.userReadys[i])
                                    {
                                        playerUIController.ReadyToPlay();
                                    }
                                    int numberOfDrawingCards = 7 + ((message.userIds.Length - 4) * 2);
                                    playerUIController.SetNumberOfOnHandCards(numberOfDrawingCards);
                                }
                                else
                                {
                                    if (message.userIds[i].Contains("bot"))
                                    {
                                        mRemotePlayers[idx].userId = message.userIds[i];
                                    }
                                    else
                                    {
                                        mRemotePlayers[idx].userId = "Guest";
                                    }
                                    mRemotePlayers[idx].playerIndex = i;

                                    mRemotePlayers[idx].score = 0;
                                    remotePlayerUIController[idx].UpdatePlayerInfo(mRemotePlayers[idx]);
                                    remotePlayerUIController[idx].SetOnHandCardsGroupVisible(true);
                                    cardTable.remotePlayerMeldingAreas[idx].playerIndex = (byte)i;
                                    int numberOfDrawingCards = 7 + ((message.userIds.Length - 4) * 2);
                                    remotePlayerUIController[idx].SetNumberOfOnHandCards(numberOfDrawingCards);
                                    remotePlayerUIController[idx].gameObject.SetActive(true);
                                    if (message.userReadys[i])
                                    {
                                        remotePlayerUIController[idx].ReadyToPlay();
                                    }
                                    idx++;
                                }
                            }

                            if (message.allPlayersReady)
                            {
                                //Debug.Log("<color=red>PLAYER READY</color>");
                                // spawn countdown tween;
                                if (!isStartingCountdown)
                                {
                                    RequestChangeState(GameRoomState.WaitForSelfEndedCountdown);
                                    StartCoroutine(startCountdown());
                                }
                            }

                            if (message.allPlayersEndedCountdown)
                            {
                                if (!isCountdownEndedTriggered)
                                {
                                    isCountdownEndedTriggered = true;
                                    playerUIController.StartGame();
                                    foreach (var item in remotePlayerUIController)
                                    {
                                        item.StartGame();
                                    }

                                    RequestChangeState(GameRoomState.StartingCardDeal);
                                }
                            }
                        }
                        break;

                    case DMProtocolConstants.kMsgId_StartingCardsDeal:
                        {
                            if (mState == GameRoomState.StartingCardDeal)
                            {
                                Debug.LogFormat("[DMGameRoomcontroller] is processing StartingCardsDealMessage");
                                StartingCardsDealMessage message = msg as StartingCardsDealMessage;
                                mLocalPlayerCards.Clear();
                                for (int i = 0; i < message.cards.Length; i++)
                                {
                                    mLocalPlayerCards.Add(message.cards[i]);
                                }
                                mSubState = 1;
                            }
                            else
                            {
                                Debug.LogError("[DMGameRoomController] receive StartingCardsDealMessage while game state is not in StartingCardsDeal State");
                            }
                        }
                        break;

                    case DMProtocolConstants.kMsgId_RespQueryTurnState:
                        {
                            RespQueryTurnStateMessage message = msg as RespQueryTurnStateMessage;

                            playerOnHandCardsController.SyncCards(message.playerOnHandCards);
                            playerOnHandCardsController.RearrangeCards();
                            cardTableController.discardCardTable.SyncCards(message.discardCards);
                            cardTableController.discardCardTable.RearrangeCards();
                            cardTable.SetNumberOfStockCards(message.numberOfStockCards);
                            playerOnHandCardsController.SetDiscardSelectable(true);
                            if (cardTable.GetNumberOfStockCards() == 0)
                            {
                                playerOnHandCardsController.setLastTurn();
                            }

                            Debug.LogFormat("[DMGameRoomController] receive QueryTurnState - sync Player OnHand cards, and Discard cards");
                        }
                        break;

                    case DMProtocolConstants.kMsgId_TurnChange:
                        {
                            TurnChangeMessage message = msg as TurnChangeMessage;
                            Debug.Log("[DMGameRoomController] receive TurnChange message");
                            // if (message.playerIndex == mLocalPlayer.playerIndex)
                            // {
                            Debug.Log("[DMGameRoomController] TurnChange Local player's Turn");
                            SetRoomIdBeforeSend(new ReqQueryTurnStateMessage());
                            // }

                            // If turn changed to the local player, set his action limit time and show Action Time indicator
                            if (message.playerIndex == playerUIController.PlayerIndex())
                            {
                                playerUIController.SetActionTime(message.limitActionTimeInSeconds);
                                playerUIController.SetActionTimeIndicatorVisible(true);
                                playerUIController.SetActionTimeText(true);

                                playerOnHandCardsController.SetActiveAsPlayerTurn(true);
                            }
                            else
                            {
                                playerUIController.SetActionTimeIndicatorVisible(false);
                                playerUIController.SetActionTimeText(false);

                                playerOnHandCardsController.SetActiveAsPlayerTurn(false);
                            }

                            // If turn changed to the remote player, set his action limit time and show Action Time indicator
                            for (int i = 0; i < remotePlayerUIController.Length; i++)
                            {
                                if (message.playerIndex == remotePlayerUIController[i].PlayerIndex())
                                {
                                    remotePlayerUIController[i].SetActionTime(message.limitActionTimeInSeconds);
                                    remotePlayerUIController[i].SetActionTimeIndicatorVisible(true);
                                    remotePlayerUIController[i].SetActionTimeText(true);
                                }
                                else
                                {
                                    remotePlayerUIController[i].SetActionTimeIndicatorVisible(false);
                                    remotePlayerUIController[i].SetActionTimeText(false);
                                }
                            }

                        }
                        break;

                    case DMProtocolConstants.kMsgId_RespDrawStockCard:
                        {
                            RespDrawStockCardMessage message = msg as RespDrawStockCardMessage;
                            Debug.LogFormat("[DMGameRoomController] receive ResponseDrawStockCard message");

                            if (message.card.CardShortName() != "")
                            {
                                playerOnHandCardsController.ResponseToRequestDrawStockCard(message.card);
                                //playerUIController.SetActionTime(message.nextActionTimeLimit);
                                cardTable.SetNumberOfStockCards(message.numberOfStockCards);
                                playerUIController.SetNumberOfOnHandCards(playerOnHandCardsController.getHandCardCount());
                                playerUIController.AddDrawStatus(message.card);
                            }
                        }
                        break;

                    case DMProtocolConstants.kMsgId_PlayerDrawStockCard:
                        {
                            PlayerDrawStockCardMessage message = msg as PlayerDrawStockCardMessage;
                            byte playerIndex = message.playerIndex;
                            Debug.LogFormat("[DMGameRoomController] receive PlayerDrawStockCardMessage message for player index: {0}", playerIndex);

                            for (int i = 0; i < remotePlayerUIController.Length; i++)
                            {
                                if (remotePlayerUIController[i].PlayerIndex() != playerIndex)
                                {
                                    continue;
                                }
                                Debug.LogFormat("[DMGameRoomController] set number of onhand cards {0} for player index: {1}", message.numberOfOnHandCards, playerIndex);
                                remotePlayerUIController[i].SetNumberOfOnHandCards(message.numberOfOnHandCards);
                                remotePlayerUIController[i].AddDrawStatus();
                                //remotePlayerUIController[i].SetActionTime(message.nextActionLimitTime);
                                break;
                            }
                            cardTable.SetNumberOfStockCards(message.numberOfStockCards);
                        }
                        break;

                    case DMProtocolConstants.kMsgId_RespDiscardCard:
                        {
                            RespDiscardCardMessage message = msg as RespDiscardCardMessage;
                            Debug.Log("[DMGameRoomController] receive ResponseDiscardCard message");
                            playerOnHandCardsController.ResponseToRequestDiscard(message.card);
                            //playerUIController.SetActionTime(message.nextActionTimeLimit);
                            cardTable.discardCardTable.AddCard(message.card, true);
                            cardTable.discardCardTable.RearrangeCards();

                            if (message.isDropFull)
                            {
                                playerUIController.AddStatus((byte)PlayerUIController.typeScore.dropFull);
                            }

                            if (message.isDropDummy)
                            {
                                playerUIController.AddStatus((byte)PlayerUIController.typeScore.dropDummy);
                            }

                            playerUIController.SetNumberOfOnHandCards(playerOnHandCardsController.getHandCardCount());
                            playerUIController.AddDiscardStatus(message.card);
                        }
                        break;

                    case DMProtocolConstants.kMsgId_PlayerDiscardCard:
                        {
                            PlayerDiscardCardMessage message = msg as PlayerDiscardCardMessage;
                            byte playerIndex = message.playerIndex;
                            Debug.LogFormat("[DMGameRoomController] receive PlayerDiscardCard message for player index: {0}", playerIndex);

                            cardTable.discardCardTable.SyncCards(message.discardCards);
                            cardTable.discardCardTable.RearrangeCards();

                            for (int i = 0; i < remotePlayerUIController.Length; i++)
                            {
                                if (remotePlayerUIController[i].PlayerIndex() != message.playerIndex)
                                {
                                    continue;
                                }
                                Debug.LogFormat("[DMGameRoomController] set number of onhand cards {0} for player index: {1}", message.numberOfOnHandCards, playerIndex);
                                remotePlayerUIController[i].SetNumberOfOnHandCards(message.numberOfOnHandCards);
                                //remotePlayerUIController[i].SetActionTime(message.nextActionLimitTime);
                                remotePlayerUIController[i].AddDiscardStatus(message.card);
                                if (message.isDropFull)
                                {
                                    remotePlayerUIController[i].AddStatus((byte)PlayerUIController.typeScore.dropFull);
                                }
                                if (message.isDropDummy)
                                {
                                    remotePlayerUIController[i].AddStatus((byte)PlayerUIController.typeScore.dropDummy);
                                }
                                break;
                            }
                        }
                        break;

                    case DMProtocolConstants.kMsgId_RespMeldingCard:
                        {
                            RespMeldingCardMessage message = msg as RespMeldingCardMessage;
                            Debug.Log("[DMGameRoomController] receive RespMeldingCard message");
                            if (!message.allow)
                            {
                                Debug.LogFormat("<color=red>[DMGameRoomController] RespMeldingCard message not allow melding</color>");
                            }
                            playerOnHandCardsController.ResponsetoRequestMeldCard(message.meldingCard, message.allow);
                            if (message.allow)
                            {
                                playerUIController.displayMeldStatus(message.playerIndexGotCondition, message.condition, remotePlayerUIController);
                                cardTable.localPlayerMeldingArea.Put(message.meldingCard);
                                cardTable.localPlayerMeldingArea.Rearrange();
                                //playerUIController.SetActionTime(message.nextActionLimitTime);
                                cardTable.localPlayerMeldingArea.Rearrange();
                            }
                        }
                        break;
                    case DMProtocolConstants.kMsgId_PlayerScore:
                        {
                            PlayerScoreMessage message = msg as PlayerScoreMessage;
                            playerUIController.SetPlayerScore((int)message.score);
                            Debug.LogFormat("[DMGameRoomController] receive PlayerScore message");

                        }
                        break;
                    case DMProtocolConstants.kMsgId_otherPlayerScore:
                        {
                            otherPlayerScoreMessage message = msg as otherPlayerScoreMessage;
                            foreach (var item in remotePlayerUIController)
                            {
                                if (item.PlayerIndex() == message.playerIndex)
                                {
                                    item.SetPlayerScore((int)message.score);
                                    Debug.LogFormat("[DMGameRoomController] update otherPlayerScore message {0}", (int)message.score);
                                }
                            }

                            Debug.LogFormat("[DMGameRoomController] update otherPlayerScore message");

                        }
                        break;

                    case DMProtocolConstants.kMsgId_PlayerMeldingCard:
                        {
                            PlayerMeldingCardMessage message = msg as PlayerMeldingCardMessage;
                            byte playerIndex = message.playerIndex;

                            playerOnHandCardsController.ResponsetoRequestMeldCard(message.meldingCard);
                            Debug.LogFormat("[DMGameRoomController] receive PlayerMeldingCardMessage for player index: {0}", playerIndex);
                            foreach (var c in message.meldingCard.cards)
                            {
                                Debug.LogErrorFormat("[DMGameRoomController] {0}", c.CardName());
                            }

                            playerUIController.displayMeldStatus(message.playerIndexGotCondition, message.condition, remotePlayerUIController);
                            for (int i = 0; i < cardTable.remotePlayerMeldingAreas.Length; i++)
                            {
                                if (cardTable.remotePlayerMeldingAreas[i].playerIndex != playerIndex) continue;
                                Debug.LogFormat("[DMGameRoomController] put melding card to meldingArea of player[{0}]", playerIndex);
                                cardTable.remotePlayerMeldingAreas[i].Put(message.meldingCard);
                                cardTable.remotePlayerMeldingAreas[i].Rearrange();
                                remotePlayerUIController[i].RemoveNumberOfOnHandCards(message.meldingCard.cards.Count);
                                break;
                            }
                        }
                        break;

                    case DMProtocolConstants.kMsgId_RespPickupAndMeldingCard:
                        {
                            RespPickupAndMeldingCardMessage message = msg as RespPickupAndMeldingCardMessage;
                            DMCard[] pickupCards = message.pickupCards;
                            DMMeldingCard meldingCard = message.meldingCard;

                            playerUIController.displayMeldStatus(message.playerIndexGotCondition, message.condition, remotePlayerUIController);
                            for (int i = 0; i < meldingCard.cards.Count; i++)
                            {
                                Debug.LogWarningFormat("[kMsgId_PlayerPickupAndMeldingCard] sort 1?{0}", meldingCard.cards[i].CardName());
                                if (meldingCard.cards[i].isHeadCard == true)
                                {
                                    playerUIController.SetActiveHeadIcon(true);
                                }
                                switch (meldingCard.cards[i].CardShortName())
                                {
                                    case "qs":
                                        playerUIController.SetActiveSpadeIcon(true);
                                        break;
                                    case "2c":
                                        playerUIController.SetActiveClubIcon(true);
                                        break;
                                }
                            }
                            playerOnHandCardsController.ResponseToRequestPickupAndMeldingCard(pickupCards, meldingCard);

                            this.cardTable.localPlayerMeldingArea.Put(meldingCard);
                            this.cardTable.localPlayerMeldingArea.Rearrange();
                            Debug.LogFormat("[DMGameRoomController] set local player action time: {0}", message.nextActionLimitTime);
                            // playerUIController.SetActionTime(message.nextActionLimitTime);

                            playerUIController.SetNumberOfOnHandCards(playerOnHandCardsController.getHandCardCount());
                        }
                        break;

                    case DMProtocolConstants.kMsgId_PlayerPickupAndMeldingCard:
                        {
                            PlayerPickupAndMeldingCardMessage message = msg as PlayerPickupAndMeldingCardMessage;
                            byte playerIndex = message.playerIndex;
                            DMCard[] pickupCards = message.pickupCards;
                            DMMeldingCard meldingCard = message.meldingCard;

                            DMCard[] tempCards = cardTable.discardCardTable.GetCopyOfAllCards();
                            int startIndex = 99;
                            for (int i = 0; i < pickupCards.Length; i++)
                            {
                                for (int j = tempCards.Length - 1; j >= 0; j--)
                                {
                                    if (pickupCards[i].IsIdentical(tempCards[j]))
                                    {
                                        if (startIndex >= j)
                                        {
                                            startIndex = j;
                                        }
                                        //   this.cardTable.discardCardTable.RemoveCard(tempCards[j]);
                                        break;
                                    }
                                }
                            }
                            for (int i = tempCards.Length - 1; i >= startIndex; i--)
                            {
                                cardTable.discardCardTable.RemoveCard(tempCards[i]);
                            }

                            playerUIController.displayMeldStatus(message.playerIndexGotCondition, message.condition, remotePlayerUIController);
                            bool gotQS = false;
                            bool got2C = false;
                            bool gotH = false;
                            for (int i = 0; i < meldingCard.cards.Count; i++)
                            {
                                Debug.LogWarningFormat("[kMsgId_PlayerPickupAndMeldingCard] sort 1?{0}", meldingCard.cards[i].CardName());
                                if (meldingCard.cards[i].isHeadCard == true)
                                {
                                    gotH = true;
                                }
                                switch (meldingCard.cards[i].CardShortName())
                                {
                                    case "qs":
                                        gotQS = true;
                                        break;
                                    case "2c":
                                        got2C = true;
                                        break;
                                }
                            }

                            for (int i = 0; i < remotePlayerUIController.Length; i++)
                            {
                                if (remotePlayerUIController[i].PlayerIndex() != playerIndex) continue;
                                if (gotQS) remotePlayerUIController[i].SetActiveSpadeIcon(true);
                                if (got2C) remotePlayerUIController[i].SetActiveClubIcon(true);
                                if (gotH) remotePlayerUIController[i].SetActiveHeadIcon(true);
                                //remotePlayerUIController[i].SetActionTime(message.nextActionLimitTime);
                                remotePlayerUIController[i].SetNumberOfOnHandCards(message.numberOfOnHandCards);
                                Debug.LogWarningFormat("[kMsgId_PlayerPickupAndMeldingCard] Settime And numberOfOnHandCards");
                                break;
                            }

                            for (int i = 0; i < cardTable.remotePlayerMeldingAreas.Length; i++)
                            {
                                if (cardTable.remotePlayerMeldingAreas[i].playerIndex != playerIndex) continue;
                                cardTable.remotePlayerMeldingAreas[i].Put(meldingCard);
                                cardTable.remotePlayerMeldingAreas[i].Rearrange();
                                Debug.LogWarningFormat("[kMsgId_PlayerPickupAndMeldingCard] Add");
                                break;
                            }
                        }
                        break;

                    case DMProtocolConstants.kMsgId_RespLayoffCard:
                        {
                            RespLayoffCardMessage message = msg as RespLayoffCardMessage;
                            UInt16 targetMeldingCardId = message.targetMeldingCardId;
                            DMCard[] layoffCard = message.layoffCards;

                            // OnHandCardController manage response to layoff card
                            playerOnHandCardsController.ResponseToRequestLayoff(message.layoffCards, targetMeldingCardId);

                            // Find which melding card area that is the target melding card
                            byte playerIndex = 0;
                            byte meldingIndex = 0;
                            DMMeldingCard.DecodeMeldingCardId(targetMeldingCardId, out playerIndex, out meldingIndex);
                            MeldingAreaController meldingArea = null;
                            if (cardTable.localPlayerMeldingArea.playerIndex == playerIndex)
                            {
                                meldingArea = cardTable.localPlayerMeldingArea;
                            }
                            else
                            {
                                for (int i = 0; i < cardTable.remotePlayerMeldingAreas.Length; i++)
                                {
                                    if (cardTable.remotePlayerMeldingAreas[i].playerIndex == playerIndex)
                                    {
                                        meldingArea = cardTable.remotePlayerMeldingAreas[i];
                                        break;
                                    }
                                }
                            }
                            if (meldingArea == null)
                            {
                                Debug.LogWarningFormat("[DMGameRoomController] cannot find melding area of player which player index: {0}", playerIndex);
                                return;
                            }

                            var layoffPlayer = cardTable.localPlayerMeldingArea.playerIndex;
                            var targetPlayer = playerIndex;

                            // Insert the layoff card to target melding card
                            foreach (var card in layoffCard)
                            {
                                meldingArea.InsertLayoffCard(card, targetMeldingCardId, (layoffPlayer != targetPlayer) ? cardTable.localPlayerMeldingArea.playerIndex : -1);
                            }


                            if (message.targetCondition != 0)
                            {
                                foreach (var ui in remotePlayerUIController)
                                {
                                    if (ui.PlayerIndex() == message.targetPlayerIndex)
                                    {
                                        ui.AddStatus(message.targetCondition);
                                    }
                                }
                            }



                            if (layoffPlayer != targetPlayer)
                            {
                                playerUIController.AddLayoffStatus(message.layoffCards);
                                if (playerUIController.PlayerIndex() == layoffPlayer)
                                {
                                    foreach (var card in layoffCard)
                                        playerUIController.resultPlayerController.mLayoffCards.Add(new ResultPlayerController.LayoffCard() { card = card, playerIndex = layoffPlayer });
                                }
                                else
                                {
                                    foreach (var ui in remotePlayerUIController)
                                    {
                                        if (ui.PlayerIndex() == layoffPlayer)
                                        {
                                            foreach (var card in layoffCard)
                                                ui.resultPlayerController.mLayoffCards.Add(new ResultPlayerController.LayoffCard() { card = card, playerIndex = layoffPlayer });
                                        }
                                    }
                                }
                            }

                        }
                        break;

                    case DMProtocolConstants.kMsgId_PlayerLayoffCard:
                        {
                            PlayerLayoffCardMessage message = msg as PlayerLayoffCardMessage;
                            Debug.LogFormat("[DMGameRoomController] Player[{0}] layoff card: {1}", message.playerIndex, message.layoffCards[0].CardName());
                            UInt16 targetMeldingCardId = message.targetMeldingCardId;
                            DMCard layoffCard = message.layoffCards[0];

                            // OnHandCardController manage response to layoff card
                            //playerOnHandCardsController.ResponseToRequestLayoff(message.layoffCards[0], targetMeldingCardId);

                            // Find which melding card area that is the target melding card
                            byte playerIndex = 0;
                            byte meldingIndex = 0;
                            DMMeldingCard.DecodeMeldingCardId(targetMeldingCardId, out playerIndex, out meldingIndex);
                            MeldingAreaController meldingArea = null;



                            if (cardTable.localPlayerMeldingArea.playerIndex == playerIndex)
                            {
                                meldingArea = cardTable.localPlayerMeldingArea;
                            }
                            else
                            {
                                for (int i = 0; i < cardTable.remotePlayerMeldingAreas.Length; i++)
                                {
                                    if (this.cardTable.remotePlayerMeldingAreas[i].playerIndex == playerIndex)
                                    {
                                        meldingArea = cardTable.remotePlayerMeldingAreas[i];
                                        break;
                                    }
                                }
                            }

                            if (playerUIController.PlayerIndex() == message.playerIndex)
                            {
                                playerUIController.RemoveNumberOfOnHandCards(1);

                            }
                            else
                            {
                                for (int i = 0; i < remotePlayerUIController.Length; i++)
                                {
                                    if (message.playerIndex == remotePlayerUIController[i].PlayerIndex())
                                    {
                                        remotePlayerUIController[i].RemoveNumberOfOnHandCards(1);
                                        break;
                                    }
                                }
                            }

                            if (meldingArea == null)
                            {
                                Debug.LogWarningFormat("[DMGameRoomController] cannot find melding area of player which player index: {0}", playerIndex);
                                return;
                            }

                            foreach (var ui in remotePlayerUIController)
                            {
                                if (ui.PlayerIndex() == message.targetPlayerIndex)
                                {
                                    ui.AddDiscardStatus(layoffCard);
                                }
                            }

                            if (message.targetCondition != 0)
                            {
                                if (playerUIController.PlayerIndex() != message.targetPlayerIndex)
                                {
                                    playerUIController.AddStatus(message.targetCondition);
                                }
                                else
                                {
                                    foreach (var ui in remotePlayerUIController)
                                    {
                                        if (ui.PlayerIndex() == message.targetPlayerIndex)
                                        {
                                            ui.AddStatus(message.targetCondition);
                                        }
                                    }
                                }
                            }



                            // layoff player
                            var layoffPlayer = message.playerIndex;
                            var targetPlayer = playerIndex;

                            if (layoffPlayer != targetPlayer)
                            {
                                if (playerUIController.PlayerIndex() == layoffPlayer)
                                {
                                    playerUIController.resultPlayerController.mLayoffCards.Add(new ResultPlayerController.LayoffCard() { card = layoffCard, playerIndex = layoffPlayer });
                                }
                                else
                                {
                                    foreach (var ui in remotePlayerUIController)
                                    {
                                        if (ui.PlayerIndex() == layoffPlayer)
                                        {
                                            ui.resultPlayerController.mLayoffCards.Add(new ResultPlayerController.LayoffCard() { card = layoffCard, playerIndex = layoffPlayer });
                                        }
                                    }
                                }
                            }

                            // Insert the layoff card to target melding card
                            meldingArea.InsertLayoffCard(layoffCard, targetMeldingCardId, (layoffPlayer != targetPlayer) ? message.playerIndex : -1);

                        }
                        break;
                    case DMProtocolConstants.kMsgId_RespKnockCard:
                        {
                            RespKnockCardMessage message = msg as RespKnockCardMessage;
                            Debug.LogFormat("[DMGameRoomController] U win");
                            playerUIController.SetActionTimeIndicatorVisible(false);
                            playerUIController.SetActionTimeText(false);
                            playerUIController.AddKnockStatus((byte)message.knockType);

                            for (int i = 0; i < remotePlayerUIController.Length; i++)
                            {
                                remotePlayerUIController[i].SetActionTimeIndicatorVisible(false);
                                remotePlayerUIController[i].SetActionTimeText(false);
                                if (remotePlayerUIController[i].PlayerIndex() != message.targetIndex)
                                {
                                    continue;
                                }
                                if (message.haveDropFool)
                                {
                                    remotePlayerUIController[i].AddStatus((byte)PlayerUIController.typeScore.dropFool);
                                }
                            }

                            for (int i = 0; i < cardTable.remotePlayerMeldingAreas.Length; i++)
                            {
                                if (cardTable.remotePlayerMeldingAreas[i].getMeldingCount() == 0)
                                {
                                    remotePlayerUIController[i].AddKnockStatus((byte)PlayerUIController.typeScore.NoOneTurn);
                                }
                            }
                            RequestChangeState(GameRoomState.End);
                        }
                        break;
                    case DMProtocolConstants.kMsgId_PlayerKnockCard:
                        {
                            PlayerKnockCardMessage message = msg as PlayerKnockCardMessage;

                            playerUIController.SetActionTimeIndicatorVisible(false);
                            playerUIController.SetActionTimeText(false);
                            for (int i = 0; i < remotePlayerUIController.Length; i++)
                            {
                                remotePlayerUIController[i].SetActionTimeIndicatorVisible(false);
                                remotePlayerUIController[i].SetActionTimeText(false);
                            }

                            if (message.haveDropFool)
                            {
                                if (playerUIController.PlayerIndex() == message.targetIndex)
                                {
                                    playerUIController.AddStatus((byte)PlayerUIController.typeScore.dropFool);
                                }
                                for (int i = 0; i < remotePlayerUIController.Length; i++)
                                {
                                    if (remotePlayerUIController[i].PlayerIndex() != message.targetIndex)
                                    {
                                        continue;
                                    }
                                    if (message.haveDropFool)
                                    {
                                        remotePlayerUIController[i].AddStatus((byte)PlayerUIController.typeScore.dropFool);
                                    }
                                }
                            }

                            if (cardTable.localPlayerMeldingArea.playerIndex != message.playerIndex)
                            {
                                if (cardTable.localPlayerMeldingArea.getMeldingCount() == 0)
                                {
                                    playerUIController.AddKnockStatus((byte)PlayerUIController.typeScore.NoOneTurn);
                                }
                            }

                            for (int i = 0; i < cardTable.remotePlayerMeldingAreas.Length; i++)
                            {
                                if (cardTable.remotePlayerMeldingAreas[i].playerIndex != message.playerIndex)
                                {
                                    if (cardTable.remotePlayerMeldingAreas[i].getMeldingCount() == 0)
                                    {
                                        remotePlayerUIController[i].AddKnockStatus((byte)PlayerUIController.typeScore.NoOneTurn);
                                    }
                                }
                                else
                                {
                                    remotePlayerUIController[i].AddKnockStatus(message.knockType);
                                }
                            }
                            Debug.LogFormat("[DMGameRoomController] player : {0} knockby : {1} card : {2}", message.playerIndex, message.knockType, message.card.CardShortName());
                            RequestChangeState(GameRoomState.End);
                        }
                        break;
                    case DMProtocolConstants.kMsgId_RespPassLastTurn:
                        {
                            RespPassLastTurnMessage message = msg as RespPassLastTurnMessage;
                            playerUIController.SetActionTimeIndicatorVisible(false);
                            playerUIController.SetActionTimeText(false);
                            for (int i = 0; i < remotePlayerUIController.Length; i++)
                            {
                                remotePlayerUIController[i].SetActionTimeIndicatorVisible(false);
                                remotePlayerUIController[i].SetActionTimeText(false);
                            }
                            RequestChangeState(GameRoomState.End);
                        }
                        break;
                    case DMProtocolConstants.kMsgId_PlayerPassLastTurn:
                        {
                            PlayerPassLastTurnMessage message = msg as PlayerPassLastTurnMessage;
                            playerUIController.SetActionTimeIndicatorVisible(false);
                            playerUIController.SetActionTimeText(false);
                            for (int i = 0; i < remotePlayerUIController.Length; i++)
                            {
                                remotePlayerUIController[i].SetActionTimeIndicatorVisible(false);
                                remotePlayerUIController[i].SetActionTimeText(false);
                            }
                            RequestChangeState(GameRoomState.End);
                        }
                        break;
                    case DMProtocolConstants.kMsgId_PlayerCountTypeScore:
                        {
                            PlayerCountTypeScore message = msg as PlayerCountTypeScore;
                            Debug.LogError(message.data);
                            playerUIController.SetupResult(message.data, true); // self setup
                            for (int i = 0; i < remotePlayerUIController.Length; i++)
                            {
                                remotePlayerUIController[i].SetupResult(message.data);
                            }
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        bool isStartingCountdown = false;

        IEnumerator startCountdown()
        {
            isStartingCountdown = true;
            var countdown = Instantiate(countdownPrefabs, playerUIControllerGroup.transform.parent);
            countdown.GetComponent<CountdownController>().countdownType = this.countdownType;
            yield return new WaitForSeconds(countdown.GetComponent<CountdownController>().getTotalDuration());

            ReqEndCountdownMessage msg = new ReqEndCountdownMessage();
            SetRoomIdBeforeSend(msg);
        }

        void Update()
        {
            if (mState == GameRoomState.WaitForSelfReady)
            {
                if (mRequestNextState == GameRoomState.WaitForAllReady)
                {
                    waitForReadyGroup.SetActive(false);
                    playerUIControllerGroup.SetActive(true);
                    mState = mRequestNextState;
                }
            }
            else if (mState == GameRoomState.WaitForAllReady)
            {
                if (mRequestNextState == GameRoomState.WaitForSelfEndedCountdown)
                {
                    mState = mRequestNextState;
                }
            }
            else if (mState == GameRoomState.WaitForSelfEndedCountdown)
            {
                if (mRequestNextState == GameRoomState.WaitForAllEndedCountdown)
                {
                    mState = mRequestNextState;
                }
            }
            
            else if (mState == GameRoomState.WaitForAllEndedCountdown)
            {
                if (mRequestNextState == GameRoomState.StartingCardDeal)
                {
                    playerOnHandCardsController.gameObject.SetActive(true);
                    cardTableController.gameObject.SetActive(true);
                    mSubState = 0;
                    mState = mRequestNextState;
                }
            }
            else if (mState == GameRoomState.StartingCardDeal)
            {
                if (mSubState == 0)
                {
                    // Wait for StartingCardDealMessage from server, to query the starting card deal.
                    // For filling in mLocalPlayerCards
                }
                else if (mSubState == 1)
                {
                    playerOnHandCardsController.SyncCards(mLocalPlayerCards.ToArray());
                    playerOnHandCardsController.RearrangeCards(true);
                    mSubState = 2;
                }
                else if (mSubState == 2)
                {
                    StartingCardsDealReadyMessage message = new StartingCardsDealReadyMessage();
                    SetRoomIdBeforeSend(message);
                    mState = GameRoomState.Playing;
                }
            }
            else if (mState == GameRoomState.Playing)
            {
                if (mRequestNextState == GameRoomState.End)
                {
                    resultScreenGroup.SetActive(true);
                    mState = mRequestNextState;
                }
            }
            else if (mState == GameRoomState.End)
            {

            }
        }
        public void SetRoomIdBeforeSend(DMBaseMessage message)
        {
            message.SetRoomID(mClient.mockupRoomId);
            mClient.Send(message);
        }
        void RequestChangeState(GameRoomState nextState)
        {
            Debug.LogFormat("[DMGameRoomController] Request to Change State {0} => {1}", mState.ToString(), nextState.ToString());
            mRequestNextState = nextState;
        }

        #region PlayerOnHandCardsController - Delegate

        public void PlayerOnHandCardsControllerRequestDrawStockCard()
        {
            ReqDrawStockCardMessage msg = new ReqDrawStockCardMessage();
            SetRoomIdBeforeSend(msg);
        }

        //Pooh public void PlayerOnHandCardsControllerRequestPickupDiscardCardAndMeld(DMCard card, DMCard[] cardsToBeMelding) {
        public void PlayerOnHandCardsControllerRequestPickupDiscardCardAndMeld(DMCard card, DMMeldingCard meldingCard)
        {
            ReqPickupAndMeldingCardMessage msg = new ReqPickupAndMeldingCardMessage();
            msg.pickupCard = card;
            msg.meldingCard = meldingCard;
            SetRoomIdBeforeSend(msg);
        }

        public void PlayerOnHandCardsControllerRequestMeldCard(DMCard[] cards)
        {
            ReqMeldingCardMessage msg = new ReqMeldingCardMessage();
            msg.meldingCard = new DMMeldingCard(cards);
            SetRoomIdBeforeSend(msg);
        }

        public void PlayerOnHandCardsControllerRequestDiscard(DMCard card)
        {
            ReqDiscardCardMessage msg = new ReqDiscardCardMessage();
            msg.card = card;
            SetRoomIdBeforeSend(msg);
        }

        public void PlayerOnHandCardsControllerRequestLayoff(DMCard card, DMMeldingCard targetMeldingCard)
        {
            ReqLayoffCardMessage msg = new ReqLayoffCardMessage();
            msg.layoffCards = new DMCard[] { card };
            msg.targetMeldingCardId = targetMeldingCard.id;
            SetRoomIdBeforeSend(msg);
        }

        public void PlayerOnHandCardsControllerRequestLayoffs(DMCard[] cards, ushort targetMeldCardID)
        {
            ReqLayoffCardMessage msg = new ReqLayoffCardMessage();
            msg.layoffCards = cards;
            msg.targetMeldingCardId = targetMeldCardID;
            SetRoomIdBeforeSend(msg);
        }

        public void leaveAndRemoveRoom(string roomID)
        {
            leaveAndRemoveRoom msg = new leaveAndRemoveRoom();
            msg.roomID = roomID;
            SetRoomIdBeforeSend(msg);
            mClient.RemoveMessageReceiver();
        }

        public void PlayerOnHandCardsControllerRequestKnockCard(DMCard card)
        {
            ReqKnockCardMessage msg = new ReqKnockCardMessage();
            msg.card = card;
            SetRoomIdBeforeSend(msg);
        }
        #endregion

        public void PlayerOnHandCardsControllerPassLastTurn()
        {
            ReqPassLastTurnMessage msg = new ReqPassLastTurnMessage();
            SetRoomIdBeforeSend(msg);
        }

    }

}