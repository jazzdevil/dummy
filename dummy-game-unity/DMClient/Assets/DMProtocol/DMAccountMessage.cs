﻿using System;
using System.Collections.Generic;
using System.IO;


namespace Dummy.DMProtocol {
    public class ReqWhoAreYouMessage : DMBaseMessage {
        public ReqWhoAreYouMessage() : base(DMProtocolConstants.kMsgId_ReqWhoAreYou) { }
        
    }

    public class RespWhoAreYouMessage : DMBaseMessage {
        public string userId;

        public RespWhoAreYouMessage() : base(DMProtocolConstants.kMsgId_RespWhoAreYou) { }

        public override void WriteBytes(BinaryWriter writer) {
            base.WriteBytes(writer);
            writer.Write(this.userId);
        }

        public override void ReadBytes(BinaryReader reader) {
            base.ReadBytes(reader);
            this.userId = reader.ReadString();
        }
    }
}
