﻿using System;
using System.Collections.Generic;

namespace Protocol.DMProtocol {
    public class DMPacketProtocol {

        private enum ReadingStateEnum {
            ReadingLengthHeader,
            ReadingData
        }

        private int mMaxMessageSize;
        private byte[] mLengthHeaderBuffer;
        private byte[] mDataBuffer;
        private int mLengthHeaderBufferBytesReceived;
        private int mDataBufferBytesReceived;
        private ReadingStateEnum mReadingState;
        public Action<byte[]> OnMessageReceived;

        public static byte[] WrapMessage(byte[] message) {
            Int32 messageLength = message.Length;
            byte[] lengthHeader = BitConverter.GetBytes((Int32)messageLength);
            byte[] wrappedMessage = new byte[lengthHeader.Length + messageLength];
            lengthHeader.CopyTo(wrappedMessage, 0);
            message.CopyTo(wrappedMessage, lengthHeader.Length);
            return wrappedMessage;
        }

        public static byte[] WrapKeepAliveMessage() {
            return BitConverter.GetBytes((int)0);
        }

        public DMPacketProtocol(int maxMessageSize) {
            mMaxMessageSize = maxMessageSize;
            mLengthHeaderBuffer = new byte[sizeof(Int32)];
            mDataBuffer = null;
            mReadingState = ReadingStateEnum.ReadingLengthHeader;
        }

        public void DataReceived(byte[] data) {
            int i = 0;
            while (i < data.Length) {
                int bytesAvailable = data.Length - i;
                if (mReadingState == ReadingStateEnum.ReadingLengthHeader) {
                    int bytesRequested = mLengthHeaderBuffer.Length - mLengthHeaderBufferBytesReceived;
                    int byteTransferred = Math.Min(bytesRequested, bytesAvailable);
                    Array.Copy(data, i, mLengthHeaderBuffer, mLengthHeaderBufferBytesReceived, byteTransferred);

                    mLengthHeaderBufferBytesReceived += byteTransferred;
                    if (mLengthHeaderBufferBytesReceived == mLengthHeaderBuffer.Length) {
                        // receive length header completely
                        int l = BitConverter.ToInt32(mLengthHeaderBuffer, 0);
                        if (l < 0) {
                            throw new System.Net.ProtocolViolationException("Message length is less than zero");
                        }
                        if (l > mMaxMessageSize) {
                            throw new System.Net.ProtocolViolationException(string.Format("Message length is larger than maximum size: {0}", mMaxMessageSize));
                        }

                        if (l == 0) {
                            // zero-length packets are allowed as Keppalive
                            mLengthHeaderBufferBytesReceived = 0;
                            if (this.OnMessageReceived != null) {
                                this.OnMessageReceived(new byte[0]);
                            }
                        }
                        else {
                            mDataBuffer = new byte[l];
                            mDataBufferBytesReceived = 0;
                            mReadingState = ReadingStateEnum.ReadingData;
                        }
                    }
                    else {
                        // if mLengthHeaderBufferBytesReceived is still not equal to Header buffer size
                        // Keep waiting to next arrival data
                    }
                    i += byteTransferred;
                }
                else if (mReadingState == ReadingStateEnum.ReadingData) {
                    int bytesRequested = mDataBuffer.Length - mDataBufferBytesReceived;
                    int bytesTransferred = Math.Min(bytesRequested, bytesAvailable);
                    Array.Copy(data, i, mDataBuffer, mDataBufferBytesReceived, bytesTransferred);
                    mDataBufferBytesReceived += bytesTransferred;
                    if (mDataBufferBytesReceived == mDataBuffer.Length) {
                        if (this.OnMessageReceived != null) {
                            this.OnMessageReceived(mDataBuffer);
                        }

                        mDataBuffer = null;
                        mLengthHeaderBufferBytesReceived = 0;
                        mReadingState = ReadingStateEnum.ReadingLengthHeader;
                    }
                    else {
                        // if mDataBufferBytesReceived is still not equal to Data buffer size
                        // Keep waiting to next arrival data
                    }
                    i += bytesTransferred;
                }
            }
        }
    }
}
