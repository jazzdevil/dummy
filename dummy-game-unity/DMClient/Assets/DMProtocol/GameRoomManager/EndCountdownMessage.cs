namespace Dummy.DMProtocol
{
  public class ReqEndCountdownMessage : DMBaseMessage
  {
    public ReqEndCountdownMessage() : base(DMProtocolConstants.kMsgId_ReqEndCountdown) { }
  }

  public class RespEndCountdownMessage : DMBaseMessage
  {
    public RespEndCountdownMessage() : base(DMProtocolConstants.kMsgId_RespEndCountdown) { }
  }
}