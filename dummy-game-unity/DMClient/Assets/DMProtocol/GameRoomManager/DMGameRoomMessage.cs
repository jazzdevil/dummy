using System;
using System.Collections.Generic;
using System.IO;

namespace Dummy.DMProtocol
{

    /// <summary>
    /// The message does not contain any user information.
    /// Please lookup connection ID table for player information
    /// </summary>
    public class InitGameRoomMessage : DMBaseMessage
    {
        public InitGameRoomMessage() : base(DMProtocolConstants.kMsgId_InitGameRoom) { }
    }
    public class ReqReadyToPlayMessage : DMBaseMessage
    {
        public ReqReadyToPlayMessage() : base(DMProtocolConstants.kMsgId_ReqReadyToPlay) { }
    }
    public class RespReadyToPlayMessage : DMBaseMessage
    {
        public RespReadyToPlayMessage() : base(DMProtocolConstants.kMsgId_RespReadyToPlay) { }

        public byte playerIndex;

        public override void ReadBytes(BinaryReader reader)
        {
            base.ReadBytes(reader);
            this.playerIndex = reader.ReadByte();
        }

        public override void WriteBytes(BinaryWriter writer)
        {
            base.WriteBytes(writer);
            writer.Write(this.playerIndex);
        }
    }

    public class ReqUpdateRoomStatusMessage : DMBaseMessage
    {
        public ReqUpdateRoomStatusMessage() : base(DMProtocolConstants.kMsgId_ReqUpdateRoomStatus) { }
    }

    public class UpdateRoomStatusMessage : DMBaseMessage
    {
        public byte numberOfPlayers;
        public string[] userIds;
        public bool[] userReadys;
        public bool allPlayersReady;

        public bool allPlayersEndedCountdown;

        public UpdateRoomStatusMessage() : base(DMProtocolConstants.kMsgId_UpdateRoomStatus) { }

        public override void ReadBytes(BinaryReader reader)
        {
            base.ReadBytes(reader);
            numberOfPlayers = reader.ReadByte();
            userIds = new string[numberOfPlayers];
            userReadys = new bool[numberOfPlayers];
            for (int i = 0; i < numberOfPlayers; i++)
            {
                userIds[i] = reader.ReadString();
            }
            for (int i = 0; i < numberOfPlayers; i++)
            {
                userReadys[i] = reader.ReadBoolean();
            }
            allPlayersReady = reader.ReadBoolean();
            allPlayersEndedCountdown = reader.ReadBoolean();
        }

        public override void WriteBytes(BinaryWriter writer)
        {
            base.WriteBytes(writer);
            writer.Write((byte)numberOfPlayers);
            for (int i = 0; i < numberOfPlayers; i++)
            {
                writer.Write((string)userIds[i]);
            }
            for (int i = 0; i < numberOfPlayers; i++)
            {
                writer.Write((bool)userReadys[i]);
            }
            writer.Write((bool)allPlayersReady);
            writer.Write((bool)allPlayersEndedCountdown);
        }
    }

    /// <summary>
    /// Send to client when start Game
    /// </summary>
    public class StartingCardsDealMessage : DMBaseMessage
    {
        public DMCard[] cards;

        public StartingCardsDealMessage() : base(DMProtocolConstants.kMsgId_StartingCardsDeal) { }

        public override void WriteBytes(BinaryWriter writer)
        {
            base.WriteBytes(writer);
            writer.Write((byte)cards.Length);
            for (int i = 0; i < cards.Length; i++)
            {
                writer.Write((UInt16)cards[i].ToUInt16());
            }
        }

        public override void ReadBytes(BinaryReader reader)
        {
            base.ReadBytes(reader);
            int numberOfCards = reader.ReadByte();
            cards = new DMCard[numberOfCards];
            for (int i = 0; i < numberOfCards; i++)
            {
                UInt16 cardAsUint = reader.ReadUInt16();
                cards[i] = DMCard.fromUInt16(cardAsUint);
            }
        }
    }

    /// <summary>
    /// Could be sent from client as soon as after the client received StartingCardsDeal Message
    /// </summary>
    public class StartingCardsDealReadyMessage : DMBaseMessage
    {
        public StartingCardsDealReadyMessage() : base(DMProtocolConstants.kMsgId_StartingCardsDealReady) { }
    }

    public class ReqQueryTurnStateMessage : DMBaseMessage
    {
        public ReqQueryTurnStateMessage() : base(DMProtocolConstants.kMsgId_ReqQueryTurnState) { }
    }

    public class RespQueryTurnStateMessage : DMBaseMessage
    {
        public DMCard[] discardCards;
        public DMCard[] playerOnHandCards;
        public DMMeldingCard[] meldingCards;
        public byte numberOfStockCards;

        public RespQueryTurnStateMessage() : base(DMProtocolConstants.kMsgId_RespQueryTurnState) { }

        public override void WriteBytes(BinaryWriter writer)
        {
            base.WriteBytes(writer);

            writer.Write((byte)discardCards.Length);
            for (int i = 0; i < discardCards.Length; i++)
            {
                writer.Write((UInt16)discardCards[i].ToUInt16());
            }

            writer.Write((byte)playerOnHandCards.Length);
            for (int i = 0; i < playerOnHandCards.Length; i++)
            {
                writer.Write((UInt16)playerOnHandCards[i].ToUInt16());
            }

            writer.Write((byte)meldingCards.Length);
            for (int i = 0; i < meldingCards.Length; i++)
            {
                meldingCards[i].WriteBytes(writer);
            }

            writer.Write((byte)numberOfStockCards);
        }

        public override void ReadBytes(BinaryReader reader)
        {
            base.ReadBytes(reader);

            int numberOfDiscardCards = reader.ReadByte();
            discardCards = new DMCard[numberOfDiscardCards];
            for (int i = 0; i < numberOfDiscardCards; i++)
            {
                discardCards[i] = DMCard.fromUInt16(reader.ReadUInt16());
            }

            int numberOfPlayerOnHandCards = reader.ReadByte();
            playerOnHandCards = new DMCard[numberOfPlayerOnHandCards];
            for (int i = 0; i < numberOfPlayerOnHandCards; i++)
            {
                playerOnHandCards[i] = DMCard.fromUInt16(reader.ReadUInt16());
            }

            int numberOfMeldingCards = reader.ReadByte();
            meldingCards = new DMMeldingCard[numberOfMeldingCards];
            for (int i = 0; i < numberOfMeldingCards; i++)
            {
                meldingCards[i] = new DMMeldingCard();
                meldingCards[i].ReadBytes(reader);
            }

            this.numberOfStockCards = reader.ReadByte();
        }
    }

    public class TurnChangeMessage : DMBaseMessage
    {
        public byte playerIndex; // Player who get the current turn
        public byte limitActionTimeInSeconds; // Limit time in seconds

        public TurnChangeMessage() : base(DMProtocolConstants.kMsgId_TurnChange) { }

        public override void WriteBytes(BinaryWriter writer)
        {
            base.WriteBytes(writer);
            writer.Write((byte)playerIndex);
            writer.Write((byte)limitActionTimeInSeconds);
        }

        public override void ReadBytes(BinaryReader reader)
        {
            base.ReadBytes(reader);
            playerIndex = reader.ReadByte();
            limitActionTimeInSeconds = reader.ReadByte();
        }
    }

    public class ReqDrawStockCardMessage : DMBaseMessage
    {
        public ReqDrawStockCardMessage() : base(DMProtocolConstants.kMsgId_ReqDrawStockCard) { }
    }

    public class RespDrawStockCardMessage : DMBaseMessage
    {
        public DMCard card;
        public byte numberOfStockCards;
        public byte nextActionTimeLimit;

        public RespDrawStockCardMessage() : base(DMProtocolConstants.kMsgId_RespDrawStockCard) { }

        public override void WriteBytes(BinaryWriter writer)
        {
            base.WriteBytes(writer);
            writer.Write((UInt16)card.ToUInt16());
            writer.Write((byte)numberOfStockCards);
            writer.Write((byte)nextActionTimeLimit);
        }

        public override void ReadBytes(BinaryReader reader)
        {
            base.ReadBytes(reader);
            card = DMCard.fromUInt16(reader.ReadUInt16());
            numberOfStockCards = reader.ReadByte();
            nextActionTimeLimit = reader.ReadByte();
        }
    }

    public class PlayerDrawStockCardMessage : DMBaseMessage
    {
        public byte playerIndex;
        public byte numberOfStockCards;
        public byte numberOfOnHandCards;
        public byte nextActionLimitTime;

        public PlayerDrawStockCardMessage() : base(DMProtocolConstants.kMsgId_PlayerDrawStockCard) { }

        public override void WriteBytes(BinaryWriter writer)
        {
            base.WriteBytes(writer);
            writer.Write((byte)playerIndex);
            writer.Write((byte)numberOfStockCards);
            writer.Write((byte)numberOfOnHandCards);
            writer.Write((byte)nextActionLimitTime);
        }

        public override void ReadBytes(BinaryReader reader)
        {
            base.ReadBytes(reader);
            playerIndex = reader.ReadByte();
            numberOfStockCards = reader.ReadByte();
            numberOfOnHandCards = reader.ReadByte();
            nextActionLimitTime = reader.ReadByte();
        }
    }

    public class ReqDiscardCardMessage : DMBaseMessage
    {
        public DMCard card;

        public ReqDiscardCardMessage() : base(DMProtocolConstants.kMsgId_ReqDiscardCard) { }

        public override void WriteBytes(BinaryWriter writer)
        {
            base.WriteBytes(writer);
            writer.Write((UInt16)card.ToUInt16());
        }

        public override void ReadBytes(BinaryReader reader)
        {
            base.ReadBytes(reader);
            card = DMCard.fromUInt16(reader.ReadUInt16());
        }
    }

    public class RespDiscardCardMessage : DMBaseMessage
    {
        public DMCard card;
        public byte nextActionTimeLimit;
        public bool isDropFull;
        public bool isDropDummy;
        public RespDiscardCardMessage() : base(DMProtocolConstants.kMsgId_RespDiscardCard) { }

        public override void WriteBytes(BinaryWriter writer)
        {
            base.WriteBytes(writer);
            writer.Write((UInt16)card.ToUInt16());
            writer.Write((byte)nextActionTimeLimit);
            writer.Write((bool)isDropFull);
            writer.Write((bool)isDropDummy);
        }

        public override void ReadBytes(BinaryReader reader)
        {
            base.ReadBytes(reader);
            card = DMCard.fromUInt16(reader.ReadUInt16());
            nextActionTimeLimit = reader.ReadByte();
            isDropFull = reader.ReadBoolean();
            isDropDummy = reader.ReadBoolean();
        }
    }

    public class PlayerDiscardCardMessage : DMBaseMessage
    {
        public byte playerIndex;
        public DMCard card; // Card that had been discarded
        public byte numberOfOnHandCards; // Number of on hand card of the player
        public DMCard[] discardCards; // Discard card pile
        public byte nextActionLimitTime;
        public bool isDropFull;
        public bool isDropDummy;
        public PlayerDiscardCardMessage() : base(DMProtocolConstants.kMsgId_PlayerDiscardCard) { }

        public override void WriteBytes(BinaryWriter writer)
        {
            base.WriteBytes(writer);
            writer.Write((byte)playerIndex);
            writer.Write((UInt16)card.ToUInt16());
            writer.Write((byte)numberOfOnHandCards);
            writer.Write((byte)discardCards.Length);
            for (int i = 0; i < discardCards.Length; i++)
            {
                writer.Write((UInt16)discardCards[i].ToUInt16());
            }
            writer.Write((byte)nextActionLimitTime);
            writer.Write((bool)isDropFull);
            writer.Write((bool)isDropDummy);
        }

        public override void ReadBytes(BinaryReader reader)
        {
            base.ReadBytes(reader);
            playerIndex = reader.ReadByte();
            card = DMCard.fromUInt16(reader.ReadUInt16());
            numberOfOnHandCards = reader.ReadByte();
            int numberOfDiscardedCards = reader.ReadByte();
            discardCards = new DMCard[numberOfDiscardedCards];
            for (int i = 0; i < numberOfDiscardedCards; i++)
            {
                discardCards[i] = DMCard.fromUInt16(reader.ReadUInt16());
            }
            nextActionLimitTime = reader.ReadByte();
            isDropFull = reader.ReadBoolean();
            isDropDummy = reader.ReadBoolean();
        }
    }

    public class ReqMeldingCardMessage : DMBaseMessage
    {
        public DMMeldingCard meldingCard;

        public ReqMeldingCardMessage() : base(DMProtocolConstants.kMsgId_ReqMeldingCard) { }

        public override void WriteBytes(BinaryWriter writer)
        {
            base.WriteBytes(writer);
            meldingCard.WriteBytes(writer);
        }

        public override void ReadBytes(BinaryReader reader)
        {
            base.ReadBytes(reader);
            if (meldingCard == null) meldingCard = new DMMeldingCard();
            meldingCard.ReadBytes(reader);
        }
    }

    public class RespMeldingCardMessage : DMBaseMessage
    {
        public bool allow;
        public DMMeldingCard meldingCard;
        public byte nextActionLimitTime;

        public string playerIndexGotCondition;
        //1 , 2 , 1  ...
        public string condition;

        public RespMeldingCardMessage() : base(DMProtocolConstants.kMsgId_RespMeldingCard) { }

        public override void WriteBytes(BinaryWriter writer)
        {
            base.WriteBytes(writer);

            writer.Write((bool)allow);
            meldingCard.WriteBytes(writer);
            writer.Write((byte)nextActionLimitTime);
            writer.Write((string)playerIndexGotCondition);
            writer.Write((string)condition);
        }

        public override void ReadBytes(BinaryReader reader)
        {
            base.ReadBytes(reader);

            allow = reader.ReadBoolean();
            meldingCard = new DMMeldingCard();
            if (meldingCard == null) meldingCard = new DMMeldingCard();
            meldingCard.ReadBytes(reader);
            nextActionLimitTime = reader.ReadByte();
            playerIndexGotCondition = reader.ReadString();
            condition = reader.ReadString();
        }
    }

    public class PlayerMeldingCardMessage : DMBaseMessage
    {
        public byte playerIndex;
        public DMMeldingCard meldingCard;

        public string playerIndexGotCondition;
        //1 , 2 , 1  ...
        public string condition;
        public PlayerMeldingCardMessage() : base(DMProtocolConstants.kMsgId_PlayerMeldingCard) { }

        public override void WriteBytes(BinaryWriter writer)
        {
            base.WriteBytes(writer);
            writer.Write((byte)playerIndex);
            meldingCard.WriteBytes(writer);
            writer.Write((string)playerIndexGotCondition);
            writer.Write((string)condition);
        }

        public override void ReadBytes(BinaryReader reader)
        {
            base.ReadBytes(reader);
            playerIndex = reader.ReadByte();
            meldingCard = new DMMeldingCard();
            if (meldingCard == null) meldingCard = new DMMeldingCard();
            meldingCard.ReadBytes(reader);
            playerIndexGotCondition = reader.ReadString();
            condition = reader.ReadString();
        }
    }

    public class ReqPickupAndMeldingCardMessage : DMBaseMessage
    {
        public DMCard pickupCard;
        public DMMeldingCard meldingCard;

        public ReqPickupAndMeldingCardMessage() : base(DMProtocolConstants.kMsgId_ReqPickupAndMeldingCard) { }

        public override void WriteBytes(BinaryWriter writer)
        {
            base.WriteBytes(writer);
            writer.Write((UInt16)pickupCard.ToUInt16());
            meldingCard.WriteBytes(writer);
        }

        public override void ReadBytes(BinaryReader reader)
        {
            base.ReadBytes(reader);
            pickupCard = DMCard.fromUInt16(reader.ReadUInt16());
            if (meldingCard == null) meldingCard = new DMMeldingCard();
            meldingCard.ReadBytes(reader);
        }
    }

    public class RespPickupAndMeldingCardMessage : DMBaseMessage
    {

        public DMCard[] pickupCards;
        public DMMeldingCard meldingCard;
        public byte nextActionLimitTime;
        public string playerIndexGotCondition;
        //1 , 2 , 1  ...
        public string condition;
        //เต็ม ,ทิ้งสเปโต , ทิ้งโง่  ...
        //playerIndexGotCondition index == condition index

        public RespPickupAndMeldingCardMessage() : base(DMProtocolConstants.kMsgId_RespPickupAndMeldingCard) { }

        public override void WriteBytes(BinaryWriter writer)
        {
            base.WriteBytes(writer);
            writer.Write((byte)pickupCards.Length);
            for (int i = 0; i < pickupCards.Length; i++)
            {
                writer.Write((UInt16)pickupCards[i].ToUInt16());
            }
            meldingCard.WriteBytes(writer);
            writer.Write((byte)nextActionLimitTime);
            writer.Write((string)playerIndexGotCondition);
            writer.Write((string)condition);
        }

        public override void ReadBytes(BinaryReader reader)
        {
            base.ReadBytes(reader);
            int numberOfPickupCard = reader.ReadByte();
            pickupCards = new DMCard[numberOfPickupCard];
            for (int i = 0; i < numberOfPickupCard; i++)
            {
                pickupCards[i] = DMCard.fromUInt16(reader.ReadUInt16());
            }
            if (meldingCard == null) meldingCard = new DMMeldingCard();
            meldingCard.ReadBytes(reader);
            nextActionLimitTime = reader.ReadByte();
            playerIndexGotCondition = reader.ReadString();
            condition = reader.ReadString();
        }
    }

    public class PlayerPickupAndMeldingCardMessage : DMBaseMessage
    {
        public byte playerIndex;
        public DMCard[] pickupCards;
        public DMMeldingCard meldingCard;
        public byte numberOfOnHandCards;
        public byte nextActionLimitTime;
        public string playerIndexGotCondition;
        //1 , 2 , 1  ...
        public string condition;
        //ปี้หัว ,ทิ้งสเปโต 

        //playerIndexGotCondition index == condition index

        public PlayerPickupAndMeldingCardMessage() : base(DMProtocolConstants.kMsgId_PlayerPickupAndMeldingCard) { }

        public override void WriteBytes(BinaryWriter writer)
        {
            base.WriteBytes(writer);
            writer.Write((byte)playerIndex);
            writer.Write((byte)numberOfOnHandCards);
            writer.Write((byte)pickupCards.Length);
            for (int i = 0; i < pickupCards.Length; i++)
            {
                writer.Write((UInt16)pickupCards[i].ToUInt16());
            }
            meldingCard.WriteBytes(writer);
            writer.Write((byte)nextActionLimitTime);
            writer.Write((string)playerIndexGotCondition);
            writer.Write((string)condition);
        }

        public override void ReadBytes(BinaryReader reader)
        {
            base.ReadBytes(reader);
            playerIndex = reader.ReadByte();
            numberOfOnHandCards = reader.ReadByte();
            int numberOfPickupCard = reader.ReadByte();
            pickupCards = new DMCard[numberOfPickupCard];
            for (int i = 0; i < numberOfPickupCard; i++)
            {
                pickupCards[i] = DMCard.fromUInt16(reader.ReadUInt16());
            }
            if (meldingCard == null) meldingCard = new DMMeldingCard();
            meldingCard.ReadBytes(reader);
            nextActionLimitTime = reader.ReadByte();
            playerIndexGotCondition = reader.ReadString();
            condition = reader.ReadString();
        }
    }

    public class ReqLayoffCardMessage : DMBaseMessage
    {
        public DMCard[] layoffCards;
        public UInt16 targetMeldingCardId;

        public ReqLayoffCardMessage() : base(DMProtocolConstants.kMsgId_ReqLayoffCard) { }

        public override void WriteBytes(BinaryWriter writer)
        {
            base.WriteBytes(writer);
            writer.Write((byte)layoffCards.Length);
            for (int i = 0; i < layoffCards.Length; i++)
            {
                writer.Write((UInt16)layoffCards[i].ToUInt16());
            }
            writer.Write((UInt16)targetMeldingCardId);
        }

        public override void ReadBytes(BinaryReader reader)
        {
            base.ReadBytes(reader);
            int numberOfLayoffCards = reader.ReadByte();
            if (layoffCards == null) layoffCards = new DMCard[numberOfLayoffCards];
            for (int i = 0; i < numberOfLayoffCards; i++)
            {
                layoffCards[i] = DMCard.fromUInt16(reader.ReadUInt16());
            }
            targetMeldingCardId = reader.ReadUInt16();
        }
    }

    public class RespLayoffCardMessage : DMBaseMessage
    {

        public byte playerIndex;
        public bool allow;
        public DMCard[] layoffCards;
        public UInt16 targetMeldingCardId;
        public byte nextActionLimitTime;
        public byte targetPlayerIndex;
        public byte targetCondition;
        // 0 = None , 6 = gotDummySpato
        public RespLayoffCardMessage() : base(DMProtocolConstants.kMsgId_RespLayoffCard) { }

        public override void WriteBytes(BinaryWriter writer)
        {
            base.WriteBytes(writer);
            writer.Write((byte)playerIndex);
            writer.Write((bool)allow);
            writer.Write((byte)layoffCards.Length);
            for (int i = 0; i < layoffCards.Length; i++)
            {
                writer.Write((UInt16)layoffCards[i].ToUInt16());
            }
            writer.Write((UInt16)targetMeldingCardId);
            writer.Write((byte)nextActionLimitTime);
            writer.Write((byte)targetPlayerIndex);
            writer.Write((byte)targetCondition);
        }

        public override void ReadBytes(BinaryReader reader)
        {
            base.ReadBytes(reader);
            playerIndex = reader.ReadByte();
            allow = reader.ReadBoolean();
            int numberOfLayoffCards = reader.ReadByte();
            if (layoffCards == null) layoffCards = new DMCard[numberOfLayoffCards];
            for (int i = 0; i < numberOfLayoffCards; i++)
            {
                layoffCards[i] = DMCard.fromUInt16(reader.ReadUInt16());
            }
            targetMeldingCardId = reader.ReadUInt16();
            nextActionLimitTime = reader.ReadByte();
            targetPlayerIndex = reader.ReadByte();
            targetCondition = reader.ReadByte();
        }
    }

    public class PlayerLayoffCardMessage : DMBaseMessage
    {
        public byte playerIndex;
        public DMCard[] layoffCards;
        public UInt16 targetMeldingCardId;
        public byte nextActionLimitTime;
        public byte targetPlayerIndex;
        public byte targetCondition;
        // 0 = None , 6 = gotDummySpato

        public PlayerLayoffCardMessage() : base(DMProtocolConstants.kMsgId_PlayerLayoffCard) { }

        public override void WriteBytes(BinaryWriter writer)
        {
            base.WriteBytes(writer);
            writer.Write((byte)playerIndex);
            writer.Write((byte)layoffCards.Length);
            for (int i = 0; i < layoffCards.Length; i++)
            {
                writer.Write((UInt16)layoffCards[i].ToUInt16());
            }
            writer.Write((UInt16)targetMeldingCardId);
            writer.Write((byte)nextActionLimitTime);
            writer.Write((byte)targetPlayerIndex);
            writer.Write((byte)targetCondition);
        }

        public override void ReadBytes(BinaryReader reader)
        {
            base.ReadBytes(reader);
            playerIndex = reader.ReadByte();
            int numberOfLayoffCards = reader.ReadByte();
            if (layoffCards == null) layoffCards = new DMCard[numberOfLayoffCards];
            for (int i = 0; i < numberOfLayoffCards; i++)
            {
                layoffCards[i] = DMCard.fromUInt16(reader.ReadUInt16());
            }
            targetMeldingCardId = reader.ReadUInt16();
            nextActionLimitTime = reader.ReadByte();
            targetPlayerIndex = reader.ReadByte();
            targetCondition = reader.ReadByte();
        }
    }
    public class PlayerScoreMessage : DMBaseMessage
    {
        public int score;

        public PlayerScoreMessage() : base(DMProtocolConstants.kMsgId_PlayerScore) { }

        public override void WriteBytes(BinaryWriter writer)
        {
            base.WriteBytes(writer);
            writer.Write((int)score);
        }

        public override void ReadBytes(BinaryReader reader)
        {
            base.ReadBytes(reader);
            score = reader.ReadInt32();
        }
    }

    public class otherPlayerScoreMessage : DMBaseMessage
    {
        public byte playerIndex;
        public int score;

        public otherPlayerScoreMessage() : base(DMProtocolConstants.kMsgId_otherPlayerScore) { }

        public override void WriteBytes(BinaryWriter writer)
        {
            base.WriteBytes(writer);
            writer.Write((byte)playerIndex);
            writer.Write((int)score);
        }

        public override void ReadBytes(BinaryReader reader)
        {
            base.ReadBytes(reader);
            playerIndex = reader.ReadByte();
            score = reader.ReadInt32();
        }
    }

    public class leaveAndRemoveRoom : DMBaseMessage
    {

        public string roomID;

        public leaveAndRemoveRoom() : base(DMProtocolConstants.kMsgId_leaveAndRemoveRoom) { }

        public override void WriteBytes(BinaryWriter writer)
        {
            base.WriteBytes(writer);
            writer.Write((string)roomID);
        }

        public override void ReadBytes(BinaryReader reader)
        {
            base.ReadBytes(reader);
            roomID = reader.ReadString();
        }

    }

    public class ReqKnockCardMessage : DMBaseMessage
    {

        public DMCard card; // Card that had been discarded

        public ReqKnockCardMessage() : base(DMProtocolConstants.kMsgId_ReqKnockCard) { }

        public override void WriteBytes(BinaryWriter writer)
        {
            base.WriteBytes(writer);
            writer.Write((UInt16)card.ToUInt16());
        }

        public override void ReadBytes(BinaryReader reader)
        {
            base.ReadBytes(reader);
            card = DMCard.fromUInt16(reader.ReadUInt16());
        }

    }

    public class RespKnockCardMessage : DMBaseMessage
    {

        // public string roomID;
        public bool allow;
        public byte knockType;
        public bool haveDropFool;
        public byte targetIndex;

        public RespKnockCardMessage() : base(DMProtocolConstants.kMsgId_RespKnockCard) { }

        public override void WriteBytes(BinaryWriter writer)
        {
            base.WriteBytes(writer);
            writer.Write((bool)allow);
            writer.Write((byte)knockType);
            writer.Write((bool)haveDropFool);
            writer.Write((byte)targetIndex);
        }

        public override void ReadBytes(BinaryReader reader)
        {
            base.ReadBytes(reader);
            allow = reader.ReadBoolean();
            knockType = reader.ReadByte();
            haveDropFool = reader.ReadBoolean();
            targetIndex = reader.ReadByte();
        }

    }

    public class PlayerKnockCardMessage : DMBaseMessage
    {

        // public string roomID;
        public byte knockType;
        public byte playerIndex;
        public DMCard card; // Card that had been discarded

        public bool haveDropFool;
        public byte targetIndex;

        public PlayerKnockCardMessage() : base(DMProtocolConstants.kMsgId_PlayerKnockCard) { }

        public override void WriteBytes(BinaryWriter writer)
        {
            base.WriteBytes(writer);
            // writer.Write((string)roomID);
            writer.Write((byte)playerIndex);
            writer.Write((byte)knockType);
            writer.Write((UInt16)card.ToUInt16());
            writer.Write((bool)haveDropFool);
            writer.Write((byte)targetIndex);
        }

        public override void ReadBytes(BinaryReader reader)
        {
            base.ReadBytes(reader);
            // roomID = reader.ReadString();
            playerIndex = reader.ReadByte();
            knockType = reader.ReadByte();
            card = DMCard.fromUInt16(reader.ReadUInt16());
            haveDropFool = reader.ReadBoolean();
            targetIndex = reader.ReadByte();
        }

    }

    public class ReqPassLastTurnMessage : DMBaseMessage
    {
        public ReqPassLastTurnMessage() : base(DMProtocolConstants.kMsgId_ReqPassLastTurn) { }
        public override void WriteBytes(BinaryWriter writer)
        {
            base.WriteBytes(writer);
        }
        public override void ReadBytes(BinaryReader reader)
        {
            base.ReadBytes(reader);
        }
    }

    public class RespPassLastTurnMessage : DMBaseMessage
    {
        public RespPassLastTurnMessage() : base(DMProtocolConstants.kMsgId_RespPassLastTurn) { }
        public override void WriteBytes(BinaryWriter writer)
        {
            base.WriteBytes(writer);
        }
        public override void ReadBytes(BinaryReader reader)
        {
            base.ReadBytes(reader);
        }
    }

    public class PlayerPassLastTurnMessage : DMBaseMessage
    {
        public PlayerPassLastTurnMessage() : base(DMProtocolConstants.kMsgId_PlayerPassLastTurn) { }
        public override void WriteBytes(BinaryWriter writer)
        {
            base.WriteBytes(writer);
        }
        public override void ReadBytes(BinaryReader reader)
        {
            base.ReadBytes(reader);
        }
    }

    public class PlayerCountTypeScore : DMBaseMessage
    {
        public string data;
        public PlayerCountTypeScore() : base(DMProtocolConstants.kMsgId_PlayerCountTypeScore) { }
        public override void WriteBytes(BinaryWriter writer)
        {
            base.WriteBytes(writer);
            writer.Write((string)data);
        }
        public override void ReadBytes(BinaryReader reader)
        {
            base.ReadBytes(reader);
            data = reader.ReadString();
        }
    }


}

