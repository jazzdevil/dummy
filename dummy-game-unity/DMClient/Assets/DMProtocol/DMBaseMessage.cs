﻿using System;
using System.IO;
using System.Collections.Generic;
using UnityEngine;


namespace Dummy.DMProtocol
{
    [Serializable]
    public class DMBaseMessage
    {

        public ushort messageID;
        public string roomID = "";

        public DMBaseMessage(ushort messageId)
        {
            this.messageID = messageId;
        }

        public void SetRoomID(string _roomId)
        {
            roomID = _roomId;
        }

        public virtual void WriteBytes(BinaryWriter writer)
        {
            writer.Write((ushort)messageID);
            writer.Write((string)roomID);
        }

        public virtual void ReadBytes(BinaryReader reader)
        {
            messageID = reader.ReadUInt16();
            roomID = reader.ReadString();
        }
    }
}
