﻿using System;
using System.IO;

namespace Dummy.DMProtocol {

    public class DMProtocolUtility {
        public static void CopyBytes(byte[] src, int srcStartIndex, byte[] dst, int dstStartIndex, int count) {
            for (int i = srcStartIndex; i < srcStartIndex + count; i++) {
                dst[i] = src[i];
            }
        }

        public static T CreateDMMessageFromBuffer<T>(byte[] buffer) where T : DMBaseMessage, new() {
            T msg = new T();
            using (Stream stream = new MemoryStream(buffer)) {
                using (BinaryReader reader = new BinaryReader(stream)) {
                    msg.ReadBytes(reader);
                }
            }
            return msg;
        }
    }

}
