﻿using System;

namespace Dummy.DMProtocol {
    public class ReqClientHeartbeat : DMBaseMessage {
        public ReqClientHeartbeat() : base(DMProtocolConstants.kMsgId_ReqClientHeartbeat) { }
    }

    public class RespClientHeartbeat : DMBaseMessage {
        public RespClientHeartbeat() : base(DMProtocolConstants.kMsgId_RespClientHeartbeat) { }
    }
}
